package mx.gob.scjn.ugacj.sga_captura.controller;

import mx.gob.scjn.ugacj.sga_captura.domain.Sentencia;
import mx.gob.scjn.ugacj.sga_captura.domain.SentenciaMetadatos;
import mx.gob.scjn.ugacj.sga_captura.service.SentenciasMetadatosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping( value = "/api/sga")
public class SentenciaMetadatosController {

    @Autowired
    private SentenciasMetadatosService _sentenciasMetadatosService;

    @RequestMapping(method = RequestMethod.GET,path = "/sentenciaMeta")
    @CrossOrigin()
    public Page<SentenciaMetadatos> getSentenciaMetaAll(@RequestParam(name = "page") Integer page,
                                             @RequestParam(name = "size") Integer size,
                                             @RequestParam(required = false, name = "filtros") String filtros){
        Pageable paging = PageRequest.of(page-1, size);
        return _sentenciasMetadatosService.getAllSentenciasMetaPages(paging, filtros);
    }

    @RequestMapping(method = RequestMethod.GET,path = "/sentenciaMeta/{sentenciaId}", produces = "application/json")
    @CrossOrigin()
    public SentenciaMetadatos getSentenciaMetaById(@PathVariable String sentenciaId){
        return _sentenciasMetadatosService.getSentenciaMetaById(sentenciaId);
    }

    @RequestMapping(method = RequestMethod.POST,path = "/sentenciaMeta")
    @CrossOrigin()
    public SentenciaMetadatos saveSentenciaMeta(@RequestBody SentenciaMetadatos sentencia){
        return _sentenciasMetadatosService.saveSentenciaMeta(sentencia);
    }

    @RequestMapping(method = RequestMethod.PUT,path = "/sentenciaMeta")
    @CrossOrigin()
    public SentenciaMetadatos updateSentenciaMeta(@RequestBody SentenciaMetadatos sentencia){
        return _sentenciasMetadatosService.updateSentenciaMeta(sentencia);
    }

    @RequestMapping(method = RequestMethod.DELETE,path = "/sentenciaMeta")
    @CrossOrigin()
    public void deleteSentenciaMeta(@RequestParam(name = "id") String id){
        _sentenciasMetadatosService.deleteSentenciaMeta(id);
    }
}
