package mx.gob.scjn.ugacj.sga_captura.service.acuerdos;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import mx.gob.scjn.ugacj.sga_captura.domain.AcuerdosGenerales;
import mx.gob.scjn.ugacj.sga_captura.dto.statecode.AcuerdosResponseDTO;
import mx.gob.scjn.ugacj.sga_captura.repository.AcuerdosGeneralesRepository;
import mx.gob.scjn.ugacj.sga_captura.service.bitacora.BitacoraService;
import mx.gob.scjn.ugacj.sga_captura.service.porcentaje.PorcentajeService;
import mx.gob.scjn.ugacj.sga_captura.utils.ConvertFechaFiltros;
import mx.gob.scjn.ugacj.sga_captura.utils.FiltrosUtils;
import mx.gob.scjn.ugacj.sga_captura.utils.RegexUtils;
import mx.gob.scjn.ugacj.sga_captura.utils.StatusBitacora;

@Service
public class AcuerdosService {
	@Autowired
	private AcuerdosGeneralesRepository _acuerdosRepository;

	@Autowired
	private MongoOperations mongoOperations;

	@Autowired
	private BitacoraService _bitacoraService;
	
	@Autowired
	private PorcentajeService porcentajeService;

	public AcuerdosResponseDTO saveAcuerdo(@Valid AcuerdosGenerales acuerdo) {
		AcuerdosResponseDTO respuesta = null;
		if (acuerdo.getFechaAprobacion() != null || acuerdo.getFechaAprobacion().equals("")) {
			porcentajeService.updatePorcentaje(acuerdo);
			
			acuerdo.setFechaAprobacion(ConvertFechaFiltros.changeFormat(acuerdo.getFechaAprobacion(), RegexUtils.REGEX_YYYYMMDD));
			acuerdo.setFechaPublicacionDOF(ConvertFechaFiltros.changeFormat(acuerdo.getFechaPublicacionDOF(), RegexUtils.REGEX_YYYYMMDD));
			
			
			AcuerdosGenerales acuerdoNew = _acuerdosRepository.save(acuerdo);
			_bitacoraService.saveOperacion(StatusBitacora._ACUERDOS, StatusBitacora._SAVE, acuerdoNew, null);
			respuesta = new AcuerdosResponseDTO(HttpStatus.OK.value(), "Acuerdo General creado");
			respuesta.setAcuerdosGenerales(acuerdoNew);
			return respuesta;
		}
		respuesta = new AcuerdosResponseDTO(HttpStatus.BAD_REQUEST.value(), "No fué posible crear el Acuerdo General");
		respuesta.setAcuerdosGenerales(acuerdo);
		return respuesta;
	}

	public AcuerdosGenerales getAcuerdosById(String acuerdoId) {
		Optional<AcuerdosGenerales> acuerdo = _acuerdosRepository.findById(acuerdoId);
		acuerdo.get().setFechaAprobacion(ConvertFechaFiltros.changeFormat(acuerdo.get().getFechaAprobacion(), RegexUtils.REGEX_DDMMYYYY));
		acuerdo.get().setFechaPublicacionDOF(ConvertFechaFiltros.changeFormat(acuerdo.get().getFechaPublicacionDOF(), RegexUtils.REGEX_DDMMYYYY));
		
		
		if (acuerdo.isPresent()) {
			return acuerdo.get();
		}
		// TODO Auto-generated method stub
		return null;
	}

	public AcuerdosResponseDTO updateAcuerdo(@Valid AcuerdosGenerales acuerdo) {
		AcuerdosResponseDTO respuesta = null;
		Optional<AcuerdosGenerales> acuerdoExist = _acuerdosRepository.findById(acuerdo.getId());
		if (acuerdoExist.isPresent()) {
			porcentajeService.updatePorcentaje(acuerdo);
			
			acuerdo.setFechaAprobacion(ConvertFechaFiltros.changeFormat(acuerdo.getFechaAprobacion(), RegexUtils.REGEX_YYYYMMDD));
			acuerdo.setFechaPublicacionDOF(ConvertFechaFiltros.changeFormat(acuerdo.getFechaPublicacionDOF(), RegexUtils.REGEX_YYYYMMDD));
			
			
			_acuerdosRepository.save(acuerdo);
			_bitacoraService.saveOperacion(StatusBitacora._ACUERDOS, StatusBitacora._UPDATE, acuerdo, acuerdoExist.get());
			respuesta = new AcuerdosResponseDTO(HttpStatus.OK.value(), "Acuerdo General Actualizado");
			respuesta.setAcuerdosGenerales(acuerdo);
			return respuesta;

		}
		respuesta = new AcuerdosResponseDTO(HttpStatus.BAD_REQUEST.value(),
				"No fué posible actualizar el Acuerdo General");
		respuesta.setAcuerdosGenerales(acuerdo);
		return respuesta;
	}

	public boolean deleteAcuerdo(String acuerdoId) {
		Optional<AcuerdosGenerales> acuerdo = _acuerdosRepository.findById(acuerdoId);
		if (acuerdo.isPresent()) {
			_bitacoraService.saveOperacion(StatusBitacora._ACUERDOS, StatusBitacora._DELETE, acuerdo.get(), null);
			_acuerdosRepository.delete(acuerdo.get());
			return true;
		}
		// TODO Auto-generated method stub
		return false;
	}

	public Page<AcuerdosGenerales> getAllAcuerdosPages(Pageable paging, String filtros) throws ParseException {
		long count = 0;
		Page<AcuerdosGenerales> acuerdosPaginable = null;
		List<AcuerdosGenerales> acuerdos = null;
		Page<AcuerdosGenerales> acuerdosPaginableResponse = null;
		Pageable paging2 = PageRequest.of(paging.getPageNumber(), paging.getPageSize());
		if (filtros != null) {
			Query query = new Query().with(paging);
			Query queryNoPage = new Query();
			//String subs[] = filtros.split(",");
			String subs[] = FiltrosUtils.getSubfiltros(filtros);
			for (String sub : subs) {
				System.out.println(sub);
				String sTempFiltros[] = sub.split(":");
				String filtro = sTempFiltros[0].trim();
				String filtroValores = sTempFiltros[1].trim();
				if (filtro.trim().equals("fechaAprobacion")) {
					query.with(Sort.by(Sort.Direction.DESC, "fechaAprobacion"));
					String fecha = ConvertFechaFiltros.changeFormat(filtroValores.trim(), RegexUtils.REGEX_YYYYMMDD);
					query.addCriteria(Criteria.where(filtro).gte(fecha).lte(fecha));
					queryNoPage.addCriteria(Criteria.where(filtro).gte(fecha).lte(fecha));					
				} else {
					if(filtro.trim().equals("rubro")) {
						query.addCriteria(Criteria.where(filtro).regex(".*"+filtroValores+".*", "i"));
						queryNoPage.addCriteria(Criteria.where(filtro).regex(".*"+filtroValores+".*", "i"));
					}else {
						query.addCriteria(Criteria.where(filtro).is(filtroValores));
						queryNoPage.addCriteria(Criteria.where(filtro).is(filtroValores));
					}
					
					
					query.with(Sort.by(Sort.Direction.DESC, "fechaAprobacion"));
					queryNoPage.with(Sort.by(Sort.Direction.DESC, "fechaAprobacion"));
				}
			}
			List<AcuerdosGenerales> listAcuerdos = mongoOperations.find(query, AcuerdosGenerales.class);
			count = mongoOperations.count(queryNoPage, AcuerdosGenerales.class);
//			count = _acuerdosRepository.findAll().size();
			acuerdosPaginable = new PageImpl<AcuerdosGenerales>(listAcuerdos, paging2, count);
			acuerdos = acuerdosPaginable.getContent();
			
		} else {
			acuerdosPaginable = _acuerdosRepository.findAllByOrderByFechaAprobacionDesc(paging);
			acuerdos = acuerdosPaginable.getContent();
			count = acuerdosPaginable.getTotalElements();
		}
		for(AcuerdosGenerales acuerdo:acuerdos) acuerdo.setFechaAprobacion(ConvertFechaFiltros.changeFormat(acuerdo.getFechaAprobacion(), RegexUtils.REGEX_DDMMYYYY));
		acuerdosPaginableResponse = new PageImpl<AcuerdosGenerales>(acuerdos, paging2, count);
		return acuerdosPaginableResponse;
	}
}
