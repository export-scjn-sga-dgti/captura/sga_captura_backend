package mx.gob.scjn.ugacj.sga_captura.dto.normativa;

import mx.gob.scjn.ugacj.sga_captura.domain.MateriaRegulacion;
import mx.gob.scjn.ugacj.sga_captura.dto.statecode.ResponseDTO;

public class MateriaRegulacionDTO extends ResponseDTO {
	
	MateriaRegulacion materiaRegulacion;
	
	public MateriaRegulacionDTO(int statusCode, String message) {
		super(statusCode, message);
		// TODO Auto-generated constructor stub
	}

	public MateriaRegulacion getMateriaRegulacion() {
		return materiaRegulacion;
	}

	public void setMateriaRegulacion(MateriaRegulacion materiaRegulacion) {
		this.materiaRegulacion = materiaRegulacion;
	}
	
}
