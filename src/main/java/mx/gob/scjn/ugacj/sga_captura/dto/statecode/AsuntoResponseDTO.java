package mx.gob.scjn.ugacj.sga_captura.dto.statecode;

import mx.gob.scjn.ugacj.sga_captura.domain.AsuntoAbordado;
import mx.gob.scjn.ugacj.sga_captura.domain.VTaquigraficaAsuntos;

public class AsuntoResponseDTO extends ResponseDTO{
	
	String idVersionTaqui;
	AsuntoAbordado asunto;
	VTaquigraficaAsuntos vt_asuntos;
	
	public AsuntoResponseDTO(int statusCode, String message) {
		super(statusCode, message);
		// TODO Auto-generated constructor stub
	}

	public String getIdVersionTaqui() {
		return idVersionTaqui;
	}

	public void setIdVersionTaqui(String idVersionTaqui) {
		this.idVersionTaqui = idVersionTaqui;
	}

	public AsuntoAbordado getAsunto() {
		return asunto;
	}

	public void setAsunto(AsuntoAbordado asunto) {
		this.asunto = asunto;
	}

	public VTaquigraficaAsuntos getVt_asuntos() {
		return vt_asuntos;
	}

	public void setVt_asuntos(VTaquigraficaAsuntos vt_asuntos) {
		this.vt_asuntos = vt_asuntos;
	}
}
