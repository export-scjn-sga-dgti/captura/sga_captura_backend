package mx.gob.scjn.ugacj.sga_captura.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import mx.gob.scjn.ugacj.sga_captura.domain.Sentencia;
import mx.gob.scjn.ugacj.sga_captura.domain.SentenciaOld;

@Repository
public interface SentenciaOldRepository extends MongoRepository<SentenciaOld,String>{
	
	List<SentenciaOld> findByTipoAsuntoAndNumExpedientes(String tipoAsunto, String numExpediente);

}
