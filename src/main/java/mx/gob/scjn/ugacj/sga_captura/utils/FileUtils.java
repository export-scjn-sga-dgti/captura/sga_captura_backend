package mx.gob.scjn.ugacj.sga_captura.utils;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class FileUtils {
	
	public void writeListInFile(List<String> listString, String urlPathFile) {
		
		
		  try {
			  FileWriter writer = new FileWriter(urlPathFile); 
			  for(String str: listString) {
				writer.write(str + System.lineSeparator());
				
			  }
			  writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
