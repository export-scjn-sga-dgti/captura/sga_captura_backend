package mx.gob.scjn.ugacj.sga_captura.service.normativa;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import mx.gob.scjn.ugacj.sga_captura.domain.MateriaRegulacion;
import mx.gob.scjn.ugacj.sga_captura.dto.normativa.MateriaRegulacionDTO;
import mx.gob.scjn.ugacj.sga_captura.repository.MateriaRegulacionRepository;
import mx.gob.scjn.ugacj.sga_captura.service.bitacora.BitacoraService;
import mx.gob.scjn.ugacj.sga_captura.utils.StatusBitacora;

@Service
public class MateriaRegulacionService {

	@Autowired
	private MateriaRegulacionRepository _mrRepository;

	@Autowired
	private MongoOperations mongoOperations;

	@Autowired
	private BitacoraService _bitacoraService;
	
	public MateriaRegulacionDTO saveMateriaRegulacion(@Valid MateriaRegulacion materia) {
		// TODO Auto-generated method stub
		MateriaRegulacionDTO respuesta = null;
		if (materia.getIdDiversaNormativa()!=null  && materia.getMateriaRegulacion()!=null) {
			materia = _mrRepository.save(materia);
			_bitacoraService.saveOperacion(StatusBitacora._MATERIA, StatusBitacora._SAVE, materia, null);
			respuesta = new MateriaRegulacionDTO(HttpStatus.OK.value(), "materia creada");
			respuesta.setMateriaRegulacion(materia);
			return respuesta;
		}
		respuesta = new MateriaRegulacionDTO(HttpStatus.BAD_REQUEST.value(),
				"No fue posible crear la materia por que no esta asociada a una normativa");
		respuesta.setMateriaRegulacion(materia);
		return respuesta;
	}

	public MateriaRegulacionDTO updateMateriaRegulacion(@Valid MateriaRegulacion materia) {
		MateriaRegulacionDTO respuesta = null;
		if (materia.getIdDiversaNormativa()!=null && materia.getMateriaRegulacion()!=null ) {
			Optional<MateriaRegulacion> materiaExist = _mrRepository.findById(materia.getId());
			_mrRepository.save(materia);
			_bitacoraService.saveOperacion(StatusBitacora._MATERIA, StatusBitacora._UPDATE, materia, materiaExist.get());
			respuesta = new MateriaRegulacionDTO(HttpStatus.OK.value(), "materia actualizada");
			respuesta.setMateriaRegulacion(materia);
			return respuesta;
		}
		respuesta = new MateriaRegulacionDTO(HttpStatus.BAD_REQUEST.value(),
				"No fue posible actualizar la materia");
		respuesta.setMateriaRegulacion(materia);
		return respuesta;
	}

	public boolean deleteMateriaRegulacion(String id) {
		Optional<MateriaRegulacion>	materiaR = _mrRepository.findById(id);
		if(materiaR.isPresent()) {
			_bitacoraService.saveOperacion(StatusBitacora._MATERIA, StatusBitacora._DELETE, materiaR.get(), null);
			_mrRepository.deleteById(id);
			return true;
		}
		// TODO Auto-generated method stub
		return false;
	}


	public MateriaRegulacion getMateriaRegulacion(String id) {
		Optional<MateriaRegulacion>	materiaR = _mrRepository.findById(id);
		if(materiaR.isPresent()) {
			return materiaR.get();
		}
		// TODO Auto-generated method stub
		return null;
	}
	
	

}
