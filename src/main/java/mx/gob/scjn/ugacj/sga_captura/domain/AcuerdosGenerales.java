package mx.gob.scjn.ugacj.sga_captura.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonFormat;

@Document(collection = "#{@environment.getProperty('indice.acuerdos')}")
public class AcuerdosGenerales implements Serializable {

	@Id
	String id;
	
	@NotBlank
	String rubro;
	
	//@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
	@NotNull( message ="El campo fecha de aprobación no puede estar vacío")
	String fechaAprobacion;
	String ambitoIncide;
	String vigencia;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
	String fechaPublicacionDOF;
	List<String> clasificacionMateriaRegulacion;
	String organoEmisor;
	String ministroPresidente;
	String votacion;
	List<String> votacionAcuerdoFavor;
	List<String> votacionAcuerdoEnContra;
	List<String> ministrosAusentes;
	List<String> temasEspecificosRegulacion;
	
	double porcentaje;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getFechaAprobacion() {
		return fechaAprobacion;
	}
	public void setFechaAprobacion(String fechaAprobacion) {
		this.fechaAprobacion = fechaAprobacion;
	}
	public String getAmbitoIncide() {
		return ambitoIncide;
	}
	public void setAmbitoIncide(String ambitoIncide) {
		this.ambitoIncide = ambitoIncide;
	}
	public String getVigencia() {
		return vigencia;
	}
	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}
	
	public String getFechaPublicacionDOF() {
		return fechaPublicacionDOF;
	}
	public void setFechaPublicacionDOF(String fechaPublicacionDOF) {
		this.fechaPublicacionDOF = fechaPublicacionDOF;
	}
	public List<String> getClasificacionMateriaRegulacion() {
		return clasificacionMateriaRegulacion;
	}
	public void setClasificacionMateriaRegulacion(List<String> clasificacionMateriaRegulacion) {
		this.clasificacionMateriaRegulacion = clasificacionMateriaRegulacion;
	}
	public String getOrganoEmisor() {
		return organoEmisor;
	}
	public void setOrganoEmisor(String organoEmisor) {
		this.organoEmisor = organoEmisor;
	}
	public String getMinistroPresidente() {
		return ministroPresidente;
	}
	public void setMinistroPresidente(String ministroPresidente) {
		this.ministroPresidente = ministroPresidente;
	}
	public String getVotacion() {
		return votacion;
	}
	public void setVotacion(String votacion) {
		this.votacion = votacion;
	}
	public List<String> getVotacionAcuerdoFavor() {
		return votacionAcuerdoFavor;
	}
	public void setVotacionAcuerdoFavor(List<String> votacionAcuerdoFavor) {
		this.votacionAcuerdoFavor = votacionAcuerdoFavor;
	}
	public List<String> getVotacionAcuerdoEnContra() {
		return votacionAcuerdoEnContra;
	}
	public void setVotacionAcuerdoEnContra(List<String> votacionAcuerdoEnContra) {
		this.votacionAcuerdoEnContra = votacionAcuerdoEnContra;
	}
	public List<String> getMinistrosAusentes() {
		return ministrosAusentes;
	}
	public void setMinistrosAusentes(List<String> ministrosAusentes) {
		this.ministrosAusentes = ministrosAusentes;
	}
	public List<String> getTemasEspecificosRegulacion() {
		return temasEspecificosRegulacion;
	}
	public void setTemasEspecificosRegulacion(List<String> temasEspecificosRegulacion) {
		this.temasEspecificosRegulacion = temasEspecificosRegulacion;
	}
	public String getRubro() {
		return rubro;
	}
	public void setRubro(String rubro) {
		this.rubro = rubro;
	}
	public double getPorcentaje() {
		return porcentaje;
	}
	public void setPorcentaje(double porcentaje) {
		this.porcentaje = porcentaje;
	}
	
}
