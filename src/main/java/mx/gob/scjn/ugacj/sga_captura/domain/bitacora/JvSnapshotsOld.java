package mx.gob.scjn.ugacj.sga_captura.domain.bitacora;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "#{@environment.getProperty('indice.bitacoraold')}")
public class JvSnapshotsOld implements Serializable {

    @Id
    String id;
    Object beginState = null;
    Object state = null;
    CommitMetaData commitMetaData;
    String type;
    String seccion;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Object getBeginState() {
        return beginState;
    }

    public void setBeginState(Object beginState) {
        this.beginState = beginState;
    }

    public Object getState() {
        return state;
    }

    public void setState(Object state) {
        this.state = state;
    }

    public CommitMetaData getCommitMetaData() {
        return commitMetaData;
    }

    public void setCommitMetaData(CommitMetaData commitMetaData) {
        this.commitMetaData = commitMetaData;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSeccion() {
        return seccion;
    }

    public void setSeccion(String seccion) {
        this.seccion = seccion;
    }
    

	@Override
    public String toString() {
        return "JvSnapshots{" +
                "id='" + id + '\'' +
                ", beginState=" + beginState +
                ", state=" + state +
                ", commitMetaData=" + commitMetaData +
                ", type='" + type + '\'' +
                ", seccion='" + seccion + '\'' +
                '}';
    }
}
