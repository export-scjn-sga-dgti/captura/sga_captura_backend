package mx.gob.scjn.ugacj.sga_captura.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import mx.gob.scjn.ugacj.sga_captura.domain.VTaquigraficaAsuntos;

public interface VTaquigraficaAsuntosRepository extends MongoRepository<VTaquigraficaAsuntos,String> {

	List<VTaquigraficaAsuntos> findByIdAsunto(String id);

	List<VTaquigraficaAsuntos> findByIdVtaquigrafica(String id);
	
	List<VTaquigraficaAsuntos> findAllByIdVtaquigraficaInAndIdAsuntoIn(List<String> vtaqIds, List<String> asuntoIds);
	
	Optional<VTaquigraficaAsuntos> findByIdVtaquigraficaAndIdAsunto(String vtaqIds, String asuntoIds);
	
	List<VTaquigraficaAsuntos> findAllByIdAsuntoIn(List<String> asuntoIds);
}
