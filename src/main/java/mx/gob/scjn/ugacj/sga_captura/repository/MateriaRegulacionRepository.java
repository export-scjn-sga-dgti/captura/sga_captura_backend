package mx.gob.scjn.ugacj.sga_captura.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import mx.gob.scjn.ugacj.sga_captura.domain.MateriaRegulacion;

public interface MateriaRegulacionRepository extends MongoRepository<MateriaRegulacion,String> {

	List<MateriaRegulacion> findByIdDiversaNormativa(String id);

}
