package mx.gob.scjn.ugacj.sga_captura.domain;

import java.util.ArrayList;
import java.util.List;

public class VotacionTipoSentencia {
    
    List<String> votacionAFavorTipoSentencia = new ArrayList<String>(); 
    List<String> votaciontionEnContraTipoSentencia = new ArrayList<String>();
    List<String> todosMinistrosTipoSentencia = new ArrayList<String>();
    List<String> rubroTemTipSentencia = new ArrayList<String>();
    String textoVotacionTipoSentencia;

    public List<String> getVotacionAFavorTipoSentencia() {
        return this.votacionAFavorTipoSentencia;
    }

    public void setVotacionAFavorTipoSentencia(List<String> votacionAFavorTipoSentencia) {
        this.votacionAFavorTipoSentencia = votacionAFavorTipoSentencia;
    }

    public List<String> getVotaciontionEnContraTipoSentencia() {
        return this.votaciontionEnContraTipoSentencia;
    }

    public void setVotaciontionEnContraTipoSentencia(List<String> votaciontionEnContraTipoSentencia) {
        this.votaciontionEnContraTipoSentencia = votaciontionEnContraTipoSentencia;
    }

    public List<String> getTodosMinistrosTipoSentencia() {
        return this.todosMinistrosTipoSentencia;
    }

    public void setTodosMinistrosTipoSentencia(List<String> todosMinistrosTipoSentencia) {
        this.todosMinistrosTipoSentencia = todosMinistrosTipoSentencia;
    }

    public List<String> getRubroTemTipSentencia() {
        return this.rubroTemTipSentencia;
    }

    public void setRubroTemTipSentencia(List<String> rubroTemTipSentencia) {
        this.rubroTemTipSentencia = rubroTemTipSentencia;
    }

    public String getTextoVotacionTipoSentencia() {
        return this.textoVotacionTipoSentencia;
    }

    public void setTextoVotacionTipoSentencia(String textoVotacionTipoSentencia) {
        this.textoVotacionTipoSentencia = textoVotacionTipoSentencia;
    }
    
}
