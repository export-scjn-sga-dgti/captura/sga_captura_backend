package mx.gob.scjn.ugacj.sga_captura.controller.sentencias;

import mx.gob.scjn.ugacj.sga_captura.domain.RubrosTablaVotaciones;
import mx.gob.scjn.ugacj.sga_captura.dto.RubrosTablaVotacionesDTO;
import mx.gob.scjn.ugacj.sga_captura.service.sentencias.RubrosTablaVotacionesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import javax.validation.Valid;

@RestController
@RequestMapping( value = "/api/sga/rubroTablaVotacion")
public class RubrosTablasVotacionesController {

    @Autowired
    private RubrosTablaVotacionesService _RubrosTablaVotacionesService;

    @RequestMapping(method = RequestMethod.POST,path = "/")
    @CrossOrigin()
    public RubrosTablaVotaciones saveRubro(@Valid @RequestBody RubrosTablaVotacionesDTO rubro){
        return _RubrosTablaVotacionesService.saveRubroVotacion(rubro);
    }
    @RequestMapping(method = RequestMethod.PUT,path = "/")
    @CrossOrigin()
    public RubrosTablaVotaciones updateRubro(@RequestBody RubrosTablaVotacionesDTO rubro){
        return _RubrosTablaVotacionesService.updateVotacionYRubro(rubro);
    }
    
    @RequestMapping(method = RequestMethod.PUT,path = "/orden-rubro")
    @CrossOrigin()
    public RubrosTablaVotaciones updateOrdenRubro(@RequestBody RubrosTablaVotaciones rubro){
        return _RubrosTablaVotacionesService.updateOrdenRubro(rubro);
    }

    @RequestMapping(method = RequestMethod.DELETE,path = "/rubro/{idRubro}/votacion/{idVotacion}/sentencia/{idSentencia}")
    @CrossOrigin()
    public void deleteRubroVotacion( @PathVariable String idRubro, @PathVariable String idVotacion, @PathVariable String idSentencia){
        _RubrosTablaVotacionesService.deleteVotacionRubroVotacion(idRubro, idVotacion, idSentencia);
    }
}
