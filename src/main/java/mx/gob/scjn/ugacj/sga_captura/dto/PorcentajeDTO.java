package mx.gob.scjn.ugacj.sga_captura.dto;

import java.util.Map;

public class PorcentajeDTO {
	
	Map<String, Double> fields;
	double valC = 10d;
	Map<String, String> fieldsConstraint;
	
	public Map<String, Double> getFields() {
		return fields;
	}
	public void setFields(Map<String, Double> fields) {
		this.fields = fields;
	}
	public double getValC() {
		return valC;
	}
	public void setValC(double valC) {
		this.valC = valC;
	}
	public Map<String, String> getFieldsConstraint() {
		return fieldsConstraint;
	}
	public void setFieldsConstraint(Map<String, String> fieldsConstraint) {
		this.fieldsConstraint = fieldsConstraint;
	}
	
	
	
	

}
