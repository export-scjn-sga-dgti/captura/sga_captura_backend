package mx.gob.scjn.ugacj.sga_captura.dto.vtaquigraficas;

import java.util.List;

import mx.gob.scjn.ugacj.sga_captura.domain.VersionTaquigrafica;

public class VersionTaquigraficaDTO {
	
	VersionTaquigrafica vtaq;
	List<AsuntoDTO> asuntos;
	
	public VersionTaquigrafica getVtaq() {
		return vtaq;
	}
	public void setVtaq(VersionTaquigrafica vtaq) {
		this.vtaq = vtaq;
	}
	public List<AsuntoDTO> getAsuntos() {
		return asuntos;
	}
	public void setAsuntos(List<AsuntoDTO> asuntos) {
		this.asuntos = asuntos;
	}	
}
