package mx.gob.scjn.ugacj.sga_captura.controller.resetdb;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping( value = "/api/sga/migracion")
public class ResetDB {
	
	@Value("${indice.vtaquigrafica}")
	private String vtaquigrafica;
	
	@Value("${indice.vtaquigrafica.asuntoabordado}")
	private String asuntoabordado;
	
	@Value("${indice.vtaquigrafica.vtaqasunto}")
	private String vtaqasunto;
	
	@Value("${indice.sentencia}")
	private String sentencia;
	
	@Value("${indice.issues}")
	private String issues;
	
	@Value("${indice.sentencia.votacion}")
	private String votacion;
	
	@Value("${indice.sentencia.votacion.rubros}")
	private String votacionrubros;
	
	@Value("${indice.usuario}")
	private String usuario;
	
	@Value("${indice.voto}")
	private String voto;
	
	@Value("${indice.acuerdos}")
	private String acuerdos;
	
	@Autowired
	private MongoOperations mongoOperations;

	@RequestMapping(method = RequestMethod.DELETE,path = "/delete-vtaq")
    @CrossOrigin()
    public boolean deleteTaquigraficas(){
		mongoOperations.dropCollection(vtaquigrafica);
		mongoOperations.dropCollection(asuntoabordado);
		mongoOperations.dropCollection(vtaqasunto);
		mongoOperations.dropCollection(sentencia);
		mongoOperations.dropCollection(issues);
		mongoOperations.dropCollection(votacion);
		mongoOperations.dropCollection(votacionrubros);
		
		return !mongoOperations.collectionExists(vtaquigrafica)&
				!mongoOperations.collectionExists(vtaqasunto)&
				!mongoOperations.collectionExists(asuntoabordado)&
				!mongoOperations.collectionExists(votacion)&
				!mongoOperations.collectionExists(votacionrubros)&
				!mongoOperations.collectionExists(sentencia)&
				!mongoOperations.collectionExists(issues);
    } 
	
	@RequestMapping(method = RequestMethod.DELETE,path = "/delete-votos")
    @CrossOrigin()
    public boolean deleteVotos(){
		mongoOperations.dropCollection(voto);

		return !mongoOperations.collectionExists(voto);
    } 
	
	@RequestMapping(method = RequestMethod.DELETE,path = "/delete-acuerdos")
    @CrossOrigin()
    public boolean deleteAcuerdos(){
		mongoOperations.dropCollection(acuerdos);

		return !mongoOperations.collectionExists(acuerdos);
    }
	
	@RequestMapping(method = RequestMethod.DELETE,path = "/delete-usuarios")
    @CrossOrigin()
    public boolean deleteUsuarios(){
		mongoOperations.dropCollection(usuario);

		return !mongoOperations.collectionExists(usuario);
    } 
	
}
