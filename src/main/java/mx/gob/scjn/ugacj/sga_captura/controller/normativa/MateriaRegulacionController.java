package mx.gob.scjn.ugacj.sga_captura.controller.normativa;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import mx.gob.scjn.ugacj.sga_captura.domain.MateriaRegulacion;
import mx.gob.scjn.ugacj.sga_captura.dto.normativa.MateriaRegulacionDTO;
import mx.gob.scjn.ugacj.sga_captura.service.normativa.MateriaRegulacionService;

@RestController
@RequestMapping(path = "/api/sga/materia-regulacion")
public class MateriaRegulacionController {

	@Autowired
	private MateriaRegulacionService _mrService;

	@CrossOrigin()
	@RequestMapping(method = RequestMethod.GET, path = "/{id}", produces = "application/json")
	public MateriaRegulacion getMateriaRegulacion(@PathVariable String id) {
		return _mrService.getMateriaRegulacion(id);
	}

	@RequestMapping(method = RequestMethod.POST, path = "/")
	@CrossOrigin()
	public MateriaRegulacionDTO saveMateriaRegulacion(@Valid @RequestBody MateriaRegulacion materia) {
		return _mrService.saveMateriaRegulacion(materia);
	}

	@CrossOrigin()
	@RequestMapping(method = RequestMethod.PUT, path = "/", produces = "application/json")
	public MateriaRegulacionDTO updateMateriaRegulacion(@Valid @RequestBody MateriaRegulacion materia) {
		return _mrService.updateMateriaRegulacion(materia);
	}

	@CrossOrigin()
	@RequestMapping(method = RequestMethod.DELETE, path = "/{id}", produces = "application/json")
	public boolean deleteMateriaRegulacion(@PathVariable String id) {
		return _mrService.deleteMateriaRegulacion(id);
	}

}
