package mx.gob.scjn.ugacj.sga_captura.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import mx.gob.scjn.ugacj.sga_captura.domain.DiversaNormativa;
import mx.gob.scjn.ugacj.sga_captura.domain.DiversaNormativaOld;

public interface DiversaNormativaOldRepository extends MongoRepository<DiversaNormativaOld,String> {

}
