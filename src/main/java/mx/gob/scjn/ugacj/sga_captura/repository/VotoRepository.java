package mx.gob.scjn.ugacj.sga_captura.repository;

import mx.gob.scjn.ugacj.sga_captura.domain.Voto;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VotoRepository extends MongoRepository<Voto,String> {
	Voto findByTipoAsuntoAndNumExpediente(String tipoAsunto, String numeroExpediente);
}
