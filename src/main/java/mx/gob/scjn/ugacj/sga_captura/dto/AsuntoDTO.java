package mx.gob.scjn.ugacj.sga_captura.dto;

public class AsuntoDTO {
	String tipoAsunto;
	String numExpediente;
	public String getTipoAsunto() {
		return tipoAsunto;
	}
	public void setTipoAsunto(String tipoAsunto) {
		this.tipoAsunto = tipoAsunto;
	}
	public String getNumExpediente() {
		return numExpediente;
	}
	public void setNumExpediente(String numExpediente) {
		this.numExpediente = numExpediente;
	}

	

}
