package mx.gob.scjn.ugacj.sga_captura.repository;

import mx.gob.scjn.ugacj.sga_captura.domain.RubrosTablaVotaciones;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VotacionesRubrosRepository extends MongoRepository<RubrosTablaVotaciones,String> {

}
