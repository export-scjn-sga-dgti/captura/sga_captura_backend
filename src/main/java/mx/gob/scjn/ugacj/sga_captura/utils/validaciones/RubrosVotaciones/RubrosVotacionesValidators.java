package mx.gob.scjn.ugacj.sga_captura.utils.validaciones.RubrosVotaciones;

import mx.gob.scjn.ugacj.sga_captura.domain.RubrosTablaVotaciones;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class RubrosVotacionesValidators implements ConstraintValidator<ValidatorsRubrosConstrains, RubrosTablaVotaciones> {

    @Override
    public void initialize(ValidatorsRubrosConstrains constraintAnnotation) {
        //ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(RubrosTablaVotaciones rubro, ConstraintValidatorContext constraintValidatorContext) {

        boolean validacion = true;
        String mensaje = "";
        if (rubro.getTipoRubro() != null && rubro.getTipoRubro().length() > 0) {
            //Este tipo de Rubro aplica solo para el tipo de votacion "votacionCausasImprocedenciaySobreseimientoAnalizadas"
            if (rubro.getTipoRubro().equals("rubrosTematicos")) {
                if ((rubro.getRubrosTematicos() == null || rubro.getRubrosTematicos().size() == 0)
                        && (rubro.getAnalisisProcedencia() == null || rubro.getAnalisisProcedencia().size() == 0)
                ) {
                    mensaje = "Debe ingresar datos en al menos un campo.";
                    validacion = false;
                }
            } else {
                //Aplica para los 4 tipos de votaciones restantes, los cuales pueden contener los
                //siguientes 2 tipos de rubros: "rubrosEfectoSentencia" y "rubrosExtencionInvalidez"
                if ((rubro.getVotacionFavor() == null || rubro.getVotacionFavor().size() == 0)
                        && (rubro.getVotacionEnContra() == null || rubro.getVotacionEnContra().size() == 0)
                ) {
                    mensaje = "Debe ingresar al menos 1 voto dentro del campo VotosAFavor y/o VotosEnContra";
                    validacion = false;
                }
            }
        }
        else {
            mensaje = "Defina a que tipo de Rubro pertenece. Tipos aceptados: \"rubrosTematicos\" o  \"rubrosEfectoSentencia\" o \"rubrosExtencionInvalidez\" ";
            validacion = false;
        }
        //disable existing violation message
        constraintValidatorContext.disableDefaultConstraintViolation();
        //Aqui se cambia el menssage definido en la interface "ValidatorsRubrosConstrains"
        constraintValidatorContext.buildConstraintViolationWithTemplate(mensaje).addConstraintViolation();
        return validacion;
    }
}
