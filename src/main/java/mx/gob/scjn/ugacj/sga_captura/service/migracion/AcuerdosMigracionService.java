package mx.gob.scjn.ugacj.sga_captura.service.migracion;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.gob.scjn.ugacj.sga_captura.domain.AcuerdosGenerales;
import mx.gob.scjn.ugacj.sga_captura.domain.AcuerdosGeneralesOld;
import mx.gob.scjn.ugacj.sga_captura.domain.issues.Issues;
import mx.gob.scjn.ugacj.sga_captura.dto.statecode.ResponseDTO;
import mx.gob.scjn.ugacj.sga_captura.repository.AcuerdosGeneralesOldRepository;
import mx.gob.scjn.ugacj.sga_captura.repository.AcuerdosGeneralesRepository;
import mx.gob.scjn.ugacj.sga_captura.repository.issues.IssuesRepository;
import mx.gob.scjn.ugacj.sga_captura.service.porcentaje.PorcentajeService;
import mx.gob.scjn.ugacj.sga_captura.utils.ConvertFechaFiltros;

@Service
public class AcuerdosMigracionService {

	@Autowired
	private AcuerdosGeneralesOldRepository acuerdosOldRepository;
	
	@Autowired
	private AcuerdosGeneralesRepository acuerdosRepository;
	
	@Autowired
	private PorcentajeService porcentajeService;
	
	@Autowired
	private IssuesRepository issuesRepository;
	
	public List<String> upperCaseListString(List<String> list){
		List<String> newList = new ArrayList<String>();
		for(String val:list) newList.add(val.trim().toUpperCase());
		return newList;
	}
	
	public ResponseDTO populateAcuerdos() {

		AcuerdosGenerales newAcuerdo;
		List<AcuerdosGeneralesOld> acuerdosOld = acuerdosOldRepository.findAll();

		for (AcuerdosGeneralesOld oldAcuerdo : acuerdosOld) {

			newAcuerdo = new AcuerdosGenerales();
			System.out.println(oldAcuerdo.getId());
			if(oldAcuerdo.getFechaAprobacion()==null) {
				Issues issue = new Issues();
				issue.setResumen("No Tiene fecha se aprobación");
				issue.setSeccion("Acuerdos");
				issue.setType("ERROR_CAPTURA");
				
				Map<String, String> mapaIssue = new HashMap<>();
				mapaIssue.put("idAcuerdoOld", oldAcuerdo.getId());
				
				issue.setDetail(mapaIssue);
				issuesRepository.save(issue);
			}
			
			newAcuerdo.setFechaAprobacion(ConvertFechaFiltros.convertEngFechaTextToDateString(oldAcuerdo.getFechaAprobacion()));
			newAcuerdo.setAmbitoIncide(oldAcuerdo.getAmbitoIncident().trim().toUpperCase());
			newAcuerdo.setVigencia(oldAcuerdo.getVigencia().trim().toUpperCase());
			newAcuerdo.setFechaPublicacionDOF(ConvertFechaFiltros.convertEngFechaTextToDateString(oldAcuerdo.getFechaPublicacionDOF()));
			newAcuerdo.setClasificacionMateriaRegulacion(upperCaseListString(Arrays.asList(oldAcuerdo.getClasificacionMateriaRegulacion())));
			newAcuerdo.setOrganoEmisor(oldAcuerdo.getOrganoEmisor().trim().toUpperCase());
			newAcuerdo.setMinistroPresidente(oldAcuerdo.getMinistroPresidente().trim().toUpperCase());
			newAcuerdo.setVotacion(oldAcuerdo.getVotacion().trim().toUpperCase());
			newAcuerdo.setVotacionAcuerdoFavor(upperCaseListString(Arrays.asList(oldAcuerdo.getVotacionAcuerdoFavor())));
			newAcuerdo.setVotacionAcuerdoEnContra(upperCaseListString(Arrays.asList(oldAcuerdo.getVotacionAcuerdoContra())));
			newAcuerdo.setMinistrosAusentes(upperCaseListString(Arrays.asList(oldAcuerdo.getMinistrosAusentes())));
			newAcuerdo.setTemasEspecificosRegulacion(Arrays.asList(oldAcuerdo.getTemasRegulacion()));
			newAcuerdo.setRubro(oldAcuerdo.getRubro());
			newAcuerdo.setPorcentaje(0);
			porcentajeService.updatePorcentaje(newAcuerdo);
			acuerdosRepository.save(newAcuerdo);
		}

		return null;
	}
}
