package mx.gob.scjn.ugacj.sga_captura.dto.normativa;

import java.util.List;

import mx.gob.scjn.ugacj.sga_captura.domain.DiversaNormativa;
import mx.gob.scjn.ugacj.sga_captura.domain.MateriaRegulacion;

public class DiversaNormativaDTO {
	
	DiversaNormativa diversaNormativa;
	List<MateriaRegulacion> materias;
	
	public DiversaNormativa getDiversaNormativa() {
		return diversaNormativa;
	}
	public void setDiversaNormativa(DiversaNormativa diversaNormativa) {
		this.diversaNormativa = diversaNormativa;
	}
	public List<MateriaRegulacion> getMaterias() {
		return materias;
	}
	public void setMaterias(List<MateriaRegulacion> materias) {
		this.materias = materias;
	}
	

}
