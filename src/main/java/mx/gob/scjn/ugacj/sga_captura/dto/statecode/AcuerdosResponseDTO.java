package mx.gob.scjn.ugacj.sga_captura.dto.statecode;

import mx.gob.scjn.ugacj.sga_captura.domain.AcuerdosGenerales;

public class AcuerdosResponseDTO extends ResponseDTO {
	
	AcuerdosGenerales acuerdosGenerales;
	
	public AcuerdosResponseDTO(int statusCode, String message) {
		super(statusCode, message);
		// TODO Auto-generated constructor stub
	}

	public AcuerdosGenerales getAcuerdosGenerales() {
		return acuerdosGenerales;
	}

	public void setAcuerdosGenerales(AcuerdosGenerales acuerdosGenerales) {
		this.acuerdosGenerales = acuerdosGenerales;
	}



}
