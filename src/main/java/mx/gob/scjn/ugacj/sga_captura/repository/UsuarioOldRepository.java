package mx.gob.scjn.ugacj.sga_captura.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import mx.gob.scjn.ugacj.sga_captura.domain.UsuarioOld;

@Repository
public interface UsuarioOldRepository extends MongoRepository<UsuarioOld,String> {
}