package mx.gob.scjn.ugacj.sga_captura.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import mx.gob.scjn.ugacj.sga_captura.domain.Catalogo;

@JsonInclude(Include.NON_NULL)
public class CatalogoDTO {

	List<Catalogo> tipoAsunto;
	List<Catalogo> organoRadicacion;
	List<Catalogo> personaPromovente;
	List<Catalogo> personaJuridicoColectiva;
	List<Catalogo> violacionPlanteadaDemanda;
	List<Catalogo> derechosHumanos;
	List<Catalogo> vicioProcesoLegislativo;
	List<Catalogo> violacionInvacionEsferas;
	List<Catalogo> violacionInvacionPoderes;
	
	List<Catalogo> violacionInvacionEsferasFederal;
	List<Catalogo> violacionInvacionEsferasLocal;
	List<Catalogo> violacionInvacionEsferasMunicipal;

		
	List<Catalogo> causaImprocedencia;
	List<Catalogo> ministros;
	List<Catalogo> analisisConstitucionlidad;
	List<Catalogo> tipoSentencia;
	List<Catalogo> violacionOrganicaAnalizada;
	List<Catalogo> congresoEmitio;
	List<Catalogo> violacionInvacionEsferasAnalizada;
	List<Catalogo> materia;
	List<Catalogo> violacionInvacionPoderesAnalizado;
	List<Catalogo> declaracionInvalidez;
	List<Catalogo> tramiteEngrose;
	List<Catalogo> asuntoAbordado;
	List<Catalogo> sentidoResolucion;
	
	List<Catalogo> voto_tipoAsunto;
	List<Catalogo> tipoVoto;
	List<Catalogo> tipoTemaAbordado;
	List<Catalogo> tipoTemaProcesal;
	List<Catalogo> tipoTemaSustantivo;
	
	//acuerdos
	List<Catalogo> ambitoIncide;
	List<Catalogo> vigencia;
	List<Catalogo> materiaRegulacion;
	List<Catalogo> organoEmisor;
	List<Catalogo> votacion;
	List<Catalogo> ministrosPresidentes;
	
	//Seguimiento
	List<Catalogo> usuarios;
	
	//diversa
	List<Catalogo> tipoCircular;
	List<Catalogo> tipoInstrumento;
	
	
	
	public List<Catalogo> getViolacionOrganicaAnalizada() {
		return violacionOrganicaAnalizada;
	}
	public void setViolacionOrganicaAnalizada(List<Catalogo> violacionOrganicaAnalizada) {
		this.violacionOrganicaAnalizada = violacionOrganicaAnalizada;
	}
	public List<Catalogo> getTipoAsunto() {
		return tipoAsunto;
	}
	public void setTipoAsunto(List<Catalogo> tipoAsunto) {
		this.tipoAsunto = tipoAsunto;
	}
	public List<Catalogo> getOrganoRadicacion() {
		return organoRadicacion;
	}
	public void setOrganoRadicacion(List<Catalogo> organoRadicacion) {
		this.organoRadicacion = organoRadicacion;
	}
	public List<Catalogo> getPersonaPromovente() {
		return personaPromovente;
	}
	public void setPersonaPromovente(List<Catalogo> personaPromovente) {
		this.personaPromovente = personaPromovente;
	}
	public List<Catalogo> getPersonaJuridicoColectiva() {
		return personaJuridicoColectiva;
	}
	public void setPersonaJuridicoColectiva(List<Catalogo> personaJuridicoColectiva) {
		this.personaJuridicoColectiva = personaJuridicoColectiva;
	}
	public List<Catalogo> getViolacionPlanteadaDemanda() {
		return violacionPlanteadaDemanda;
	}
	public void setViolacionPlanteadaDemanda(List<Catalogo> violacionPlanteadaDemanda) {
		this.violacionPlanteadaDemanda = violacionPlanteadaDemanda;
	}
	public List<Catalogo> getDerechosHumanos() {
		return derechosHumanos;
	}
	public void setDerechosHumanos(List<Catalogo> derechosHumanos) {
		this.derechosHumanos = derechosHumanos;
	}
	public List<Catalogo> getVicioProcesoLegislativo() {
		return vicioProcesoLegislativo;
	}
	public void setVicioProcesoLegislativo(List<Catalogo> vicioProcesoLegislativo) {
		this.vicioProcesoLegislativo = vicioProcesoLegislativo;
	}
	public List<Catalogo> getViolacionInvacionEsferas() {
		return violacionInvacionEsferas;
	}
	
	
	public void setViolacionInvacionEsferas(List<Catalogo> violacionInvacionEsferas) {
		this.violacionInvacionEsferas = violacionInvacionEsferas;
	}
	public List<Catalogo> getViolacionInvacionPoderes() {
		return violacionInvacionPoderes;
	}
	public void setViolacionInvacionPoderes(List<Catalogo> violacionInvacionPoderes) {
		this.violacionInvacionPoderes = violacionInvacionPoderes;
	}
	public List<Catalogo> getCausaImprocedencia() {
		return causaImprocedencia;
	}
	public void setCausaImprocedencia(List<Catalogo> causaImprocedencia) {
		this.causaImprocedencia = causaImprocedencia;
	}
	public List<Catalogo> getMinistros() {
		return ministros;
	}
	public void setMinistros(List<Catalogo> ministros) {
		this.ministros = ministros;
	}
	public List<Catalogo> getAnalisisConstitucionlidad() {
		return analisisConstitucionlidad;
	}
	public void setAnalisisConstitucionlidad(List<Catalogo> analisisConstitucionlidad) {
		this.analisisConstitucionlidad = analisisConstitucionlidad;
	}
	public List<Catalogo> getTipoSentencia() {
		return tipoSentencia;
	}
	public void setTipoSentencia(List<Catalogo> tipoSentencia) {
		this.tipoSentencia = tipoSentencia;
	}
	public List<Catalogo> getCongresoEmitio() {
		return congresoEmitio;
	}
	public void setCongresoEmitio(List<Catalogo> congresoEmitio) {
		this.congresoEmitio = congresoEmitio;
	}
	public List<Catalogo> getViolacionInvacionEsferasAnalizada() {
		return violacionInvacionEsferasAnalizada;
	}
	public void setViolacionInvacionEsferasAnalizada(List<Catalogo> violacionInvacionEsferasAnalizada) {
		this.violacionInvacionEsferasAnalizada = violacionInvacionEsferasAnalizada;
	}
	public List<Catalogo> getMateria() {
		return materia;
	}
	public void setMateria(List<Catalogo> materia) {
		this.materia = materia;
	}
	public List<Catalogo> getViolacionInvacionPoderesAnalizado() {
		return violacionInvacionPoderesAnalizado;
	}
	public void setViolacionInvacionPoderesAnalizado(List<Catalogo> violacionInvacionPoderesAnalizado) {
		this.violacionInvacionPoderesAnalizado = violacionInvacionPoderesAnalizado;
	}
	public List<Catalogo> getDeclaracionInvalidez() {
		return declaracionInvalidez;
	}
	public void setDeclaracionInvalidez(List<Catalogo> declaracionInvalidez) {
		this.declaracionInvalidez = declaracionInvalidez;
	}
	public List<Catalogo> getTramiteEngrose() {
		return tramiteEngrose;
	}
	public void setTramiteEngrose(List<Catalogo> tramiteEngrose) {
		this.tramiteEngrose = tramiteEngrose;
	}
	public List<Catalogo> getAsuntoAbordado() {
		return asuntoAbordado;
	}
	public void setAsuntoAbordado(List<Catalogo> asuntoAbordado) {
		this.asuntoAbordado = asuntoAbordado;
	}
	public List<Catalogo> getSentidoResolucion() {
		return sentidoResolucion;
	}
	public void setSentidoResolucion(List<Catalogo> sentidoResolucion) {
		this.sentidoResolucion = sentidoResolucion;
	}
	public List<Catalogo> getVoto_tipoAsunto() {
		return voto_tipoAsunto;
	}
	public void setVoto_tipoAsunto(List<Catalogo> voto_tipoAsunto) {
		this.voto_tipoAsunto = voto_tipoAsunto;
	}
	public List<Catalogo> getTipoVoto() {
		return tipoVoto;
	}
	public void setTipoVoto(List<Catalogo> tipoVoto) {
		this.tipoVoto = tipoVoto;
	}
	public List<Catalogo> getTipoTemaAbordado() {
		return tipoTemaAbordado;
	}
	public void setTipoTemaAbordado(List<Catalogo> tipoTemaAbordado) {
		this.tipoTemaAbordado = tipoTemaAbordado;
	}
	public List<Catalogo> getTipoTemaProcesal() {
		return tipoTemaProcesal;
	}
	public void setTipoTemaProcesal(List<Catalogo> tipoTemaProcesal) {
		this.tipoTemaProcesal = tipoTemaProcesal;
	}
	public List<Catalogo> getTipoTemaSustantivo() {
		return tipoTemaSustantivo;
	}
	public void setTipoTemaSustantivo(List<Catalogo> tipoTemaSustantivo) {
		this.tipoTemaSustantivo = tipoTemaSustantivo;
	}
	public List<Catalogo> getAmbitoIncide() {
		return ambitoIncide;
	}
	public void setAmbitoIncide(List<Catalogo> ambitoIncide) {
		this.ambitoIncide = ambitoIncide;
	}
	public List<Catalogo> getVigencia() {
		return vigencia;
	}
	public void setVigencia(List<Catalogo> vigencia) {
		this.vigencia = vigencia;
	}
	public List<Catalogo> getMateriaRegulacion() {
		return materiaRegulacion;
	}
	public void setMateriaRegulacion(List<Catalogo> materiaRegulacion) {
		this.materiaRegulacion = materiaRegulacion;
	}
	public List<Catalogo> getOrganoEmisor() {
		return organoEmisor;
	}
	public void setOrganoEmisor(List<Catalogo> organoEmisor) {
		this.organoEmisor = organoEmisor;
	}
	public List<Catalogo> getVotacion() {
		return votacion;
	}
	public void setVotacion(List<Catalogo> votacion) {
		this.votacion = votacion;
	}
	public List<Catalogo> getMinistrosPresidentes() {
		return ministrosPresidentes;
	}
	public void setMinistrosPresidentes(List<Catalogo> ministrosPresidentes) {
		this.ministrosPresidentes = ministrosPresidentes;
	}
	public List<Catalogo> getTipoCircular() {
		return tipoCircular;
	}
	public void setTipoCircular(List<Catalogo> tipoCircular) {
		this.tipoCircular = tipoCircular;
	}
	public List<Catalogo> getTipoInstrumento() {
		return tipoInstrumento;
	}
	public void setTipoInstrumento(List<Catalogo> tipoInstrumento) {
		this.tipoInstrumento = tipoInstrumento;
	}
	public List<Catalogo> getUsuarios() {
		return usuarios;
	}
	public void setUsuarios(List<Catalogo> usuarios) {
		this.usuarios = usuarios;
	}
	public List<Catalogo> getViolacionInvacionEsferasFederal() {
		return violacionInvacionEsferasFederal;
	}
	public void setViolacionInvacionEsferasFederal(List<Catalogo> violacionInvacionEsferasFederal) {
		this.violacionInvacionEsferasFederal = violacionInvacionEsferasFederal;
	}
	public List<Catalogo> getViolacionInvacionEsferasLocal() {
		return violacionInvacionEsferasLocal;
	}
	public void setViolacionInvacionEsferasLocal(List<Catalogo> violacionInvacionEsferasLocal) {
		this.violacionInvacionEsferasLocal = violacionInvacionEsferasLocal;
	}
	public List<Catalogo> getViolacionInvacionEsferasMunicipal() {
		return violacionInvacionEsferasMunicipal;
	}
	public void setViolacionInvacionEsferasMunicipal(List<Catalogo> violacionInvacionEsferasMunicipal) {
		this.violacionInvacionEsferasMunicipal = violacionInvacionEsferasMunicipal;
	}
	
	
}
