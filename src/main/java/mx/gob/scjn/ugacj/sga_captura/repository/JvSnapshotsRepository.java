package mx.gob.scjn.ugacj.sga_captura.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import mx.gob.scjn.ugacj.sga_captura.domain.bitacora.JvSnapshots;


@Repository
public interface JvSnapshotsRepository extends MongoRepository<JvSnapshots,String> {

	List<JvSnapshots> findByTypeAndSeccion(String type, String seccion);
}
