package mx.gob.scjn.ugacj.sga_captura.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import mx.gob.scjn.ugacj.sga_captura.domain.AcuerdosGeneralesOld;

public interface AcuerdosGeneralesOldRepository extends MongoRepository<AcuerdosGeneralesOld,String> {

}
