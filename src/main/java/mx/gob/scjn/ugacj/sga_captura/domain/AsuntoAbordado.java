package mx.gob.scjn.ugacj.sga_captura.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "#{@environment.getProperty('indice.vtaquigrafica.asuntoabordado')}")
public class AsuntoAbordado implements Serializable {

	@Id
	String id;
	boolean isAcumulada = false;
	boolean isCa = false;

	String asuntoAbordado;

	String numeroExpediente;

	List<String> acumulados = new ArrayList<String>();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean isAcumulada() {
		return isAcumulada;
	}

	public void setAcumulada(boolean isAcumulada) {
		this.isAcumulada = isAcumulada;
	}

	public boolean isCa() {
		return isCa;
	}

	public void setCa(boolean isCa) {
		this.isCa = isCa;
	}

	public String getAsuntoAbordado() {
		return asuntoAbordado.toUpperCase();
	}

	public void setAsuntoAbordado(String asuntoAbordado) {
		this.asuntoAbordado = asuntoAbordado;
	}

	public String getNumeroExpediente() {
		return numeroExpediente;
	}

	public void setNumeroExpediente(String numeroExpediente) {
		this.numeroExpediente = numeroExpediente;
	}

	public List<String> getAcumulados() {
		return acumulados;
	}

	public void setAcumulados(List<String> acumulados) {
		this.acumulados = acumulados;
	}
}
