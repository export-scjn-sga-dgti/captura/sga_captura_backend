package mx.gob.scjn.ugacj.sga_captura.dto;

public class InformacionDTO {
    
    String info;

    public String getInfo() {
        return this.info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

}
