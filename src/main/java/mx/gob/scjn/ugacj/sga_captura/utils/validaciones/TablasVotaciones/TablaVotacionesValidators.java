package mx.gob.scjn.ugacj.sga_captura.utils.validaciones.TablasVotaciones;

import mx.gob.scjn.ugacj.sga_captura.dto.TablaVotacionesRubrosDTO;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class TablaVotacionesValidators implements ConstraintValidator<ValidatorsTablaVotacionesConstrain, TablaVotacionesRubrosDTO> {

    @Override
    public void initialize(ValidatorsTablaVotacionesConstrain constraintAnnotation) {
        //ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(TablaVotacionesRubrosDTO validatorVotacion, ConstraintValidatorContext constraintValidatorContext) {

        if ((validatorVotacion.getVotacionFavor() != null && validatorVotacion.getVotacionFavor().size() > 0) ||
                validatorVotacion.getVotacionEnContra() != null && validatorVotacion.getVotacionEnContra().size() > 0
        ) {
            return true;
        }

        return false;
    }


}
