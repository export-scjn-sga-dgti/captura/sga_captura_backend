package mx.gob.scjn.ugacj.sga_captura.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Date;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SentenciaDTO {

    String id;
    String tipoAsunto;
    String numeroExpediente;
    String organoRadicacion;
    //@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    String fechaResolucion;
    String ponente;
    String tipoPersonaPromovente;
    String tipoPersonaJuridicoColectiva;
    String tipoViolacionPlanteadaenDemanda;
    List<String> derechosHumanosCuyaViolacionPlantea;
    List<String> derechosHumanosCuyaViolacionPlanteaByDerechosHumanos;
    List<String> violacionOrganicaPlanteada;
    List<String> tipoVicioProcesoLegislativo;
    String tipoViolacionInvacionEsferas;
    String tipoViolacionInvacionPoderes;
    
    String materiaAnalizadaInvasionEsferas;
	String materiaAnalizadaInvasionPoderes;
	
    List<TablaVotacionesRubrosDTO> votacionCausasImprocedenciaySobreseimientoAnalizadas;
    List<TablaVotacionesRubrosDTO> votacionDerechosHumanosCuyaViolacionAnalizaSentencia;
    List<TablaVotacionesRubrosDTO> votacionTipoVicioProcesoLegislativoAnalizadoenSentencia;
    List<TablaVotacionesRubrosDTO> votacionTipoViolacionInvacionEsferasAnalizadoenSentencia;
    List<TablaVotacionesRubrosDTO> votacionTipoViolacionInvacionPoderesAnalizadoenSentencia;
    List<String> violacionOrganicaAnalizadaenSentencia;
    String tipoViolacionInvacionEsferasMateriaAnalizadaenSentencia;
    String tipoViolacionInvacionPoderesAnalizadoenSentencia;
    String momentoSurteEfectoSentencia;
    int numeroVotacionesRealizadas = 0;
    int numeroVotacionesRegistradas = 0;
    String tramiteEngrose;
    int diasTranscurridosEntreDictadoSentenciayFirmaEngrose = 0;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    Date fechaSentencia;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    Date fechaFirmaEngrose;
    private List<String> acumulados;
    private  Boolean isAcumulada;
    private  Boolean isCA;
    Integer totalVotacionCausasImprocedenciaySobreseimientoAnalizadas = 0;
    Integer totalVotacionDerechosHumanosCuyaViolacionAnalizaSentencia = 0;
    Integer totalVotacionTipoVicioProcesoLegislativoAnalizadoenSentencia = 0;
    Integer totalVotacionTipoViolacionInvacionEsferasAnalizadoenSentencia = 0;
    Integer totalVotacionTipoViolacionInvacionPoderesAnalizadoenSentencia = 0;
    String origen = "";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTipoAsunto() {
        return tipoAsunto;
    }

    public void setTipoAsunto(String tipoAsunto) {
        this.tipoAsunto = tipoAsunto;
    }

    public String getNumeroExpediente() {
        return numeroExpediente;
    }

    public void setNumeroExpediente(String numeroExpediente) {
        this.numeroExpediente = numeroExpediente;
    }

    public String getOrganoRadicacion() {
        return organoRadicacion;
    }

    public void setOrganoRadicacion(String organoRadicacion) {
        this.organoRadicacion = organoRadicacion;
    }

    public String getFechaResolucion() {
        return fechaResolucion;
    }

    public void setFechaResolucion(String fechaResolucion) {
        this.fechaResolucion = fechaResolucion;
    }

    public String getPonente() {
        return ponente;
    }

    public void setPonente(String ponente) {
        this.ponente = ponente;
    }

    public String getTipoPersonaPromovente() {
        return tipoPersonaPromovente;
    }

    public void setTipoPersonaPromovente(String tipoPersonaPromovente) {
        this.tipoPersonaPromovente = tipoPersonaPromovente;
    }

    public String getTipoPersonaJuridicoColectiva() {
        return tipoPersonaJuridicoColectiva;
    }

    public void setTipoPersonaJuridicoColectiva(String tipoPersonaJuridicoColectiva) {
        this.tipoPersonaJuridicoColectiva = tipoPersonaJuridicoColectiva;
    }

    public String getTipoViolacionPlanteadaenDemanda() {
        return tipoViolacionPlanteadaenDemanda;
    }

    public void setTipoViolacionPlanteadaenDemanda(String tipoViolacionPlanteadaenDemanda) {
        this.tipoViolacionPlanteadaenDemanda = tipoViolacionPlanteadaenDemanda;
    }

    public List<String> getDerechosHumanosCuyaViolacionPlantea() {
        return derechosHumanosCuyaViolacionPlantea;
    }

    public void setDerechosHumanosCuyaViolacionPlantea(List<String> derechosHumanosCuyaViolacionPlantea) {
        this.derechosHumanosCuyaViolacionPlantea = derechosHumanosCuyaViolacionPlantea;
    }

    public List<String> getViolacionOrganicaPlanteada() {
        return violacionOrganicaPlanteada;
    }

    public void setViolacionOrganicaPlanteada(List<String> violacionOrganicaPlanteada) {
        this.violacionOrganicaPlanteada = violacionOrganicaPlanteada;
    }

    public List<String> getTipoVicioProcesoLegislativo() {
        return tipoVicioProcesoLegislativo;
    }

    public void setTipoVicioProcesoLegislativo(List<String> tipoVicioProcesoLegislativo) {
        this.tipoVicioProcesoLegislativo = tipoVicioProcesoLegislativo;
    }

    public String getTipoViolacionInvacionEsferas() {
        return tipoViolacionInvacionEsferas;
    }

    public void setTipoViolacionInvacionEsferas(String tipoViolacionInvacionEsferas) {
        this.tipoViolacionInvacionEsferas = tipoViolacionInvacionEsferas;
    }

    public String getTipoViolacionInvacionPoderes() {
        return tipoViolacionInvacionPoderes;
    }

    public void setTipoViolacionInvacionPoderes(String tipoViolacionInvacionPoderes) {
        this.tipoViolacionInvacionPoderes = tipoViolacionInvacionPoderes;
    }

    public List<TablaVotacionesRubrosDTO> getVotacionCausasImprocedenciaySobreseimientoAnalizadas() {
        return votacionCausasImprocedenciaySobreseimientoAnalizadas;
    }

    public void setVotacionCausasImprocedenciaySobreseimientoAnalizadas(List<TablaVotacionesRubrosDTO> votacionCausasImprocedenciaySobreseimientoAnalizadas) {
        this.votacionCausasImprocedenciaySobreseimientoAnalizadas = votacionCausasImprocedenciaySobreseimientoAnalizadas;
    }

    public List<TablaVotacionesRubrosDTO> getVotacionDerechosHumanosCuyaViolacionAnalizaSentencia() {
        return votacionDerechosHumanosCuyaViolacionAnalizaSentencia;
    }

    public void setVotacionDerechosHumanosCuyaViolacionAnalizaSentencia(List<TablaVotacionesRubrosDTO> votacionDerechosHumanosCuyaViolacionAnalizaSentencia) {
        this.votacionDerechosHumanosCuyaViolacionAnalizaSentencia = votacionDerechosHumanosCuyaViolacionAnalizaSentencia;
    }

    public List<TablaVotacionesRubrosDTO> getVotacionTipoVicioProcesoLegislativoAnalizadoenSentencia() {
        return votacionTipoVicioProcesoLegislativoAnalizadoenSentencia;
    }

    public void setVotacionTipoVicioProcesoLegislativoAnalizadoenSentencia(List<TablaVotacionesRubrosDTO> votacionTipoVicioProcesoLegislativoAnalizadoenSentencia) {
        this.votacionTipoVicioProcesoLegislativoAnalizadoenSentencia = votacionTipoVicioProcesoLegislativoAnalizadoenSentencia;
    }

    public List<TablaVotacionesRubrosDTO> getVotacionTipoViolacionInvacionEsferasAnalizadoenSentencia() {
        return votacionTipoViolacionInvacionEsferasAnalizadoenSentencia;
    }

    public void setVotacionTipoViolacionInvacionEsferasAnalizadoenSentencia(List<TablaVotacionesRubrosDTO> votacionTipoViolacionInvacionEsferasAnalizadoenSentencia) {
        this.votacionTipoViolacionInvacionEsferasAnalizadoenSentencia = votacionTipoViolacionInvacionEsferasAnalizadoenSentencia;
    }

    public List<TablaVotacionesRubrosDTO> getVotacionTipoViolacionInvacionPoderesAnalizadoenSentencia() {
        return votacionTipoViolacionInvacionPoderesAnalizadoenSentencia;
    }

    public void setVotacionTipoViolacionInvacionPoderesAnalizadoenSentencia(List<TablaVotacionesRubrosDTO> votacionTipoViolacionInvacionPoderesAnalizadoenSentencia) {
        this.votacionTipoViolacionInvacionPoderesAnalizadoenSentencia = votacionTipoViolacionInvacionPoderesAnalizadoenSentencia;
    }

    public List<String> getViolacionOrganicaAnalizadaenSentencia() {
        return violacionOrganicaAnalizadaenSentencia;
    }

    public void setViolacionOrganicaAnalizadaenSentencia(List<String> violacionOrganicaAnalizadaenSentencia) {
        this.violacionOrganicaAnalizadaenSentencia = violacionOrganicaAnalizadaenSentencia;
    }

    public String getTipoViolacionInvacionEsferasMateriaAnalizadaenSentencia() {
        return tipoViolacionInvacionEsferasMateriaAnalizadaenSentencia;
    }

    public void setTipoViolacionInvacionEsferasMateriaAnalizadaenSentencia(String tipoViolacionInvacionEsferasMateriaAnalizadaenSentencia) {
        this.tipoViolacionInvacionEsferasMateriaAnalizadaenSentencia = tipoViolacionInvacionEsferasMateriaAnalizadaenSentencia;
    }

    public String getTipoViolacionInvacionPoderesAnalizadoenSentencia() {
        return tipoViolacionInvacionPoderesAnalizadoenSentencia;
    }

    public void setTipoViolacionInvacionPoderesAnalizadoenSentencia(String tipoViolacionInvacionPoderesAnalizadoenSentencia) {
        this.tipoViolacionInvacionPoderesAnalizadoenSentencia = tipoViolacionInvacionPoderesAnalizadoenSentencia;
    }

    public String getMomentoSurteEfectoSentencia() {
        return momentoSurteEfectoSentencia;
    }

    public void setMomentoSurteEfectoSentencia(String momentoSurteEfectoSentencia) {
        this.momentoSurteEfectoSentencia = momentoSurteEfectoSentencia;
    }

    public int getNumeroVotacionesRealizadas() {
        return numeroVotacionesRealizadas;
    }

    public void setNumeroVotacionesRealizadas(int numeroVotacionesRealizadas) {
        this.numeroVotacionesRealizadas = numeroVotacionesRealizadas;
    }

    public int getNumeroVotacionesRegistradas() {
        return numeroVotacionesRegistradas;
    }

    public void setNumeroVotacionesRegistradas(int numeroVotacionesRegistradas) {
        this.numeroVotacionesRegistradas = numeroVotacionesRegistradas;
    }

    public String getTramiteEngrose() {
        return tramiteEngrose;
    }

    public void setTramiteEngrose(String tramiteEngrose) {
        this.tramiteEngrose = tramiteEngrose;
    }

   

    public int getDiasTranscurridosEntreDictadoSentenciayFirmaEngrose() {
		return diasTranscurridosEntreDictadoSentenciayFirmaEngrose;
	}

	public void setDiasTranscurridosEntreDictadoSentenciayFirmaEngrose(
			int diasTranscurridosEntreDictadoSentenciayFirmaEngrose) {
		this.diasTranscurridosEntreDictadoSentenciayFirmaEngrose = diasTranscurridosEntreDictadoSentenciayFirmaEngrose;
	}

	public Boolean getIsAcumulada() {
		return isAcumulada;
	}

	public void setIsAcumulada(Boolean isAcumulada) {
		this.isAcumulada = isAcumulada;
	}

	public Boolean getIsCA() {
		return isCA;
	}

	public void setIsCA(Boolean isCA) {
		this.isCA = isCA;
	}

	public Date getFechaSentencia() {
        return fechaSentencia;
    }

    public void setFechaSentencia(Date fechaSentencia) {
        this.fechaSentencia = fechaSentencia;
    }

    public Date getFechaFirmaEngrose() {
        return fechaFirmaEngrose;
    }

    public void setFechaFirmaEngrose(Date fechaFirmaEngrose) {
        this.fechaFirmaEngrose = fechaFirmaEngrose;
    }

    public List<String> getAcumulados() {
        return acumulados;
    }

    public void setAcumulados(List<String> acumulados) {
        this.acumulados = acumulados;
    }

    public Boolean getAcumulada() {
        return isAcumulada;
    }

    public void setAcumulada(Boolean acumulada) {
        isAcumulada = acumulada;
    }

    public Boolean getCA() {
        return isCA;
    }

    public void setCA(Boolean CA) {
        isCA = CA;
    }

    public Integer getTotalVotacionCausasImprocedenciaySobreseimientoAnalizadas() {
        return totalVotacionCausasImprocedenciaySobreseimientoAnalizadas;
    }

    public void setTotalVotacionCausasImprocedenciaySobreseimientoAnalizadas(Integer totalVotacionCausasImprocedenciaySobreseimientoAnalizadas) {
        this.totalVotacionCausasImprocedenciaySobreseimientoAnalizadas = totalVotacionCausasImprocedenciaySobreseimientoAnalizadas;
    }

    public Integer getTotalVotacionDerechosHumanosCuyaViolacionAnalizaSentencia() {
        return totalVotacionDerechosHumanosCuyaViolacionAnalizaSentencia;
    }

    public void setTotalVotacionDerechosHumanosCuyaViolacionAnalizaSentencia(Integer totalVotacionDerechosHumanosCuyaViolacionAnalizaSentencia) {
        this.totalVotacionDerechosHumanosCuyaViolacionAnalizaSentencia = totalVotacionDerechosHumanosCuyaViolacionAnalizaSentencia;
    }

    public Integer getTotalVotacionTipoVicioProcesoLegislativoAnalizadoenSentencia() {
        return totalVotacionTipoVicioProcesoLegislativoAnalizadoenSentencia;
    }

    public void setTotalVotacionTipoVicioProcesoLegislativoAnalizadoenSentencia(Integer totalVotacionTipoVicioProcesoLegislativoAnalizadoenSentencia) {
        this.totalVotacionTipoVicioProcesoLegislativoAnalizadoenSentencia = totalVotacionTipoVicioProcesoLegislativoAnalizadoenSentencia;
    }

    public Integer getTotalVotacionTipoViolacionInvacionEsferasAnalizadoenSentencia() {
        return totalVotacionTipoViolacionInvacionEsferasAnalizadoenSentencia;
    }

    public void setTotalVotacionTipoViolacionInvacionEsferasAnalizadoenSentencia(Integer totalVotacionTipoViolacionInvacionEsferasAnalizadoenSentencia) {
        this.totalVotacionTipoViolacionInvacionEsferasAnalizadoenSentencia = totalVotacionTipoViolacionInvacionEsferasAnalizadoenSentencia;
    }

    public Integer getTotalVotacionTipoViolacionInvacionPoderesAnalizadoenSentencia() {
        return totalVotacionTipoViolacionInvacionPoderesAnalizadoenSentencia;
    }

    public void setTotalVotacionTipoViolacionInvacionPoderesAnalizadoenSentencia(Integer totalVotacionTipoViolacionInvacionPoderesAnalizadoenSentencia) {
        this.totalVotacionTipoViolacionInvacionPoderesAnalizadoenSentencia = totalVotacionTipoViolacionInvacionPoderesAnalizadoenSentencia;
    }

    public List<String> getDerechosHumanosCuyaViolacionPlanteaByDerechosHumanos() {
        return derechosHumanosCuyaViolacionPlanteaByDerechosHumanos;
    }

    public void setDerechosHumanosCuyaViolacionPlanteaByDerechosHumanos(List<String> derechosHumanosCuyaViolacionPlanteaByDerechosHumanos) {
        this.derechosHumanosCuyaViolacionPlanteaByDerechosHumanos = derechosHumanosCuyaViolacionPlanteaByDerechosHumanos;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }
    
    

    public String getMateriaAnalizadaInvasionEsferas() {
		return materiaAnalizadaInvasionEsferas;
	}

	public void setMateriaAnalizadaInvasionEsferas(String materiaAnalizadaInvasionEsferas) {
		this.materiaAnalizadaInvasionEsferas = materiaAnalizadaInvasionEsferas;
	}

	public String getMateriaAnalizadaInvasionPoderes() {
		return materiaAnalizadaInvasionPoderes;
	}

	public void setMateriaAnalizadaInvasionPoderes(String materiaAnalizadaInvasionPoderes) {
		this.materiaAnalizadaInvasionPoderes = materiaAnalizadaInvasionPoderes;
	}

    @Override
    public String toString() {
        return "SentenciaDTO{" +
                "id='" + id + '\'' +
                ", tipoAsunto='" + tipoAsunto + '\'' +
                ", numeroExpediente='" + numeroExpediente + '\'' +
                ", organoRadicacion='" + organoRadicacion + '\'' +
                ", fechaResolucion='" + fechaResolucion + '\'' +
                ", ponente='" + ponente + '\'' +
                ", tipoPersonaPromovente='" + tipoPersonaPromovente + '\'' +
                ", tipoPersonaJuridicoColectiva='" + tipoPersonaJuridicoColectiva + '\'' +
                ", tipoViolacionPlanteadaenDemanda='" + tipoViolacionPlanteadaenDemanda + '\'' +
                ", derechosHumanosCuyaViolacionPlantea=" + derechosHumanosCuyaViolacionPlantea +
                ", derechosHumanosCuyaViolacionPlanteaByDerechosHumanos=" + derechosHumanosCuyaViolacionPlanteaByDerechosHumanos +
                ", violacionOrganicaPlanteada=" + violacionOrganicaPlanteada +
                ", tipoVicioProcesoLegislativo=" + tipoVicioProcesoLegislativo +
                ", tipoViolacionInvacionEsferas='" + tipoViolacionInvacionEsferas + '\'' +
                ", tipoViolacionInvacionPoderes='" + tipoViolacionInvacionPoderes + '\'' +
                ", materiaAnalizadaInvasionEsferas='" + materiaAnalizadaInvasionEsferas + '\'' +
                ", materiaAnalizadaInvasionPoderes='" + materiaAnalizadaInvasionPoderes + '\'' +
                ", votacionCausasImprocedenciaySobreseimientoAnalizadas=" + votacionCausasImprocedenciaySobreseimientoAnalizadas +
                ", votacionDerechosHumanosCuyaViolacionAnalizaSentencia=" + votacionDerechosHumanosCuyaViolacionAnalizaSentencia +
                ", votacionTipoVicioProcesoLegislativoAnalizadoenSentencia=" + votacionTipoVicioProcesoLegislativoAnalizadoenSentencia +
                ", votacionTipoViolacionInvacionEsferasAnalizadoenSentencia=" + votacionTipoViolacionInvacionEsferasAnalizadoenSentencia +
                ", votacionTipoViolacionInvacionPoderesAnalizadoenSentencia=" + votacionTipoViolacionInvacionPoderesAnalizadoenSentencia +
                ", violacionOrganicaAnalizadaenSentencia=" + violacionOrganicaAnalizadaenSentencia +
                ", tipoViolacionInvacionEsferasMateriaAnalizadaenSentencia='" + tipoViolacionInvacionEsferasMateriaAnalizadaenSentencia + '\'' +
                ", tipoViolacionInvacionPoderesAnalizadoenSentencia='" + tipoViolacionInvacionPoderesAnalizadoenSentencia + '\'' +
                ", momentoSurteEfectoSentencia='" + momentoSurteEfectoSentencia + '\'' +
                ", numeroVotacionesRealizadas=" + numeroVotacionesRealizadas +
                ", numeroVotacionesRegistradas=" + numeroVotacionesRegistradas +
                ", tramiteEngrose='" + tramiteEngrose + '\'' +
                ", diasTranscurridosEntreDictadoSentenciayFirmaEngrose=" + diasTranscurridosEntreDictadoSentenciayFirmaEngrose +
                ", fechaSentencia=" + fechaSentencia +
                ", fechaFirmaEngrose=" + fechaFirmaEngrose +
                ", acumulados=" + acumulados +
                ", isAcumulada=" + isAcumulada +
                ", isCA=" + isCA +
                ", totalVotacionCausasImprocedenciaySobreseimientoAnalizadas=" + totalVotacionCausasImprocedenciaySobreseimientoAnalizadas +
                ", totalVotacionDerechosHumanosCuyaViolacionAnalizaSentencia=" + totalVotacionDerechosHumanosCuyaViolacionAnalizaSentencia +
                ", totalVotacionTipoVicioProcesoLegislativoAnalizadoenSentencia=" + totalVotacionTipoVicioProcesoLegislativoAnalizadoenSentencia +
                ", totalVotacionTipoViolacionInvacionEsferasAnalizadoenSentencia=" + totalVotacionTipoViolacionInvacionEsferasAnalizadoenSentencia +
                ", totalVotacionTipoViolacionInvacionPoderesAnalizadoenSentencia=" + totalVotacionTipoViolacionInvacionPoderesAnalizadoenSentencia +
                ", origen='" + origen + '\'' +
                '}';
    }
}
