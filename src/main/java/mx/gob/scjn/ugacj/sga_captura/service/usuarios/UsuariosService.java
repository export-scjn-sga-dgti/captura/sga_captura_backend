package mx.gob.scjn.ugacj.sga_captura.service.usuarios;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import mx.gob.scjn.ugacj.sga_captura.domain.Usuario;
import mx.gob.scjn.ugacj.sga_captura.dto.ModuloDTO;
import mx.gob.scjn.ugacj.sga_captura.repository.UsuarioRepository;
import mx.gob.scjn.ugacj.sga_captura.service.bitacora.BitacoraService;
import mx.gob.scjn.ugacj.sga_captura.utils.StatusBitacora;

@Service
public class UsuariosService {

	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Autowired
	private MongoOperations mongoOperations;
	
	@Autowired
	private BitacoraService bitacoraService;
	
	@Autowired
	private HttpServletRequest request;
	
	public Usuario getLoggedUser() {
		if (request.getAttribute("username") != null && !request.getAttribute("username").toString().isEmpty()) {
			return getUsuarioByUserName(request.getAttribute("username").toString());
		}
		return null;
	}

	public ModuloDTO validateUserAccessToModulo(String pantalla) {
    	Usuario user = getLoggedUser();
    	if(user!=null) {
    		for(ModuloDTO modulo: user.getModulos()) {
    			if(pantalla.equals(modulo.getModulo())) {
    				return modulo;
    			}
    		}
    	}
    	return null;
    }
	
	public Page<Usuario> getAllusuariosPages(Pageable paging, String filtros) throws ParseException {
		List<Usuario> listUsers = new ArrayList<Usuario>();
		Page<Usuario> usuarioPaginable = null;
		List<String> usersNotIn= new ArrayList<String>(Arrays.asList("ggarciab", "avazquez", "jegonzaleza", "fgsandovalf", "cmariscal", "atoledob"));
		
		Query query = new Query();
		query.addCriteria(Criteria.where("correo").nin(usersNotIn));
		
		if (filtros != null) {
			String sTempFiltros[] = filtros.split(":");
			String filtro = sTempFiltros[0].trim();
			String filtroValores = sTempFiltros[1].trim();
			query.addCriteria(Criteria.where(filtro).is(filtroValores));
			listUsers = mongoOperations.find(query, Usuario.class);
			long count = mongoOperations.count(query, Usuario.class);
			usuarioPaginable = new PageImpl<Usuario>(listUsers, paging, count);
			return usuarioPaginable;
		}
		
		listUsers = mongoOperations.find(query, Usuario.class);
		//listUsers = usuarioRepository.findAll();
		usuarioPaginable = new PageImpl<Usuario>(listUsers, paging, listUsers.size());
		return usuarioPaginable;
	}

	public Usuario getUsuarioById(String usuarioId) {
		Optional<Usuario> userExist = usuarioRepository.findById(usuarioId);
		if (userExist.isPresent()) {
			return userExist.get();
		}
		return null;
	}
	
	public Usuario getUsuarioByUserName(String username) {
		Optional<Usuario> userExist = usuarioRepository.findByCorreo(username);
		if (userExist.isPresent()) {
			return userExist.get();
		}
		return null;
	}
	
	public Usuario getUsuarioByNombre(String nombre) {
		Optional<Usuario> userExist = usuarioRepository.findByNombreRegex(nombre);
		if (userExist.isPresent()) {
			return userExist.get();
		}
		return null;
	}

	public Usuario saveUsuario(Usuario usuario) {
		Optional<Usuario> userExist = usuarioRepository.findByCorreo(usuario.getCorreo());
		if (!userExist.isPresent()) {
			usuario.setNombre(usuario.getNombre().trim());
			usuario.setCorreo(usuario.getCorreo().trim());
			usuario.setFh_creacion(new Date());
			usuario.setFh_modificacion(new Date());
			usuarioRepository.save(usuario);
			bitacoraService.saveOperacion(StatusBitacora._USUARIOS, StatusBitacora._SAVE, usuario, null);
			return usuario;
		}
		return null;
	}

	public Usuario updateUsuario(Usuario usuario) {
		Optional<Usuario> userExist = usuarioRepository.findById(usuario.getId());
		if (userExist.isPresent()) {
			Optional<Usuario> begin = usuarioRepository.findById(usuario.getId());
			usuario.setFh_modificacion(new Date());
			usuario.setNombre(usuario.getNombre().trim());
			usuario.setCorreo(usuario.getCorreo().trim());
			usuarioRepository.save(usuario);
			bitacoraService.saveOperacion(StatusBitacora._USUARIOS, StatusBitacora._UPDATE, userExist.get(), begin.get());
			return usuario;
		}
		return null;
	}
	
	public boolean eliminarUsuario(String id) {
		Optional<Usuario> userExist = usuarioRepository.findById(id);
		if (userExist.isPresent()) {
			bitacoraService.saveOperacion(StatusBitacora._USUARIOS, StatusBitacora._DELETE,userExist.get(), null);
			usuarioRepository.deleteById(id);
			return true;
		}
		return false;
	}
	

}
