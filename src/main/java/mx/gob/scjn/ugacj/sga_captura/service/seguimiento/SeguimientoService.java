package mx.gob.scjn.ugacj.sga_captura.service.seguimiento;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import mx.gob.scjn.ugacj.sga_captura.domain.AsuntoAbordado;
import mx.gob.scjn.ugacj.sga_captura.domain.Sentencia;
import mx.gob.scjn.ugacj.sga_captura.domain.Usuario;
import mx.gob.scjn.ugacj.sga_captura.domain.VTaquigraficaAsuntos;
import mx.gob.scjn.ugacj.sga_captura.domain.VersionTaquigrafica;
import mx.gob.scjn.ugacj.sga_captura.domain.bitacora.JvSnapshots;
import mx.gob.scjn.ugacj.sga_captura.dto.AsuntoDTO;
import mx.gob.scjn.ugacj.sga_captura.dto.ModuloDTO;
import mx.gob.scjn.ugacj.sga_captura.dto.seguimiento.AsuntoSeguimientoDTO;
import mx.gob.scjn.ugacj.sga_captura.dto.seguimiento.FiltroSeguimientoDTO;
import mx.gob.scjn.ugacj.sga_captura.repository.AsuntoAbordadoRepository;
import mx.gob.scjn.ugacj.sga_captura.repository.JvSnapshotsRepository;
import mx.gob.scjn.ugacj.sga_captura.repository.SentenciaRepository;
import mx.gob.scjn.ugacj.sga_captura.repository.UsuarioRepository;
import mx.gob.scjn.ugacj.sga_captura.repository.VTaquigraficaAsuntosRepository;
import mx.gob.scjn.ugacj.sga_captura.repository.VersionTaquigraficaRepository;
import mx.gob.scjn.ugacj.sga_captura.service.migracion.MigracionService;
import mx.gob.scjn.ugacj.sga_captura.service.porcentaje.PorcentajeService;
import mx.gob.scjn.ugacj.sga_captura.service.sentencias.TablasVotacionesService;
import mx.gob.scjn.ugacj.sga_captura.service.usuarios.UsuariosService;
import mx.gob.scjn.ugacj.sga_captura.utils.ConvertFechaFiltros;
import mx.gob.scjn.ugacj.sga_captura.utils.RegexUtils;

@Service
public class SeguimientoService {
	
	@Autowired
    private SentenciaRepository _sentenciaRepository;

    @Autowired
    private TablasVotacionesService _votacionService;

    @Autowired
    private MongoOperations mongoOperations;
    
    @Autowired
	private PorcentajeService porcentajeService;
    
    @Autowired
    MigracionService migracionService;
    
    @Autowired
	UsuariosService usuariosService;
    
    @Autowired
    private UsuarioRepository usuarioRepository;
    
    @Autowired
	private AsuntoAbordadoRepository _asuntoRepository;

	@Autowired
	private VersionTaquigraficaRepository _vtRepository;

	@Autowired
	private VTaquigraficaAsuntosRepository _vtAsuntosRepository;
	
	@Autowired
	JvSnapshotsRepository snapshotsRepository;

	public enum tiposConteos {
		TEMASPROCESALES, TEMASFONDO, CONTEMAS, SINTEMAS
	}
    
	public FiltroSeguimientoDTO getFiltroData(String filtros) {
		FiltroSeguimientoDTO filtroData = null;
		if(filtros!=null) {
			filtroData = new FiltroSeguimientoDTO();
			String subs[] = filtros.split(",");
			for (String sub : subs) {
				String sTempFiltros[] = sub.split(":");
				String filtro = sTempFiltros[0].trim();
				String filtroValores = sTempFiltros[1].trim();
				if(filtro.equals("asuntoAbordado")) filtroData.setAsuntoAbordado(filtroValores);
				else if(filtro.equals("numeroExpediente"))
					filtroData.setNumeroExpediente(filtroValores);
				else if(filtro.equals("secretario"))
					filtroData.setSecretario(usuariosService.getUsuarioByNombre(filtroValores));
				else if(filtro.equals("anioResolucion")) filtroData.setAnio(filtroValores);
			}
		}
		return filtroData;
	}
	
	public AsuntoSeguimientoDTO setBasicData(AsuntoSeguimientoDTO asuntoDTO, AsuntoAbordado asunto) {
		asuntoDTO = new AsuntoSeguimientoDTO();
		asuntoDTO.setTipoAsunto(asunto.getAsuntoAbordado());
		asuntoDTO.setNumExpediente(asunto.getNumeroExpediente());
		asuntoDTO.setAcumulados(asunto.getAcumulados());
		asuntoDTO.setAcumulada(asunto.isAcumulada());
		asuntoDTO.setCa(asunto.isCa());
		return asuntoDTO;
	}
	
	
	public List<String> getListOfAsuntosBySecretarioAsignado(String secretario, String tipoAsunto, String numExpediente, List<String> asuntosList, Query query) {
		query = new Query();
		if(secretario!=null) query.addCriteria(Criteria.where("correo").is(secretario));
   		List<Usuario> listUsuarios = mongoOperations.find(query, Usuario.class);
   		
		for(Usuario user:listUsuarios) {
			for(ModuloDTO modulo:user.getModulos()) {
				for(AsuntoDTO asunto: modulo.getAsuntos()) {
					if(tipoAsunto!=null&&asunto.getTipoAsunto()!=null) {
						if(numExpediente!=null&&asunto.getNumExpediente()!=null) {
							if(asunto.getTipoAsunto().equals(tipoAsunto)&&asunto.getNumExpediente().equals(numExpediente)) {
								if(!asuntosList.contains(asunto.getTipoAsunto()+" : "+asunto.getNumExpediente())&&
										asunto.getTipoAsunto()!=null&&asunto.getNumExpediente()!=null) 
										asuntosList.add(asunto.getTipoAsunto()+" : "+asunto.getNumExpediente());
							}
						}else {
							if(asunto.getTipoAsunto().equals(tipoAsunto)) {
								if(!asuntosList.contains(asunto.getTipoAsunto()+" : "+asunto.getNumExpediente())&&
										asunto.getTipoAsunto()!=null&&asunto.getNumExpediente()!=null) 
										asuntosList.add(asunto.getTipoAsunto()+" : "+asunto.getNumExpediente());
							}
						}
					}else if(numExpediente!=null&&asunto.getNumExpediente()!=null) {
						if(asunto.getNumExpediente().equals(numExpediente)) {
							if(!asuntosList.contains(asunto.getTipoAsunto()+" : "+asunto.getNumExpediente())) 
									asuntosList.add(asunto.getTipoAsunto()+" : "+asunto.getNumExpediente());
						}
					}else {
						if(!asuntosList.contains(asunto.getTipoAsunto()+" : "+asunto.getNumExpediente())&&
								asunto.getTipoAsunto()!=null&&asunto.getNumExpediente()!=null) 
								asuntosList.add(asunto.getTipoAsunto()+" : "+asunto.getNumExpediente());
					}
					
				}
			}
		}
		return asuntosList;
	}
	
	public List<String> getListOfAsuntosBySecretarioEditorAndAsuntoOrNumExp(String secretario, String tipoAsunto, String numExpediente, List<String> asuntosList, Query query) {
		query = new Query();
		if(tipoAsunto!=null) query.addCriteria(Criteria.where("state.asunto.asuntoAbordado").is(tipoAsunto));
		if(numExpediente!=null) query.addCriteria(Criteria.where("state.asunto.numeroExpediente").is(numExpediente));
		
		query.addCriteria(Criteria.where("commitMetaData.author").is(secretario));
		query.addCriteria(Criteria.where("seccion").is("Asuntos Abordados"));
		List<JvSnapshots> bitacoraList = mongoOperations.find(query, JvSnapshots.class);
		for(JvSnapshots bitacora:bitacoraList) {
			String jsonInString = new Gson().toJson(bitacora.getState());
			try {
				JSONObject mJSONObject = new JSONObject(jsonInString);
				AsuntoAbordado asuntoAbordado = new Gson().fromJson(mJSONObject.getString("asunto").toString(), AsuntoAbordado.class);
				if(!asuntosList.contains(asuntoAbordado.getAsuntoAbordado()+" : "+asuntoAbordado.getNumeroExpediente())) 
					asuntosList.add(asuntoAbordado.getAsuntoAbordado()+" : "+asuntoAbordado.getNumeroExpediente());		
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return asuntosList;
	}
	
	public List<String> getAsuntosByAsuntoAndNumeroExpAndAnioS(String tipoAsunto, String numExpediente, String anio, List<String> asuntosList, Query query){
		query = new Query();
		List<String> nuevaLista = new ArrayList<String>();
		if(tipoAsunto!=null) query.addCriteria(Criteria.where("tipoAsunto").is(tipoAsunto));
		if(numExpediente!=null) query.addCriteria(Criteria.where("numeroExpediente").is(numExpediente));
		if(anio!=null) query.addCriteria(Criteria.where("fechaResolucion").regex(".*"+anio+".*"));
		
		List<Sentencia> sentenciasList = mongoOperations.find(query, Sentencia.class);
		for(Sentencia sent : sentenciasList) {
			String cadenaSent = sent.getTipoAsunto()+ " : "+ sent.getNumeroExpediente(); 
			if(asuntosList.contains(cadenaSent)) nuevaLista.add(cadenaSent);
		}
		asuntosList.clear();
		asuntosList.addAll(nuevaLista);
		return asuntosList;
	}
	
	public List<String> getAsuntosByAsuntoAndNumeroExpAndAnio(String tipoAsunto, String numExpediente, String anio, Query query){
		query = new Query();
		List<String> nuevaLista = new ArrayList<String>();
		if(tipoAsunto!=null) query.addCriteria(Criteria.where("tipoAsunto").is(tipoAsunto));
		if(numExpediente!=null) query.addCriteria(Criteria.where("numeroExpediente").is(numExpediente));
		if(anio!=null) query.addCriteria(Criteria.where("fechaResolucion").regex(".*"+anio+".*"));
		
		List<Sentencia> sentenciasList = mongoOperations.find(query, Sentencia.class);
		for(Sentencia sent : sentenciasList) {
			String cadenaSent = sent.getTipoAsunto()+ " : "+ sent.getNumeroExpediente();
			nuevaLista.add(cadenaSent);
			//System.out.println(cadenaSent);
		}
		
		return nuevaLista;
	}
	
	
	public List<AsuntoAbordado> getListOfAsuntosByAsuntoAndNumExp(List<String> asuntosList, Query query) {
		List<AsuntoAbordado> asuntosListAll = new ArrayList<AsuntoAbordado>();
		for(String asunto:asuntosList) {
			String[] asuntoArr = asunto.split(":");
			query = new Query();
			query.addCriteria(Criteria.where("asuntoAbordado").is(asuntoArr[0].trim()));
	   		query.addCriteria(Criteria.where("numeroExpediente").is(asuntoArr[1].trim()));
	   		List<AsuntoAbordado> asuntoAbordado = mongoOperations.find(query, AsuntoAbordado.class);
	   		if(asuntoAbordado.size()>0) {
	   			asuntosListAll.add(asuntoAbordado.get(0));
	   		}else {
	   			System.out.println("Dont exist "+asuntoArr[0].trim()+" "+asuntoArr[1].trim());
	   		}
		}
		return asuntosListAll;
	}
	
	public List<String> getSecretariosAsignadosByAsuntoOrNumEXpediente(String asunto, String numExpediente, List<String> usersNotIn, Query query) {
		query = new Query();
		List<String> userNames = new ArrayList<String>();

		if(asunto!=null&&numExpediente!=null) {
			query.addCriteria(Criteria.where("modulos.asuntos.tipoAsunto").is(asunto.toUpperCase())
					.and("modulos.asuntos.numExpediente").is(numExpediente));
		}else if(asunto!=null) {
			query.addCriteria(Criteria.where("modulos.asuntos.tipoAsunto").is(asunto.toUpperCase()));
			query.addCriteria(Criteria.where("modulos.asuntos.numExpediente").is(numExpediente));
		}
		
		 List<Usuario> listUsuarios = mongoOperations.find(query, Usuario.class);
		 
		 for(Usuario user:listUsuarios) {
				for(ModuloDTO modulo:user.getModulos()) {
					for(AsuntoDTO asuntoAbordadi: modulo.getAsuntos()) {
						if(asuntoAbordadi.getTipoAsunto()!=null&&asuntoAbordadi.getNumExpediente()!=null) {
							if(asuntoAbordadi.getTipoAsunto().equals(asunto)&&asuntoAbordadi.getNumExpediente().equals(numExpediente)) {
								if(!usersNotIn.contains(user.getCorreo())) {
									if(!userNames.contains(user.getCorreo())) userNames.add(user.getCorreo());
								}
									
							}
						}
					}
				}
		 }
		 
		if(userNames.size()<=0) userNames.add("Sin asignación");
		return userNames;
	}
	
	public List<String> getSecretariosEditoresByAsuntoOrNumEXpediente(String asunto, String numExpediente, List<String> usersNotIn, Query query, String secretario) {
		query = new Query();
		List<String> userNames = new ArrayList<String>();
		query.addCriteria(Criteria.where("commitMetaData.author").ne(usersNotIn));
		query.addCriteria(Criteria.where("state.asunto.asuntoAbordado").is(asunto.toUpperCase()));
		query.addCriteria(Criteria.where("state.asunto.numeroExpediente").is(numExpediente));
		List<JvSnapshots> bitacoraList = mongoOperations.find(query, JvSnapshots.class);
		for(JvSnapshots bitacora:bitacoraList) {
			if(secretario!=null) {
				if(bitacora.getCommitMetaData().getAuthor().equals(secretario)) userNames.add(bitacora.getCommitMetaData().getAuthor());
			}else {
				if(!usersNotIn.contains(bitacora.getCommitMetaData().getAuthor())) {
					if(!userNames.contains(bitacora.getCommitMetaData().getAuthor())) {
						userNames.add(bitacora.getCommitMetaData().getAuthor());
					}
				}
			}
		}

		if(userNames.size()<=0) userNames.add("Sin edición");
		return userNames;
	}
	
	public String getFechaTema(String idAsunto, String numeroExpediente, String asuntoAbordado) {
		
		if(idAsunto.equals("62c32589d730222611bb4387")) {
//			System.out.println(idAsunto);
		}
		
		Query query = new Query();
		query.addCriteria(Criteria.where("state.asunto.numeroExpediente").is(numeroExpediente));
		query.addCriteria(Criteria.where("state.asunto.asuntoAbordado").is(asuntoAbordado));
		query.addCriteria(Criteria.where("type").is("Update"));
		query.addCriteria(Criteria.where("seccion").is("Asuntos Abordados"));
		List<JvSnapshots> bitacoraList = mongoOperations.find(query, JvSnapshots.class);
		if(bitacoraList.size()>=1) {
			for(Object snapshot: bitacoraList) {
				String jsonInString = new Gson().toJson(snapshot);
				JSONObject mJSONObject = null;
				try {
					mJSONObject = new JSONObject(jsonInString);
					String date = (String) mJSONObject.getJSONObject("commitMetaData").get("commitDate");
					String autor = (String) mJSONObject.getJSONObject("commitMetaData").get("author");
					if(!autor.equals("ggarciab")) {
						String[] parts = date.split(",");
						date = parts[1];
						parts = date.trim().split(" ");
						date = parts[0];
						return date;
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
//		List<JvSnapshots> listSnapshots = snapshotsRepository.findByTypeAndSeccion("Update", "Asuntos Abordados");
		return "";
	}
	
	public AsuntoSeguimientoDTO getSesionesTemasInformeTematicas(String asuntoId, AsuntoSeguimientoDTO asuntoDTO) {
		//Sesiones
    	List<String> sesionesList = new ArrayList<String>();
    	List<String> temasFList = new ArrayList<String>();
    	List<String> temasPList = new ArrayList<String>();
    	List<String> vtaqIdsTT = new ArrayList<String>();
		List<VTaquigraficaAsuntos> listTT = _vtAsuntosRepository.findByIdAsunto(asuntoId);
		vtaqIdsTT = new ArrayList<String>();
		for(VTaquigraficaAsuntos tt:listTT) {
			vtaqIdsTT.add(tt.getIdVtaquigrafica());
			temasFList.addAll(tt.getTemasFondo());
			temasPList.addAll(tt.getTemasProcesales());
		}
		
		if(temasFList.size()>0||temasPList.size()>0) {
			asuntoDTO.setSesiones(sesionesList);
			asuntoDTO.setTemasFondo(temasFList);
			asuntoDTO.setTemasProcesales(temasPList);
			
			return asuntoDTO;
		}else {
//			System.out.println("NO");
		}
		return null;
		
	}
	public AsuntoSeguimientoDTO getSesionesTemas(String asuntoId, AsuntoSeguimientoDTO asuntoDTO) {
		//Sesiones
    	List<String> sesionesList = new ArrayList<String>();
    	List<String> temasFList = new ArrayList<String>();
    	List<String> temasPList = new ArrayList<String>();
    	List<String> vtaqIdsTT = new ArrayList<String>();
		List<VTaquigraficaAsuntos> listTT = _vtAsuntosRepository.findByIdAsunto(asuntoId);
		vtaqIdsTT = new ArrayList<String>();
		for(VTaquigraficaAsuntos tt:listTT) {
			vtaqIdsTT.add(tt.getIdVtaquigrafica());
			temasFList.addAll(tt.getTemasFondo());
			temasPList.addAll(tt.getTemasProcesales());
		}
		if(vtaqIdsTT.size()>0) {
			List<VersionTaquigrafica> vtaqList = (List<VersionTaquigrafica>) _vtRepository.findAllById(vtaqIdsTT);
			for(VersionTaquigrafica vtaq:vtaqList) {
				sesionesList.add(ConvertFechaFiltros.changeFormat(vtaq.getFechaSesion(), RegexUtils.REGEX_DDMMYYYY));
			}
		}
		asuntoDTO.setSesiones(sesionesList);
		asuntoDTO.setTemasFondo(temasFList);
		asuntoDTO.setTemasProcesales(temasPList);
		
		return asuntoDTO;
	}
	
	
	
	public byte[] makeExcel(List<String> asunto, List<String> temasp, List<String> temasf,
		List<Integer> anio, List<String> asuntosConTemas, List<Integer> aniosConTemas,
		List<String> asuntosSinTemas, List<Integer> aniosSinTemas, Map<String, Map<String, Integer>> conteoAnios,
		Integer anioMax, Integer anioMin) throws IOException{
		Workbook workbook = new XSSFWorkbook();

		CellStyle headerStyle = workbook.createCellStyle();
		headerStyle.setFillForegroundColor(IndexedColors.DARK_BLUE.getIndex());
		headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		
		XSSFFont font = ((XSSFWorkbook) workbook).createFont();
		font.setFontName("Arial");
		font.setFontHeightInPoints((short) 12);
		font.setColor(IndexedColors.WHITE.getIndex());
		font.setBold(true);
		headerStyle.setFont(font);
		
		CellStyle style = workbook.createCellStyle();
		style.setWrapText(true);
		
		
		Sheet sheet4 = workbook.createSheet("Presentación");
		sheet4.setColumnWidth(0, 15000);
		
		Integer cantidadAnios = anioMax - anioMin;
		
		for(int i=0; i<=cantidadAnios; i++) {
			sheet4.setColumnWidth(i+1, 3000);
		}
		
		
		Row header4 = sheet4.createRow(0);

		Cell headerCell4 = header4.createCell(0);
		headerCell4.setCellValue("Tipo de tema");
		headerCell4.setCellStyle(headerStyle);
		
		for(int i=0; i<=cantidadAnios; i++) {
			headerCell4 = header4.createCell(i+1);
			headerCell4.setCellValue("Año "+ (anioMin + i));
			headerCell4.setCellStyle(headerStyle);
		}
		
		headerCell4 = header4.createCell(cantidadAnios+2);
		headerCell4.setCellValue("Total");
		headerCell4.setCellStyle(headerStyle);		
		
		Map<String, Integer> temasProc = conteoAnios.get(tiposConteos.TEMASPROCESALES.name());
		Map<String, Integer> temasFondo = conteoAnios.get(tiposConteos.TEMASFONDO.name());
		
		Row rowProce = sheet4.createRow(1);
		
		Cell cellProc = rowProce.createCell(0);
		cellProc.setCellValue("Temas Procesales");
		cellProc.setCellStyle(style);
		
		Integer conteoProcesales = 0;
		for(int i=0; i<=cantidadAnios;i++) {
				
			Integer conteoAnio = temasProc.get(Integer.toString(anioMin+i));
			if(conteoAnio == null) conteoAnio = 0;
			conteoProcesales += conteoAnio;
			cellProc = rowProce.createCell(1+i);
			cellProc.setCellValue(conteoAnio);
			cellProc.setCellStyle(style);
		}
		
		cellProc = rowProce.createCell(cantidadAnios+2);
		cellProc.setCellValue(conteoProcesales);
		cellProc.setCellStyle(style);
		
		Row rowFondo = sheet4.createRow(2);
		
		Cell cellFondo = rowFondo.createCell(0);
		cellFondo.setCellValue("Temas Fondo");
		cellFondo.setCellStyle(style);
		
		Integer conteoFondo = 0;
		for(int i=0; i<=cantidadAnios;i++) {
				
			Integer conteoAnio = temasFondo.get(Integer.toString(anioMin+i));
			if(conteoAnio == null) conteoAnio = 0;
			conteoFondo += conteoAnio;
			cellFondo = rowFondo.createCell(1+i);
			cellFondo.setCellValue(conteoAnio);
			cellFondo.setCellStyle(style);
		}
		cellFondo = rowFondo.createCell(cantidadAnios+2);
		cellFondo.setCellValue(conteoFondo);
		cellFondo.setCellStyle(style);
		
		Row rowTotalTemas = sheet4.createRow(3);
		
		Cell cellTotalTemas = rowTotalTemas.createCell(0);
		cellTotalTemas.setCellValue("Total");
		cellTotalTemas.setCellStyle(style);
		
		for(int i=0; i<=cantidadAnios;i++) {
				
			Integer conteoAnioP = temasProc.get(Integer.toString(anioMin+i));
			Integer conteoAnioF = temasFondo.get(Integer.toString(anioMin+i));
			if(conteoAnioP == null) conteoAnioP = 0;
			if(conteoAnioF == null) conteoAnioF = 0;
			Integer conteoAnio = conteoAnioP + conteoAnioF;
			cellTotalTemas = rowTotalTemas.createCell(1+i);
			cellTotalTemas.setCellValue(conteoAnio);
			cellTotalTemas.setCellStyle(style);
		}
		cellTotalTemas = rowTotalTemas.createCell(cantidadAnios+2);
		cellTotalTemas.setCellValue(conteoFondo + conteoProcesales);
		cellTotalTemas.setCellStyle(headerStyle);
		
		
		
		Row header5 = sheet4.createRow(5);

		Cell headerCell5 = header5.createCell(0);
		headerCell5.setCellValue("Asunto");
		headerCell5.setCellStyle(headerStyle);
		
		for(int i=0; i<=cantidadAnios; i++) {
			headerCell5 = header5.createCell(i+1);
			headerCell5.setCellValue("Año "+ (anioMin + i));
			headerCell5.setCellStyle(headerStyle);
		}
		
		headerCell5 = header5.createCell(cantidadAnios+2);
		headerCell5.setCellValue("Total");
		headerCell5.setCellStyle(headerStyle);
		
		CellStyle style5 = workbook.createCellStyle();
		style5.setWrapText(true);
		
		Map<String, Integer> conTemas = conteoAnios.get(tiposConteos.CONTEMAS.name());
		Map<String, Integer> sinTemas = conteoAnios.get(tiposConteos.SINTEMAS.name());
		
		Row rowConTemas = sheet4.createRow(6);
		
		Cell cellConTemas = rowConTemas.createCell(0);
		cellConTemas.setCellValue("Asuntos con temas");
		cellConTemas.setCellStyle(style);
		
		Integer conteoConTemas = 0;
		for(int i=0; i<=cantidadAnios;i++) {
				
			Integer conteoAnio = conTemas.get(Integer.toString(anioMin+i));
			if(conteoAnio == null) conteoAnio = 0;
			conteoConTemas += conteoAnio;
			cellConTemas = rowConTemas.createCell(1+i);
			cellConTemas.setCellValue(conteoAnio);
			cellConTemas.setCellStyle(style);
		}
		
		cellConTemas = rowConTemas.createCell(cantidadAnios+2);
		cellConTemas.setCellValue(conteoConTemas);
		cellConTemas.setCellStyle(style);
		
		Row rowSinTemas = sheet4.createRow(7);
		
		Cell cellSinTemas = rowSinTemas.createCell(0);
		cellSinTemas.setCellValue("Asuntos sin temas");
		cellSinTemas.setCellStyle(style);
		
		Integer conteoSinTemas = 0;
		for(int i=0; i<=cantidadAnios;i++) {
				
			Integer conteoAnio = sinTemas.get(Integer.toString(anioMin+i));
			if(conteoAnio == null) conteoAnio = 0;
			conteoSinTemas += conteoAnio;
			cellSinTemas = rowSinTemas.createCell(1+i);
			cellSinTemas.setCellValue(conteoAnio);
			cellSinTemas.setCellStyle(style);
		}
		cellSinTemas = rowSinTemas.createCell(cantidadAnios+2);
		cellSinTemas.setCellValue(conteoSinTemas);
		cellSinTemas.setCellStyle(style);
		
		Row rowTotalAsuntos = sheet4.createRow(8);
		
		Cell cellTotalAsuntos = rowTotalAsuntos.createCell(0);
		cellTotalAsuntos.setCellValue("Total");
		cellTotalAsuntos.setCellStyle(style);
		
		for(int i=0; i<=cantidadAnios;i++) {
				
			Integer conteoAnioC = conTemas.get(Integer.toString(anioMin+i));
			Integer conteoAnioS = sinTemas.get(Integer.toString(anioMin+i));
			if(conteoAnioC == null) conteoAnioC = 0;
			if(conteoAnioS == null) conteoAnioS = 0;
			Integer conteoAnio = conteoAnioC + conteoAnioS;
			cellTotalAsuntos = rowTotalAsuntos.createCell(1+i);
			cellTotalAsuntos.setCellValue(conteoAnio);
			cellTotalAsuntos.setCellStyle(style);
		}
		cellTotalAsuntos = rowTotalAsuntos.createCell(cantidadAnios+2);
		cellTotalAsuntos.setCellValue(conteoConTemas + conteoSinTemas);
		cellTotalAsuntos.setCellStyle(headerStyle);
		
		Row rowFecha = sheet4.createRow(15);
		
		String patronFecha = "dd/MM/yyyy HH:mm:ss";
		SimpleDateFormat simpleDateFormat =new SimpleDateFormat(patronFecha);
		String fechaAct = simpleDateFormat.format(new Date());
		
		Cell cellFecha = rowFecha.createCell(0);
		cellFecha.setCellValue("Fecha de actualización:");
		cellFecha.setCellStyle(style);
		Cell cellFecha2 = rowFecha.createCell(1);
		cellFecha2.setCellValue(fechaAct);
		cellFecha2.setCellStyle(style);

		
		

		Sheet sheet = workbook.createSheet("Asuntos - Temas");
		sheet.setColumnWidth(0, 6000);
		sheet.setColumnWidth(1, 15000);
		sheet.setColumnWidth(2, 15000);
		sheet.setColumnWidth(3, 6000);
		
		Row header = sheet.createRow(0);

		Cell headerCell = header.createCell(0);
		headerCell.setCellValue("Asunto");
		headerCell.setCellStyle(headerStyle);

		headerCell = header.createCell(1);
		headerCell.setCellValue("Tema Procesal");
		headerCell.setCellStyle(headerStyle);
		
		headerCell = header.createCell(2);
		headerCell.setCellValue("Tema Fondo");
		headerCell.setCellStyle(headerStyle);
		
		headerCell = header.createCell(3);
		headerCell.setCellValue("Año");
		headerCell.setCellStyle(headerStyle);
		
		for(int i=0; i<temasp.size();i++) {
			Row row = sheet.createRow(i+1);
			
			Cell cell = row.createCell(0);
			cell.setCellValue(asunto.get(i));
			cell.setCellStyle(style);

			cell = row.createCell(1);
			cell.setCellValue(temasp.get(i));
			cell.setCellStyle(style);
			
			cell = row.createCell(2);
			cell.setCellValue(temasf.get(i));
			cell.setCellStyle(style);
			
			cell = row.createCell(3);
			cell.setCellValue(anio.get(i));
			cell.setCellStyle(style);
		}
		
		Sheet sheet2 = workbook.createSheet("Asuntos con Temas");
		sheet2.setColumnWidth(0, 15000);
		sheet2.setColumnWidth(1, 6000);
		
		Row header2 = sheet2.createRow(0);

		Cell headerCell2 = header2.createCell(0);
		headerCell2.setCellValue("Asunto");
		headerCell2.setCellStyle(headerStyle);

		headerCell2 = header2.createCell(1);
		headerCell2.setCellValue("Año");
		headerCell2.setCellStyle(headerStyle);
		
		for(int i=0; i<asuntosConTemas.size();i++) {
			Row row = sheet2.createRow(i+1);
			
			Cell cell = row.createCell(0);
			cell.setCellValue(asuntosConTemas.get(i));
			cell.setCellStyle(style);

			cell = row.createCell(1);
			cell.setCellValue(aniosConTemas.get(i));
			cell.setCellStyle(style);
		}
		
		Sheet sheet3 = workbook.createSheet("Asuntos sin Temas");
		sheet3.setColumnWidth(0, 15000);
		sheet3.setColumnWidth(1, 6000);
		
		Row header3 = sheet3.createRow(0);

		Cell headerCell3 = header3.createCell(0);
		headerCell3.setCellValue("Asunto");
		headerCell3.setCellStyle(headerStyle);

		headerCell3 = header3.createCell(1);
		headerCell3.setCellValue("Año");
		headerCell3.setCellStyle(headerStyle);
		
		for(int i=0; i<asuntosSinTemas.size();i++) {
			Row row = sheet3.createRow(i+1);
			
			Cell cell = row.createCell(0);
			cell.setCellValue(asuntosSinTemas.get(i));
			cell.setCellStyle(style);

			cell = row.createCell(1);
			cell.setCellValue(aniosSinTemas.get(i));
			cell.setCellStyle(style);
		}
		
		
		Sheet sheet6 = workbook.createSheet("Todos Asuntos");
		sheet6.setColumnWidth(0, 15000);
		sheet6.setColumnWidth(1, 6000);
		
		Row header6 = sheet6.createRow(0);

		Cell headerCell6 = header6.createCell(0);
		headerCell6.setCellValue("Asunto");
		headerCell6.setCellStyle(headerStyle);

		headerCell6 = header6.createCell(1);
		headerCell6.setCellValue("Año");
		headerCell6.setCellStyle(headerStyle);
		
		for(int i=0; i<asuntosConTemas.size();i++) {
			Row row = sheet6.createRow(i+1);
			
			Cell cell = row.createCell(0);
			cell.setCellValue(asuntosConTemas.get(i));
			cell.setCellStyle(style);

			cell = row.createCell(1);
			cell.setCellValue(aniosConTemas.get(i));
			cell.setCellStyle(style);
		}
		
		for(int i=0; i<asuntosSinTemas.size();i++) {
			Row row = sheet6.createRow(asuntosConTemas.size()+i+1);
			
			Cell cell = row.createCell(0);
			cell.setCellValue(asuntosSinTemas.get(i));
			cell.setCellStyle(style);

			cell = row.createCell(1);
			cell.setCellValue(aniosSinTemas.get(i));
			cell.setCellStyle(style);
		}
		
		
		
		//File currDir = new File(".");
		//String path = "D:\\SCJN\\tempAsuntoTemas.xlsx";
		//String fileLocation = path;

		//FileOutputStream outputStream = new FileOutputStream(fileLocation);
		//workbook.write(outputStream);
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try {
			workbook.write(bos);
			return bos.toByteArray();
		} catch (Exception e){
			return null;
		} finally {
			workbook.close();
		}
		//workbook.close();
		//System.out.println("Ce fini");
		
	}
	
	public void makeExcelAsunto(List<String> asunto, List<String> anios) throws IOException{
		Workbook workbook = new XSSFWorkbook();

		Sheet sheet = workbook.createSheet("Asuntos con Temas");
		sheet.setColumnWidth(0, 15000);
		sheet.setColumnWidth(1, 6000);
		
		Row header = sheet.createRow(0);
		
		CellStyle headerStyle = workbook.createCellStyle();
		headerStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
		headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		
		XSSFFont font = ((XSSFWorkbook) workbook).createFont();
		font.setFontName("Arial");
		font.setFontHeightInPoints((short) 16);
		font.setBold(true);
		headerStyle.setFont(font);

		Cell headerCell = header.createCell(0);
		headerCell.setCellValue("Asunto");
		headerCell.setCellStyle(headerStyle);

		headerCell = header.createCell(1);
		headerCell.setCellValue("Año");
		headerCell.setCellStyle(headerStyle);
		
		CellStyle style = workbook.createCellStyle();
		style.setWrapText(true);
		
		for(int i=0; i<asunto.size();i++) {
			Row row = sheet.createRow(i);
			
			Cell cell = row.createCell(0);
			cell.setCellValue(asunto.get(i));
			cell.setCellStyle(style);

			cell = row.createCell(1);
			cell.setCellValue(anios.get(i));
			cell.setCellStyle(style);
		}
		
		File currDir = new File(".");
		String path = "D:\\SCJN\\tempAsuntoAnio.xlsx";
		String fileLocation = path;

		FileOutputStream outputStream = new FileOutputStream(fileLocation);
		workbook.write(outputStream);
		workbook.close();
		System.out.println("Ce fini");
		
	}
	
	String getFechaResolucion(String asunto, String numExpediente) {
		Sentencia sentencia = _sentenciaRepository.findByTipoAsuntoAndNumeroExpediente(asunto, numExpediente);
		if(sentencia!=null) {
			String[] parts = sentencia.getFechaResolucion().split("-");
			String anio = parts[0];
			return anio;
		}
		return null;
	}
	
	public void getAllAsuntos() {
		Query query = new Query();
		List<AsuntoAbordado> asuntosList = mongoOperations.find(query, AsuntoAbordado.class);
		System.out.println("total: "+asuntosList.size());
		for(AsuntoAbordado asunto:asuntosList) {
			String asuntoST = asunto.getAsuntoAbordado()+" "+asunto.getNumeroExpediente();
			System.out.println(asuntoST);
		}
	}
	
	public void getAllAsuntosSentencias() {
		Query query = new Query();
		List<Sentencia> sentenciasList = mongoOperations.find(query, Sentencia.class);
		System.out.println("total: "+sentenciasList.size());
		for(Sentencia sentenciaO:sentenciasList) {
			String asuntoST = sentenciaO.getTipoAsunto() + " " + sentenciaO.getNumeroExpediente();
			System.out.println(asuntoST);
		}
	}
	
	public byte[] informeTematicasAsuntos() throws ParseException {
		Query query = new Query();
		AsuntoSeguimientoDTO asuntoDTO = null;
		List<AsuntoSeguimientoDTO> asuntosListDTO = new ArrayList<AsuntoSeguimientoDTO>();
		List<AsuntoAbordado> asuntosList = mongoOperations.find(query, AsuntoAbordado.class);
		//List<AsuntoAbordado> asuntosList2 = _asuntoRepository.findAll();
		//int pos=0;
		
		List<String> asuntosL = new ArrayList<String>();
		List<Integer> aniosL = new ArrayList<Integer>();
		
		List<String> asuntosSinTemas = new ArrayList<String>();
		List<Integer> aniosSinTemas = new ArrayList<Integer>();
		
		Map<String, Map<String, Integer>> conteoAnios = new HashMap<>();
		Integer anioMax = 0;
		Integer anioMin = 0;
		
		for(AsuntoAbordado asunto:asuntosList) {
			//pos++;
			String asuntoST = asunto.getAsuntoAbordado()+" "+asunto.getNumeroExpediente();
//			System.out.println(asunto.getNumeroExpediente());
//			if(asunto.getNumeroExpediente().equals("28/2022")) {
//				System.out.println("here");
//			}
//			System.out.println(""+asunto.getAsuntoAbordado()+" "+asunto.getNumeroExpediente());
			asuntoDTO = setBasicData(asuntoDTO, asunto);
			asuntoDTO = getSesionesTemasInformeTematicas(asunto.getId(), asuntoDTO);
			
			//Fecha de captura
//			String anio = getFechaTema(asunto.getId(), asunto.getNumeroExpediente(), asunto.getAsuntoAbordado());
//			if(anio.equals("")) {
//				anio = anio = migracionService.getDateAsunto(asunto.getNumeroExpediente(), asunto.getAsuntoAbordado());
//				if(anio.equals("")) {
//					anio = "2021";
//				}
//			}
			
			String anio = getFechaResolucion(asunto.getAsuntoAbordado(), asunto.getNumeroExpediente());
			if(anio==null) {
				List<VTaquigraficaAsuntos> vTaqAs = _vtAsuntosRepository.findByIdAsunto(asunto.getId());
				Integer anioInt = 0;
				for(VTaquigraficaAsuntos vTA : vTaqAs) {
					Optional<VersionTaquigrafica> versionTaq = _vtRepository.findById(vTA.getIdVtaquigrafica());
					if(versionTaq.isPresent()) {
						String[] parts = versionTaq.get().getFechaSesion().split("-");
						if(anioInt < Integer.parseInt(parts[0])) anioInt = Integer.parseInt(parts[0]);
					}
				}
				if(!anioInt.equals(0)) {
					anio = anioInt.toString();
				}else {
					System.out.println("SIN FECHA RESOLUCIÓN: "+asuntoST);
					anio = "";
				}
			}

			
			if(asuntoDTO!=null) {
				asuntoDTO.setAnioResolucion(anio);
				asuntosListDTO.add(asuntoDTO);
				
				asuntosL.add(asunto.getAsuntoAbordado()+" "+asunto.getNumeroExpediente());
				aniosL.add(Integer.parseInt(anio));
				
				// Conteos para pestaña de presentación
				if(!anio.equals("")) {
					boolean conTemas = false;
					if(asuntoDTO.getTemasFondo() != null && !asuntoDTO.getTemasFondo().isEmpty()) {
						Map<String, Integer> conteosAnt = conteoAnios.get(tiposConteos.TEMASFONDO.name());
						if (conteosAnt == null)  conteosAnt = new HashMap<>();
						Integer conteoAnio = conteosAnt.get(anio);
						if(conteoAnio == null)	conteoAnio = 0;
						
						Integer conteoTF = 0;
						for(int i = 0; i < asuntoDTO.getTemasFondo().size(); i++) {
							if(asuntoDTO.getTemasFondo().get(i) != null && !asuntoDTO.getTemasFondo().get(i).trim().equals("")) {
								conteoTF++;
							}
						}
						if(conteoTF > 0) {
							conteosAnt.put(anio, (conteoAnio == null) ? conteoTF : conteoAnio + conteoTF);
							conTemas = true;
						}
						
						conteoAnios.put(tiposConteos.TEMASFONDO.name(), conteosAnt);
					}
					
					if(asuntoDTO.getTemasProcesales() != null && !asuntoDTO.getTemasProcesales().isEmpty()) {
						Map<String, Integer> conteosAnt = conteoAnios.get(tiposConteos.TEMASPROCESALES.name());
						if (conteosAnt == null)  conteosAnt = new HashMap<>();
						Integer conteoAnio = conteosAnt.get(anio);
						if(conteoAnio == null)	conteoAnio = 0;
						
						Integer conteoTP = 0;
						for(int i = 0; i < asuntoDTO.getTemasProcesales().size(); i++) {
							if(asuntoDTO.getTemasProcesales().get(i) != null && !asuntoDTO.getTemasProcesales().get(i).trim().equals("")) {
								conteoTP++;
							}
						}
						if(conteoTP > 0) {
							conteosAnt.put(anio, (conteoAnio == null) ? conteoTP : conteoAnio + conteoTP);
							conTemas = true;
						}
						
						conteoAnios.put(tiposConteos.TEMASPROCESALES.name(), conteosAnt);
					}
					
					if(conTemas) {
						Map<String, Integer> conteosAnt = conteoAnios.get(tiposConteos.CONTEMAS.name());
						if (conteosAnt == null)  conteosAnt = new HashMap<>();
						Integer conteoAnio = conteosAnt.get(anio);
						conteosAnt.put(anio, (conteoAnio == null) ? 1 : conteoAnio + 1);
						conteoAnios.put(tiposConteos.CONTEMAS.name(), conteosAnt);
						if(anioMin == 0) anioMin = Integer.parseInt(anio);
						if(anioMin > Integer.parseInt(anio)) anioMin = Integer.parseInt(anio);
						if(anioMax < Integer.parseInt(anio)) anioMax = Integer.parseInt(anio);
					}else {
						Map<String, Integer> conteosAnt = conteoAnios.get(tiposConteos.SINTEMAS.name());
						if (conteosAnt == null)  conteosAnt = new HashMap<>();
						Integer conteoAnio = conteosAnt.get(anio);
						conteosAnt.put(anio, (conteoAnio == null) ? 1 : conteoAnio + 1);
						conteoAnios.put(tiposConteos.SINTEMAS.name(), conteosAnt);
						if(anioMin == 0) anioMin = Integer.parseInt(anio);
						if(anioMin > Integer.parseInt(anio)) anioMin = Integer.parseInt(anio);
						if(anioMax < Integer.parseInt(anio)) anioMax = Integer.parseInt(anio);
					}
				}
			}else {
				//System.out.println("SIN TEMA: "+asuntoST+" - "+anio);
				
				if(!anio.equals("")) {
					Map<String, Integer> conteosAnt = conteoAnios.get(tiposConteos.SINTEMAS.name());
					if (conteosAnt == null)  conteosAnt = new HashMap<>();
					Integer conteoAnio = conteosAnt.get(anio);
					conteosAnt.put(anio, (conteoAnio == null) ? 1 : conteoAnio + 1);
					conteoAnios.put(tiposConteos.SINTEMAS.name(), conteosAnt);
					if(anioMin == 0) anioMin = Integer.parseInt(anio);
					if(anioMin > Integer.parseInt(anio)) anioMin = Integer.parseInt(anio);
					if(anioMax < Integer.parseInt(anio)) anioMax = Integer.parseInt(anio);
				}
				
				asuntosSinTemas.add(asunto.getAsuntoAbordado()+" "+asunto.getNumeroExpediente());
				aniosSinTemas.add(Integer.parseInt(anio));
			}
		}
		List<String> datosCSV = null;
		List<String> allRows = new ArrayList<String>();
//		int rowPos=1;
		
		List<String> tipoAsunto = new ArrayList<String>();
		List<Integer> anio = new ArrayList<Integer>();
		List<String> temap = new ArrayList<String>();
		List<String> temaf = new ArrayList<String>();
		
		for(AsuntoSeguimientoDTO asunto:asuntosListDTO) {
			datosCSV = new ArrayList<String>();
			String asuntoNumExp =asunto.getTipoAsunto()+" "+asunto.getNumExpediente();
			datosCSV.add(asuntoNumExp);
			int dif=0;
			if(asunto.getTemasProcesales().size()>asunto.getTemasFondo().size()) {
				dif = asunto.getTemasProcesales().size()-asunto.getTemasFondo().size();
				for(int x=0; x<dif;x++) {
					asunto.getTemasFondo().add("");
				}
			}else {
				dif = asunto.getTemasFondo().size()-asunto.getTemasProcesales().size();
				for(int x=0; x<dif;x++) {
					asunto.getTemasProcesales().add("");
				}
			}
			
			for(int i=0; i<asunto.getTemasProcesales().size();i++) {
				tipoAsunto.add(asuntoNumExp);
				anio.add(Integer.parseInt(asunto.getAnioResolucion()));
				temap.add(asunto.getTemasProcesales().get(i));
				temaf.add(asunto.getTemasFondo().get(i));
			}
		}
		
		try {
			return makeExcel(tipoAsunto, temap, temaf, anio, asuntosL, aniosL, asuntosSinTemas, aniosSinTemas, conteoAnios, anioMax, anioMin);
			//makeExcelAsunto(asuntosL, aniosL);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public void addLineToCSV(String asuntoNumExp, List<String> datosCSV, 
			List<String> temaList, boolean isTemaProcesal) {
		
		for(int i=0;i<temaList.size();i++) {
			if(i<datosCSV.size()) {
				String tema = temaList.get(i);
				String line = "";
				if(isTemaProcesal) {
					line = datosCSV.get(i)+"@"+tema+"@null";
					datosCSV.set(i, line);
				}else {
					line = datosCSV.get(i);
					line = line.contains("null")? line.replace("null", tema):datosCSV.get(i)+"@null@"+tema;
					datosCSV.set(i, line);
				}
			}else {
				String tema = temaList.get(i);
				String line = "";
				if(isTemaProcesal) {
					line = asuntoNumExp+"@"+tema+"@null";
				}else {
					line = asuntoNumExp+"@null@"+tema;
				}
				datosCSV.add(line);
			}
		}
	}
	
	public Page<AsuntoSeguimientoDTO> getAllSeguimientoPages(Pageable paging, String filtros) throws ParseException {
		FiltroSeguimientoDTO filtroData = getFiltroData(filtros);
		List<String> usersNotIn= new ArrayList<String>(Arrays.asList("ggarciab", "avazquez"));
		Query query = null;
		List<AsuntoAbordado> asuntosListAll = null;
		List<AsuntoSeguimientoDTO> asuntosListDTO = new ArrayList<AsuntoSeguimientoDTO>();
    	Page<AsuntoSeguimientoDTO> asuntosListDTOPage = null;
    	AsuntoSeguimientoDTO asuntoDTO = null;

    	if(filtroData!=null) {
			if(filtroData.getSecretario()!=null) {
				List<String> asuntosList = new ArrayList<String>();
				getListOfAsuntosBySecretarioAsignado(filtroData.getSecretario().getCorreo(),
						filtroData.getAsuntoAbordado(),
						filtroData.getNumeroExpediente(),
						asuntosList, query);
				getListOfAsuntosBySecretarioEditorAndAsuntoOrNumExp(filtroData.getSecretario().getCorreo(), 
						filtroData.getAsuntoAbordado(),
						filtroData.getNumeroExpediente(),
						asuntosList, query);
				
				if(filtroData.getAnio() != null) {
					getAsuntosByAsuntoAndNumeroExpAndAnioS(filtroData.getAsuntoAbordado(), 
							filtroData.getNumeroExpediente(), 
							filtroData.getAnio(), asuntosList, query);
				}
				
				asuntosListAll = getListOfAsuntosByAsuntoAndNumExp(asuntosList, query);
				for(int i=(paging.getPageNumber()*paging.getPageSize()); i<asuntosListAll.size();i++) {	
					if(asuntosListDTO.size()>=paging.getPageSize()) {
						break;
					}
					AsuntoAbordado asunto = asuntosListAll.get(i);
					asuntoDTO = setBasicData(asuntoDTO, asunto);

		    		List<String> listSecretariosAsignados =  getSecretariosAsignadosByAsuntoOrNumEXpediente(asunto.getAsuntoAbordado(), asunto.getNumeroExpediente(), usersNotIn, query);
		    		asuntoDTO.setSecretarios(listSecretariosAsignados);
		    		
		    		List<String> listEditores = getSecretariosEditoresByAsuntoOrNumEXpediente(asunto.getAsuntoAbordado(), asunto.getNumeroExpediente(),usersNotIn, query, null);
		    		asuntoDTO.setEditores(listEditores);
		    		
		    		getSesionesTemas(asunto.getId(), asuntoDTO);
		    		
		    		//Here colocar el año
		    		String anio = getFechaResolucion(asunto.getAsuntoAbordado(), asunto.getNumeroExpediente());
		    		asuntoDTO.setAnioResolucion(anio);
		    		
		    		asuntosListDTO.add(asuntoDTO);
				}
				asuntosListDTOPage = new PageImpl<AsuntoSeguimientoDTO>(asuntosListDTO, paging, asuntosListAll.size());
		    	
				System.out.println("U");
			}else {
				query = new Query().with(paging);

				List<AsuntoAbordado> asuntosList = new ArrayList<AsuntoAbordado>();
				if(filtroData.getAnio() != null) {
					List<String> asuntosCadenas = getAsuntosByAsuntoAndNumeroExpAndAnio(filtroData.getAsuntoAbordado(), 
							filtroData.getNumeroExpediente(), 
							filtroData.getAnio(), query);
					
					asuntosList = getListOfAsuntosByAsuntoAndNumExp(asuntosCadenas, query);

					for(int i=(paging.getPageNumber()*paging.getPageSize()); i<asuntosList.size();i++) {	
						if(asuntosListDTO.size()>=paging.getPageSize()) {
							break;
						}
						
						AsuntoAbordado asunto = asuntosList.get(i);
						asuntoDTO = setBasicData(asuntoDTO, asunto);

			    		List<String> listSecretariosAsignados =  getSecretariosAsignadosByAsuntoOrNumEXpediente(asunto.getAsuntoAbordado(), asunto.getNumeroExpediente(), usersNotIn, query);
			    		asuntoDTO.setSecretarios(listSecretariosAsignados);
			    		
			    		List<String> listEditores = getSecretariosEditoresByAsuntoOrNumEXpediente(asunto.getAsuntoAbordado(), asunto.getNumeroExpediente(),usersNotIn, query, null);
			    		asuntoDTO.setEditores(listEditores);
			    		
			    		getSesionesTemas(asunto.getId(), asuntoDTO);
			    		
			    		//Here colocar el año
			    		String anio = getFechaResolucion(asunto.getAsuntoAbordado(), asunto.getNumeroExpediente());
			    		asuntoDTO.setAnioResolucion(anio);
			    		
			    		asuntosListDTO.add(asuntoDTO);
					}
					asuntosListDTOPage = new PageImpl<AsuntoSeguimientoDTO>(asuntosListDTO, paging, asuntosList.size());
					
				}else {
					if(filtroData!=null) {
						if(filtroData.getAsuntoAbordado()!=null) query.addCriteria(Criteria.where("asuntoAbordado").is(filtroData.getAsuntoAbordado()));
						if(filtroData.getNumeroExpediente()!=null) query.addCriteria(Criteria.where("numeroExpediente").is(filtroData.getNumeroExpediente()));
					}
					asuntosList = mongoOperations.find(query, AsuntoAbordado.class);
					
					for(AsuntoAbordado asunto:asuntosList) {
						asuntoDTO = setBasicData(asuntoDTO, asunto);
						
			    		List<String> listSecretariosAsignados =  getSecretariosAsignadosByAsuntoOrNumEXpediente(asunto.getAsuntoAbordado(), asunto.getNumeroExpediente(), usersNotIn, query);
			    		asuntoDTO.setSecretarios(listSecretariosAsignados);
			    		
			    		List<String> listEditores = getSecretariosEditoresByAsuntoOrNumEXpediente(asunto.getAsuntoAbordado(), asunto.getNumeroExpediente(),usersNotIn, query, null);
			    		asuntoDTO.setEditores(listEditores);
			    		
			    		getSesionesTemas(asunto.getId(), asuntoDTO);
			    		//Here colocar el año
			    		String anio = getFechaResolucion(asunto.getAsuntoAbordado(), asunto.getNumeroExpediente());
			    		asuntoDTO.setAnioResolucion(anio);
			    		asuntosListDTO.add(asuntoDTO);
					}
					query = new Query();
					if(filtroData!=null) {
						if(filtroData.getAsuntoAbordado()!=null) query.addCriteria(Criteria.where("asuntoAbordado").is(filtroData.getAsuntoAbordado()));
						if(filtroData.getNumeroExpediente()!=null) query.addCriteria(Criteria.where("numeroExpediente").is(filtroData.getNumeroExpediente()));
					}
					asuntosListDTOPage = new PageImpl<AsuntoSeguimientoDTO>(asuntosListDTO, paging, mongoOperations.count(query, AsuntoAbordado.class));
				}
				
				
		    	
			}
			
		}else if(filtroData==null) {
			query = new Query().with(paging);
			List<AsuntoAbordado> asuntosList = mongoOperations.find(query, AsuntoAbordado.class);
			for(AsuntoAbordado asunto:asuntosList) {
				asuntoDTO = setBasicData(asuntoDTO, asunto);
				
	    		List<String> listSecretariosAsignados =  getSecretariosAsignadosByAsuntoOrNumEXpediente(asunto.getAsuntoAbordado(), asunto.getNumeroExpediente(), usersNotIn, query);
	    		asuntoDTO.setSecretarios(listSecretariosAsignados);
	    		
	    		List<String> listEditores = getSecretariosEditoresByAsuntoOrNumEXpediente(asunto.getAsuntoAbordado(), asunto.getNumeroExpediente(),usersNotIn, query, null);
	    		asuntoDTO.setEditores(listEditores);
	    		
	    		String anio = getFechaResolucion(asunto.getAsuntoAbordado(), asunto.getNumeroExpediente());
	    		asuntoDTO.setAnioResolucion(anio);
	    		
	    		getSesionesTemas(asunto.getId(), asuntoDTO);
	    		
	    		//Here colocar el año
	    		String anio2 = getFechaResolucion(asunto.getAsuntoAbordado(), asunto.getNumeroExpediente());
	    		asuntoDTO.setAnioResolucion(anio2);
	    		asuntosListDTO.add(asuntoDTO);
			}
			query = new Query();
			asuntosListDTOPage = new PageImpl<AsuntoSeguimientoDTO>(asuntosListDTO, paging, mongoOperations.count(query, AsuntoAbordado.class));
		}
    	System.out.println("ce fini");
		return asuntosListDTOPage;
	}
	

}
