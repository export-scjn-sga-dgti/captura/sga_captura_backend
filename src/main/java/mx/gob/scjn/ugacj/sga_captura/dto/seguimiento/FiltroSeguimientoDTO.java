package mx.gob.scjn.ugacj.sga_captura.dto.seguimiento;

import mx.gob.scjn.ugacj.sga_captura.domain.Usuario;

public class FiltroSeguimientoDTO {

	String asuntoAbordado = null;
	String numeroExpediente = null;
	Usuario secretario = null;
	String anio = null;
	
	public String getAsuntoAbordado() {
		return asuntoAbordado;
	}
	public void setAsuntoAbordado(String asuntoAbordado) {
		this.asuntoAbordado = asuntoAbordado;
	}
	public String getNumeroExpediente() {
		return numeroExpediente;
	}
	public void setNumeroExpediente(String numeroExpediente) {
		this.numeroExpediente = numeroExpediente;
	}
	public Usuario getSecretario() {
		return secretario;
	}
	public void setSecretario(Usuario secretario) {
		this.secretario = secretario;
	}
	public String getAnio() {
		return anio;
	}
	public void setAnio(String anio) {
		this.anio = anio;
	}
	
	
}
