package mx.gob.scjn.ugacj.sga_captura.domain;

import java.util.ArrayList;
import java.util.List;

public class DereHumaViolaAnaliSenten {

   	List<String> articulos;
	List<String> derechos;
	List<String> rubros;
	String metodologia;
	List<String> votacionAFavorVicioPlanteado;
	List<String> votaciontionEnContraVicioPlanteado;
	List<String> todosMinistrosVicioPlanteado;
	String fundamentacion;
	List<String> tipoSentencia = new ArrayList<String>();
	boolean votacionMayoriaVicioPlanetado;
	int cantidadAFavorVicioPlanteado;
	String textoVotacionVicioPlanteado;
	List<VotacionTipoSentencia> votacionesTipoSentencia = new ArrayList<VotacionTipoSentencia>();
	List<VotacionTipoSentencia> votacionRelacionadosExtensionValidez= new ArrayList<VotacionTipoSentencia>();

    public List<VotacionTipoSentencia> getVotacionRelacionadosExtensionValidez() {
        return this.votacionRelacionadosExtensionValidez;
    }

    public void setVotacionRelacionadosExtensionValidez(
            List<VotacionTipoSentencia> votacionRelacionadosExtensionValidez) {
        this.votacionRelacionadosExtensionValidez = votacionRelacionadosExtensionValidez;
    }
	

	public List<VotacionTipoSentencia> getVotacionesTipoSentencia() {
		return this.votacionesTipoSentencia;
	}

	public void setVotacionesTipoSentencia(List<VotacionTipoSentencia> votacionesTipoSentencia) {
		this.votacionesTipoSentencia = votacionesTipoSentencia;
	}
	

	public List<String> getArticulos() {
		return this.articulos;
	}

	public void setArticulos(List<String> articulos) {
		this.articulos = articulos;
	}

	public List<String> getDerechos() {
		return this.derechos;
	}

	public void setDerechos(List<String> derechos) {
		this.derechos = derechos;
	}

	public List<String> getRubros() {
		return this.rubros;
	}

	public void setRubros(List<String> rubros) {
		this.rubros = rubros;
	}

	public String getMetodologia() {
		return this.metodologia;
	}

	public void setMetodologia(String metodologia) {
		this.metodologia = metodologia;
	}


	public List<String> getVotacionAFavorVicioPlanteado() {
		return this.votacionAFavorVicioPlanteado;
	}

	public void setVotacionAFavorVicioPlanteado(List<String> votacionAFavorVicioPlanteado) {
		this.votacionAFavorVicioPlanteado = votacionAFavorVicioPlanteado;
	}

	public List<String> getVotaciontionEnContraVicioPlanteado() {
		return this.votaciontionEnContraVicioPlanteado;
	}

	public void setVotaciontionEnContraVicioPlanteado(List<String> votaciontionEnContraVicioPlanteado) {
		this.votaciontionEnContraVicioPlanteado = votaciontionEnContraVicioPlanteado;
	}

	public List<String> getTodosMinistrosVicioPlanteado() {
		return this.todosMinistrosVicioPlanteado;
	}

	public void setTodosMinistrosVicioPlanteado(List<String> todosMinistrosVicioPlanteado) {
		this.todosMinistrosVicioPlanteado = todosMinistrosVicioPlanteado;
	}


	public String getFundamentacion() {
		return this.fundamentacion;
	}

	public void setFundamentacion(String fundamentacion) {
		this.fundamentacion = fundamentacion;
	}

	public List<String> getTipoSentencia() {
		return this.tipoSentencia;
	}

	public void setTipoSentencia(List<String> tipoSentencia) {
		this.tipoSentencia = tipoSentencia;
	}


	public boolean isVotacionMayoriaVicioPlanetado() {
		return this.votacionMayoriaVicioPlanetado;
	}

	public void setVotacionMayoriaVicioPlanetado(boolean votacionMayoriaVicioPlanetado) {
		this.votacionMayoriaVicioPlanetado = votacionMayoriaVicioPlanetado;
	}

	public int getCantidadAFavorVicioPlanteado() {
		return this.cantidadAFavorVicioPlanteado;
	}

	public void setCantidadAFavorVicioPlanteado(int cantidadAFavorVicioPlanteado) {
		this.cantidadAFavorVicioPlanteado = cantidadAFavorVicioPlanteado;
	}

	public String getTextoVotacionVicioPlanteado() {
		return this.textoVotacionVicioPlanteado;
	}

	public void setTextoVotacionVicioPlanteado(String textoVotacionVicioPlanteado) {
		this.textoVotacionVicioPlanteado = textoVotacionVicioPlanteado;
	}


    
}
