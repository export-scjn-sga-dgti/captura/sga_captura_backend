package mx.gob.scjn.ugacj.sga_captura.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import mx.gob.scjn.ugacj.sga_captura.domain.AcuerdosGenerales;

public interface AcuerdosGeneralesRepository extends MongoRepository<AcuerdosGenerales,String> {

	Page<AcuerdosGenerales> findAllByOrderByFechaAprobacionDesc(Pageable paging);

}
