package mx.gob.scjn.ugacj.sga_captura.dto.migracion;

public class AsuntosAbordados {
    
String asuntoAbordado;
Temas[] temasProcesalesAbordados;
Temas[] temasFondoAbordados;
String[] nombreMinistrosParticiparon;
NumeroMinistrosParticiparon numeroMinistrosParticiparon[];
String numExpediente;
String numExpedienteAcumulados[];
boolean acumulados;
String ponente;
boolean ca;

public boolean getCa() {
    return this.ca;
}

public void setCa(boolean ca) {
    this.ca = ca;
}

public void setAcumulados(boolean acumulados){
    this.acumulados = acumulados;
}

public boolean getAcumulados(){
    return this.acumulados;
}

public void setNumExpedienteAcumulados(String numExpedienteAcumulados[]){
    this.numExpedienteAcumulados = numExpedienteAcumulados;
}

public String [] getNumExpedienteAcumulados(){
    return this.numExpedienteAcumulados;
}

public String getNumExpediente() {
	return numExpediente;
}

public void setNumExpediente(String numExpediente) {
	this.numExpediente = numExpediente;
}

public String getAsuntoAbordado() {
    return this.asuntoAbordado;
}

public void setAsuntoAbordado(String asuntoAbordado) {
    this.asuntoAbordado = asuntoAbordado;
}

public Temas [] getTemasProcesalesAbordados() {
    return this.temasProcesalesAbordados;
}

public void setTemasProcesalesAbordados(Temas [] temasProcesalesAbordados) {
    this.temasProcesalesAbordados = temasProcesalesAbordados;
}

public Temas [] getTemasFondoAbordados() {
    return this.temasFondoAbordados;
}

public void setTemasFondoAbordados(Temas [] temasFondoAbordados) {
    this.temasFondoAbordados = temasFondoAbordados;
}

    public String[] getNombreMinistrosParticiparon() {
    return this.nombreMinistrosParticiparon;
    }

    public void setNombreMinistrosParticiparon(String[] nombreMinistrosParticiparon) {
    this.nombreMinistrosParticiparon = nombreMinistrosParticiparon;
    }

    public NumeroMinistrosParticiparon[] getNumeroMinistrosParticiparon() {
		return this.numeroMinistrosParticiparon;
	}

    public void setNumeroMinistrosParticiparon(NumeroMinistrosParticiparon numeroMinistrosParticiparon[]) {
		this.numeroMinistrosParticiparon = numeroMinistrosParticiparon;
	}

    public String getPonente() {
        return this.ponente;
    }
    
    public void setPonente(String ponente) {
        this.ponente = ponente;
    }



}
