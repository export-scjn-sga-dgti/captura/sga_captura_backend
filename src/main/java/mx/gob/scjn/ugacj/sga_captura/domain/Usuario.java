package mx.gob.scjn.ugacj.sga_captura.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import mx.gob.scjn.ugacj.sga_captura.dto.ModuloDTO;

@Document(collection = "#{@environment.getProperty('indice.usuario')}")
public class Usuario implements Serializable{

	@Id
	String id;
	String nombre;
	String correo;
	List<ModuloDTO> modulos = new ArrayList<ModuloDTO>();
	Date fh_creacion;
	Date fh_modificacion;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public List<ModuloDTO> getModulos() {
		return modulos;
	}
	public void setModulos(List<ModuloDTO> modulos) {
		this.modulos = modulos;
	}
	public Date getFh_creacion() {
		return fh_creacion;
	}
	public void setFh_creacion(Date fh_creacion) {
		this.fh_creacion = fh_creacion;
	}
	public Date getFh_modificacion() {
		return fh_modificacion;
	}
	public void setFh_modificacion(Date fh_modificacion) {
		this.fh_modificacion = fh_modificacion;
	}
	
	
	
}
