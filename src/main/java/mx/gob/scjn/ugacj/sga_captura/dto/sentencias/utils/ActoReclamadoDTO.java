package mx.gob.scjn.ugacj.sga_captura.dto.sentencias.utils;

public class ActoReclamadoDTO {
    
    String tipoAsunto;
    int consecutivo;
    int anio;
    
	public String getTipoAsunto() {
		return tipoAsunto;
	}
	public void setTipoAsunto(String tipoAsunto) {
		this.tipoAsunto = tipoAsunto;
	}
	public int getConsecutivo() {
		return consecutivo;
	}
	public void setConsecutivo(int consecutivo) {
		this.consecutivo = consecutivo;
	}
	public int getAnio() {
		return anio;
	}
	public void setAnio(int anio) {
		this.anio = anio;
	}
    
}
