package mx.gob.scjn.ugacj.sga_captura.dto;

import mx.gob.scjn.ugacj.sga_captura.domain.RubrosTablaVotaciones;
import mx.gob.scjn.ugacj.sga_captura.utils.validaciones.RubrosVotaciones.ValidatorsRubrosConstrains;

public class RubrosTablaVotacionesDTO {

    private String idVotacion;
    private String idSentencia;

    @ValidatorsRubrosConstrains
    private RubrosTablaVotaciones rubro;

    public String getIdVotacion() {
        return idVotacion;
    }

    public void setIdVotacion(String idVotacion) {
        this.idVotacion = idVotacion;
    }

    public RubrosTablaVotaciones getRubro() {
        return rubro;
    }

    public void setRubro(RubrosTablaVotaciones rubro) {
        this.rubro = rubro;
    }
    

    public String getIdSentencia() {
		return idSentencia;
	}

	public void setIdSentencia(String idSentencia) {
		this.idSentencia = idSentencia;
	}

	@Override
    public String toString() {
        return "RubrosVotacionDTO{" +
                "idVotacion='" + idVotacion + '\'' +
                "idVotacion='" + idSentencia + '\'' +
                ", rubro=" + rubro +
                '}';
    }
}
