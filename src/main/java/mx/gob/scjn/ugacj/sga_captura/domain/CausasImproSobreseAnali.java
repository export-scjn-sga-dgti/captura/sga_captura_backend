package mx.gob.scjn.ugacj.sga_captura.domain;

import java.util.List;

public class CausasImproSobreseAnali {
 
    List<String> actosImpugnados;
    String fundamentacion;
    List<String> votacionAFavorVicioPlanteado;
    List<String> votaciontionEnContraVicioPlanteado;
    List<String> todosMinistrosVicioPlanteado;
    boolean votacionMayoriaVicioPlanetado;
    int cantidadAFavorVicioPlanteado;
    String textoVotacionVicioPlanteado;
    List<CausaImprocedencia> causasDeImprocedencias;

    public List<CausaImprocedencia> getCausasDeImprocedencias() {
        return this.causasDeImprocedencias;
    }

    public void setCausasDeImprocedencias(List<CausaImprocedencia> causasDeImprocedencias) {
        this.causasDeImprocedencias = causasDeImprocedencias;
    }

    public String getTextoVotacionVicioPlanteado() {
        return this.textoVotacionVicioPlanteado;
    }

    public void setTextoVotacionVicioPlanteado(String textoVotacionVicioPlanteado) {
        this.textoVotacionVicioPlanteado = textoVotacionVicioPlanteado;
    }

    public boolean isVotacionMayoriaVicioPlanetado() {
        return this.votacionMayoriaVicioPlanetado;
    }

    public void setVotacionMayoriaVicioPlanetado(boolean votacionMayoriaVicioPlanetado) {
        this.votacionMayoriaVicioPlanetado = votacionMayoriaVicioPlanetado;
    }

    public int getCantidadAFavorVicioPlanteado() {
        return this.cantidadAFavorVicioPlanteado;
    }

    public void setCantidadAFavorVicioPlanteado(int cantidadAFavorVicioPlanteado) {
        this.cantidadAFavorVicioPlanteado = cantidadAFavorVicioPlanteado;
    }

    public List<String> getVotacionAFavorVicioPlanteado() {
        return this.votacionAFavorVicioPlanteado;
    }

    public void setVotacionAFavorVicioPlanteado(List<String> votacionAFavorVicioPlanteado) {
        this.votacionAFavorVicioPlanteado = votacionAFavorVicioPlanteado;
    }

    public List<String> getVotaciontionEnContraVicioPlanteado() {
        return this.votaciontionEnContraVicioPlanteado;
    }

    public void setVotaciontionEnContraVicioPlanteado(List<String> votaciontionEnContraVicioPlanteado) {
        this.votaciontionEnContraVicioPlanteado = votaciontionEnContraVicioPlanteado;
    }

    public List<String> getTodosMinistrosVicioPlanteado() {
        return this.todosMinistrosVicioPlanteado;
    }

    public void setTodosMinistrosVicioPlanteado(List<String> todosMinistrosVicioPlanteado) {
        this.todosMinistrosVicioPlanteado = todosMinistrosVicioPlanteado;
    }
 

    public List<String> getActosImpugnados() {
        return this.actosImpugnados;
    }

    public void setActosImpugnados(List<String> actosImpugnados) {
        this.actosImpugnados = actosImpugnados;
    }
   
    public String getFundamentacion() {
        return this.fundamentacion;
    }

    public void setFundamentacion(String fundamentacion) {
        this.fundamentacion = fundamentacion;
    }
  
}
