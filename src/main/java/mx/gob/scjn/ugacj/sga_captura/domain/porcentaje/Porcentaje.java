package mx.gob.scjn.ugacj.sga_captura.domain.porcentaje;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import mx.gob.scjn.ugacj.sga_captura.dto.porcentaje.FieldValidationDTO;

@Document(collection = "#{@environment.getProperty('indice.porcentajesform')}")
public class Porcentaje implements Serializable {

    @Id
    String id;
    String orden;
    String fieldName;
	String criterio;//NotEmpty, optionEmbebed-container, OptionEmbebed-DefaultNotEmpty
	List<String> optionValue;
	double value;
	List<FieldValidationDTO> fieldsValidation;
	String form;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getOrden() {
		return orden;
	}
	public void setOrden(String orden) {
		this.orden = orden;
	}
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	public String getCriterio() {
		return criterio;
	}
	public void setCriterio(String criterio) {
		this.criterio = criterio;
	}
	public List<String> getOptionValue() {
		return optionValue;
	}
	public void setOptionValue(List<String> optionValue) {
		this.optionValue = optionValue;
	}
	public double getValue() {
		return value;
	}
	public void setValue(double value) {
		this.value = value;
	}
	public List<FieldValidationDTO> getFieldsValidation() {
		return fieldsValidation;
	}
	public void setFieldsValidation(List<FieldValidationDTO> fieldsValidation) {
		this.fieldsValidation = fieldsValidation;
	}
	public String getForm() {
		return form;
	}
	public void setForm(String form) {
		this.form = form;
	}
	
	
	
}
