package mx.gob.scjn.ugacj.sga_captura.utils.validaciones.AsuntosAbordados;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = { AsuntosAbordadosValidators.class})
@Target( {ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidatorsAsuntosAbordadosConstrains {

    String message() default "Los campos Asunto Abordado y Número de Expediente no pueden estar vacíos";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

}
