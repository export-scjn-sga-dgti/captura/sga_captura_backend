package mx.gob.scjn.ugacj.sga_captura.repository.porcentaje;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import mx.gob.scjn.ugacj.sga_captura.domain.porcentaje.Porcentaje;

public interface PorcentajeRepository extends MongoRepository<Porcentaje,String> {
	
	List<Porcentaje> findByForm(String form);

}
