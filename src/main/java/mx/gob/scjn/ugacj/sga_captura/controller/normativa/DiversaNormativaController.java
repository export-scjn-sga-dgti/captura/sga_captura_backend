package mx.gob.scjn.ugacj.sga_captura.controller.normativa;

import java.text.ParseException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import mx.gob.scjn.ugacj.sga_captura.domain.DiversaNormativa;
import mx.gob.scjn.ugacj.sga_captura.dto.normativa.DiversaNormativaDTO;
import mx.gob.scjn.ugacj.sga_captura.dto.statecode.DivNormativaResponseDTO;
import mx.gob.scjn.ugacj.sga_captura.service.normativa.DiversaNormativaService;

@RestController
@RequestMapping(path = "/api/sga/diversa-normativa")
public class DiversaNormativaController {

	@Autowired
	private DiversaNormativaService _dnService;

	@RequestMapping(method = RequestMethod.GET, path = "/normativa")
	@CrossOrigin()
	public Page<DiversaNormativa> getNormativaAll(@RequestParam(name = "page") Integer page,
			@RequestParam(name = "size") Integer size, @RequestParam(required = false, name = "filtros") String filtros)
			throws ParseException {
		Pageable paging = PageRequest.of(page - 1, size);
		return _dnService.getNormativaAllPages(paging, filtros);
	}

	@CrossOrigin()
	@RequestMapping(method = RequestMethod.GET, path = "/{id}", produces = "application/json")
	public DiversaNormativaDTO getDiversaNormativa(@PathVariable String id) {
		return _dnService.getDiversaNormativa(id);
	}

	@RequestMapping(method = RequestMethod.POST, path = "/")
	@CrossOrigin()
	public DivNormativaResponseDTO saveDiversaNormativa(@Valid @RequestBody DiversaNormativa diversaN) {
		return _dnService.saveDiversaNormativa(diversaN);
	}

	@CrossOrigin()
	@RequestMapping(method = RequestMethod.PUT, path = "/", produces = "application/json")
	public DivNormativaResponseDTO updateDiversaNormativa(@Valid @RequestBody DiversaNormativa diversaN) {
		return _dnService.updateDiversaNormativa(diversaN);
	}

	@CrossOrigin()
	@RequestMapping(method = RequestMethod.DELETE, path = "/{id}", produces = "application/json")
	public boolean deleteDiversaNormativa(@PathVariable String id) {
		return _dnService.deleteDiversaNormativa(id);
	}

}
