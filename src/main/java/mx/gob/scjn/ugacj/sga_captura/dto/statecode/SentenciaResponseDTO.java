package mx.gob.scjn.ugacj.sga_captura.dto.statecode;


public class SentenciaResponseDTO {

    private String idSentencia;

    private  String message;

    public String getIdSentencia() {
        return idSentencia;
    }

    public void setIdSentencia(String idSentencia) {
        this.idSentencia = idSentencia;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "SentenciaOnlyDTO{" +
                "idSentencia='" + idSentencia + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
