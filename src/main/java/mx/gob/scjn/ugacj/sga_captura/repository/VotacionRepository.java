package mx.gob.scjn.ugacj.sga_captura.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import mx.gob.scjn.ugacj.sga_captura.domain.TablaVotaciones;
import org.springframework.stereotype.Repository;

@Repository
public interface VotacionRepository extends MongoRepository<TablaVotaciones,String>{

}
