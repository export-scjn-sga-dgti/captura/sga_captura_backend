package mx.gob.scjn.ugacj.sga_captura.controller.acuerdos;

import java.text.ParseException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mx.gob.scjn.ugacj.sga_captura.domain.AcuerdosGenerales;
import mx.gob.scjn.ugacj.sga_captura.dto.statecode.AcuerdosResponseDTO;
import mx.gob.scjn.ugacj.sga_captura.service.acuerdos.AcuerdosService;
@RestController
@RequestMapping( value = "/api/sga/acuerdos")
public class AcuerdosController {

	@Autowired
    private AcuerdosService _acuerdosService;
	
    @RequestMapping(method = RequestMethod.GET,path = "/")
    @CrossOrigin()
    public Page<AcuerdosGenerales> getAcuerdosAll(@RequestParam(name = "page") Integer page,
                                             @RequestParam(name = "size") Integer size,
                                             @RequestParam(required = false, name = "filtros") String filtros) throws ParseException{
        Pageable paging = PageRequest.of(page-1, size);
        return _acuerdosService.getAllAcuerdosPages(paging, filtros);
    }

    @RequestMapping(method = RequestMethod.GET,path = "/{acuerdoId}", produces = "application/json")
    @CrossOrigin()
    public AcuerdosGenerales getAcuerdosById(@PathVariable String acuerdoId){
        return _acuerdosService.getAcuerdosById(acuerdoId);
    }

    @RequestMapping(method = RequestMethod.POST,path = "/")
    @CrossOrigin()
    public AcuerdosResponseDTO saveAcuerdo(@Valid @RequestBody AcuerdosGenerales acuerdo){
        return _acuerdosService.saveAcuerdo(acuerdo);
    }

    @RequestMapping(method = RequestMethod.PUT,path = "/")
    @CrossOrigin()
    public AcuerdosResponseDTO updateAcuerdo(@Valid @RequestBody AcuerdosGenerales acuerdo){
        return _acuerdosService.updateAcuerdo(acuerdo);
    }
    
    @RequestMapping(method = RequestMethod.DELETE,path = "/{acuerdoId}")
    @CrossOrigin()
    public boolean deleteAcuerdo(@PathVariable String acuerdoId){
    	return _acuerdosService.deleteAcuerdo(acuerdoId);
    }
}
