package mx.gob.scjn.ugacj.sga_captura.dto;

import mx.gob.scjn.ugacj.sga_captura.utils.validaciones.TablasVotaciones.ValidatorsTablaVotacionesConstrain;

import javax.validation.Valid;

//@JsonInclude(JsonInclude.Include.NON_NULL)
public class TablaVotacionSentenciaDTO {

    private  String idSentencia;

    @Valid
    @ValidatorsTablaVotacionesConstrain
    private TablaVotacionesRubrosDTO votacion;

    public String getIdSentencia() {
        return idSentencia;
    }

    public void setIdSentencia(String idSentencia) {
        this.idSentencia = idSentencia;
    }

    public TablaVotacionesRubrosDTO getVotacion() {
        return votacion;
    }

    public void setVotacion(TablaVotacionesRubrosDTO votacion) {
        this.votacion = votacion;
    }

    @Override
    public String toString() {
        return "VotacionSentenciaDTO{" +
                "idSentencia='" + idSentencia + '\'' +
                ", votacion=" + votacion +
                '}';
    }
}
