package mx.gob.scjn.ugacj.sga_captura.dto.statecode;

import mx.gob.scjn.ugacj.sga_captura.domain.VersionTaquigrafica;

public class VtaqResponseDTO extends ResponseDTO{

	VersionTaquigrafica versionTaquigrafica;
	
	public VtaqResponseDTO(int statusCode, String message) {
		super(statusCode, message);
		// TODO Auto-generated constructor stub
	}

	public VersionTaquigrafica getVersionTaquigrafica() {
		return versionTaquigrafica;
	}

	public void setVersionTaquigrafica(VersionTaquigrafica versionTaquigrafica) {
		this.versionTaquigrafica = versionTaquigrafica;
	}
	
}
