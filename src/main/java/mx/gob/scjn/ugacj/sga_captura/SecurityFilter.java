package mx.gob.scjn.ugacj.sga_captura;

import java.io.BufferedReader;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.stream.Collectors;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.filter.OncePerRequestFilter;
import mx.gob.scjn.ugacj.sga_captura.dto.UserInfoDTO;
import mx.gob.scjn.ugacj.sga_captura.utils.DisableSSL;

import com.google.gson.Gson;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component
public class SecurityFilter extends OncePerRequestFilter {

	@Value("${keycloak.server.url}")
	private String keycloakUrl;
	Logger logger = LoggerFactory.getLogger(SecurityFilter.class);

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		String token = request.getHeader("Authorization");
		if (token != null && token.startsWith("Bearer")) {
			if (isTokenValid(token, request)) {
				logger.info("sesion activa, redireccionando");
				filterChain.doFilter(request, response);
			} else {
				logger.info("token invalido", token);
				response.setStatus(HttpStatus.UNAUTHORIZED.value());
			}

		} else {
			logger.info("No se encontro el Bearer en el token", token);
			response.setStatus(HttpStatus.UNAUTHORIZED.value());
		}

	}
	
	
	public boolean isTokenValid(String token, HttpServletRequest request){
		DisableSSL ssl = new DisableSSL();

		RestTemplate restTemplate;
		try {
			restTemplate = ssl.DisableSSL();
			 HttpHeaders headers = new HttpHeaders();
			    headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
			    headers.setBearerAuth(token.replace("Bearer ", ""));
			    
			    HttpEntity<String> httpEntity = new HttpEntity<>(null,headers);
			    ResponseEntity<String> responseEntity = restTemplate.postForEntity( keycloakUrl, httpEntity,String.class);
			    String json = responseEntity.getBody();
			    Gson gson = new Gson();
			    UserInfoDTO userInfo = gson.fromJson(json, UserInfoDTO.class);
				if (userInfo.getSub() != null) {
					request.setAttribute("username", userInfo.getPreferredUsername());
					logger.info("la sesion esta activa", token);
				}
				return userInfo.getSub() != null;
		} catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
			
	}

	private boolean isTokenValid_old(String token, HttpServletRequest request) {
		HttpPost post = new HttpPost(keycloakUrl);
		post.addHeader("Content-Type", "application/x-www-form-urlencoded");
		post.addHeader("Authorization", token);
		try {
			CloseableHttpClient httpClient = HttpClients.createDefault();
			CloseableHttpResponse response = httpClient.execute(post);
			String json = EntityUtils.toString(response.getEntity());
			Gson gson = new Gson();
			UserInfoDTO userInfo = gson.fromJson(json, UserInfoDTO.class);
			if (userInfo.getSub() != null) {
				request.setAttribute("username", userInfo.getPreferredUsername());
				logger.info("la sesion esta activa", token);
			}
			return userInfo.getSub() != null;
		} catch (Exception e) {
			System.out.println(e);
			return false;
		}

	}

}