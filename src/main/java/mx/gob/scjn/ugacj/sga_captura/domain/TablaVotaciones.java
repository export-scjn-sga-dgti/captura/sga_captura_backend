package mx.gob.scjn.ugacj.sga_captura.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotBlank;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Document(collection = "#{@environment.getProperty('indice.sentencia.votacion')}")
@JsonInclude(Include.NON_NULL)
public class TablaVotaciones implements Serializable {
	@Id
	String id;
	String tipoVotacion;
	List<String> tipoVicio;
	String congresoEmitioDecreto;
	String decretoImpugnado;
	
	
	List<String> ministros;
	List<String> votacionFavor;
	List<String> votacionEnContra;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	Boolean aprobadasConcideraciones = Boolean.parseBoolean(null);
	String sentidoResolucion;

	@NotBlank
	String textoVotacion;

	List<String> tipoSentencia;
	List<String> rubrosEfectoSentencia = new ArrayList<String>();
	
	List<String> rubrosTematicosPrincipal = new ArrayList<String>();;
	//Los contenedores de las votaciones multiples embebidas
	List<String> rubrosExtencionInvalidez = new ArrayList<String>();
	List<String> rubrosTematicos;
	//end
	
	List<String> actosImpugnados = new ArrayList<String>();
	List<String> votacionRubrosTematicos;
	List<String> articulosImpugnados = new ArrayList<String>();
	List<String> derechosHumanos;
	String metodologiaAnalisisConstitucionalidad;
	String tipoViolacionInvacionEsferas;
	String tipoViolacionInvacionPoderes;
	private Integer numeroVotos;
	private String materiaAnalizadaPorInvacionEsferas;
	private String materiaAnalizadaPorInvacionPoderes;

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTipoVotacion() {
		return tipoVotacion;
	}
	public void setTipoVotacion(String tipoVotacion) {
		this.tipoVotacion = tipoVotacion;
	}
	
	public List<String> getTipoVicio() {
		return tipoVicio;
	}
	public void setTipoVicio(List<String> tipoVicio) {
		this.tipoVicio = tipoVicio;
	}
	public String getCongresoEmitioDecreto() {
		return congresoEmitioDecreto;
	}
	public void setCongresoEmitioDecreto(String congresoEmitioDecreto) {
		this.congresoEmitioDecreto = congresoEmitioDecreto;
	}
	public String getDecretoImpugnado() {
		return decretoImpugnado;
	}
	public void setDecretoImpugnado(String decretoImpugnado) {
		this.decretoImpugnado = decretoImpugnado;
	}
	public List<String> getRubrosTematicos() {
		return rubrosTematicos;
	}
	public void setRubrosTematicos(List<String> rubrosTematicos) {
		this.rubrosTematicos = rubrosTematicos;
	}
	public List<String> getVotacionFavor() {
		return votacionFavor;
	}
	public void setVotacionFavor(List<String> votacionFavor) {
		this.votacionFavor = votacionFavor;
	}
	public List<String> getVotacionEnContra() {
		return votacionEnContra;
	}
	public void setVotacionEnContra(List<String> votacionEnContra) {
		this.votacionEnContra = votacionEnContra;
	}
	public String getSentidoResolucion() {
		return sentidoResolucion;
	}
	public void setSentidoResolucion(String sentidoResolucion) {
		this.sentidoResolucion = sentidoResolucion;
	}
	public String getTextoVotacion() {
		return textoVotacion;
	}
	public void setTextoVotacion(String textoVotacion) {
		this.textoVotacion = textoVotacion;
	}
	public List<String> getTipoSentencia() {
		return tipoSentencia;
	}
	public void setTipoSentencia(List<String> tipoSentencia) {
		this.tipoSentencia = tipoSentencia;
	}
	public List<String> getRubrosEfectoSentencia() {
		return rubrosEfectoSentencia;
	}
	public void setRubrosEfectoSentencia(List<String> rubrosEfectoSentencia) {
		this.rubrosEfectoSentencia = rubrosEfectoSentencia;
	}
	public List<String> getRubrosExtencionInvalidez() {
		return rubrosExtencionInvalidez;
	}
	public void setRubrosExtencionInvalidez(List<String> rubrosExtencionInvalidez) {
		this.rubrosExtencionInvalidez = rubrosExtencionInvalidez;
	}
	public List<String> getActosImpugnados() {
		return actosImpugnados;
	}
	public void setActosImpugnados(List<String> actosImpugnados) {
		this.actosImpugnados = actosImpugnados;
	}
	public List<String> getVotacionRubrosTematicos() {
		return votacionRubrosTematicos;
	}
	public void setVotacionRubrosTematicos(List<String> votacionRubrosTematicos) {
		this.votacionRubrosTematicos = votacionRubrosTematicos;
	}
	public List<String> getArticulosImpugnados() {
		return articulosImpugnados;
	}
	public void setArticulosImpugnados(List<String> articulosImpugnados) {
		this.articulosImpugnados = articulosImpugnados;
	}
	public List<String> getDerechosHumanos() {
		return derechosHumanos;
	}
	public void setDerechosHumanos(List<String> derechosHumanos) {
		this.derechosHumanos = derechosHumanos;
	}
	public String getMetodologiaAnalisisConstitucionalidad() {
		return metodologiaAnalisisConstitucionalidad;
	}
	public void setMetodologiaAnalisisConstitucionalidad(String metodologiaAnalisisConstitucionalidad) {
		this.metodologiaAnalisisConstitucionalidad = metodologiaAnalisisConstitucionalidad;
	}
	public String getTipoViolacionInvacionEsferas() {
		return tipoViolacionInvacionEsferas;
	}
	public void setTipoViolacionInvacionEsferas(String tipoViolacionInvacionEsferas) {
		this.tipoViolacionInvacionEsferas = tipoViolacionInvacionEsferas;
	}
	public String getTipoViolacionInvacionPoderes() {
		return tipoViolacionInvacionPoderes;
	}
	public void setTipoViolacionInvacionPoderes(String tipoViolacionInvacionPoderes) {
		this.tipoViolacionInvacionPoderes = tipoViolacionInvacionPoderes;
	}

	public Boolean getAprobadasConcideraciones() {
		return aprobadasConcideraciones;
	}

	public void setAprobadasConcideraciones(Boolean aprobadasConcideraciones) {
		this.aprobadasConcideraciones = aprobadasConcideraciones;
	}

	public Integer getNumeroVotos() {
		return numeroVotos;
	}

	public void setNumeroVotos(Integer numeroVotos) {
		this.numeroVotos = numeroVotos;
	}

	public List<String> getRubrosTematicosPrincipal() {
		return rubrosTematicosPrincipal;
	}

	public void setRubrosTematicosPrincipal(List<String> rubrosTematicosPrincipal) {
		this.rubrosTematicosPrincipal = rubrosTematicosPrincipal;
	}

	public String getMateriaAnalizadaPorInvacionEsferas() {
		return materiaAnalizadaPorInvacionEsferas;
	}

	public void setMateriaAnalizadaPorInvacionEsferas(String materiaAnalizadaPorInvacionEsferas) {
		this.materiaAnalizadaPorInvacionEsferas = materiaAnalizadaPorInvacionEsferas;
	}

	public String getMateriaAnalizadaPorInvacionPoderes() {
		return materiaAnalizadaPorInvacionPoderes;
	}

	public void setMateriaAnalizadaPorInvacionPoderes(String materiaAnalizadaPorInvacionPoderes) {
		this.materiaAnalizadaPorInvacionPoderes = materiaAnalizadaPorInvacionPoderes;
	}

	public List<String> getMinistros() {
		return ministros;
	}

	public void setMinistros(List<String> ministros) {
		this.ministros = ministros;
	}

	@Override
	public String toString() {
		return "TablaVotaciones{" +
				"id='" + id + '\'' +
				", tipoVotacion='" + tipoVotacion + '\'' +
				", tipoVicio='" + tipoVicio + '\'' +
				", congresoEmitioDecreto='" + congresoEmitioDecreto + '\'' +
				", decretoImpugnado='" + decretoImpugnado + '\'' +
				", rubrosTematicosPrincipal=" + rubrosTematicosPrincipal +
				", rubrosTematicos=" + rubrosTematicos +
				", ministros=" + ministros +
				", votacionFavor=" + votacionFavor +
				", votacionEnContra=" + votacionEnContra +
				", aprobadasConcideraciones=" + aprobadasConcideraciones +
				", sentidoResolucion='" + sentidoResolucion + '\'' +
				", textoVotacion='" + textoVotacion + '\'' +
				", tipoSentencia=" + tipoSentencia +
				", rubrosEfectoSentencia=" + rubrosEfectoSentencia +
				", rubrosExtencionInvalidez=" + rubrosExtencionInvalidez +
				", actosImpugnados=" + actosImpugnados +
				", votacionRubrosTematicos=" + votacionRubrosTematicos +
				", articulosImpugnados=" + articulosImpugnados +
				", derechosHumanos=" + derechosHumanos +
				", metodologiaAnalisisConstitucionalidad='" + metodologiaAnalisisConstitucionalidad + '\'' +
				", tipoViolacionInvacionEsferas='" + tipoViolacionInvacionEsferas + '\'' +
				", tipoViolacionInvacionPoderes='" + tipoViolacionInvacionPoderes + '\'' +
				", numeroVotos=" + numeroVotos +
				", materiaAnalizadaPorInvacionEsferas='" + materiaAnalizadaPorInvacionEsferas + '\'' +
				", materiaAnalizadaPorInvacionPoderes='" + materiaAnalizadaPorInvacionPoderes + '\'' +
				'}';
	}
}
