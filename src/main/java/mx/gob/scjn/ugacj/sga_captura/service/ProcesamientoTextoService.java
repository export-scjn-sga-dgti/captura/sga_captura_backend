package mx.gob.scjn.ugacj.sga_captura.service;

import mx.gob.scjn.ugacj.sga_captura.domain.Catalogo;
import mx.gob.scjn.ugacj.sga_captura.domain.ProcesamientoTexto;
import mx.gob.scjn.ugacj.sga_captura.dto.AnalizaTextoDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public class ProcesamientoTextoService {

    @Value("${urlAnalisisDeTexto}")
    private String urlAnalisisTexto;

    @Autowired
    RestTemplate restTemplate;

    @Bean
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }

    @Autowired
    private CatalogoService _catalogoService;

    public ProcesamientoTexto getTextoAnalizado(AnalizaTextoDTO analisis) {
        ProcesamientoTexto procesamiento = new ProcesamientoTexto();
        procesamiento.setText(analisis.getText());
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<ProcesamientoTexto> entity = new HttpEntity<ProcesamientoTexto>(procesamiento,headers);
        Optional<ProcesamientoTexto> procesado = null;
        List<String> ministros = new ArrayList<>();
        try {
        	procesado = Optional.ofNullable(restTemplate.exchange(urlAnalisisTexto, HttpMethod.POST, entity, ProcesamientoTexto.class).getBody());
        	
        	if (procesado.isPresent()) {
                ministros.addAll(procesado.get().getMinistrosAFavor());
                ministros.addAll(procesado.get().getMinistrosEnContra());
                procesado.get().setMinistros(ministros);
            }
            else {
                procesado = Optional.of(new ProcesamientoTexto());
                List<Catalogo> catalogos = _catalogoService.consultarMinistrosPorLapso(analisis.getFechaResolucion());
                catalogos.forEach(xx -> ministros.add(xx.getNombre()));
                procesado.get().setMinistros(ministros);
            }
        	
        }catch(Exception e) {
        	procesado = Optional.of(new ProcesamientoTexto());
            List<Catalogo> catalogos = _catalogoService.consultarMinistrosPorLapso(analisis.getFechaResolucion());
            catalogos.forEach(xx -> ministros.add(xx.getNombre()));
            procesado.get().setMinistros(ministros);
        }
        return procesado.get();
    }


}
