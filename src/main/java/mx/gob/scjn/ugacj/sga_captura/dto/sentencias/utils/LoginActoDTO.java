package mx.gob.scjn.ugacj.sga_captura.dto.sentencias.utils;

public class LoginActoDTO {
  
        String userName;
        String password;
        
		public String getUserName() {
			return userName;
		}
		public void setUserName(String userName) {
			this.userName = userName;
		}
		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}

}
