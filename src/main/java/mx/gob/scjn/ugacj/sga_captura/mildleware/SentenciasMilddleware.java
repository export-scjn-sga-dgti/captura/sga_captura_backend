package mx.gob.scjn.ugacj.sga_captura.mildleware;

import com.google.gson.JsonObject;
import mx.gob.scjn.ugacj.sga_captura.domain.Sentencia;
import mx.gob.scjn.ugacj.sga_captura.dto.statecode.SentenciaResponseDTO;
import mx.gob.scjn.ugacj.sga_captura.dto.vtaquigraficas.SentenciaIndexObjectDTO;
import mx.gob.scjn.ugacj.sga_captura.service.sentencias.SentenciaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;

import javax.annotation.PostConstruct;
import javax.validation.Valid;

@Component
public class SentenciasMilddleware {

    private static SentenciasMilddleware instance;

    @PostConstruct
    public void registerInstance() {
        instance = this;
    }

    @Autowired
    private SentenciaService _sentenciaService;

    public static String getExistSentencia(String expediente, String asunto) {

        Sentencia sentencia = instance._sentenciaService.ExistSentenciaByVersionesTaq(expediente, asunto);
        if(sentencia != null){
            return sentencia.getId();
        }
        return null;
    }

//    public static SentenciaResponseDTO saveSentencia(SentenciaIndexObjectDTO dtoSentencia)  {
//    	Sentencia sentencia = converteSentencia(dtoSentencia);
//       return instance._sentenciaService.saveSentencia(sentencia);
//    }
//
//    public static final Sentencia updateSentenciaVTaquigrafica(SentenciaIndexObjectDTO dtoSentencia)  {
//
//        return instance._sentenciaService.updateAsuntoVersionTaq(converteSentencia(dtoSentencia));
//    }

    private static Sentencia converteSentencia(SentenciaIndexObjectDTO dtoSentencia){
        Sentencia sentencia = new Sentencia();
        sentencia.setId(dtoSentencia.getId());
        sentencia.setTipoAsunto(dtoSentencia.getTipoAsunto());
        sentencia.setNumeroExpediente(dtoSentencia.getNumeroExpediente());
        sentencia.setPonente(dtoSentencia.getPonente());
        sentencia.setFechaResolucion(dtoSentencia.getFechaResolucion());
        sentencia.setOrganoRadicacion(dtoSentencia.getOrganoRadicacion());
        sentencia.setCA(dtoSentencia.isCa());
        sentencia.setAcumulada(dtoSentencia.isAcumulada());
        sentencia.setAcumulados(dtoSentencia.getAcumulados());
        return  sentencia;
    }

}
