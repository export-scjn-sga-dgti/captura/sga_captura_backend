package mx.gob.scjn.ugacj.sga_captura.dto;

public class PermisosDTO {
	boolean ver;
	boolean crear;
	boolean editar;
	boolean eliminar;
	boolean acceso;
	boolean superadmin;
	boolean asignacionAsuntos;

	public PermisosDTO() {
		super();
		this.ver = true;
		this.crear = true;
		this.editar = true;
		this.eliminar = true;
		this.acceso = true;
		this.superadmin = false;
		this.asignacionAsuntos = false;
	}
	public boolean isVer() {
		return ver;
	}
	public void setVer(boolean ver) {
		this.ver = ver;
	}
	public boolean isCrear() {
		return crear;
	}
	public void setCrear(boolean crear) {
		this.crear = crear;
	}
	public boolean isEditar() {
		return editar;
	}
	public void setEditar(boolean editar) {
		this.editar = editar;
	}
	public boolean isEliminar() {
		return eliminar;
	}
	public void setEliminar(boolean eliminar) {
		this.eliminar = eliminar;
	}
	public boolean isAcceso() {
		return acceso;
	}
	public void setAcceso(boolean acceso) {
		this.acceso = acceso;
	}
	public boolean isSuperadmin() {
		return superadmin;
	}
	public void setSuperadmin(boolean superadmin) {
		this.superadmin = superadmin;
	}
	public boolean isAsignacionAsuntos() {
		return asignacionAsuntos;
	}
	public void setAsignacionAsuntos(boolean asignacionAsuntos) {
		this.asignacionAsuntos = asignacionAsuntos;
	}
	
	
}
