package mx.gob.scjn.ugacj.sga_captura.domain;

import java.util.List;

public class CausaImprocedencia {
    List <String> causaImprocedencia;
    List<String> rubrosTematicos;

    public List<String> getCausaImprocedencia() {
        return this.causaImprocedencia;
    }

    public void setCausaImprocedencia(List<String> causaImprocedencia) {
        this.causaImprocedencia = causaImprocedencia;
    }

    public List<String> getRubrosTematicos() {
		return this.rubrosTematicos;
	}

    public void setRubrosTematicos(List<String> rubrosTematicos) {
		this.rubrosTematicos = rubrosTematicos;
	}

    
}
