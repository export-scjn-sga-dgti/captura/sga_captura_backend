package mx.gob.scjn.ugacj.sga_captura.domain;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "#{@environment.getProperty('indice.vtaquigrafica')}")
public class VersionTaquigrafica implements Serializable{
	@Id
	String id;
	
	//@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
	@NotNull( message ="El campo fecha de sesión no puede estar vacío")
	String fechaSesion;
	
	@NotEmpty(message ="El campo duración de la sesión no puede estar vacío")
	String duracionSesion;
	
	int numeroMinistrasMinistrosParticiparonEnSesion;
	List<String> nombreMinistrasMinistrosPresentesEnSesion;
	List<String> nombreMinistrasMinistrosAusentesEnSesion;
	boolean hasPresidenteDecano;
	String nombrePresidenteDecano;
	double porcentaje;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getFechaSesion() {
		return fechaSesion;
	}
	public void setFechaSesion(String fechaSesion) {
		this.fechaSesion = fechaSesion;
	}
	public String getDuracionSesion() {
		return duracionSesion;
	}
	public void setDuracionSesion(String duracionSesion) {
		this.duracionSesion = duracionSesion;
	}
	public int getNumeroMinistrasMinistrosParticiparonEnSesion() {
		return numeroMinistrasMinistrosParticiparonEnSesion;
	}
	public void setNumeroMinistrasMinistrosParticiparonEnSesion(int numeroMinistrasMinistrosParticiparonEnSesion) {
		this.numeroMinistrasMinistrosParticiparonEnSesion = numeroMinistrasMinistrosParticiparonEnSesion;
	}
	public List<String> getNombreMinistrasMinistrosPresentesEnSesion() {
		nombreMinistrasMinistrosPresentesEnSesion.replaceAll(String::toUpperCase);
		return nombreMinistrasMinistrosPresentesEnSesion;
	}
	public void setNombreMinistrasMinistrosPresentesEnSesion(List<String> nombreMinistrasMinistrosPresentesEnSesion) {
		this.nombreMinistrasMinistrosPresentesEnSesion = nombreMinistrasMinistrosPresentesEnSesion;
	}
	public List<String> getNombreMinistrasMinistrosAusentesEnSesion() {
		nombreMinistrasMinistrosAusentesEnSesion.replaceAll(String::toUpperCase);
		return nombreMinistrasMinistrosAusentesEnSesion;
	}
	public void setNombreMinistrasMinistrosAusentesEnSesion(List<String> nombreMinistrasMinistrosAusentesEnSesion) {
		this.nombreMinistrasMinistrosAusentesEnSesion = nombreMinistrasMinistrosAusentesEnSesion;
	}
	public boolean isHasPresidenteDecano() {
		return hasPresidenteDecano;
	}
	public void setHasPresidenteDecano(boolean hasPresidenteDecano) {
		this.hasPresidenteDecano = hasPresidenteDecano;
	}
	public String getNombrePresidenteDecano() {
		return nombrePresidenteDecano.toUpperCase();
	}
	public void setNombrePresidenteDecano(String nombrePresidenteDecano) {
		this.nombrePresidenteDecano = nombrePresidenteDecano;
	}
	public double getPorcentaje() {
		return porcentaje;
	}
	public void setPorcentaje(double porcentaje) {
		this.porcentaje = porcentaje;
	}	
}
