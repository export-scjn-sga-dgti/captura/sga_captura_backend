package mx.gob.scjn.ugacj.sga_captura.domain;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import mx.gob.scjn.ugacj.sga_captura.dto.migracion.AsuntosAbordados;

@Document(collection="versionesTaquigraficas")
public class Taquigraficas implements Serializable {

private static final long serialVersionUID = 1L;

@Id
String id;
Date fechaSesion;
String duracionSesion;
int numeroMinistrosParticiparon;
AsuntosAbordados asuntos[];
String[] minitrosAusentesEnSesion;
String[] ministrosPresentesEnSesion;
String presidenteDecano;
float porcentaje;

	public String getId() {
		return this.id;
	}

	public void setId(String id ) {
		this.id = id;
	}

	public Date getFechaSesion() {
    return this.fechaSesion;
	}

	public void setFechaSesion(Date fechaSesion) {
    this.fechaSesion = fechaSesion;
	}

	public String getDuracionSesion() {
    return this.duracionSesion;
	}

	public void setDuracionSesion(String duracionSesion) {
    this.duracionSesion = duracionSesion;
	}


	public int getNumeroMinistrosParticiparon() {
    return this.numeroMinistrosParticiparon;
	}

	public void setNumeroMinistrosParticiparon(int numeroMinistrosParticiparon) {
    this.numeroMinistrosParticiparon = numeroMinistrosParticiparon;
	}

	public float getPorcentaje() {
		return this.porcentaje;
	}
	
	public void setPorcentaje(float porcentaje) {
		this.porcentaje = porcentaje;
	}

	public AsuntosAbordados[] getAsuntos() {
		return this.asuntos;
	}

	public void setAsuntos(AsuntosAbordados[] asuntos) {
		this.asuntos = asuntos;
	}


	public String [] getMinitrosAusentesEnSesion() {
		return this.minitrosAusentesEnSesion;
	}
	
	public void setMinitrosAusentesEnSesion(String [] minitrosAusentesEnSesion) {
		this.minitrosAusentesEnSesion = minitrosAusentesEnSesion;
	}

	public String [] getMinistrosPrecentesEnSesion() {
		return this.ministrosPresentesEnSesion;
	}
	
	public void setMinistrosPrecentesEnSesion(String [] ministrosPrecentesEnSesion) {
		this.ministrosPresentesEnSesion = ministrosPrecentesEnSesion; 
	}

	public String getPresidenteDecano() {
		return this.presidenteDecano;
	}
	
	public void setPresidenteDecano(String presidenteDecano) {
		this.presidenteDecano = presidenteDecano;
	}
	
}
