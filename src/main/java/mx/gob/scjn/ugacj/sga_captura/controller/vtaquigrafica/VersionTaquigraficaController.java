package mx.gob.scjn.ugacj.sga_captura.controller.vtaquigrafica;

import java.text.ParseException;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mx.gob.scjn.ugacj.sga_captura.domain.VersionTaquigrafica;
import mx.gob.scjn.ugacj.sga_captura.dto.statecode.VtaqResponseDTO;
import mx.gob.scjn.ugacj.sga_captura.dto.vtaquigraficas.TablaVersionesTaquigraficasDTO;
import mx.gob.scjn.ugacj.sga_captura.dto.vtaquigraficas.VersionTaquigraficaDTO;
import mx.gob.scjn.ugacj.sga_captura.service.vtaquigrafica.VersionTaquigraficaService;

@RestController
@RequestMapping(path = "/api/sga/versiones_taquigraficas")
public class VersionTaquigraficaController {

	@Autowired
	private VersionTaquigraficaService _vtService;
	
	@CrossOrigin()
	@RequestMapping(method = RequestMethod.GET, path = "/", produces = "application/json")
	public Page<TablaVersionesTaquigraficasDTO> getVersionesPaginadas(@RequestParam(name = "page") int page,
			@RequestParam(name = "size") int size, @RequestParam(required = false, name = "filtros") String filtros) throws ParseException {
		Pageable paging = PageRequest.of(page-1, size, Sort.by(Sort.Direction.DESC, "$dateFromString('fechaSesion')"));
		return _vtService.getVersionesPaginadas(paging, filtros);
	}
	
	@CrossOrigin()
	@RequestMapping(method = RequestMethod.GET, path = "/{id}", produces = "application/json")
	public VersionTaquigraficaDTO getVersionTaquigrafica(@PathVariable String id) {
		return _vtService.getVersionTaquigrafica(id);
	}
	
	@RequestMapping(method = RequestMethod.POST, path = "/")
	@CrossOrigin()
	public VtaqResponseDTO saveVersionTaquigrafica(@Valid @RequestBody VersionTaquigrafica versionT) {
		return _vtService.saveVersionTaquigrafica(versionT);
	}
	
	@CrossOrigin()
	@RequestMapping(method = RequestMethod.PUT, path = "/", produces = "application/json")
	public VtaqResponseDTO updateVersionTaquigrafica(@Valid @RequestBody VersionTaquigrafica versionT) {
		return _vtService.updateVersionTaquigrafica(versionT);
	}
	
	@CrossOrigin()
	@RequestMapping(method = RequestMethod.DELETE, path = "/{id}", produces = "application/json")
	public boolean deleteVersionTaquigrafica(@PathVariable String id) {
		return _vtService.deleteVersionTaquigrafica(id);
	}
}
