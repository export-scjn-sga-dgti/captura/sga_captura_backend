package mx.gob.scjn.ugacj.sga_captura.service.migracion;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.gob.scjn.ugacj.sga_captura.domain.Voto;
import mx.gob.scjn.ugacj.sga_captura.domain.VotoOld;
import mx.gob.scjn.ugacj.sga_captura.dto.statecode.ResponseDTO;
import mx.gob.scjn.ugacj.sga_captura.repository.VotoOldRepository;
import mx.gob.scjn.ugacj.sga_captura.repository.VotoRepository;
import mx.gob.scjn.ugacj.sga_captura.service.porcentaje.PorcentajeService;

@Service
public class VotosMigracionService {

	@Autowired
	private VotoRepository votoRepository;

	@Autowired
	private VotoOldRepository votoOldRepository;
	
	@Autowired
	private PorcentajeService porcentajeService;
	
	public List<String> upperCaseListString(List<String> list){
		List<String> newList = new ArrayList<String>();
		for(String val:list) newList.add(val.trim().toUpperCase());
		return newList;
	}

	public ResponseDTO populateVotos() {

		Voto newVoto;
		List<VotoOld> votosOld = votoOldRepository.findAll();

		for (VotoOld oldVoto : votosOld) {
//			newVoto = new Voto();
//			
//			newVoto.setTipoAsunto(oldVoto.getTipoAsunto().trim().toUpperCase());
//			newVoto.setNumExpediente(oldVoto.getNumExpediente());
//			newVoto.setTipoVoto(oldVoto.getTipoVoto().trim().toUpperCase());
//			newVoto.setFechaResolucion(oldVoto.getFechaResolucion());
//			
//			
//			
//			newVoto.setMinistro(upperCaseListString(oldVoto.getMinistro()));
//			newVoto.setTipoTema(oldVoto.getTipoTema().trim().toUpperCase());
//			
//			newVoto.setTipoTemaProcesalSobreElVoto(oldVoto.getTipoTemaProcesalSobreElVoto().trim().toUpperCase());
//			newVoto.setTemaProcesalSobreElVoto(oldVoto.getTemaProcesalSobreElVoto());
//			
//			newVoto.setTipoTemaSustantivoSobreElVoto(oldVoto.getTipoTemaSustantivoSobreElVoto().trim().toUpperCase());
//			newVoto.setTemaSustantivoSobreElVoto(oldVoto.getTemaSustantivoSobreElVoto());
//			
//			newVoto.setMetodologiaAnalisis(oldVoto.getMetodologiaAnalisis().trim().toUpperCase());
//			
//			newVoto.setPorcentaje(oldVoto.getPorcentaje());
//					
//			newVoto.setAcumulados(null);
//			newVoto.setAcumulada(null);
//			newVoto.setCA(null);
//			porcentajeService.updatePorcentaje(newVoto);
//			 votoRepository.save(newVoto);
		}

		return null;
	}

}
