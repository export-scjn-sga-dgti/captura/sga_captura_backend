package mx.gob.scjn.ugacj.sga_captura.domain;

import java.util.List;

public class DerechosHumanos {
    
    List<MinistrosVotacion> ministros;
    String votacionObtenida;
    String preceptos[];
    String text;

    public String getText() {
        return this.text;
    }

    public void setText(String text) {
        this.text = text;
    }

	public String[] getPreceptos() {
		return this.preceptos;
	}

	public void setPreceptos(String preceptos[]) {
		this.preceptos = preceptos;
	}


    public List<MinistrosVotacion> getMinistros() {
        return this.ministros;
    }

    public void setMinistros(List<MinistrosVotacion> ministro) {
        this.ministros = ministro;
    }

    public String getVotacionObtenida() {
        return this.votacionObtenida;
    }

    public void setVotacionObtenida(String votacionObtenida) {
        this.votacionObtenida = votacionObtenida;
    }

}
