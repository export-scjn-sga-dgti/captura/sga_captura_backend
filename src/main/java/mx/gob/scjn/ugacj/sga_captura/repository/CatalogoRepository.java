package mx.gob.scjn.ugacj.sga_captura.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import mx.gob.scjn.ugacj.sga_captura.domain.Catalogo;

public interface CatalogoRepository extends MongoRepository<Catalogo,String>{

	List<Catalogo> findByCatalogoOrderByNombreAsc(String nombrecatalogo);

}
