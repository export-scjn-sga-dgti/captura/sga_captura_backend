package mx.gob.scjn.ugacj.sga_captura.utils.validaciones.AsuntosAbordados;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import mx.gob.scjn.ugacj.sga_captura.domain.AsuntoAbordado;

public class AsuntosAbordadosValidators
		implements ConstraintValidator<ValidatorsAsuntosAbordadosConstrains, AsuntoAbordado> {

	@Override
	public void initialize(ValidatorsAsuntosAbordadosConstrains constraintAnnotation) {
		// ConstraintValidator.super.initialize(constraintAnnotation);
	}

	@Override
	public boolean isValid(AsuntoAbordado asunto, ConstraintValidatorContext constraintValidatorContext) {
		// son campos obligatorios que nunca deben estar vacíos"
		if ((asunto.getAsuntoAbordado() != null && !asunto.getAsuntoAbordado().isEmpty())&&(asunto.getNumeroExpediente() != null && !asunto.getNumeroExpediente().isEmpty())) {
			return true;
		}

		return false;
	}
}
