package mx.gob.scjn.ugacj.sga_captura.dto.vtaquigraficas;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;
import java.util.List;

public class SentenciaIndexObjectDTO {
	String id;
	String tipoAsunto;
	String numeroExpediente;
	String ponente;
	String organoRadicacion = "PLENO";
	//@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
	String fechaResolucion;
	boolean isCa;
	boolean isAcumulada;
	boolean isInUse;
	List<String> acumulados;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getTipoAsunto() {
		return tipoAsunto;
	}
	public void setTipoAsunto(String tipoAsunto) {
		this.tipoAsunto = tipoAsunto;
	}
	public String getNumeroExpediente() {
		return numeroExpediente;
	}
	public void setNumeroExpediente(String numeroExpediente) {
		this.numeroExpediente = numeroExpediente;
	}
	public String getPonente() {
		return ponente;
	}
	public void setPonente(String ponente) {
		this.ponente = ponente;
	}
	public String getFechaResolucion() {
		return fechaResolucion;
	}
	public void setFechaResolucion(String fechaResolucion) {
		this.fechaResolucion = fechaResolucion;
	}
	public boolean isCa() {
		return isCa;
	}
	public void setCa(boolean isCa) {
		this.isCa = isCa;
	}
	public boolean isAcumulada() {
		return isAcumulada;
	}
	public void setAcumulada(boolean isAcumulada) {
		this.isAcumulada = isAcumulada;
	}
	public List<String> getAcumulados() {
		return acumulados;
	}
	public void setAcumulados(List<String> acumulados) {
		this.acumulados = acumulados;
	}
	public String getOrganoRadicacion() {
		return organoRadicacion;
	}
	public void setOrganoRadicacion(String organoRadicacion) {
		this.organoRadicacion = organoRadicacion;
	}
	public boolean isInUse() {
		return isInUse;
	}
	public void setInUse(boolean isInUse) {
		this.isInUse = isInUse;
	}
	
	
}
