package mx.gob.scjn.ugacj.sga_captura.dto.statecode;

import mx.gob.scjn.ugacj.sga_captura.domain.DiversaNormativa;

public class DivNormativaResponseDTO extends ResponseDTO {

	DiversaNormativa  diversaNormativa;
	
	public DivNormativaResponseDTO(int statusCode, String message) {
		super(statusCode, message);
		// TODO Auto-generated constructor stub
	}

	public DiversaNormativa getDiversaNormativa() {
		return diversaNormativa;
	}

	public void setDiversaNormativa(DiversaNormativa diversaNormativa) {
		this.diversaNormativa = diversaNormativa;
	}
	
}
