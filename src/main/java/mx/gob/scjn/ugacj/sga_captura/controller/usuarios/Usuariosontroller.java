package mx.gob.scjn.ugacj.sga_captura.controller.usuarios;

import java.text.ParseException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mx.gob.scjn.ugacj.sga_captura.domain.Usuario;
import mx.gob.scjn.ugacj.sga_captura.service.usuarios.UsuariosService;

@RestController
@RequestMapping(value = "/api/sga/usuario")
public class Usuariosontroller {
	
	@Autowired
	private UsuariosService usuariosService;
	
	@RequestMapping(method = RequestMethod.GET, path = "/")
	public Page<Usuario> getSentenciaAll(@RequestParam(name = "page") Integer page,
			@RequestParam(name = "size") Integer size, @RequestParam(required = false, name = "filtros") String filtros)
			throws ParseException {
		Pageable paging = PageRequest.of(page - 1, size);
		return usuariosService.getAllusuariosPages(paging, filtros);
	}

	@RequestMapping(method = RequestMethod.GET, path = "/{usuarioId}", produces = "application/json")
	public Usuario getSentenciaById(@PathVariable String usuarioId) {
		return usuariosService.getUsuarioById(usuarioId);
	}
	
	@RequestMapping(method = RequestMethod.GET, path = "/username/{username}", produces = "application/json")
	public Usuario getSentenciaByUsername(@PathVariable String username) {
		return usuariosService.getUsuarioByUserName(username);
	}
	
	@RequestMapping(method = RequestMethod.POST, path = "/")
	@CrossOrigin()
	public Usuario saveVersionTaquigrafica(@Valid @RequestBody Usuario usuario) {
		return usuariosService.saveUsuario(usuario);
	}

	@RequestMapping(method = RequestMethod.PUT, path = "/")
	public Usuario updateSentencia(@Valid @RequestBody Usuario usuario) {
		return usuariosService.updateUsuario(usuario);
	}
	
	@CrossOrigin()
	@RequestMapping(method = RequestMethod.DELETE, path = "/{usuarioId}", produces = "application/json")
	public boolean eliminarUsuario(@PathVariable String usuarioId) {
		return usuariosService.eliminarUsuario(usuarioId);
	}

}
