package mx.gob.scjn.ugacj.sga_captura.utils;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;


public class ConvertFechaFiltros {

    public static String convertEngFechaTextToDateString(Date date) {
    	try {
    		DecimalFormat formatter2 = new DecimalFormat("00");

    		Calendar calendar = new GregorianCalendar();
    		calendar.setTime(date);
    		int year = calendar.get(Calendar.YEAR);
    		int month = calendar.get(Calendar.MONTH);
    		int day = calendar.get(Calendar.DAY_OF_MONTH);

    		String aFormattedDay = formatter2.format(day);
    		String aFormattedMonth = formatter2.format(month + 1);
    		return year + "-" + aFormattedMonth + "-" + aFormattedDay + "";
    		//return aFormattedDay + "-" + aFormattedMonth + "-" + year + "";
    	}catch(Exception e) {
    		return null;
    	}
	}
    
    public static String convertEngFechaTextToDateStringv2_(Date date) {

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.ENGLISH);
		formatter.setTimeZone(TimeZone.getTimeZone("America/Chicago"));
		
		String fecha_parte1 = ((StringUtils.substringBefore(formatter.format(date), "T"))+"T00:00:00.000Z").replaceAll("Z$", "+0000");
        Date date2;
		try {
			date2 = (formatter.parse(fecha_parte1));
			
			DecimalFormat formatter2 = new DecimalFormat("00");

			Calendar calendar = new GregorianCalendar();
			calendar.setTime(date2);
			int year = calendar.get(Calendar.YEAR);
			int month = calendar.get(Calendar.MONTH);
			int day = calendar.get(Calendar.DAY_OF_MONTH);

			String aFormattedDay = formatter2.format(day);
			String aFormattedMonth = formatter2.format(month + 1);
			return aFormattedDay + "-" + aFormattedMonth + "-" + year + "";
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
		return null;

	}
    
    public static int textMatches(String text, String regexNumExpediente) {
		Pattern pattern = Pattern.compile(regexNumExpediente);
		Matcher matcher = pattern.matcher(text);
		if (matcher.find())
		{
			return matcher.start();
		}
		return -1;
	}
    
    public static String changeFormat(String fecha, String dateFormat) {
    	int pos = textMatches(fecha, dateFormat);
    	if(pos>=0) {
    		return fecha;
    	}else if(fecha!=null) {
    		fecha = fecha.replace(",", "");
        	String[] fechaArr = fecha.split("-");
        	if(fechaArr.length>1&&fechaArr.length<=3) {
        		return fechaArr[2]+"-"+fechaArr[1]+"-"+fechaArr[0];
        	}	
    	}
    	return null;
    }

//    public static Date getFormatFecha(String fecha) throws ParseException {
//        SimpleDateFormat parser = new SimpleDateFormat("dd/MM/yyyy");
//        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
//        String fecha_parte1 = formatter.format(parser.parse(fecha)) + "T00:00:00.000Z";
//        String defaultTimezone = TimeZone.getDefault().getID();
//        Date date = (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")).parse(fecha_parte1.replaceAll("Z$", "+0000"));
//        return date;
//    }

    public static Date getFormatFecha(String fecha, String format) throws ParseException {
        SimpleDateFormat parser = new SimpleDateFormat(format, Locale.ENGLISH);
        parser.setTimeZone(TimeZone.getTimeZone("America/Toronto"));
        
        
        //String fecha_parte1 = (formatter.format(parser.parse(fecha)) + "T00:00:00.000Z").replaceAll("Z$", "+0000");
        Date date = (parser.parse(fecha));
        return date;
    }
    
    public static Date getFormatFecha(Date fecha) throws ParseException {
    	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.ENGLISH);
    	formatter.setTimeZone(TimeZone.getTimeZone("America/Chicago"));
    	return formatter.parse(formatter.format(fecha));
//        String fecha_parte1 = ((StringUtils.substringBefore(formatter.format(fecha), "T"))+"T00:00:00.000Z").replaceAll("Z$", "+0000");
//        Date date = (formatter.parse(fecha_parte1));
//        return date;
    }
}
