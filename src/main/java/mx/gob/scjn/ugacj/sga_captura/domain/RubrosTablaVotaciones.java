package mx.gob.scjn.ugacj.sga_captura.domain;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonInclude;

@Document(collection = "#{@environment.getProperty('indice.sentencia.votacion.rubros')}")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RubrosTablaVotaciones {

    @Id
    private String id;
    private String tipoRubro;
    private String tipoVotacion;
    private String sentido;
    List<String> rubrosTematicos = new ArrayList<String>();
    private String textoVotacion;
    private List<String> ministros = new ArrayList<String>();
    private List<String> votacionFavor = new ArrayList<String>();
    private List<String> votacionEnContra = new ArrayList<String>();
    private List<String> analisisProcedencia = new ArrayList<String>();
    private List<String> tipoSentencia;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTipoRubro() {
        return tipoRubro;
    }

    public void setTipoRubro(String tipoRubro) {
        this.tipoRubro = tipoRubro;
    }

    public List<String> getRubrosTematicos() {
        return rubrosTematicos;
    }

    public void setRubrosTematicos(List<String> rubrosTematicos) {
        this.rubrosTematicos = rubrosTematicos;
    }

    public List<String> getVotacionFavor() {
        return votacionFavor;
    }

    public void setVotacionFavor(List<String> votacionFavor) {
        this.votacionFavor = votacionFavor;
    }

    public List<String> getVotacionEnContra() {
        return votacionEnContra;
    }

    public void setVotacionEnContra(List<String> votacionEnContra) {
        this.votacionEnContra = votacionEnContra;
    }

    public String getTextoVotacion() {
        return textoVotacion;
    }

    public void setTextoVotacion(String textoVotacion) {
        this.textoVotacion = textoVotacion;
    }

    public String getTipoVotacion() {
        return tipoVotacion;
    }

    public void setTipoVotacion(String tipoVotacion) {
        this.tipoVotacion = tipoVotacion;
    }

    public List<String> getAnalisisProcedencia() {
        return analisisProcedencia;
    }

    public void setAnalisisProcedencia(List<String> analisisProcedencia) {
        this.analisisProcedencia = analisisProcedencia;
    }

    public List<String> getMinistros() {
        return ministros;
    }

    public void setMinistros(List<String> ministros) {
        this.ministros = ministros;
    }
    
    public String getSentido() {
		return sentido;
	}

	public void setSentido(String sentido) {
		this.sentido = sentido;
	}

	public List<String> getTipoSentencia() {
		return tipoSentencia;
	}

	public void setTipoSentencia(List<String> tipoSentencia) {
		this.tipoSentencia = tipoSentencia;
	}

	@Override
	public String toString() {
		return "RubrosTablaVotaciones [id=" + id + ", tipoRubro=" + tipoRubro + ", tipoVotacion=" + tipoVotacion
				+ ", sentido=" + sentido + ", rubrosTematicos=" + rubrosTematicos + ", textoVotacion=" + textoVotacion
				+ ", ministros=" + ministros + ", votacionFavor=" + votacionFavor + ", votacionEnContra="
				+ votacionEnContra + ", analisisProcedencia=" + analisisProcedencia + ", tipoSentencia=" + tipoSentencia
				+ "]";
	}

	
}
