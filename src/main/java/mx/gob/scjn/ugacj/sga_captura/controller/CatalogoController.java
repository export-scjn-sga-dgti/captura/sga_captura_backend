package mx.gob.scjn.ugacj.sga_captura.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import mx.gob.scjn.ugacj.sga_captura.domain.Catalogo;
import mx.gob.scjn.ugacj.sga_captura.dto.CatalogoDTO;
import mx.gob.scjn.ugacj.sga_captura.dto.InformacionDTO;
import mx.gob.scjn.ugacj.sga_captura.service.CatalogoService;

@RestController
@RequestMapping(path = "/api/sga/catalogo")
public class CatalogoController {
	@Autowired
	private CatalogoService _catService;

	@CrossOrigin()
	@RequestMapping(method = RequestMethod.GET, path = "/{nombrePantalla}", produces = "application/json")
	public CatalogoDTO getCatalogosByPantalla(@PathVariable String nombrePantalla,
			@RequestParam(required = false, name = "fecha") String fecha) {
		return _catService.getCatalogosByPantalla(nombrePantalla, fecha);
	}

	@CrossOrigin()
	@RequestMapping(method = RequestMethod.GET, path = "/", produces = "application/json")
	public List<Catalogo> getAllCatalogos() {
		return _catService.getAllCatalogos();
	}

	@CrossOrigin()
	@RequestMapping(method = RequestMethod.GET, path = "/nombre/{nombreCatalogo}", produces = "application/json")
	public List<Catalogo> getCatalogoByNombre(@PathVariable String nombreCatalogo) {
		return _catService.getCatalogoByNombre(nombreCatalogo);
	}
	
	@CrossOrigin()
	@RequestMapping(method = RequestMethod.GET, path = "/id/{idCatalogo}", produces = "application/json")
	public Catalogo getCatalogoById (@PathVariable String idCatalogo){
		return _catService.getCatalogoById(idCatalogo);
	}

	@CrossOrigin()
	@RequestMapping(method = RequestMethod.POST, path = "/", produces = "application/json")
	public Catalogo savecatalogo(@RequestBody Catalogo catalogo) {
		return _catService.saveCatalogo(catalogo);
	}
	
	@CrossOrigin()
	@RequestMapping(method = RequestMethod.PUT, path = "/{idCatalogo}", produces = "application/json")
	public boolean updatecatalogo(@PathVariable String idCatalogo, @RequestBody Catalogo catalogo) {
		return _catService.updateCatalogo(idCatalogo, catalogo);
	}
	
	@CrossOrigin()
	@RequestMapping(method = RequestMethod.DELETE, path = "/{idCatalogo}", produces = "application/json")
	public boolean deletecatalogo(@PathVariable String idCatalogo) {
		return _catService.deleteCatalogo(idCatalogo);
	}
	
	@CrossOrigin()
    @RequestMapping(value="/campo-coleccion/", method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody List<InformacionDTO> consultarMinistrosAnio(@RequestParam(name = "info") String info, @RequestParam(name = "coleccion") String coleccion, @RequestParam(name = "campo") String campo){
        return _catService.consultarDatosPorColeccionYCampo(info,coleccion,campo);
    }
}
