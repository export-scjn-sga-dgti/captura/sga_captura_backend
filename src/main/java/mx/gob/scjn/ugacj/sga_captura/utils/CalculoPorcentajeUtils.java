package mx.gob.scjn.ugacj.sga_captura.utils;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import mx.gob.scjn.ugacj.sga_captura.domain.porcentaje.Porcentaje;
import mx.gob.scjn.ugacj.sga_captura.dto.PorcentajeDTO;
import mx.gob.scjn.ugacj.sga_captura.dto.porcentaje.FieldValidationDTO;
import mx.gob.scjn.ugacj.sga_captura.repository.porcentaje.PorcentajeRepository;

public class CalculoPorcentajeUtils {

	@Autowired
	private PorcentajeRepository porcentajeRepository;

	public double getPorcentaje(Field field, Object objClass, FieldValidationDTO fieldSntcData) {
		double porcentageTotal = 0d;
		String value = getFieldValue(field, objClass);
		if (fieldSntcData.getCriterio().equals("NotEmpty") && value != null) {
			return fieldSntcData.getValue();
		} else if (fieldSntcData.getCriterio().equals("OptionEmbebed-container") && value != null) {
			for (FieldValidationDTO fieldVEmbbd : fieldSntcData.getFieldsValidation()) {
				porcentageTotal += getPorcentaje(field, objClass, fieldVEmbbd);
			}
		} else if (fieldSntcData.getCriterio().equals("OptionEmbebed-DefaultNotEmpty") && value != null) {
			if (fieldSntcData.getOptionValue().contains(value.toUpperCase())) {// Si seleccionaron la opción para
																				// validar un subcampo anidado
				for (FieldValidationDTO fieldVEmbbd : fieldSntcData.getFieldsValidation()) {
					Class<?> clazz = objClass.getClass();
					try {
						Field fieldEmb = clazz.getDeclaredField(fieldVEmbbd.getFieldName());
						porcentageTotal += getPorcentaje(fieldEmb, objClass, fieldVEmbbd);
					} catch (NoSuchFieldException e) {
						e.printStackTrace();
						return 0d;
					} catch (SecurityException e) {
						e.printStackTrace();
						return 0d;
					}
				}
			} else {// Si seleccionaron una opción para validar que solo tenga valor
				return fieldSntcData.getValue();
			}
		} else {
			return 0d;
		}
		return porcentageTotal;
	}

	public double calculoPorcentajev2(Object objClass, String form) {
		double porcentageTotal = 0d;
		List<Porcentaje> fieldsSentencias = porcentajeRepository.findByForm(form);
		if (objClass != null) {
			Class<?> clazz = objClass.getClass();
			for (Field field : clazz.getDeclaredFields()) {// Recorre todos los campos de la clase
				Porcentaje fieldSntcDataP = fieldsSentencias.stream()
						.filter(fieldSntc -> field.getName().equals(fieldSntc.getFieldName())).findAny().orElse(null);
				if (fieldSntcDataP != null) {
					FieldValidationDTO fieldSntcData = new FieldValidationDTO();
					fieldSntcData.setCriterio(fieldSntcDataP.getCriterio());
					fieldSntcData.setFieldName(fieldSntcDataP.getCriterio());
					fieldSntcData.setFieldsValidation(fieldSntcDataP.getFieldsValidation());
					fieldSntcData.setOptionValue(fieldSntcDataP.getOptionValue());
					fieldSntcData.setValue(fieldSntcDataP.getValue());
					
					porcentageTotal += getPorcentaje(field, objClass, fieldSntcData);
				}
			}
		}
		return porcentageTotal;
	}

	public static double calculoPorcentaje(Object objClass, PorcentajeDTO porcentajeDTO) {

		if (objClass == null) {
			return 0d;
		}
		double porcentageTotal = 0d;
		Class<?> clazz = objClass.getClass();
//		Sección donde se validan que los campos estén llenos
		for (Field field : clazz.getDeclaredFields()) {
			if (porcentajeDTO.getFields().containsKey(field.getName())) {
				String value = getFieldValue(field, objClass);
				if (value != null) {
					porcentageTotal += porcentajeDTO.getFields().get(field.getName());
//					System.out.println("Contains: " + field.getName() + " Value: " + value);
				} else {
//					System.out.println("No se llenó: " + field.getName() + " Value: " + value);
				}
			}
		}
//		Este es la sección donde se validan los campos de restricción
		for (Map.Entry<String, String> entry : porcentajeDTO.getFieldsConstraint().entrySet()) {
			try {
				Field field = clazz.getDeclaredField(entry.getKey());
				String value = getFieldValue(field, objClass);
				if (value.equals("true")) {
//					System.out.println("Restricción True: "+entry.getKey());
					Field fieldV = clazz.getDeclaredField(entry.getValue());
					String valueV = getFieldValue(fieldV, objClass);
					if (valueV == null) {
						porcentageTotal = porcentageTotal - porcentajeDTO.getValC();
//						System.out.println("No se llenó el campo de " + entry.getValue());
					} else {
//						System.out.println("Contains: " + fieldV.getName() + " Value: " + valueV);
					}
				}
			} catch (NoSuchFieldException | SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return porcentageTotal < 0 ? 0 : porcentageTotal;
	}

	private static String getFieldValue(Field field, Object objClass) {
		try {
			field.setAccessible(true);
			Class clazz = null;
			try {
				clazz = field.get(objClass).getClass();
			} catch (Exception e) {
				return null;
			}

			Gson g = new Gson();
			String value = field.get(objClass).toString();
			if (clazz.isInstance(new Date()) | clazz.isInstance(new String()) | clazz.isInstance(new Integer(0))
					| clazz.isInstance(new Boolean(false))) {
				if (value != null) {
					return value.equals("") ? null : value;
				}
			} else if (clazz.isInstance(new ArrayList()) || clazz.getSimpleName().equals("ArrayList")) {
				Object fieldValue = field.get(objClass);
				value = "";
				if (fieldValue instanceof List) { // raw type
					for (Object element : ((List) fieldValue)) {
						value += (element + ",");
					}
				}
				return value.equals("") ? null : value;
			} else {
				System.out.println("Tipo faltante:" + clazz.getSimpleName());
				return null;
			}
		} catch (IllegalArgumentException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("fuera foco");
		return null;
	}

	public static List<FieldValidationDTO> fieldsValidationSentencias() {
		List<FieldValidationDTO> listFieldsVal = new ArrayList<FieldValidationDTO>();

		listFieldsVal.add(new FieldValidationDTO("tipoAsunto", 5d, "NotEmpty"));
		listFieldsVal.add(new FieldValidationDTO("numeroExpediente", 5d, "NotEmpty"));
		listFieldsVal.add(new FieldValidationDTO("organoRadicacion", 5d, "NotEmpty"));
		listFieldsVal.add(new FieldValidationDTO("fechaResolucion", 5d, "NotEmpty"));
		listFieldsVal.add(new FieldValidationDTO("ponente", 5d, "NotEmpty"));
		// tipoPersonaPromovente
//		List<FieldValidationDTO> tipoPersonaPromoventeFV = new ArrayList<FieldValidationDTO>();
//		tipoPersonaPromoventeFV.add(new FieldValidationDTO("tipoPersonaJuridicoColectiva", 5d, "NotEmpty"));
//		
//		listFieldsVal.add(new FieldValidationDTO("tipoPersonaPromovente", 5d, "OptionEmbebed-DefaultNotEmpty", 
//				new ArrayList<String>(Arrays.asList("JURÍDICO COLECTIVA OFICIAL", "JURÍDICO COLECTIVA PRIVADA")), 
//				tipoPersonaPromoventeFV));
//		//End

//		***Validación campo: tipoViolacionPlanteadaenDemanda

		// Opción: SÓLO VIOLACIONES A DERECHOS HUMANOS
		List<FieldValidationDTO> soloViolacionesDerHum_Options = new ArrayList<FieldValidationDTO>();
		soloViolacionesDerHum_Options
				.add(new FieldValidationDTO("derechosHumanosCuyaViolacionPlantea", 25d, "NotEmpty"));
		FieldValidationDTO tipoViolacionPlanteadaenDemanda = new FieldValidationDTO("tipoViolacionPlanteadaenDemanda",
				"optionEmbebed-DefaultNotEmpty",
				new ArrayList<String>(Arrays.asList("SÓLO VIOLACIONES A DERECHOS HUMANOS")), 25d,
				soloViolacionesDerHum_Options);
		// End Opción

		// Opción: SÓLO VIOLACIONES A DERECHOS HUMANOS
		List<FieldValidationDTO> soloViolacionesOrganicas_Options = new ArrayList<FieldValidationDTO>();
		soloViolacionesDerHum_Options
				.add(new FieldValidationDTO("derechosHumanosCuyaViolacionPlantea", 25d, "NotEmpty"));
		FieldValidationDTO violacionOrganicaPlanteada = new FieldValidationDTO("violacionOrganicaPlanteada",
				"optionEmbebed-DefaultNotEmpty", new ArrayList<String>(Arrays.asList("SÓLO VIOLACIONES ORGÁNICAS")),
				25d, soloViolacionesDerHum_Options);
		// End Opción

		List<FieldValidationDTO> tipoViolacionPlanteadaenDemanda_Options = new ArrayList<FieldValidationDTO>();
		tipoViolacionPlanteadaenDemanda_Options.add(tipoViolacionPlanteadaenDemanda);

		listFieldsVal.add(new FieldValidationDTO("tipoViolacionPlanteadaenDemanda", "optionEmbebed-container", 25d,
				tipoViolacionPlanteadaenDemanda_Options));
//		***End Validación campo

		listFieldsVal.add(new FieldValidationDTO("tipoViolacionPlanteadaenDemanda", 5d, "NotEmpty"));

		listFieldsVal.add(new FieldValidationDTO("violacionOrganicaPlanteada", 5d, "NotEmpty"));
		listFieldsVal.add(new FieldValidationDTO("tipoViciosProcesoLegislativo", 5d, "NotEmpty"));
		listFieldsVal.add(new FieldValidationDTO("tipoViolacionInvacionEsferas", 5d, "NotEmpty"));
		listFieldsVal.add(new FieldValidationDTO("tipoViolacionInvacionPoderes", 5d, "NotEmpty"));
		listFieldsVal.add(new FieldValidationDTO("violacionOrganicaAnalizadaenSentencia", 5d, "NotEmpty"));
		listFieldsVal
				.add(new FieldValidationDTO("tipoViolacionInvacionEsferasMateriaAnalizadaenSentencia", 5d, "NotEmpty"));
		listFieldsVal.add(new FieldValidationDTO("materiaAnalizadaInvasionEsferas", 5d, "NotEmpty"));
		listFieldsVal.add(new FieldValidationDTO("tipoViolacionInvacionPoderesAnalizadoenSentencia", 5d, "NotEmpty"));
		listFieldsVal.add(new FieldValidationDTO("momentoSurteEfectoSentencia", 5d, "NotEmpty"));
		listFieldsVal.add(new FieldValidationDTO("numeroVotacionesRealizadas", 5d, "NotEmpty"));
		listFieldsVal.add(new FieldValidationDTO("numeroVotacionesRegistradas", 3d, "NotEmpty"));
		listFieldsVal.add(new FieldValidationDTO("tramiteEngrose", 3d, "NotEmpty"));
		listFieldsVal
				.add(new FieldValidationDTO("diasTranscurridosEntreDictadoSentenciayFirmaEngrose", 3d, "NotEmpty"));
		listFieldsVal.add(new FieldValidationDTO("fechaSentencia", 3d, "NotEmpty"));
		listFieldsVal.add(new FieldValidationDTO("fechaFirmaEngrose", 3d, "NotEmpty"));

		return listFieldsVal;

	}

	public static PorcentajeDTO mapaPorcetajeSentencia() {
		PorcentajeDTO porcentajeDTO = new PorcentajeDTO();

//		Campo - Valor (peso del campo en el llenado total de la versión taquigrafica)
		Map<String, Double> fields = new HashMap<>();

		fields.put("tipoAsunto", 5d);
		fields.put("numeroExpediente", 5d);
		fields.put("organoRadicacion", 5d);
		fields.put("fechaResolucion", 5d);
		fields.put("ponente", 5d);
		fields.put("tipoPersonaPromovente", 5d);
		fields.put("tipoViolacionPlanteadaenDemanda", 5d);
		fields.put("violacionOrganicaPlanteada", 5d);
		fields.put("tipoViciosProcesoLegislativo", 5d);
		fields.put("tipoViolacionInvacionEsferas", 5d);
		fields.put("tipoViolacionInvacionPoderes", 5d);
		fields.put("violacionOrganicaAnalizadaenSentencia", 5d);
		fields.put("tipoViolacionInvacionEsferasMateriaAnalizadaenSentencia", 5d);
		fields.put("materiaAnalizadaInvasionEsferas", 5d);
		fields.put("tipoViolacionInvacionPoderesAnalizadoenSentencia", 5d);
		fields.put("momentoSurteEfectoSentencia", 5d);
		fields.put("numeroVotacionesRealizadas", 5d);
		fields.put("numeroVotacionesRegistradas", 3d);
		fields.put("tramiteEngrose", 3d);
		fields.put("diasTranscurridosEntreDictadoSentenciayFirmaEngrose", 3d);
		fields.put("fechaSentencia", 3d);
		fields.put("fechaFirmaEngrose", 3d);

		porcentajeDTO.setFields(fields);

//		Valor global de los campos de restricción, si no se llena el campo le restamos, caso contrario no se contempla en el valor final
		porcentajeDTO.setValC(10d);

//		Los campos de restricción funcionan solo cuando hay un campo booleano marcado como true y requiere que su campo valor sea llenado; pero no se llenó.
//		El prmero es el campo booleano y el segundo el campo que deberá ser llenado.
		Map<String, String> fieldsConstraint = new HashMap<>();
		porcentajeDTO.setFieldsConstraint(fieldsConstraint);
		return porcentajeDTO;

	}

	public static PorcentajeDTO mapaPorcetajeVersionTaq() {
		PorcentajeDTO porcentajeDTO = new PorcentajeDTO();

//		Campo - Valor (peso del campo en el llenado total de la versión taquigrafica)
		Map<String, Double> fields = new HashMap<>();
		fields.put("fechaSesion", 30d);
		fields.put("duracionSesion", 30d);
		fields.put("numeroMinistrasMinistrosParticiparonEnSesion", 40d);
		porcentajeDTO.setFields(fields);

//		Valor global de los campos de restricción, si no se llena el campo le restamos, caso contrario no se contempla en el valor final
		porcentajeDTO.setValC(10d);

//		Los campos de restricción funcionan solo cuando hay un campo booleano marcado como true y requiere que su campo valor sea llenado; pero no se llenó.
//		El prmero es el campo booleano y el segundo el campo que deberá ser llenado.
		Map<String, String> fieldsConstraint = new HashMap<>();
		fieldsConstraint.put("hasPresidenteDecano", "nombrePresidenteDecano");
		porcentajeDTO.setFieldsConstraint(fieldsConstraint);
		return porcentajeDTO;
	}

	public static PorcentajeDTO mapaPorcetajeAsuntoAbordado() {
		PorcentajeDTO porcentajeDTO = new PorcentajeDTO();

//		Campo - Valor (peso del campo en el llenado total de la versión taquigrafica)
		Map<String, Double> fields = new HashMap<>();
		fields.put("asuntoAbordado", 50d);
		fields.put("numeroExpediente", 50d);
		porcentajeDTO.setFields(fields);

//		Valor global de los campos de restricción, si no se llena el campo le restamos, caso contrario no se contempla en el valor final
		porcentajeDTO.setValC(40d);

//		Los campos de restricción funcionan solo cuando hay un campo booleano marcado como true y requiere que su campo valor sea llenado; pero no se llenó.
//		El prmero es el campo booleano y el segundo el campo que deberá ser llenado.
		Map<String, String> fieldsConstraint = new HashMap<>();
		fieldsConstraint.put("isAcumulada", "acumulados");
		porcentajeDTO.setFieldsConstraint(fieldsConstraint);
		return porcentajeDTO;
	}

	public static PorcentajeDTO mapaPorcetajeVTaquigraficaAsuntos() {
		PorcentajeDTO porcentajeDTO = new PorcentajeDTO();

//		Campo - Valor (peso del campo en el llenado total de la versión taquigrafica)
		Map<String, Double> fields = new HashMap<>();
		fields.put("ponente", 20d);
		fields.put("temasProcesales", 30d);
		fields.put("temasFondo", 30d);
		fields.put("numeroMinistrosParticiparonEnAsunto", 20d);
		porcentajeDTO.setFields(fields);

//		Valor global de los campos de restricción, si no se llena el campo le restamos, caso contrario no se contempla en el valor final
		porcentajeDTO.setValC(40d);

//		Los campos de restricción funcionan solo cuando hay un campo booleano marcado como true y requiere que su campo valor sea llenado; pero no se llenó.
//		El prmero es el campo booleano y el segundo el campo que deberá ser llenado.
		Map<String, String> fieldsConstraint = new HashMap<>();
		porcentajeDTO.setFieldsConstraint(fieldsConstraint);
		return porcentajeDTO;
	}

	public static double round(double value, int places) {
		if (places < 0)
			throw new IllegalArgumentException();

		BigDecimal bd = BigDecimal.valueOf(value);
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}

}
