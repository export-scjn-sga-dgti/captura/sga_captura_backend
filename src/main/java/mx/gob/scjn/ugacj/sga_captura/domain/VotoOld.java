package mx.gob.scjn.ugacj.sga_captura.domain;

import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonFormat;

@Document(collection = "voto")
public class VotoOld {

	private static final long serialVersionUID = 1L;

	private String id;
	private String tipoVoto;
	private List<String> ministro;
	private String tipoTemaProcesalSobreElVoto;
	private String temaProcesalSobreElVoto;
	private String tipoTemaSustantivoSobreElVoto;
	private String temaSustantivoSobreElVoto;
	private String tipoTema;
	private String tipoAsunto;
	private String numExpediente;
	private Double porcentaje;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
	private Date fechaResolucion;
	private String metodologiaAnalisis;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTipoVoto() {
		return tipoVoto;
	}

	public void setTipoVoto(String tipoVoto) {
		this.tipoVoto = tipoVoto;
	}

	public List<String> getMinistro() {
		return ministro;
	}

	public void setMinistro(List<String> ministro) {
		this.ministro = ministro;
	}

	public String getTipoTemaProcesalSobreElVoto() {
		return tipoTemaProcesalSobreElVoto;
	}

	public void setTipoTemaProcesalSobreElVoto(String tipoTemaProcesalSobreElVoto) {
		this.tipoTemaProcesalSobreElVoto = tipoTemaProcesalSobreElVoto;
	}

	public String getTemaProcesalSobreElVoto() {
		return temaProcesalSobreElVoto;
	}

	public void setTemaProcesalSobreElVoto(String temaProcesalSobreElVoto) {
		this.temaProcesalSobreElVoto = temaProcesalSobreElVoto;
	}

	public String getTipoTemaSustantivoSobreElVoto() {
		return tipoTemaSustantivoSobreElVoto;
	}

	public void setTipoTemaSustantivoSobreElVoto(String tipoTemaSustantivoSobreElVoto) {
		this.tipoTemaSustantivoSobreElVoto = tipoTemaSustantivoSobreElVoto;
	}

	public String getTemaSustantivoSobreElVoto() {
		return temaSustantivoSobreElVoto;
	}

	public void setTemaSustantivoSobreElVoto(String temaSustantivoSobreElVoto) {
		this.temaSustantivoSobreElVoto = temaSustantivoSobreElVoto;
	}

	public String getTipoTema() {
		return tipoTema;
	}

	public void setTipoTema(String tipoTema) {
		this.tipoTema = tipoTema;
	}

	public String getTipoAsunto() {
		return tipoAsunto;
	}

	public void setTipoAsunto(String tipoAsunto) {
		this.tipoAsunto = tipoAsunto;
	}

	public String getNumExpediente() {
		return numExpediente;
	}

	public void setNumExpediente(String numExpediente) {
		this.numExpediente = numExpediente;
	}

	public Double getPorcentaje() {
		return porcentaje;
	}

	public void setPorcentaje(Double porcentaje) {
		this.porcentaje = porcentaje;
	}

	public Date getFechaResolucion() {
		return fechaResolucion;
	}

	public void setFechaResolucion(Date fechaResolucion) {
		this.fechaResolucion = fechaResolucion;
	}

	public String getMetodologiaAnalisis() {
		return metodologiaAnalisis;
	}

	public void setMetodologiaAnalisis(String metodologiaAnalisis) {
		this.metodologiaAnalisis = metodologiaAnalisis;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
