package mx.gob.scjn.ugacj.sga_captura.domain;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Document(collection = "#{@environment.getProperty('indice.materiaregulacion')}")
@JsonInclude(Include.NON_NULL)
public class MateriaRegulacion {
	@Id
	String id;
	String idDiversaNormativa;
	String materiaRegulacion;
	List<String> temas;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIdDiversaNormativa() {
		return idDiversaNormativa;
	}
	public void setIdDiversaNormativa(String idDiversaNormativa) {
		this.idDiversaNormativa = idDiversaNormativa;
	}
	public String getMateriaRegulacion() {
		return materiaRegulacion;
	}
	public void setMateriaRegulacion(String materiaRegulacion) {
		this.materiaRegulacion = materiaRegulacion;
	}
	public List<String> getTemas() {
		return temas;
	}
	public void setTemas(List<String> temas) {
		this.temas = temas;
	}
}
