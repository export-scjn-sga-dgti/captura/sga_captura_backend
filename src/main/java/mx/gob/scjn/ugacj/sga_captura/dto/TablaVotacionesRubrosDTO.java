package mx.gob.scjn.ugacj.sga_captura.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import mx.gob.scjn.ugacj.sga_captura.domain.RubrosTablaVotaciones;

import javax.validation.constraints.NotBlank;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class TablaVotacionesRubrosDTO {

    String id;
    String idSentencia;
    String tipoVotacion;
    List<String> tipoVicio;
    String congresoEmitioDecreto;
    String decretoImpugnado;
    List<String> rubrosTematicosPrincipal = new ArrayList<String>();
    List<RubrosTablaVotaciones> rubrosTematicos = new ArrayList<RubrosTablaVotaciones>();
    List<String> ministros = new ArrayList<String>();
    List<String> votacionFavor = new ArrayList<String>();
    List<String> votacionEnContra = new ArrayList<String>();
    Boolean aprobadasConcideraciones = false;
    String sentidoResolucion;

    @NotBlank(message = "Este campo no debe ser nulo ni estar vacio")
    String textoVotacion;

    List<String> tipoSentencia;
    List<RubrosTablaVotaciones> rubrosEfectoSentencia;
    List<RubrosTablaVotaciones> rubrosExtencionInvalidez;
    List<String> actosImpugnados;
    List<String> votacionRubrosTematicos;
    List<String> articulosImpugnados;
    List<String> derechosHumanos;
    String metodologiaAnalisisConstitucionalidad;
    String tipoViolacionInvacionEsferas;
    String tipoViolacionInvacionPoderes;
    private Integer numeroVotos;
    private String materiaAnalizadaPorInvacionEsferas;
    private String materiaAnalizadaPorInvacionPoderes;

    
    
    public String getIdSentencia() {
		return idSentencia;
	}

	public void setIdSentencia(String idSentencia) {
		this.idSentencia = idSentencia;
	}

	public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTipoVotacion() {
        return tipoVotacion;
    }

    public void setTipoVotacion(String tipoVotacion) {
        this.tipoVotacion = tipoVotacion;
    }

    

    public List<String> getTipoVicio() {
		return tipoVicio;
	}

	public void setTipoVicio(List<String> tipoVicio) {
		this.tipoVicio = tipoVicio;
	}

	public String getCongresoEmitioDecreto() {
        return congresoEmitioDecreto;
    }

    public void setCongresoEmitioDecreto(String congresoEmitioDecreto) {
        this.congresoEmitioDecreto = congresoEmitioDecreto;
    }

    public String getDecretoImpugnado() {
        return decretoImpugnado;
    }

    public void setDecretoImpugnado(String decretoImpugnado) {
        this.decretoImpugnado = decretoImpugnado;
    }

    public List<RubrosTablaVotaciones> getRubrosTematicos() {
        return rubrosTematicos;
    }

    public void setRubrosTematicos(List<RubrosTablaVotaciones> rubrosTematicos) {
        this.rubrosTematicos = rubrosTematicos;
    }

    public List<String> getVotacionFavor() {
        return votacionFavor;
    }

    public void setVotacionFavor(List<String> votacionFavor) {
        this.votacionFavor = votacionFavor;
    }

    public List<String> getVotacionEnContra() {
        return votacionEnContra;
    }

    public void setVotacionEnContra(List<String> votacionEnContra) {
        this.votacionEnContra = votacionEnContra;
    }

    public Boolean getAprobadasConcideraciones() {
        return aprobadasConcideraciones;
    }

    public void setAprobadasConcideraciones(Boolean aprobadasConcideraciones) {
        this.aprobadasConcideraciones = aprobadasConcideraciones;
    }

    public String getSentidoResolucion() {
        return sentidoResolucion;
    }

    public void setSentidoResolucion(String sentidoResolucion) {
        this.sentidoResolucion = sentidoResolucion;
    }

    public String getTextoVotacion() {
        return textoVotacion;
    }

    public void setTextoVotacion(String textoVotacion) {
        this.textoVotacion = textoVotacion;
    }

    public List<String> getTipoSentencia() {
        return tipoSentencia;
    }

    public void setTipoSentencia(List<String> tipoSentencia) {
        this.tipoSentencia = tipoSentencia;
    }

    public List<RubrosTablaVotaciones> getRubrosEfectoSentencia() {
        return rubrosEfectoSentencia;
    }

    public void setRubrosEfectoSentencia(List<RubrosTablaVotaciones> rubrosEfectoSentencia) {
        this.rubrosEfectoSentencia = rubrosEfectoSentencia;
    }

    public List<RubrosTablaVotaciones> getRubrosExtencionInvalidez() {
        return rubrosExtencionInvalidez;
    }

    public void setRubrosExtencionInvalidez(List<RubrosTablaVotaciones> rubrosExtencionInvalidez) {
        this.rubrosExtencionInvalidez = rubrosExtencionInvalidez;
    }

    public List<String> getActosImpugnados() {
        return actosImpugnados;
    }

    public void setActosImpugnados(List<String> actosImpugnados) {
        this.actosImpugnados = actosImpugnados;
    }

    public List<String> getVotacionRubrosTematicos() {
        return votacionRubrosTematicos;
    }

    public void setVotacionRubrosTematicos(List<String> votacionRubrosTematicos) {
        this.votacionRubrosTematicos = votacionRubrosTematicos;
    }

    public List<String> getArticulosImpugnados() {
        return articulosImpugnados;
    }

    public void setArticulosImpugnados(List<String> articulosImpugnados) {
        this.articulosImpugnados = articulosImpugnados;
    }

    public List<String> getDerechosHumanos() {
        return derechosHumanos;
    }

    public void setDerechosHumanos(List<String> derechosHumanos) {
        this.derechosHumanos = derechosHumanos;
    }

    public String getMetodologiaAnalisisConstitucionalidad() {
        return metodologiaAnalisisConstitucionalidad;
    }

    public void setMetodologiaAnalisisConstitucionalidad(String metodologiaAnalisisConstitucionalidad) {
        this.metodologiaAnalisisConstitucionalidad = metodologiaAnalisisConstitucionalidad;
    }

    public String getTipoViolacionInvacionEsferas() {
        return tipoViolacionInvacionEsferas;
    }

    public void setTipoViolacionInvacionEsferas(String tipoViolacionInvacionEsferas) {
        this.tipoViolacionInvacionEsferas = tipoViolacionInvacionEsferas;
    }

    public String getTipoViolacionInvacionPoderes() {
        return tipoViolacionInvacionPoderes;
    }

    public void setTipoViolacionInvacionPoderes(String tipoViolacionInvacionPoderes) {
        this.tipoViolacionInvacionPoderes = tipoViolacionInvacionPoderes;
    }

    public Integer getNumeroVotos() {
        return numeroVotos;
    }

    public void setNumeroVotos(Integer numeroVotos) {
        this.numeroVotos = numeroVotos;
    }

    public List<String> getRubrosTematicosPrincipal() {
        return rubrosTematicosPrincipal;
    }

    public void setRubrosTematicosPrincipal(List<String> rubrosTematicosPrincipal) {
        this.rubrosTematicosPrincipal = rubrosTematicosPrincipal;
    }

    public String getMateriaAnalizadaPorInvacionEsferas() {
        return materiaAnalizadaPorInvacionEsferas;
    }

    public void setMateriaAnalizadaPorInvacionEsferas(String materiaAnalizadaPorInvacionEsferas) {
        this.materiaAnalizadaPorInvacionEsferas = materiaAnalizadaPorInvacionEsferas;
    }

    public String getMateriaAnalizadaPorInvacionPoderes() {
        return materiaAnalizadaPorInvacionPoderes;
    }

    public void setMateriaAnalizadaPorInvacionPoderes(String materiaAnalizadaPorInvacionPoderes) {
        this.materiaAnalizadaPorInvacionPoderes = materiaAnalizadaPorInvacionPoderes;
    }

    public List<String> getMinistros() {
        return ministros;
    }

    public void setMinistros(List<String> ministros) {
        this.ministros = ministros;
    }

    @Override
    public String toString() {
        return "TablaVotacionesRubrosDTO{" +
                "id='" + id + '\'' +
                ", tipoVotacion='" + tipoVotacion + '\'' +
                ", tipoVicio='" + tipoVicio + '\'' +
                ", congresoEmitioDecreto='" + congresoEmitioDecreto + '\'' +
                ", decretoImpugnado='" + decretoImpugnado + '\'' +
                ", rubrosTematicosPrincipal=" + rubrosTematicosPrincipal +
                ", rubrosTematicos=" + rubrosTematicos +
                ", ministros=" + ministros +
                ", votacionFavor=" + votacionFavor +
                ", votacionEnContra=" + votacionEnContra +
                ", aprobadasConcideraciones=" + aprobadasConcideraciones +
                ", sentidoResolucion='" + sentidoResolucion + '\'' +
                ", textoVotacion='" + textoVotacion + '\'' +
                ", tipoSentencia=" + tipoSentencia +
                ", rubrosEfectoSentencia=" + rubrosEfectoSentencia +
                ", rubrosExtencionInvalidez=" + rubrosExtencionInvalidez +
                ", actosImpugnados=" + actosImpugnados +
                ", votacionRubrosTematicos=" + votacionRubrosTematicos +
                ", articulosImpugnados=" + articulosImpugnados +
                ", derechosHumanos=" + derechosHumanos +
                ", metodologiaAnalisisConstitucionalidad='" + metodologiaAnalisisConstitucionalidad + '\'' +
                ", tipoViolacionInvacionEsferas='" + tipoViolacionInvacionEsferas + '\'' +
                ", tipoViolacionInvacionPoderes='" + tipoViolacionInvacionPoderes + '\'' +
                ", numeroVotos=" + numeroVotos +
                ", materiaAnalizadaPorInvacionEsferas='" + materiaAnalizadaPorInvacionEsferas + '\'' +
                ", materiaAnalizadaPorInvacionPoderes='" + materiaAnalizadaPorInvacionPoderes + '\'' +
                '}';
    }
}
