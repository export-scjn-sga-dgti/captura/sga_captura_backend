package mx.gob.scjn.ugacj.sga_captura.dto.migracion;

import java.util.List;

public class VistasOldDTO {

	String nombre;
    List<Boolean> campos;
    List<UsuarioCatalogoOldDTO> catalogo;
	Boolean visor;
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public List<Boolean> getCampos() {
		return campos;
	}
	public void setCampos(List<Boolean> campos) {
		this.campos = campos;
	}
	public List<UsuarioCatalogoOldDTO> getCatalogo() {
		return catalogo;
	}
	public void setCatalogo(List<UsuarioCatalogoOldDTO> catalogo) {
		this.catalogo = catalogo;
	}
	public Boolean getVisor() {
		return visor;
	}
	public void setVisor(Boolean visor) {
		this.visor = visor;
	}
	
	
	
}
	
	