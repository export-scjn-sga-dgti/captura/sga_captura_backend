package mx.gob.scjn.ugacj.sga_captura.controller.seguimiento;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mx.gob.scjn.ugacj.sga_captura.dto.seguimiento.AsuntoSeguimientoDTO;
import mx.gob.scjn.ugacj.sga_captura.service.seguimiento.SeguimientoService;

@RestController
@RequestMapping(value = "/api/sga/seguimiento")
public class SeguimientoController {

	@Autowired
	private SeguimientoService seguimientoService;


	@RequestMapping(method = RequestMethod.GET, path = "/")
	public Page<AsuntoSeguimientoDTO> getSentenciaAll(@RequestParam(name = "page") Integer page,
			@RequestParam(name = "size") Integer size, @RequestParam(required = false, name = "filtros") String filtros)
			throws ParseException {
		Pageable paging = PageRequest.of(page - 1, size);
		return seguimientoService.getAllSeguimientoPages(paging, filtros);
	}
	
/*	@RequestMapping(method = RequestMethod.GET, path = "/informe-tematicas-asuntos")
	public void informeTematicasAsuntos()
			throws ParseException {
//		Pageable paging = PageRequest.of(page - 1, size);
		seguimientoService.informeTematicasAsuntos();
//		seguimientoService.getAllAsuntos
//		seguimientoService.getAllAsuntosSentencias();
	}*/
	
	@RequestMapping(method = RequestMethod.GET, path = "/informe-tematicas-asuntos")
	public ResponseEntity<InputStreamResource>  informeTematicasAsuntos()
			throws ParseException {
//		Pageable paging = PageRequest.of(page - 1, size);
		HttpHeaders header = new HttpHeaders();
		String filename = "ReporteAsuntosTemas.xlsx";
        header.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename="+filename);
        header.add("Cache-Control", "no-cache, no-store, must-revalidate");
        header.add("Pragma", "no-cache");
        header.add("Expires", "0");
		byte[] fileBytes = new byte[1024];
		fileBytes = seguimientoService.informeTematicasAsuntos();
		InputStream inputStream = new ByteArrayInputStream(fileBytes);
		InputStreamResource resource = new InputStreamResource(inputStream);
		
		return ResponseEntity.ok()
		        .headers(header)
		        .contentLength(fileBytes.length)
		        .contentType(MediaType.parseMediaType("application/octet-stream"))
		        .body(resource);
		
//		seguimientoService.getAllAsuntos
//		seguimientoService.getAllAsuntosSentencias();
	}

}
