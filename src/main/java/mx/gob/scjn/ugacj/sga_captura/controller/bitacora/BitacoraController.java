package mx.gob.scjn.ugacj.sga_captura.controller.bitacora;

import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mx.gob.scjn.ugacj.sga_captura.domain.bitacora.JvSnapshots;
import mx.gob.scjn.ugacj.sga_captura.service.bitacora.BitacoraService;

@RestController
@RequestMapping(path = "/api/sga/bitacora")
public class BitacoraController {

	@Autowired
	private BitacoraService _serBitacora;

	@CrossOrigin()
	@RequestMapping(method = RequestMethod.GET, path = "/", produces = "application/json")
	public Page<JvSnapshots> getAllJvSnapshotsByPagin(@RequestParam(name = "page") int page,
			@RequestParam(name = "size") int size, @RequestParam(required = false, name = "filtros") String filtros)
			throws ParseException {
		Pageable paging = PageRequest.of(page - 1, size, Sort.by("commitMetaData.commitDate").descending());
		return _serBitacora.getAllJvSnapshotsByPagin(paging, filtros);
	}

	@CrossOrigin()
	@RequestMapping(method = RequestMethod.GET, path = "/{id}", produces = "application/json")
	public JvSnapshots getBitacoraById(@PathVariable String id) {
		return _serBitacora.getBitacoraById(id);
	}

}
