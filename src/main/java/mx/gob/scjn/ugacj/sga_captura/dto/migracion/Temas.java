package mx.gob.scjn.ugacj.sga_captura.dto.migracion;

public class Temas {
    
    String temas[];
    String categoria;

	public String[] getTemas() {
		return this.temas;
	}

	public void setTemas(String temas[]) {
		this.temas = temas;
	}

	public String getCategoria() {
		return this.categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

}
