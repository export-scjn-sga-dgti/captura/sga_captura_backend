package mx.gob.scjn.ugacj.sga_captura.domain;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProcesamientoTexto {

    private String text;
    private String decision;
    private Integer numVotacionDecision;
    private List<String> ministros;
    private List<String> ministrosAFavor;
    private List<String> ministrosEnContra;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDecision() {
        return decision;
    }

    public void setDecision(String decision) {
        this.decision = decision;
    }

    public Integer getNumVotacionDecision() {
        return numVotacionDecision;
    }

    public void setNumVotacionDecision(Integer numVotacionDecision) {
        this.numVotacionDecision = numVotacionDecision;
    }

    public List<String> getMinistrosAFavor() {
        return ministrosAFavor;
    }

    public void setMinistrosAFavor(List<String> ministrosAFavor) {
        this.ministrosAFavor = ministrosAFavor;
    }

    public List<String> getMinistrosEnContra() {
        return ministrosEnContra;
    }

    public void setMinistrosEnContra(List<String> ministrosEnContra) {
        this.ministrosEnContra = ministrosEnContra;
    }

    public List<String> getMinistros() {
        return ministros;
    }

    public void setMinistros(List<String> ministros) {
        this.ministros = ministros;
    }

    @Override
    public String toString() {
        return "ProcesamientoTexto{" +
                "texto='" + text + '\'' +
                ", decision='" + decision + '\'' +
                ", numVotacionDecision=" + numVotacionDecision +
                ", ministros=" + ministros +
                ", ministrosAFavor=" + ministrosAFavor +
                ", ministrosEnContra=" + ministrosEnContra +
                '}';
    }
}
