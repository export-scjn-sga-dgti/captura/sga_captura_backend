package mx.gob.scjn.ugacj.sga_captura.service.sentencias;

import mx.gob.scjn.ugacj.sga_captura.domain.Sentencia;
import mx.gob.scjn.ugacj.sga_captura.domain.TablaVotaciones;
import mx.gob.scjn.ugacj.sga_captura.domain.RubrosTablaVotaciones;
import mx.gob.scjn.ugacj.sga_captura.dto.RubrosTablaVotacionesDTO;
import mx.gob.scjn.ugacj.sga_captura.dto.TablaVotacionSentenciaDTO;
import mx.gob.scjn.ugacj.sga_captura.dto.TablaVotacionesRubrosDTO;
import mx.gob.scjn.ugacj.sga_captura.repository.SentenciaRepository;
import mx.gob.scjn.ugacj.sga_captura.repository.VotacionRepository;
import mx.gob.scjn.ugacj.sga_captura.service.bitacora.BitacoraService;
import mx.gob.scjn.ugacj.sga_captura.service.porcentaje.PorcentajeService;
import mx.gob.scjn.ugacj.sga_captura.utils.CalculoPorcentajeUtils;
import mx.gob.scjn.ugacj.sga_captura.utils.StatusBitacora;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

@Service
public class TablasVotacionesService {

    @Autowired
    private VotacionRepository _votacionRepository;

    @Autowired
    private SentenciaRepository _sentenciaRepository;

    @Autowired
    private MongoOperations mongoOperations;

    @Autowired
    private RubrosTablaVotacionesService _RubrosTablaVotacionesService;
    
    @Autowired
	private PorcentajeService porcentajeService;
    
    @Autowired
	private BitacoraService _bitacoraService;
    
    @Autowired
    private TablasVotacionesService _votacionService;

    public Page<TablaVotaciones> getAllVotacionesPages(Pageable paging, String filtros) {

        long count = 0;
        Page<TablaVotaciones> votacionPaginable = null;
        List<TablaVotaciones> votacion = null;
        Page<TablaVotaciones> votacionesPaginableResponse = null;
        Pageable paging2 = PageRequest.of(paging.getPageNumber(), paging.getPageSize());
        if (filtros != null) {
            Query query = new Query().with(paging);
            Query queryNoPage = new Query();
            String subs[] = filtros.split(",");
            for (String sub : subs) {
                String sTempFiltros[] = sub.split(":");
                String filtro = sTempFiltros[0].trim();
                String filtroValores = sTempFiltros[1].trim();
                query.addCriteria(Criteria.where(filtro).is(filtroValores));
                queryNoPage.addCriteria(Criteria.where(filtro).is(filtroValores));
            }
            List<TablaVotaciones> listPrecedentes = mongoOperations.find(query, TablaVotaciones.class);
            count = mongoOperations.count(queryNoPage, Sentencia.class);

            votacionPaginable = new PageImpl<TablaVotaciones>(listPrecedentes, paging2, count);
            votacion = votacionPaginable.getContent();
        } else {
            votacionPaginable = _votacionRepository.findAll(paging);
            votacion = votacionPaginable.getContent();
            count = votacionPaginable.getTotalElements();
        }

        votacionesPaginableResponse = new PageImpl<TablaVotaciones>(votacion, paging2, count);
        return votacionesPaginableResponse;
    }

    public TablaVotacionesRubrosDTO getVotacionById(String votacionId) {
        Optional<TablaVotaciones> votacion = _votacionRepository.findById(votacionId);
        if (votacion.isPresent()) {
            return getVotacionRubro(votacion.get());
        }
        return null;
    }

//    public Integer getNumeroVotosByVotacion(String votacionId) {
//        Optional<TablaVotaciones> votacion = _votacionRepository.findById(votacionId);
//        if (votacion.isPresent()) {
//            return votacion.get().getNumeroVotos();
//        }
//        return 0;
//    }
    
    public Sentencia updateTotalVotaciones(Sentencia sentencia) {
    	sentencia.setTotalVotacionCausasImprocedenciaySobreseimientoAnalizadas(getTotalVotacion(sentencia.getVotacionCausasImprocedenciaySobreseimientoAnalizadas()));
		sentencia.setTotalVotacionDerechosHumanosCuyaViolacionAnalizaSentencia(getTotalVotacion(sentencia.getVotacionDerechosHumanosCuyaViolacionAnalizaSentencia()));
		sentencia.setTotalVotacionTipoVicioProcesoLegislativoAnalizadoenSentencia(getTotalVotacion(sentencia.getVotacionTipoVicioProcesoLegislativoAnalizadoenSentencia()));
		sentencia.setTotalVotacionTipoViolacionInvacionEsferasAnalizadoenSentencia(getTotalVotacion(sentencia.getVotacionTipoViolacionInvacionEsferasAnalizadoenSentencia()));
		sentencia.setTotalVotacionTipoViolacionInvacionPoderesAnalizadoenSentencia(getTotalVotacion(sentencia.getVotacionTipoViolacionInvacionPoderesAnalizadoenSentencia()));
		return sentencia;
    }
    
    public int getTotalVotacion(List<String> idsTablasVotacion) {
		if(idsTablasVotacion.size()>0) {
			System.out.println("U here");
		}
		int totalRubros = 0;
		List<TablaVotaciones> tablasVotacionList = (List<TablaVotaciones>) _votacionRepository.findAllById(idsTablasVotacion);
		for(TablaVotaciones tablaVotacion: tablasVotacionList) {
			totalRubros+=1;//Esto por defecto una tabla de votación global corresponde a un voto
			//Esto por cada votación adicional se suma el voto
			totalRubros+=(tablaVotacion.getRubrosEfectoSentencia().size()+tablaVotacion.getRubrosExtencionInvalidez().size());
		}
		return totalRubros;
	}

    public TablaVotacionesRubrosDTO saveVotacion(TablaVotacionSentenciaDTO votacionDTO) {

        TablaVotaciones saveVotacion = _votacionRepository.save(getVotacion(votacionDTO.getVotacion()));
        Optional<Sentencia> sentenciaUpdate = _sentenciaRepository.findById(votacionDTO.getIdSentencia());
        //Update tipo de Votacion en Sentencia
        updateVotacionesBySentencia(saveVotacion.getId(), saveVotacion.getTipoVotacion().toString(), true, sentenciaUpdate.get());
        updateTotalVotaciones(sentenciaUpdate.get());
        porcentajeService.updatePorcentaje(sentenciaUpdate.get());
        _sentenciaRepository.save(sentenciaUpdate.get());
        _bitacoraService.saveOperacion(StatusBitacora._SENTENCIAS_VOTACION, StatusBitacora._SAVE, saveVotacion, null);
        return getVotacionRubro(saveVotacion); 
    }
    
    public TablaVotaciones updateOrdenVotacion(TablaVotacionesRubrosDTO votacion) {

        Optional<TablaVotaciones> votacionUpdate = _votacionRepository.findById(votacion.getId());
        if (votacionUpdate.isPresent()) {
        	votacionUpdate.get().setRubrosTematicosPrincipal(votacion.getRubrosTematicosPrincipal());
        	_votacionRepository.save(votacionUpdate.get());
        	return votacionUpdate.get();
        }
        return null;
    }

    public TablaVotacionesRubrosDTO updateVotacion(TablaVotacionesRubrosDTO votacion) {

        Optional<TablaVotaciones> votacionUpdate = _votacionRepository.findById(votacion.getId());
        TablaVotaciones votacionOld = null;
        if (votacionUpdate.isPresent()) {
        	votacionOld = votacionUpdate.get();
            votacionUpdate.map(voto -> {
                voto.setActosImpugnados(votacion.getActosImpugnados());
                voto.setAprobadasConcideraciones(votacion.getAprobadasConcideraciones());
                voto.setArticulosImpugnados(votacion.getArticulosImpugnados());
                voto.setCongresoEmitioDecreto(votacion.getCongresoEmitioDecreto());
                voto.setDecretoImpugnado(votacion.getDecretoImpugnado());
                voto.setDerechosHumanos(votacion.getDerechosHumanos());
                voto.setMetodologiaAnalisisConstitucionalidad(votacion.getMetodologiaAnalisisConstitucionalidad());
                voto.setSentidoResolucion(votacion.getSentidoResolucion());
                voto.setTextoVotacion(votacion.getTextoVotacion());
                voto.setTipoSentencia(votacion.getTipoSentencia());
                voto.setTipoSentencia(votacion.getTipoSentencia());
                voto.setTipoVicio(votacion.getTipoVicio());
                voto.setTipoViolacionInvacionEsferas(votacion.getTipoViolacionInvacionEsferas());
                voto.setTipoViolacionInvacionPoderes(votacion.getTipoViolacionInvacionPoderes());
                voto.setTipoVotacion(votacion.getTipoVotacion());
                voto.setVotacionEnContra(votacion.getVotacionEnContra());
                voto.setVotacionFavor(votacion.getVotacionFavor());
                voto.setMinistros(votacion.getMinistros());
                voto.setVotacionRubrosTematicos(votacion.getVotacionRubrosTematicos());
                voto.setRubrosTematicosPrincipal(votacion.getRubrosTematicosPrincipal());
                voto.setMateriaAnalizadaPorInvacionPoderes(votacion.getMateriaAnalizadaPorInvacionPoderes());
                voto.setMateriaAnalizadaPorInvacionEsferas(votacion.getMateriaAnalizadaPorInvacionEsferas());
                //voto.setMetodologiaAnalisisConstitucionalidad(votacion.getMetodologiaAnalisisConstitucionalidad());
                //Update de los Rubros de la Votacion
               /* getUpdateRubrosVotaciones(votacion.getRubrosTematicos(), votacionUpdate.get().getRubrosTematicos());
                getUpdateRubrosVotaciones(votacion.getRubrosEfectoSentencia(), votacionUpdate.get().getRubrosEfectoSentencia());
                getUpdateRubrosVotaciones(votacion.getRubrosExtencionInvalidez(), votacionUpdate.get().getRubrosExtencionInvalidez());*/

                return votacionUpdate;
            });
            _votacionRepository.save(votacionUpdate.get());
            _bitacoraService.saveOperacion(StatusBitacora._SENTENCIAS_VOTACION, StatusBitacora._UPDATE, votacionUpdate.get(), votacionOld);
            Optional<Sentencia> sentenciaUpdate = _sentenciaRepository.findById(votacion.getIdSentencia());
            if(sentenciaUpdate.isPresent()) {
            	porcentajeService.updatePorcentaje(sentenciaUpdate.get());
            	updateTotalVotaciones(sentenciaUpdate.get());
//            	updateVotacionesBySentencia(votacion.getId(), votacion.getTipoVotacion().toString(), true, sentenciaUpdate.get());
            } 
        }
        return votacion;
    }

    public void deleteVotacion(String idVotacion, String idSentencia, Boolean bolSentencia) {
        Optional<TablaVotaciones> votacion = _votacionRepository.findById(idVotacion);
        if (votacion.isPresent()) {
            //Se eliminan todos los rubros pertenecientes a la Votacion
            deleteRubrosVotacion(votacion.get().getRubrosTematicos());
            deleteRubrosVotacion(votacion.get().getRubrosEfectoSentencia());
            deleteRubrosVotacion(votacion.get().getRubrosExtencionInvalidez());
            if (bolSentencia) {
                //Eliminar id del tipo de Votacion, dentro de Sentencia
                Optional<Sentencia> sentenciaUpdate = _sentenciaRepository.findById(idSentencia);
                updateVotacionesBySentencia(votacion.get().getId(), votacion.get().getTipoVotacion().toString(), false, sentenciaUpdate.get());
            }
            //Se elimina la votacion
            _votacionRepository.deleteById(idVotacion);
        }
    }

    public Map<String, Integer> deleteVotacionWithZiseVotos(String idVotacion, String idSentencia) throws JSONException {
        Optional<TablaVotaciones> votacion = _votacionRepository.findById(idVotacion);
        Integer totalVotacion = 0;
        Map<String, Integer> countVotacion = new HashMap<>();
        if (votacion.isPresent()) {
            //Se eliminan todos los rubros pertenecientes a la Votacion
            deleteRubrosVotacion(votacion.get().getRubrosTematicos());
            deleteRubrosVotacion(votacion.get().getRubrosEfectoSentencia());
            deleteRubrosVotacion(votacion.get().getRubrosExtencionInvalidez());

            String tipoVotacion = votacion.get().getTipoVotacion().toString();
            String nombreTotal = "";
            //Se elimina la votacion
            _votacionRepository.deleteById(idVotacion);

            //Eliminar id del tipo de Votacion, dentro de Sentencia
            Optional<Sentencia> sentenciaUpdate = _sentenciaRepository.findById(idSentencia);
            totalVotacion = updateVotacionesBySentencia(idVotacion, tipoVotacion, false, sentenciaUpdate.get());
            nombreTotal = getNameVotacion(tipoVotacion);
            countVotacion.put(nombreTotal, totalVotacion);
            porcentajeService.updatePorcentaje(sentenciaUpdate.get());
            updateTotalVotaciones(sentenciaUpdate.get());
            _sentenciaRepository.save(sentenciaUpdate.get());
            _bitacoraService.saveOperacion(StatusBitacora._SENTENCIAS_VOTACION, StatusBitacora._DELETE, votacion.get(), votacion.get());
        }
        return countVotacion;
    }

    private String getNameVotacion(String tipoVotacion) {
        String nombreTotal = "";
        switch (tipoVotacion) {
            case "votacionCausasImprocedenciaySobreseimientoAnalizadas":
                nombreTotal = "totalVotacionCausasImprocedenciaySobreseimientoAnalizadas";
                break;
            case "votacionDerechosHumanosCuyaViolacionAnalizaSentencia":
                nombreTotal = "totalVotacionDerechosHumanosCuyaViolacionAnalizaSentencia";
                break;
            case "votacionTipoVicioProcesoLegislativoAnalizadoenSentencia":
                nombreTotal = "totalVotacionTipoVicioProcesoLegislativoAnalizadoenSentencia";
                break;
            case "votacionTipoViolacionEsferasAnalizadoenSentencia":
                nombreTotal = "totalVotacionTipoViolacionInvacionEsferasAnalizadoenSentencia";
                break;
            case "votacionTipoViolacionInvacionPoderesAnalizadoenSentencia":
                nombreTotal = "totalVotacionTipoViolacionInvacionPoderesAnalizadoenSentencia";

                break;
        }
        return nombreTotal;
    }

    private Integer updateVotacionesBySentencia(String idVotacion, String tipoVotacion, Boolean tipoOperacion, Sentencia sentencia) {

        ArrayList<String> listVotacionSentencias = new ArrayList<>();
        Integer totalVotacion = 0;
        switch (tipoVotacion) {
            case "votacionCausasImprocedenciaySobreseimientoAnalizadas":
                listVotacionSentencias = (ArrayList<String>) sentencia.getVotacionCausasImprocedenciaySobreseimientoAnalizadas();
                sentencia.setVotacionCausasImprocedenciaySobreseimientoAnalizadas(getListVotaciones(idVotacion, listVotacionSentencias, tipoOperacion));
//                totalVotacion = getTotalVotacion(sentencia.getVotacionCausasImprocedenciaySobreseimientoAnalizadas());
                break;
            case "votacionDerechosHumanosCuyaViolacionAnalizaSentencia":
                listVotacionSentencias = (ArrayList<String>) sentencia.getVotacionDerechosHumanosCuyaViolacionAnalizaSentencia();
                sentencia.setVotacionDerechosHumanosCuyaViolacionAnalizaSentencia(getListVotaciones(idVotacion, listVotacionSentencias, tipoOperacion));
//                totalVotacion = getTotalVotacion(sentencia.getVotacionDerechosHumanosCuyaViolacionAnalizaSentencia());
                break;
            case "votacionTipoVicioProcesoLegislativoAnalizadoenSentencia":
                listVotacionSentencias = (ArrayList<String>) sentencia.getVotacionTipoVicioProcesoLegislativoAnalizadoenSentencia();
                sentencia.setVotacionTipoVicioProcesoLegislativoAnalizadoenSentencia(getListVotaciones(idVotacion, listVotacionSentencias, tipoOperacion));
//                totalVotacion = getTotalVotacion(sentencia.getVotacionTipoVicioProcesoLegislativoAnalizadoenSentencia());
                break;
            case "votacionTipoViolacionEsferasAnalizadoenSentencia":
                listVotacionSentencias = (ArrayList<String>) sentencia.getVotacionTipoViolacionInvacionEsferasAnalizadoenSentencia();
                sentencia.setVotacionTipoViolacionInvacionEsferasAnalizadoenSentencia(getListVotaciones(idVotacion, listVotacionSentencias, tipoOperacion));
//                totalVotacion = getTotalVotacion(sentencia.getVotacionTipoViolacionInvacionEsferasAnalizadoenSentencia());
                break;
            case "votacionTipoViolacionInvacionPoderesAnalizadoenSentencia":
                listVotacionSentencias = (ArrayList<String>) sentencia.getVotacionTipoViolacionInvacionPoderesAnalizadoenSentencia();
                sentencia.setVotacionTipoViolacionInvacionPoderesAnalizadoenSentencia(getListVotaciones(idVotacion, listVotacionSentencias, tipoOperacion));
//                totalVotacion = getTotalVotacion(sentencia.getVotacionTipoViolacionInvacionPoderesAnalizadoenSentencia());
                break;
        }
        _sentenciaRepository.save(sentencia);
        return totalVotacion;
    }

//    private Integer getTotalVotacion(List<String> listIDVotaciones) {
//        AtomicReference<Integer> total = new AtomicReference<>(0);
//        if (listIDVotaciones != null && listIDVotaciones.size() > 0) {
//            listIDVotaciones.stream().forEach(id -> {
//                if (id != null) {
//                    total.updateAndGet(v -> v + getNumeroVotosByVotacion(id));
//                }
//            });
//            return total.get();
//        }
//        return total.get();
//    }

    private ArrayList<String> getListVotaciones(String idVotacion, ArrayList<String> listVotacionSentencias, Boolean tipoOperacion) {
        if (tipoOperacion) {
        	if(!listVotacionSentencias.contains(idVotacion))listVotacionSentencias.add(idVotacion);
        } else {
            listVotacionSentencias.remove(idVotacion);
        }
        return listVotacionSentencias;
    }

    private List<RubrosTablaVotaciones> getRubrosByVotacion(List<String> listRubros) {
        List<RubrosTablaVotaciones> listVotacionesRubros = new ArrayList<>();
        if (listRubros != null && listRubros.size() > 0) {
            listRubros.stream().forEach(id -> {
                Optional<RubrosTablaVotaciones> vRubros = Optional.ofNullable(_RubrosTablaVotacionesService.getVotacionRubroById(id));
                if (vRubros.isPresent()) {
                    listVotacionesRubros.add(vRubros.get());
                }
            });
            return listVotacionesRubros;
        }
        return listVotacionesRubros;
    }

    private TablaVotaciones getVotacion(TablaVotacionesRubrosDTO votacionDTO) {

        TablaVotaciones votacion = new TablaVotaciones();
        votacion.setId(votacionDTO.getId());
        votacion.setTextoVotacion(votacionDTO.getTextoVotacion());
        votacion.setActosImpugnados(votacionDTO.getActosImpugnados());
        votacion.setTipoVotacion(votacionDTO.getTipoVotacion());
        votacion.setVotacionEnContra(votacionDTO.getVotacionEnContra());
        votacion.setVotacionFavor(votacionDTO.getVotacionFavor());
        votacion.setMinistros(votacionDTO.getMinistros());
        votacion.setAprobadasConcideraciones(votacionDTO.getAprobadasConcideraciones());
        votacion.setArticulosImpugnados(votacionDTO.getArticulosImpugnados());
        votacion.setCongresoEmitioDecreto(votacionDTO.getCongresoEmitioDecreto());
        votacion.setDecretoImpugnado(votacionDTO.getDecretoImpugnado());
        votacion.setDerechosHumanos(votacionDTO.getDerechosHumanos());
        votacion.setMetodologiaAnalisisConstitucionalidad(votacionDTO.getMetodologiaAnalisisConstitucionalidad());
        votacion.setSentidoResolucion(votacionDTO.getSentidoResolucion());
        votacion.setTipoSentencia(votacionDTO.getTipoSentencia());
        votacion.setTipoVicio(votacionDTO.getTipoVicio());
        votacion.setTipoViolacionInvacionEsferas(votacionDTO.getTipoViolacionInvacionEsferas());
        votacion.setTipoViolacionInvacionPoderes(votacionDTO.getTipoViolacionInvacionPoderes());
        votacion.setRubrosTematicosPrincipal(votacionDTO.getRubrosTematicosPrincipal());
        votacion.setMateriaAnalizadaPorInvacionEsferas(votacionDTO.getMateriaAnalizadaPorInvacionEsferas());
        votacion.setMateriaAnalizadaPorInvacionPoderes(votacionDTO.getMateriaAnalizadaPorInvacionPoderes());
        votacion.setNumeroVotos(1);

        if (votacionDTO.getTipoVotacion().equals("votacionCausasImprocedenciaySobreseimientoAnalizadas")) {
            votacion.setRubrosTematicos(new ArrayList<>());
        } else {
            votacion.setRubrosEfectoSentencia(new ArrayList<>());
            votacion.setRubrosExtencionInvalidez(new ArrayList<>());
        }

        /*votacion.setRubrosTematicos(getSaveRubrosVotacion(votacionDTO.getRubrosTematicos(), votacionDTO.getTipoVotacion()));
        votacion.setRubrosEfectoSentencia(getSaveRubrosVotacion(votacionDTO.getRubrosEfectoSentencia(), votacionDTO.getTipoVotacion()));
        votacion.setRubrosExtencionInvalidez(getSaveRubrosVotacion(votacionDTO.getRubrosExtencionInvalidez(), votacionDTO.getTipoVotacion()));*/
        return votacion;
    }

    private Integer getNumeroVotosEnVotacion(TablaVotaciones tablaVotaciones) {

        Integer countVotos = 1;
        if (tablaVotaciones.getRubrosTematicos() != null && tablaVotaciones.getRubrosTematicos().size() > 0) {
            countVotos += tablaVotaciones.getRubrosTematicos().size();
        }

        if (tablaVotaciones.getRubrosEfectoSentencia() != null && tablaVotaciones.getRubrosEfectoSentencia().size() > 0) {
            countVotos += tablaVotaciones.getRubrosEfectoSentencia().size();
        }

        if (tablaVotaciones.getRubrosExtencionInvalidez() != null && tablaVotaciones.getRubrosExtencionInvalidez().size() > 0) {
            countVotos += tablaVotaciones.getRubrosExtencionInvalidez().size();
        }

        return countVotos;
    }

    private List<String> getSaveRubrosVotacion(List<RubrosTablaVotaciones> rubros, String tipoVotacion) {

        List<String> listRubros = new ArrayList<>();
        if (rubros != null && rubros.size() > 0) {
            rubros.stream().forEach(rubro -> {
                rubro.setTipoVotacion(tipoVotacion);
                listRubros.add(_RubrosTablaVotacionesService.saveRubro(rubro));
                System.out.println(rubro);
            });
            return listRubros;
        }
        return listRubros;
    }

    private List<RubrosTablaVotaciones> getUpdateRubrosVotaciones(List<RubrosTablaVotaciones> listVotacionesRubros, List<String> listIDRubros) {
        if (listVotacionesRubros != null && listVotacionesRubros.size() > 0) {
            listVotacionesRubros.stream().forEach(rubro -> {
                if (rubro.getId() != null) {
                    _RubrosTablaVotacionesService.updateVotacionRubro(rubro);
                } else {
                    String id = _RubrosTablaVotacionesService.saveRubro(rubro);
                    rubro.setId(id);
                    listIDRubros.add(id);
                }
            });
            return null;
        }
        return listVotacionesRubros;
    }

    private void deleteRubrosVotacion(List<String> listVotacionesRubros) {
        if (listVotacionesRubros != null && listVotacionesRubros.size() > 0) {
            listVotacionesRubros.stream().forEach(id -> {
                _RubrosTablaVotacionesService.deleteVotacionRubro(id);
            });
        }
    }

    private TablaVotacionesRubrosDTO getVotacionRubro(TablaVotaciones tablaVotaciones) {
        TablaVotacionesRubrosDTO votacionRubro = new TablaVotacionesRubrosDTO();
        votacionRubro.setId(tablaVotaciones.getId());
        votacionRubro.setActosImpugnados(tablaVotaciones.getActosImpugnados());
        votacionRubro.setAprobadasConcideraciones(tablaVotaciones.getAprobadasConcideraciones());
        votacionRubro.setArticulosImpugnados(tablaVotaciones.getArticulosImpugnados());
        votacionRubro.setCongresoEmitioDecreto(tablaVotaciones.getCongresoEmitioDecreto());
        votacionRubro.setDecretoImpugnado(tablaVotaciones.getDecretoImpugnado());
        votacionRubro.setDerechosHumanos(tablaVotaciones.getDerechosHumanos());
        votacionRubro.setMetodologiaAnalisisConstitucionalidad(tablaVotaciones.getMetodologiaAnalisisConstitucionalidad());
        votacionRubro.setSentidoResolucion(tablaVotaciones.getSentidoResolucion());
        votacionRubro.setTextoVotacion(tablaVotaciones.getTextoVotacion());
        votacionRubro.setTipoSentencia(tablaVotaciones.getTipoSentencia());
        votacionRubro.setTipoSentencia(tablaVotaciones.getTipoSentencia());
        votacionRubro.setTipoVicio(tablaVotaciones.getTipoVicio());
        votacionRubro.setTipoViolacionInvacionEsferas(tablaVotaciones.getTipoViolacionInvacionEsferas());
        votacionRubro.setTipoViolacionInvacionPoderes(tablaVotaciones.getTipoViolacionInvacionPoderes());
        votacionRubro.setTipoVotacion(tablaVotaciones.getTipoVotacion());
        votacionRubro.setVotacionEnContra(tablaVotaciones.getVotacionEnContra());
        votacionRubro.setVotacionFavor(tablaVotaciones.getVotacionFavor());
        votacionRubro.setMinistros(tablaVotaciones.getMinistros());
        votacionRubro.setVotacionRubrosTematicos(tablaVotaciones.getVotacionRubrosTematicos());
        votacionRubro.setRubrosTematicosPrincipal(tablaVotaciones.getRubrosTematicosPrincipal());
        votacionRubro.setMateriaAnalizadaPorInvacionEsferas(tablaVotaciones.getMateriaAnalizadaPorInvacionEsferas());
        votacionRubro.setMateriaAnalizadaPorInvacionPoderes(tablaVotaciones.getMateriaAnalizadaPorInvacionPoderes());

        if (tablaVotaciones.getRubrosTematicos() != null) {
            votacionRubro.setRubrosTematicos(getRubrosByVotacion(tablaVotaciones.getRubrosTematicos()));
        }

        if (tablaVotaciones.getRubrosEfectoSentencia() != null) {
            votacionRubro.setRubrosEfectoSentencia(getRubrosByVotacion(tablaVotaciones.getRubrosEfectoSentencia()));
        }

        if (tablaVotaciones.getRubrosExtencionInvalidez() != null) {
            votacionRubro.setRubrosExtencionInvalidez(getRubrosByVotacion(tablaVotaciones.getRubrosExtencionInvalidez()));
        }
        return votacionRubro;
    }

    public void deleteRubroVotacion(String idVotacion, RubrosTablaVotaciones rubro) {
        Optional<TablaVotaciones> votacion = _votacionRepository.findById(idVotacion);
        Integer totalVotos = 1;
        switch (rubro.getTipoRubro()) {
            case "rubrosTematicos":
                votacion.get().getRubrosTematicos().remove(rubro.getId());
                break;
            case "rubrosEfectoSentencia":
                votacion.get().getRubrosEfectoSentencia().remove(rubro.getId());
                break;
            case "rubrosExtencionInvalidez":
                votacion.get().getRubrosExtencionInvalidez().remove(rubro.getId());
                break;
        }
        //Calcula el numero de votos
        votacion.get().setNumeroVotos(getNumeroVotosEnVotacion(votacion.get()));
        _votacionRepository.save(votacion.get());
    }
    

    public void saveRubroByVotacion(RubrosTablaVotacionesDTO rubroDTO) {
        //Aqui se agregan los ID de los rubros a su votacion correspondiente
        //Se actualiza el numero de votos que integran la votacion
        Optional<TablaVotaciones> votacion = _votacionRepository.findById(rubroDTO.getIdVotacion());
//        Integer totalVotos = 1;
        if (votacion.isPresent()) {
            List<String> listRubros = new ArrayList<>();
            //System.out.println("tipo de rubro: " + rubroDTO.getRubro().getTipoRubro());
            switch (rubroDTO.getRubro().getTipoRubro().toString()) {
                case "rubrosTematicos":
                    votacion.get().setRubrosTematicos(getRubros(votacion.get().getRubrosTematicos(), rubroDTO.getRubro().getId()));
                    break;
                case "rubrosEfectoSentencia":
                    if (rubroDTO.getRubro().getSentido() != null) votacion.get().setSentidoResolucion(rubroDTO.getRubro().getSentido());
                    votacion.get().setRubrosEfectoSentencia(getRubros(votacion.get().getRubrosEfectoSentencia(), rubroDTO.getRubro().getId()));
                    break;
                case "rubrosExtencionInvalidez":
                	if (rubroDTO.getRubro().getTipoSentencia() != null ) votacion.get().setTipoSentencia(rubroDTO.getRubro().getTipoSentencia());
                    votacion.get().setRubrosExtencionInvalidez(getRubros(votacion.get().getRubrosExtencionInvalidez(), rubroDTO.getRubro().getId()));
                    break;
            }
            //Calcula el numero de votos
            votacion.get().setNumeroVotos(getNumeroVotosEnVotacion(votacion.get()));
            _votacionRepository.save(votacion.get());
        }
    }

    private List<String> getRubros(List<String> listRubrosVotacion, String idRubro) {

        if (listRubrosVotacion == null) {
            listRubrosVotacion = new ArrayList<>();
        }
        listRubrosVotacion.add(idRubro);
        return listRubrosVotacion;
    }
}
