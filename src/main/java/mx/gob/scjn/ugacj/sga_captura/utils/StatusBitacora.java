package mx.gob.scjn.ugacj.sga_captura.utils;


public class StatusBitacora {

   public static String _SAVE = "Save";
   public static final String _UPDATE = "Update";
   public static final String _DELETE = "Delete";
   public static final String _SENTENCIAS = "Sentencias";
   public static final String _SENTENCIAS_VOTACION = "Sentencias - Votacion";
   public static final String _SENTENCIAS_VOTACION_RUBROS = "Sentencias - Votacion - Rubros";
   public static final String _VERSIONES = "Versiones Taquigraficas";
   public static final String _ASUNTOS = "Asuntos Abordados";
   public static final String _CATALOGO = "Catalogos";
   public static final String _ACUERDOS = "Acuerdos";
   public static final String _VOTOS = "Votos";
   public static final String _DIVERSA = "Diversa Normativa";
   public static final String _MATERIA = "Materia de Regulacion";
   public static final String _USUARIOS = "Usuarios";
}
