package mx.gob.scjn.ugacj.sga_captura.service.sentencias;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import mx.gob.scjn.ugacj.sga_captura.domain.Sentencia;
import mx.gob.scjn.ugacj.sga_captura.domain.TablaVotaciones;
import mx.gob.scjn.ugacj.sga_captura.dto.AsuntoDTO;
import mx.gob.scjn.ugacj.sga_captura.dto.ModuloDTO;
import mx.gob.scjn.ugacj.sga_captura.dto.SentenciaDTO;
import mx.gob.scjn.ugacj.sga_captura.dto.TablaVotacionesRubrosDTO;
import mx.gob.scjn.ugacj.sga_captura.dto.statecode.SentenciaResponseDTO;
import mx.gob.scjn.ugacj.sga_captura.repository.SentenciaRepository;
import mx.gob.scjn.ugacj.sga_captura.service.bitacora.BitacoraService;
import mx.gob.scjn.ugacj.sga_captura.service.porcentaje.PorcentajeService;
import mx.gob.scjn.ugacj.sga_captura.service.usuarios.UsuariosService;
import mx.gob.scjn.ugacj.sga_captura.utils.ConvertFechaFiltros;
import mx.gob.scjn.ugacj.sga_captura.utils.FiltrosUtils;
import mx.gob.scjn.ugacj.sga_captura.utils.RegexUtils;
import mx.gob.scjn.ugacj.sga_captura.utils.StatusBitacora;

@Service
public class SentenciaService {

    @Autowired
    private SentenciaRepository _sentenciaRepository;

    @Autowired
    private TablasVotacionesService _votacionService;

    @Autowired
    private MongoOperations mongoOperations;
    
    @Autowired
	private BitacoraService _bitacoraService;
    
    @Autowired
	private PorcentajeService porcentajeService;
    
    @Autowired
	UsuariosService usuariosService;
    

    public List<String> getListIdsAsuntos(ModuloDTO moduloUserdata) {
    	List<String> listIdsAsuntos = new ArrayList<String>();
    	for(AsuntoDTO asunto : moduloUserdata.getAsuntos()) {
    		Sentencia asuntoExist = _sentenciaRepository.findByTipoAsuntoAndNumeroExpediente(asunto.getTipoAsunto(), asunto.getNumExpediente());
    		if(asuntoExist!=null) {
    			listIdsAsuntos.add(asuntoExist.getId());
    		}
    	}
    	return listIdsAsuntos;
    }
    
    
    public Page<Sentencia> getAllSentenciasPages(Pageable paging, String filtros) throws ParseException {
    	
    	List<Sentencia> listSentencias =  new ArrayList<Sentencia>();
    	long count = 0;
    	Page<Sentencia> sentenciasPaginableResponse = null;
    	 
    	ModuloDTO moduloUserdata = usuariosService.validateUserAccessToModulo("sentencia");
    	if(moduloUserdata!=null) {
    		
    		List<String> listIdsAsuntos = getListIdsAsuntos(moduloUserdata);

        	if(listIdsAsuntos.size()>0||moduloUserdata.getPermisos().isSuperadmin()) {
        		List<Sentencia> sentencias = null;
        		Page<Sentencia> sentenciaPaginable = null;
        		
                Pageable paging2 = PageRequest.of(paging.getPageNumber(), paging.getPageSize());
                Query query = new Query().with(paging);
                Query queryNoPage = new Query();
                if (filtros != null) {
                    //String subs[] = filtros.split(",");
                	String subs[] = FiltrosUtils.getSubfiltros(filtros);
                    for (String sub : subs) {
                        String sTempFiltros[] = sub.split(":");
                        String filtro = sTempFiltros[0].trim();
                        String filtroValores = sTempFiltros[1].trim();
                        if (filtro.trim().equals("fechaResolucion")) {
                            String fecha = ConvertFechaFiltros.changeFormat(filtroValores.trim(), RegexUtils.REGEX_YYYYMMDD);
                            query.addCriteria(Criteria.where(filtro).gte(fecha).lte(fecha));
                            queryNoPage.addCriteria(Criteria.where(filtro).gte(fecha).lte(fecha));
                        } else {
                            query.addCriteria(Criteria.where(filtro).is(filtroValores));
                            queryNoPage.addCriteria(Criteria.where(filtro).is(filtroValores)); 
                        }
                    }
                    
                    if (!moduloUserdata.getPermisos().isSuperadmin()) {
                    	query.addCriteria(Criteria.where("id").in(listIdsAsuntos));
                    	queryNoPage.addCriteria(Criteria.where("id").in(listIdsAsuntos));
                    }
                    query.with(Sort.by(Sort.Direction.DESC, "fechaResolucion"));
                    List<Sentencia> listPrecedentes = mongoOperations.find(query, Sentencia.class);
                    count = mongoOperations.count(queryNoPage, Sentencia.class);
                    sentenciaPaginable = new PageImpl<Sentencia>(listPrecedentes, paging2, count);
                    sentencias = sentenciaPaginable.getContent();
                } else {

                	if (!moduloUserdata.getPermisos().isSuperadmin()) {
                    	query.addCriteria(Criteria.where("id").in(listIdsAsuntos));
                    	queryNoPage.addCriteria(Criteria.where("id").in(listIdsAsuntos));
                    }
        			query.with(Sort.by(Sort.Direction.DESC, "fechaResolucion"));
        			List<Sentencia> listAcuerdos = mongoOperations.find(query, Sentencia.class);
        			
        			sentenciaPaginable = new PageImpl<Sentencia>(listAcuerdos, paging, count);
        			sentencias = sentenciaPaginable.getContent();
        			count = mongoOperations.count(queryNoPage, Sentencia.class);
                }
                for(Sentencia sentencia: sentencias) 
                	sentencia.setFechaResolucion(ConvertFechaFiltros.changeFormat(sentencia.getFechaResolucion(), RegexUtils.REGEX_DDMMYYYY));
                sentenciasPaginableResponse = new PageImpl<Sentencia>(sentencias, paging2, count);
                return sentenciasPaginableResponse;
        	}
    	}
    
    	sentenciasPaginableResponse = new PageImpl<Sentencia>(listSentencias, paging, count);
		return sentenciasPaginableResponse;
    }

    public SentenciaDTO getSentenciaById(String sentenciaId) {
        System.out.println(sentenciaId);
        Optional<Sentencia> sentencia = Optional.of(_sentenciaRepository.findById(sentenciaId).get());
        if (sentencia.isPresent()) {
        	sentencia.get().setFechaResolucion(ConvertFechaFiltros.changeFormat(sentencia.get().getFechaResolucion(), RegexUtils.REGEX_DDMMYYYY));
            return getSentenciaConvertDTO(sentencia.get());
        }
        return null;
    }


    public SentenciaResponseDTO ExistSentencia(Sentencia sentencia, SentenciaResponseDTO sentenciaResponseDTO) {

        Optional<Sentencia> existSentencia = Optional.ofNullable(_sentenciaRepository.findByTipoAsuntoAndNumeroExpedienteAndOrganoRadicacion(sentencia.getTipoAsunto(),
                sentencia.getNumeroExpediente(), sentencia.getOrganoRadicacion()));

        if (existSentencia.isPresent()) {

            if (existSentencia.get().getPonente().equals(sentencia.getPonente()) &&
                    existSentencia.get().getFechaResolucion().equals(sentencia.getFechaResolucion())) {
                sentenciaResponseDTO.setMessage("La sentencia ya existe.");
                sentenciaResponseDTO.setIdSentencia(existSentencia.get().getId());
            } else {
                sentenciaResponseDTO.setMessage("La sentencia ya existe, pero que los datos Fecha de resolución o Ponente no coinciden, favor de verificar la información.");
                sentenciaResponseDTO.setIdSentencia(existSentencia.get().getId());
            }
        }
        return sentenciaResponseDTO;
    }

    public Sentencia ExistSentenciaByVersionesTaq(String numeroExpediente, String tipoAsunto) {
        Optional<Sentencia> existSentencia = Optional.ofNullable(_sentenciaRepository.findByTipoAsuntoAndNumeroExpediente(
                tipoAsunto, numeroExpediente));
        if (existSentencia.isPresent()) {
            return existSentencia.get();
        }
        return null;
    }
    
    public Sentencia setTotalVotaciones(Sentencia sentencia) {
    	sentencia.setTotalVotacionCausasImprocedenciaySobreseimientoAnalizadas(_votacionService.getTotalVotacion(sentencia.getVotacionCausasImprocedenciaySobreseimientoAnalizadas()));
		sentencia.setTotalVotacionDerechosHumanosCuyaViolacionAnalizaSentencia(_votacionService.getTotalVotacion(sentencia.getVotacionDerechosHumanosCuyaViolacionAnalizaSentencia()));
		sentencia.setTotalVotacionTipoVicioProcesoLegislativoAnalizadoenSentencia(_votacionService.getTotalVotacion(sentencia.getVotacionTipoVicioProcesoLegislativoAnalizadoenSentencia()));
		sentencia.setTotalVotacionTipoViolacionInvacionEsferasAnalizadoenSentencia(_votacionService.getTotalVotacion(sentencia.getVotacionTipoViolacionInvacionEsferasAnalizadoenSentencia()));
		sentencia.setTotalVotacionTipoViolacionInvacionPoderesAnalizadoenSentencia(_votacionService.getTotalVotacion(sentencia.getVotacionTipoViolacionInvacionPoderesAnalizadoenSentencia()));
		return sentencia;
    }
    
    public boolean updateTotalVotaciones(String idSentencia) {
    	if(idSentencia!=null) {
    		Optional<Sentencia> sentenciaUpdate = _sentenciaRepository.findById(idSentencia);
            if (sentenciaUpdate.isPresent()) {
            	setTotalVotaciones(sentenciaUpdate.get());
            	_sentenciaRepository.save(sentenciaUpdate.get());
            	return true;
            }
    	}
    	return false;
    }
    
    

    public Sentencia updateSentencia(Sentencia sentencia) {

        Optional<Sentencia> sentenciaUpdate = _sentenciaRepository.findById(sentencia.getId());
        if (sentenciaUpdate.isPresent()) {
        	 _bitacoraService.saveOperacion(StatusBitacora._SENTENCIAS, StatusBitacora._UPDATE, sentencia, sentenciaUpdate.get());
            sentenciaUpdate.map(sen -> {
            	sen.setFechaResolucion(ConvertFechaFiltros.changeFormat(sen.getFechaResolucion(), RegexUtils.REGEX_YYYYMMDD));
                sen.setTipoPersonaPromovente(sentencia.getTipoPersonaPromovente());
                sen.setTipoPersonaJuridicoColectiva(sentencia.getTipoPersonaJuridicoColectiva());
                sen.setTipoViolacionPlanteadaenDemanda(sentencia.getTipoViolacionPlanteadaenDemanda());
                //Ocultos:
                sen.setDerechosHumanosCuyaViolacionPlantea(sentencia.getDerechosHumanosCuyaViolacionPlantea());
                sen.setViolacionOrganicaPlanteada(sentencia.getViolacionOrganicaPlanteada());
                sen.setTipoVicioProcesoLegislativo(sentencia.getTipoVicioProcesoLegislativo());
                sen.setTipoViolacionInvacionEsferas(sentencia.getTipoViolacionInvacionEsferas());
                sen.setTipoViolacionInvacionPoderes(sentencia.getTipoViolacionInvacionPoderes());
                sen.setDerechosHumanosCuyaViolacionPlanteaByDerechosHumanos(sentencia.getDerechosHumanosCuyaViolacionPlanteaByDerechosHumanos());
                sen.setViolacionOrganicaAnalizadaenSentencia(sentencia.getViolacionOrganicaAnalizadaenSentencia());
                sen.setTipoViolacionInvacionEsferasMateriaAnalizadaenSentencia(sentencia.getTipoViolacionInvacionEsferasMateriaAnalizadaenSentencia());
                sen.setTipoViolacionInvacionPoderesAnalizadoenSentencia(sentencia.getTipoViolacionInvacionPoderesAnalizadoenSentencia());
                sen.setMomentoSurteEfectoSentencia(sentencia.getMomentoSurteEfectoSentencia());
                sen.setNumeroVotacionesRealizadas(sentencia.getNumeroVotacionesRealizadas());
                sen.setNumeroVotacionesRegistradas(sentencia.getNumeroVotacionesRegistradas());
                sen.setTramiteEngrose(sentencia.getTramiteEngrose());
                sen.setDiasTranscurridosEntreDictadoSentenciayFirmaEngrose(sentencia.getDiasTranscurridosEntreDictadoSentenciayFirmaEngrose());
                sen.setFechaSentencia(sentencia.getFechaSentencia());
                sen.setFechaFirmaEngrose(sentencia.getFechaFirmaEngrose());
                sen.setMateriaAnalizadaInvasionEsferas(sentencia.getMateriaAnalizadaInvasionEsferas());
                sen.setMateriaAnalizadaInvasionPoderes(sentencia.getMateriaAnalizadaInvasionPoderes());
                    /*sen.setVotacionCausasImprocedenciaySobreseimientoAnalizadas(sentencia.getVotacionCausasImprocedenciaySobreseimientoAnalizadas());
                    sen.setVotacionDerechosHumanosCuyaViolacionAnalizaSentencia(sentencia.getVotacionDerechosHumanosCuyaViolacionAnalizaSentencia());
                    sen.setVotacionTipoVicioProcesoLegislativoAnalizadoenSentencia(sentencia.getVotacionTipoVicioProcesoLegislativoAnalizadoenSentencia());
                    sen.setVotacionTipoViolacionInvacionEsferasAnalizadoenSentencia(sentencia.getVotacionTipoViolacionInvacionEsferasAnalizadoenSentencia());
                    sen.setVotacionTipoViolacionInvacionPoderesAnalizadoenSentencia(sentencia.getVotacionTipoViolacionInvacionPoderesAnalizadoenSentencia());*/
                sen.setAcumulados(sentencia.getAcumulados());
                sen.setAcumulada(sentencia.getAcumulada());
                sen.setCA(sentencia.getCA());
                sen.setOrigen(sentencia.getOrigen());
                sen.setPorcentajeCaptura(calculaPorcentajeCaptura(sentencia));

                return sentenciaUpdate;
            });
            
        }
        porcentajeService.updatePorcentaje(sentenciaUpdate.get());
        return _sentenciaRepository.save(sentenciaUpdate.get());
    }
    
    

  
    private Double calculaPorcentajeCaptura(Sentencia sentencia) {
        return 15.0;
    }

    private void deleteVotacionesSentencias(List<String> listVotaciones, String idSentencia) {
        if (listVotaciones != null && listVotaciones.size() > 0) {
            listVotaciones.forEach(id -> _votacionService.deleteVotacion(id, idSentencia, false));
        }
    }

//    private Sentencia getSentenciasTotalesVotacion(Sentencia sentencia) {
//
//        sentencia.setTotalVotacionCausasImprocedenciaySobreseimientoAnalizadas(getTotalVotacion(sentencia.getVotacionCausasImprocedenciaySobreseimientoAnalizadas()));
//        sentencia.setTotalVotacionDerechosHumanosCuyaViolacionAnalizaSentencia(getTotalVotacion(sentencia.getVotacionDerechosHumanosCuyaViolacionAnalizaSentencia()));
//        sentencia.setTotalVotacionTipoVicioProcesoLegislativoAnalizadoenSentencia(getTotalVotacion(sentencia.getVotacionTipoVicioProcesoLegislativoAnalizadoenSentencia()));
//        sentencia.setTotalVotacionTipoViolacionInvacionEsferasAnalizadoenSentencia(getTotalVotacion(sentencia.getVotacionTipoViolacionInvacionEsferasAnalizadoenSentencia()));
//        sentencia.setTotalVotacionTipoViolacionInvacionPoderesAnalizadoenSentencia(getTotalVotacion(sentencia.getVotacionTipoViolacionInvacionPoderesAnalizadoenSentencia()));
//
//        return _sentenciaRepository.save(sentencia);
//    }

//    private Integer getTotalVotacion(List<String> listIDVotaciones) {
//        AtomicReference<Integer> total = new AtomicReference<>(0);
//        if (listIDVotaciones != null && listIDVotaciones.size() > 0) {
//            listIDVotaciones.stream().forEach(id -> {
//                if (id != null) {
//                    total.updateAndGet(v -> v + _votacionService.getNumeroVotosByVotacion(id));
//                }
//            });
//            return total.get();
//        }
//        return total.get();
//    }

    private SentenciaDTO getSentenciaConvertDTO(Sentencia sentencia) {

        SentenciaDTO sentenciaDTO = new SentenciaDTO();
        sentenciaDTO.setId(sentencia.getId());
        sentenciaDTO.setDerechosHumanosCuyaViolacionPlantea(sentencia.getDerechosHumanosCuyaViolacionPlantea());
        sentenciaDTO.setDiasTranscurridosEntreDictadoSentenciayFirmaEngrose(sentencia.getDiasTranscurridosEntreDictadoSentenciayFirmaEngrose());
        sentenciaDTO.setFechaFirmaEngrose(sentencia.getFechaFirmaEngrose());
        sentenciaDTO.setFechaResolucion(sentencia.getFechaResolucion());
        sentenciaDTO.setFechaSentencia(sentencia.getFechaSentencia());
        sentenciaDTO.setMomentoSurteEfectoSentencia(sentencia.getMomentoSurteEfectoSentencia());
        sentenciaDTO.setNumeroExpediente(sentencia.getNumeroExpediente());
        sentenciaDTO.setNumeroVotacionesRealizadas(sentencia.getNumeroVotacionesRealizadas());
        sentenciaDTO.setNumeroVotacionesRegistradas(sentencia.getNumeroVotacionesRegistradas());
        sentenciaDTO.setOrganoRadicacion(sentencia.getOrganoRadicacion());
        sentenciaDTO.setPonente(sentencia.getPonente());
        sentenciaDTO.setTipoAsunto(sentencia.getTipoAsunto());
        sentenciaDTO.setTipoPersonaJuridicoColectiva(sentencia.getTipoPersonaJuridicoColectiva());
        sentenciaDTO.setTipoPersonaPromovente(sentencia.getTipoPersonaPromovente());
        sentenciaDTO.setTipoVicioProcesoLegislativo(sentencia.getTipoVicioProcesoLegislativo());
        sentenciaDTO.setTipoViolacionInvacionEsferas(sentencia.getTipoViolacionInvacionEsferas());
        sentenciaDTO.setTipoViolacionInvacionEsferasMateriaAnalizadaenSentencia(sentencia.getTipoViolacionInvacionEsferasMateriaAnalizadaenSentencia());
        sentenciaDTO.setTipoViolacionInvacionPoderes(sentencia.getTipoViolacionInvacionPoderes());
        sentenciaDTO.setTipoViolacionInvacionPoderesAnalizadoenSentencia(sentencia.getTipoViolacionInvacionPoderesAnalizadoenSentencia());
        sentenciaDTO.setTipoViolacionPlanteadaenDemanda(sentencia.getTipoViolacionPlanteadaenDemanda());
        sentenciaDTO.setTramiteEngrose(sentencia.getTramiteEngrose());
        sentenciaDTO.setViolacionOrganicaAnalizadaenSentencia(sentencia.getViolacionOrganicaAnalizadaenSentencia());
        sentenciaDTO.setViolacionOrganicaPlanteada(sentencia.getViolacionOrganicaPlanteada());
        sentenciaDTO.setCA(sentencia.getCA());
        sentenciaDTO.setAcumulados(sentencia.getAcumulados());
        sentenciaDTO.setAcumulada(sentencia.getAcumulada());
        sentenciaDTO.setDerechosHumanosCuyaViolacionPlanteaByDerechosHumanos(sentencia.getDerechosHumanosCuyaViolacionPlanteaByDerechosHumanos());
        
        sentenciaDTO.setMateriaAnalizadaInvasionEsferas(sentencia.getMateriaAnalizadaInvasionEsferas());
        sentenciaDTO.setMateriaAnalizadaInvasionPoderes(sentencia.getMateriaAnalizadaInvasionPoderes());
        
        sentenciaDTO.setTotalVotacionCausasImprocedenciaySobreseimientoAnalizadas(sentencia.getTotalVotacionCausasImprocedenciaySobreseimientoAnalizadas());
        sentenciaDTO.setTotalVotacionDerechosHumanosCuyaViolacionAnalizaSentencia(sentencia.getTotalVotacionDerechosHumanosCuyaViolacionAnalizaSentencia());
        sentenciaDTO.setTotalVotacionTipoVicioProcesoLegislativoAnalizadoenSentencia(sentencia.getTotalVotacionTipoVicioProcesoLegislativoAnalizadoenSentencia());
        sentenciaDTO.setTotalVotacionTipoViolacionInvacionEsferasAnalizadoenSentencia(sentencia.getTotalVotacionTipoViolacionInvacionEsferasAnalizadoenSentencia());
        sentenciaDTO.setTotalVotacionTipoViolacionInvacionPoderesAnalizadoenSentencia(sentencia.getTotalVotacionTipoViolacionInvacionPoderesAnalizadoenSentencia());

        sentenciaDTO.setVotacionCausasImprocedenciaySobreseimientoAnalizadas(getVotacionesSentencias(sentencia.getVotacionCausasImprocedenciaySobreseimientoAnalizadas()));
        sentenciaDTO.setVotacionDerechosHumanosCuyaViolacionAnalizaSentencia(getVotacionesSentencias(sentencia.getVotacionDerechosHumanosCuyaViolacionAnalizaSentencia()));
        sentenciaDTO.setVotacionTipoVicioProcesoLegislativoAnalizadoenSentencia(getVotacionesSentencias(sentencia.getVotacionTipoVicioProcesoLegislativoAnalizadoenSentencia()));
        sentenciaDTO.setVotacionTipoViolacionInvacionEsferasAnalizadoenSentencia(getVotacionesSentencias(sentencia.getVotacionTipoViolacionInvacionEsferasAnalizadoenSentencia()));
        sentenciaDTO.setVotacionTipoViolacionInvacionPoderesAnalizadoenSentencia(getVotacionesSentencias(sentencia.getVotacionTipoViolacionInvacionPoderesAnalizadoenSentencia()));

        return sentenciaDTO;
    }

    private List<TablaVotacionesRubrosDTO> getVotacionesSentencias(List<String> listIDVotaciones) {
        List<TablaVotacionesRubrosDTO> listVotaciones = new ArrayList<>();
        if (listIDVotaciones != null && listIDVotaciones.size() > 0) {
            listIDVotaciones.stream().forEach(id -> {
                if (id != null) {
                    listVotaciones.add(_votacionService.getVotacionById(id));
                }
            });
            return listVotaciones;
        }
        return listVotaciones;
    }

}
