package mx.gob.scjn.ugacj.sga_captura.service;

import mx.gob.scjn.ugacj.sga_captura.domain.Sentencia;
import mx.gob.scjn.ugacj.sga_captura.domain.SentenciaMetadatos;
import mx.gob.scjn.ugacj.sga_captura.repository.SentenciaMetadatosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SentenciasMetadatosService {

    @Autowired
    private SentenciaMetadatosRepository _sentenciaMetadatosRepository;

    @Autowired
    private MongoOperations mongoOperations;

    public Page<SentenciaMetadatos> getAllSentenciasMetaPages(Pageable paging, String filtros){

        long count = 0;
        Page<SentenciaMetadatos> sentenciaPaginable = null;
        List<SentenciaMetadatos> sentencias = null;
        Page<SentenciaMetadatos> sentenciasPaginableResponse = null;
        Pageable paging2 = PageRequest.of(paging.getPageNumber()+1, paging.getPageSize());
        if (filtros != null) {
            Query query = new Query().with(paging);
            Query queryNoPage = new Query();
            String subs[] = filtros.split(",");
            for (String sub : subs) {
                String sTempFiltros[] = sub.split(":");
                String filtro = sTempFiltros[0].trim();
                String filtroValores = sTempFiltros[1].trim();
                query.addCriteria(Criteria.where(filtro).is(filtroValores));
                queryNoPage.addCriteria(Criteria.where(filtro).is(filtroValores));
            }
            List<SentenciaMetadatos> listPrecedentes = mongoOperations.find(query, SentenciaMetadatos.class);
            count = mongoOperations.count(queryNoPage, SentenciaMetadatos.class);

            sentenciaPaginable = new PageImpl<SentenciaMetadatos>(listPrecedentes, paging2, count);
            sentencias = sentenciaPaginable.getContent();
        }else {
            sentenciaPaginable = _sentenciaMetadatosRepository.findAll(paging);
            sentencias = sentenciaPaginable.getContent();
            count = sentenciaPaginable.getTotalElements();
        }

        sentenciasPaginableResponse = new PageImpl<SentenciaMetadatos>(sentencias, paging2, count);
        return  sentenciasPaginableResponse;
    }

    public SentenciaMetadatos getSentenciaMetaById(String sentenciaId){
        Optional<SentenciaMetadatos> sentencia = _sentenciaMetadatosRepository.findById(sentenciaId);
        if(sentencia.isPresent()) {
            return sentencia.get();
        }
        return null;
    }

    public SentenciaMetadatos saveSentenciaMeta(SentenciaMetadatos sentencia){
        return  _sentenciaMetadatosRepository.save(sentencia);
    }

    public SentenciaMetadatos updateSentenciaMeta(SentenciaMetadatos sentencia){

        Optional<SentenciaMetadatos> sentenciaUpdate = _sentenciaMetadatosRepository.findById(sentencia.getId());

        if(sentenciaUpdate.isPresent()) {
            sentenciaUpdate.map(sen -> {
                sen.setArchivoURL(sentencia.getArchivoURL());
                sen.setAsuntoId(sentencia.getAsuntoId());
                sen.setIdEngrose(sentencia.getIdEngrose());
                sen.setIdEngrose(sentencia.getIdEngrose());
                return sentenciaUpdate;
            });
        }
        return  _sentenciaMetadatosRepository.save(sentenciaUpdate.get());
    }

    public void deleteSentenciaMeta(String id){
        _sentenciaMetadatosRepository.deleteById(id);
    }
}
