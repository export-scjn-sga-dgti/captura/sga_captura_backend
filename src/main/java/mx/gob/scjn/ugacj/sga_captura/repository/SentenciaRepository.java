package mx.gob.scjn.ugacj.sga_captura.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import mx.gob.scjn.ugacj.sga_captura.domain.Sentencia;

@Repository
public interface SentenciaRepository extends MongoRepository<Sentencia,String>{

    Sentencia findByTipoAsuntoAndNumeroExpedienteAndOrganoRadicacion(String tipoAsunto, String numeroExpediente, String organoRadicacion);

    Sentencia findByTipoAsuntoAndNumeroExpediente(String tipoAsunto, String numeroExpediente);

	Page<Sentencia> findAllByOrderByFechaResolucionDesc(Pageable paging);

}
