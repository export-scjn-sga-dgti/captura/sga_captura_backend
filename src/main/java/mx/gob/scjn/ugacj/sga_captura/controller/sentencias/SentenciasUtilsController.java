package mx.gob.scjn.ugacj.sga_captura.controller.sentencias;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mx.gob.scjn.ugacj.sga_captura.dto.sentencias.utils.ActoReclamadoDTO;
import mx.gob.scjn.ugacj.sga_captura.dto.sentencias.utils.OrigenDocumentoDTO;
import mx.gob.scjn.ugacj.sga_captura.service.sentencias.SentenciasUtilsService;

@RestController
@RequestMapping("/api/sga")
public class SentenciasUtilsController {

	@Autowired
	private SentenciasUtilsService service;

	@CrossOrigin()
	@RequestMapping(method = RequestMethod.POST, path = "/reclamado", produces = "application/json")
	public ResponseEntity<String> getActoReclamado(@RequestBody ActoReclamadoDTO acto) {
		System.out.println("U" + acto.getAnio());
		String actoReclamado = service.getActoReclamado(acto);
		return new ResponseEntity<String>(actoReclamado, HttpStatus.OK);
	}

	@CrossOrigin()
	@RequestMapping(method = RequestMethod.POST, path = "/listado-hojas", produces = "application/json")
	public ResponseEntity<OrigenDocumentoDTO[]> getHojasVotacion(@RequestBody ActoReclamadoDTO acto) {

		return new ResponseEntity<OrigenDocumentoDTO[]>(service.getHojasVotacion(acto), HttpStatus.OK);
	}

	@CrossOrigin
	@RequestMapping(method = RequestMethod.GET, path = "/documento")
	public ResponseEntity<?> getDocumento(@RequestParam(name = "origen") int origen,
			@RequestParam(name = "asuntoId") Long asuntoId, @RequestParam(name = "documentoId") String documentoId,
			@RequestParam(name = "name") String name) {

		OrigenDocumentoDTO origenObj = new OrigenDocumentoDTO();
		origenObj.setAsuntoID(asuntoId);
		origenObj.setOrigen(origen);
		origenObj.setDocumentoID(documentoId);
		return service.getDocumento(origenObj, name);
	}
}
