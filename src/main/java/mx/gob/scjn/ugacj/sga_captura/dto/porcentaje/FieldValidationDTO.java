package mx.gob.scjn.ugacj.sga_captura.dto.porcentaje;

import java.util.List;

public class FieldValidationDTO {

	String fieldName;
	String criterio;//NotEmpty, OptionEmbebed-DefaultNotEmpty, ValueEmbebed-boolean
	List<String> optionValue;
	double value;
	List<FieldValidationDTO> fieldsValidation;
	
	public FieldValidationDTO() {
		super();
	}
	
	public FieldValidationDTO(String fieldName, String criterio, double value,
			List<FieldValidationDTO> fieldsValidation) {
		super();
		this.fieldName = fieldName;
		this.criterio = criterio;
		this.value = value;
		this.fieldsValidation = fieldsValidation;
	}
	
	public FieldValidationDTO(String fieldName, String criterio, List<String> optionValue, double value, List<FieldValidationDTO> fieldsValidation) {
		super();
		this.fieldName = fieldName;
		this.criterio = criterio;
		this.optionValue = optionValue;
		this.fieldsValidation = fieldsValidation;
		this.value = value;
	}


	public FieldValidationDTO(String fieldName, String criterio, List<String> optionValue, double value) {
		super();
		this.fieldName = fieldName;
		this.criterio = criterio;
		this.optionValue = optionValue;
		this.value = value;
	}

	public FieldValidationDTO(String fieldName, double value, String criterio) {
		super();
		this.fieldName = fieldName;
		this.criterio = criterio;
		this.value = value;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getCriterio() {
		return criterio;
	}

	public void setCriterio(String criterio) {
		this.criterio = criterio;
	}

	public List<String> getOptionValue() {
		return optionValue;
	}

	public void setOptionValue(List<String> optionValue) {
		this.optionValue = optionValue;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public List<FieldValidationDTO> getFieldsValidation() {
		return fieldsValidation;
	}

	public void setFieldsValidation(List<FieldValidationDTO> fieldsValidation) {
		this.fieldsValidation = fieldsValidation;
	}
	
}
