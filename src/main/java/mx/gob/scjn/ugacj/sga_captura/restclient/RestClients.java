package mx.gob.scjn.ugacj.sga_captura.restclient;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

import java.util.stream.Collectors;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;

public class RestClients {

    @Autowired
    private HttpServletRequest request;
	
    public InputStream callToServicePost2(String url, String paramsSend, String accesToken ){
  		HttpClient client = HttpClientBuilder.create().build();
          HttpPost post = new HttpPost(url);
          post.setEntity(new StringEntity(paramsSend, Charset.forName("UTF-8")));
          try {
              post.addHeader("Content-Type", "application/json; charset=utf-8");
              post.addHeader("Authorization", "Bearer "+accesToken);
              HttpResponse response = client.execute(post);
              return new ByteArrayInputStream(IOUtils.toByteArray(response.getEntity().getContent()));
          } catch (IOException e) {
              e.printStackTrace();
          }
          return null;
  	}
    
// realiza la consulta al elastic
	public String callToServicePost(String bodyParams, String url){

		HttpClient client = HttpClientBuilder.create().build();
        HttpPost post = new HttpPost(url);

        post.setEntity(new StringEntity(bodyParams, Charset.forName("UTF-8")));
        
        try {
            post.addHeader("Content-Type", "application/json");
            post.addHeader("Authorization", getTokenUser(1));
            HttpResponse response = client.execute(post);
            HttpEntity entity = response.getEntity();
            // lee el contenido del entity y se guarda en un string
            String content = EntityUtils.toString(entity);
            System.out.println("Done");
            return content;
            
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
	}
	
	public JsonObject callToServiceGet(String urlTo) {
		
		OkHttpClient client = new OkHttpClient();
		Request request = new Request.Builder()
	            .url(urlTo)
	            .build();		
	        Response response=null;
			try {
				response = client.newCall(request).execute();
				JsonObject convertedObject = new Gson().fromJson(response.body().string(), JsonObject.class);
				return convertedObject;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        return null;
	}
	
	public String callToServicePut(String bodyParams, String url){

		HttpClient client = HttpClientBuilder.create().build();
        HttpPut put = new HttpPut(url);

        put.setEntity(new StringEntity(bodyParams, Charset.forName("UTF-8")));
        
        try {
        	put.addHeader("Content-Type", "application/json");
            HttpResponse response = client.execute(put);
            HttpEntity entity = response.getEntity();
            // lee el contenido del entity y se guarda en un string
            String content = EntityUtils.toString(entity);
            System.out.println("Done");
            return content;
            
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
	}

    private String getTokenUser(Integer opcion){
        String value = "";
        System.out.println(request);
        switch (opcion){
            case 1:
                value = request.getHeader("Authorization");
                break;
            case  2:
                value = request.getHeader("username");
                break;
        }
        return value;
    }

}
