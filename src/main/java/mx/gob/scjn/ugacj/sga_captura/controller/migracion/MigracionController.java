package mx.gob.scjn.ugacj.sga_captura.controller.migracion;

import java.text.ParseException;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import mx.gob.scjn.ugacj.sga_captura.domain.VersionTaquigrafica;
import mx.gob.scjn.ugacj.sga_captura.dto.statecode.ResponseDTO;
import mx.gob.scjn.ugacj.sga_captura.dto.statecode.VtaqResponseDTO;
import mx.gob.scjn.ugacj.sga_captura.dto.vtaquigraficas.AsuntoAbordadoDTO;
import mx.gob.scjn.ugacj.sga_captura.service.migracion.AcuerdosMigracionService;
import mx.gob.scjn.ugacj.sga_captura.service.migracion.MigracionService;
import mx.gob.scjn.ugacj.sga_captura.service.migracion.UsuariosMigracionService;
import mx.gob.scjn.ugacj.sga_captura.service.migracion.VotosMigracionService;

@RestController
@RequestMapping( value = "/api/sga/migracion")

public class MigracionController {
	@Autowired
	private MigracionService _migracionService;
	
	@Autowired
	private VotosMigracionService votosMigracionService;
	
	@Autowired
	private AcuerdosMigracionService acuerdosMigracionService;
	
	@Autowired
	private UsuariosMigracionService usuariosMigracionService;
    
    @RequestMapping(method = RequestMethod.GET,path = "/populate/vtaq/", produces = "application/json")
    @CrossOrigin()
    public List<AsuntoAbordadoDTO> populateTaquigraficas(){
    	try {
			_migracionService.populateDatabaseTaq();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return null;
    }
    
    @RequestMapping(method = RequestMethod.GET,path = "/contadores/", produces = "application/json")
    @CrossOrigin()
    public ResponseDTO contadores(){
//    	_migracionService.deteccionIssuesVtaq();
    	_migracionService.deteccionIssuesSentencias();
    	_migracionService.contadoresSentencias();
//    	_migracionService.contadoresVtaq();
    	return null;
    }
    
    
    @RequestMapping(method = RequestMethod.GET,path = "/temasp-repetidos/", produces = "application/json")
    @CrossOrigin()
    public ResponseDTO findTemasRepetidos(){
    	_migracionService.findTemasProcesalesRepetidos();
    	return null;
    }
    
    @RequestMapping(method = RequestMethod.GET,path = "/populate/sentencias/", produces = "application/json")
    @CrossOrigin()
    public ResponseDTO populateSentencias(){
    	return _migracionService.populateSentencias();
    }
    
    @RequestMapping(method = RequestMethod.GET,path = "/populate/votos/", produces = "application/json")
    @CrossOrigin()
    public ResponseDTO populateVotos(){
    	return votosMigracionService.populateVotos();
    }   
    
    @RequestMapping(method = RequestMethod.GET,path = "/populate/acuerdos/", produces = "application/json")
    @CrossOrigin()
    public ResponseDTO populateAcuerdos(){
    	return acuerdosMigracionService.populateAcuerdos();
    }
    
    @RequestMapping(method = RequestMethod.GET,path = "/populate/normativas/", produces = "application/json")
    @CrossOrigin()
    public ResponseDTO populateNormativas(){
    	return _migracionService.populateNormativas();
    }
    
    @RequestMapping(method = RequestMethod.GET,path = "/populate/usuarios/", produces = "application/json")
    @CrossOrigin()
    public ResponseDTO populateUsuarios(){
    	return usuariosMigracionService.populateUsuarios();
    }
    
    @RequestMapping(method = RequestMethod.GET,path = "/print/catalogos/", produces = "application/json")
    @CrossOrigin()
    public void printCatalogos(){
    	usuariosMigracionService.printCatalogos();
    }
    
    @RequestMapping(method = RequestMethod.GET,path = "/tipo-asunto-ct", produces = "application/json")
    @CrossOrigin()
    public void updateTipoAsuntoCC(){
    	//Se va a cambiar de forma masiva el tipo de dato Contradicción de Tesis
    	String cc = "CONTRADICCIÓN DE TESIS";
    	String ccAct = "CONTRADICCIÓN DE CRITERIOS (ANTES CONTRADICCIÓN DE TESIS)";
    	
    	_migracionService.updateTipoAsuntoCC(cc, ccAct);
    }
    
}
