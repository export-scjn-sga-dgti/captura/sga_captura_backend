package mx.gob.scjn.ugacj.sga_captura.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.mongodb.client.DistinctIterable;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Filters;

import mx.gob.scjn.ugacj.sga_captura.domain.Catalogo;
import mx.gob.scjn.ugacj.sga_captura.domain.Usuario;
import mx.gob.scjn.ugacj.sga_captura.dto.CatalogoDTO;
import mx.gob.scjn.ugacj.sga_captura.dto.InformacionDTO;
import mx.gob.scjn.ugacj.sga_captura.repository.CatalogoRepository;
import mx.gob.scjn.ugacj.sga_captura.repository.UsuarioRepository;
import mx.gob.scjn.ugacj.sga_captura.service.bitacora.BitacoraService;
import mx.gob.scjn.ugacj.sga_captura.utils.ConvertFechaFiltros;
import mx.gob.scjn.ugacj.sga_captura.utils.RegexUtils;
import mx.gob.scjn.ugacj.sga_captura.utils.StatusBitacora;

@Service
public class CatalogoService {

	@Value("${indice.catalogo}")
    private String catalogo;
	
	@Autowired
    MongoTemplate mongoTemplate;
	
	@Autowired
	CatalogoRepository catRepository;
	
	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	private MongoOperations mongoOperations;

	@Autowired
	MongoTemplate mongot;
	
	@Autowired
	private BitacoraService _bitacoraService;

	public List<Catalogo> getAllCatalogos() {
		return catRepository.findAll();
	}

	public List<Catalogo> getCatalogoByNombre(String nombrecatalogo) {
		return catRepository.findByCatalogoOrderByNombreAsc(nombrecatalogo);
	}

	public CatalogoDTO getCatalogosByPantalla(String nombrePantalla, String fecha) {
		CatalogoDTO catalogos = null;
		switch (nombrePantalla) {
		case "sentencias":
			catalogos = new CatalogoDTO();

			List<Catalogo> listCatalogoTipoAsunto = catRepository
					.findByCatalogoOrderByNombreAsc("sentencia_tipoAsunto");
			catalogos.setTipoAsunto(listCatalogoTipoAsunto);

			List<Catalogo> listCatalogoOrgano = catRepository
					.findByCatalogoOrderByNombreAsc("sentencia_organoRadicacion");
			catalogos.setOrganoRadicacion(listCatalogoOrgano);

			List<Catalogo> listCatalogoPersonaPromovente = catRepository
					.findByCatalogoOrderByNombreAsc("sentencia_tipoPersonaPromovente");
			catalogos.setPersonaPromovente(listCatalogoPersonaPromovente);

			List<Catalogo> listCatalogoPersonaJuridicoColectiva = catRepository
					.findByCatalogoOrderByNombreAsc("sentencia_tipoPersonaJuridicaColectiva");
			catalogos.setPersonaJuridicoColectiva(listCatalogoPersonaJuridicoColectiva);

			List<Catalogo> listCatalogoViolacionPlanteadaDemanda = catRepository
					.findByCatalogoOrderByNombreAsc("sentencia_tipoViolacionPlanteadaenlaDemanda");
			catalogos.setViolacionPlanteadaDemanda(listCatalogoViolacionPlanteadaDemanda);

			List<Catalogo> listCatalogoDerechosHumanos = catRepository
					.findByCatalogoOrderByNombreAsc("sentencias_derechosHumanos");
			catalogos.setDerechosHumanos(listCatalogoDerechosHumanos);

			List<Catalogo> listCatalogoViolacionOrganicaAnalizada = catRepository
					.findByCatalogoOrderByNombreAsc("sentencias_violacionOrganicaAnalizada");
			catalogos.setViolacionOrganicaAnalizada(listCatalogoViolacionOrganicaAnalizada);

			List<Catalogo> listCatalogovicioProcesoLegislativo = catRepository
					.findByCatalogoOrderByNombreAsc("sentencia_tipoVicioProcesoLegislativo");
			catalogos.setVicioProcesoLegislativo(listCatalogovicioProcesoLegislativo);

			List<Catalogo> listCatalogoViolacionInvacionEsferas = catRepository
					.findByCatalogoOrderByNombreAsc("sentencias_violacionInvacionEsferas");
			catalogos.setViolacionInvacionEsferas(listCatalogoViolacionInvacionEsferas);

			List<Catalogo> listCatalogoViolacionInvacionPoderes = catRepository
					.findByCatalogoOrderByNombreAsc("sentencias_violacionInvacionPoderes");
			catalogos.setViolacionInvacionPoderes(listCatalogoViolacionInvacionPoderes);

			List<Catalogo> listCatalogoCausasImprocedencia = catRepository
					.findByCatalogoOrderByNombreAsc("sentencias_causasImprocedenciaAnalizadas");
			catalogos.setCausaImprocedencia(listCatalogoCausasImprocedencia);

			List<Catalogo> listCatalogoMetodologia = catRepository
					.findByCatalogoOrderByNombreAsc("sentencias_metodologiaAnalisisConstitucionalidad");
			catalogos.setAnalisisConstitucionlidad(listCatalogoMetodologia);

			List<Catalogo> listCatalogoTipoSentencia = catRepository
					.findByCatalogoOrderByNombreAsc("sentencia_tipoSentencia");
			catalogos.setTipoSentencia(listCatalogoTipoSentencia);

			List<Catalogo> listCatalogoCongresoEmitio = catRepository
					.findByCatalogoOrderByNombreAsc("sentencia_congresoEmitio");
			catalogos.setCongresoEmitio(listCatalogoCongresoEmitio);

			List<Catalogo> listCatalogoInvacionEsferasAnalizada = catRepository
					.findByCatalogoOrderByNombreAsc("sentencia_invacionEsferasAnalizado");
			catalogos.setViolacionInvacionEsferasAnalizada(listCatalogoInvacionEsferasAnalizada);

			List<Catalogo> listCatalogoInvacionPoderesAnalizado = catRepository
					.findByCatalogoOrderByNombreAsc("sentencia_invacionPoderesAnalizado");
			catalogos.setViolacionInvacionPoderesAnalizado(listCatalogoInvacionPoderesAnalizado);

			List<Catalogo> listCatalogoMateria = catRepository
					.findByCatalogoOrderByNombreAsc("sentencias_violacionInvacionEsferas");
			catalogos.setMateria(listCatalogoMateria);

			List<Catalogo> listCatalogoMomentoSurteEfectoinvalidez = catRepository
					.findByCatalogoOrderByNombreAsc("sentencia_momentoEfectoInvalidez");
			catalogos.setDeclaracionInvalidez(listCatalogoMomentoSurteEfectoinvalidez);

			List<Catalogo> listCatalogoTramiteEngrose = catRepository
					.findByCatalogoOrderByNombreAsc("sentencia_tramiteEngrose");
			catalogos.setTramiteEngrose(listCatalogoTramiteEngrose);

			List<Catalogo> listCatalogoMinistros = catRepository.findByCatalogoOrderByNombreAsc("ministros");
			catalogos.setMinistros(listCatalogoMinistros);

			List<Catalogo> listCatalogoSentidoResolucion = catRepository
					.findByCatalogoOrderByNombreAsc("sentencia_sentidoResolucion");
			catalogos.setSentidoResolucion(listCatalogoSentidoResolucion);
			;
			
			List<Catalogo> listCatalogoViolacionInvacionEsferasFederal = catRepository
					.findByCatalogoOrderByNombreAsc("sentencia_invasionEnferasFederal");
			catalogos.setViolacionInvacionEsferasFederal(listCatalogoViolacionInvacionEsferasFederal);
			
//			List<Catalogo> listCatalogoViolacionInvacionEsferasLocal = catRepository
//					.findByCatalogoOrderByNombreAsc("sentencia_invasionEnferasLocal");
//			catalogos.setViolacionInvacionEsferasLocal(listCatalogoViolacionInvacionEsferasLocal);
			
			List<Catalogo> listCatalogoViolacionInvacionEsferasMunicipal = catRepository
					.findByCatalogoOrderByNombreAsc("sentencia_invasionEnferasMunicipal");
			catalogos.setViolacionInvacionEsferasMunicipal(listCatalogoViolacionInvacionEsferasMunicipal);

			break;
		case "versionesT":
			catalogos = new CatalogoDTO();

			if (fecha == null) {
				List<Catalogo> listCatalogoAsuntoAbordado = catRepository
						.findByCatalogoOrderByNombreAsc("sentencia_tipoAsunto");
				catalogos.setAsuntoAbordado(listCatalogoAsuntoAbordado);
			} else {

				List<Catalogo> listCatalogoAsuntoAbordado = catRepository
						.findByCatalogoOrderByNombreAsc("sentencia_tipoAsunto");
				catalogos.setAsuntoAbordado(listCatalogoAsuntoAbordado);

				List<Catalogo> listCatalogoMinistrosVT = consultarMinistrosPorLapso(fecha);
				catalogos.setMinistros(listCatalogoMinistrosVT);
			}
			break;
		case "votos":
			catalogos = new CatalogoDTO();

			List<Catalogo> listCatalogoMetodologiaAnalisis = catRepository
					.findByCatalogoOrderByNombreAsc("sentencias_metodologiaAnalisisConstitucionalidad");
			catalogos.setAnalisisConstitucionlidad(listCatalogoMetodologiaAnalisis);

			List<Catalogo> listCatalogoTipoAsuntoVoto = catRepository.findByCatalogoOrderByNombreAsc("voto_tipoAsunto");
			catalogos.setVoto_tipoAsunto(listCatalogoTipoAsuntoVoto);

			List<Catalogo> listCatalogoTipoVoto = catRepository.findByCatalogoOrderByNombreAsc("voto_tipoVoto");
			catalogos.setTipoVoto(listCatalogoTipoVoto);

			List<Catalogo> listCatalogoTemaAbordado = catRepository
					.findByCatalogoOrderByNombreAsc("voto_tipoTemaAbordado");
			catalogos.setTipoTemaAbordado(listCatalogoTemaAbordado);

			List<Catalogo> listCatalogoTipoTemaProcesal = catRepository
					.findByCatalogoOrderByNombreAsc("voto_tipoTemaProcesal");
			catalogos.setTipoTemaProcesal(listCatalogoTipoTemaProcesal);

			List<Catalogo> listCatalogoTipoTemaSustantivo = catRepository
					.findByCatalogoOrderByNombreAsc("voto_tipoTemaSustantivo");
			catalogos.setTipoTemaSustantivo(listCatalogoTipoTemaSustantivo);
			
//			Catálogos de sentencias--
			List<Catalogo> listCatalogoDerechosHumanosSentencias = catRepository
					.findByCatalogoOrderByNombreAsc("sentencias_derechosHumanos");
			catalogos.setDerechosHumanos(listCatalogoDerechosHumanosSentencias);
			
			List<Catalogo> listCatalogoSentidoResolucionSentencias = catRepository
					.findByCatalogoOrderByNombreAsc("sentencia_sentidoResolucion");
			catalogos.setSentidoResolucion(listCatalogoSentidoResolucionSentencias);
			
			List<Catalogo> listCatalogoTipoSentenciaSentencia = catRepository
					.findByCatalogoOrderByNombreAsc("sentencia_tipoSentencia");
			catalogos.setTipoSentencia(listCatalogoTipoSentenciaSentencia);
			
			List<Catalogo> listCatalogoViolacionOrganicaAnalizadaSentencia = catRepository
					.findByCatalogoOrderByNombreAsc("sentencias_violacionOrganicaAnalizada");
			catalogos.setViolacionOrganicaAnalizada(listCatalogoViolacionOrganicaAnalizadaSentencia);
			
			List<Catalogo> listCatalogovicioProcesoLegislativoSentencia = catRepository
					.findByCatalogoOrderByNombreAsc("sentencia_tipoVicioProcesoLegislativo");
			catalogos.setVicioProcesoLegislativo(listCatalogovicioProcesoLegislativoSentencia);
			
			List<Catalogo> listCatalogoInvacionEsferasAnalizadaSentencia = catRepository
					.findByCatalogoOrderByNombreAsc("sentencia_invacionEsferasAnalizado");
			catalogos.setViolacionInvacionEsferasAnalizada(listCatalogoInvacionEsferasAnalizadaSentencia);

			List<Catalogo> listCatalogoInvacionPoderesAnalizadoSentencia = catRepository
					.findByCatalogoOrderByNombreAsc("sentencia_invacionPoderesAnalizado");
			catalogos.setViolacionInvacionPoderesAnalizado(listCatalogoInvacionPoderesAnalizadoSentencia);
			
			List<Catalogo> listCatalogoViolacionInvacionEsferasSentencia = catRepository
					.findByCatalogoOrderByNombreAsc("sentencias_violacionInvacionEsferas");
			catalogos.setViolacionInvacionEsferas(listCatalogoViolacionInvacionEsferasSentencia);

			List<Catalogo> listCatalogoViolacionInvacionPoderesSentencia = catRepository
					.findByCatalogoOrderByNombreAsc("sentencias_violacionInvacionPoderes");
			catalogos.setViolacionInvacionPoderes(listCatalogoViolacionInvacionPoderesSentencia);
			
			List<Catalogo> listCatalogoCausasImprocedenciaVoto = catRepository
					.findByCatalogoOrderByNombreAsc("sentencias_causasImprocedenciaAnalizadasVotos");
			catalogos.setCausaImprocedencia(listCatalogoCausasImprocedenciaVoto);
			
			List<Catalogo> listCatalogoViolacionInvacionEsferasFederalSentencia = catRepository
					.findByCatalogoOrderByNombreAsc("sentencia_invasionEnferasFederal");
			catalogos.setViolacionInvacionEsferasFederal(listCatalogoViolacionInvacionEsferasFederalSentencia);
			
//			List<Catalogo> listCatalogoViolacionInvacionEsferasLocalSentencia = catRepository
//					.findByCatalogoOrderByNombreAsc("sentencia_invasionEnferasLocal");
//			catalogos.setViolacionInvacionEsferasLocal(listCatalogoViolacionInvacionEsferasLocalSentencia);
			
			List<Catalogo> listCatalogoViolacionInvacionEsferasMunicipalSentencia = catRepository
					.findByCatalogoOrderByNombreAsc("sentencia_invasionEnferasMunicipal");
			catalogos.setViolacionInvacionEsferasMunicipal(listCatalogoViolacionInvacionEsferasMunicipalSentencia);
			
			

//			End Catálogos de sentencias--
			
			if (fecha == null) {
				List<Catalogo> listCatalogoMinistrosVotos = catRepository.findByCatalogoOrderByNombreAsc("ministros");
				catalogos.setMinistros(listCatalogoMinistrosVotos);
			} else {
				List<Catalogo> listCatalogoMinistrosVT = consultarMinistrosPorLapso(fecha);
				catalogos.setMinistros(listCatalogoMinistrosVT);
			}
			
			break;
		case "acuerdos":
			catalogos = new CatalogoDTO();

			if (fecha == null) {
				List<Catalogo> listCatalogoAmbitoIncide = catRepository
						.findByCatalogoOrderByNombreAsc("acuerdos_ambitoIncide");
				catalogos.setAmbitoIncide(listCatalogoAmbitoIncide);

				List<Catalogo> listCatalogoOrganoEmisor = catRepository
						.findByCatalogoOrderByNombreAsc("acuerdos_organo");
				catalogos.setOrganoEmisor(listCatalogoOrganoEmisor);

				List<Catalogo> listCatalogoVigencia = catRepository.findByCatalogoOrderByNombreAsc("acuerdos_vigencia");
				catalogos.setVigencia(listCatalogoVigencia);

				List<Catalogo> listCatalogoVotacion = catRepository.findByCatalogoOrderByNombreAsc("acuerdos_votacion");
				catalogos.setVotacion(listCatalogoVotacion);

				List<Catalogo> listCatalogoMateriaRegulacion = catRepository
						.findByCatalogoOrderByNombreAsc("normativa_materiaRegulacion");
				catalogos.setMateriaRegulacion(listCatalogoMateriaRegulacion);
				
				List<Catalogo> listCatalogoPresidentes = catRepository
						.findByCatalogoOrderByNombreAsc("ministros_presidentes");
				catalogos.setMinistrosPresidentes(listCatalogoPresidentes);

			} else {
				List<Catalogo> listCatalogoAmbitoIncide = catRepository
						.findByCatalogoOrderByNombreAsc("acuerdos_ambitoIncide");
				catalogos.setAmbitoIncide(listCatalogoAmbitoIncide);

				List<Catalogo> listCatalogoOrganoEmisor = catRepository
						.findByCatalogoOrderByNombreAsc("acuerdos_organo");
				catalogos.setOrganoEmisor(listCatalogoOrganoEmisor);

				List<Catalogo> listCatalogoVigencia = catRepository.findByCatalogoOrderByNombreAsc("acuerdos_vigencia");
				catalogos.setVigencia(listCatalogoVigencia);

				List<Catalogo> listCatalogoVotacion = catRepository.findByCatalogoOrderByNombreAsc("acuerdos_votacion");
				catalogos.setVotacion(listCatalogoVotacion);

				List<Catalogo> listCatalogoMateriaRegulacion = catRepository
						.findByCatalogoOrderByNombreAsc("normativa_materiaRegulacion");
				catalogos.setMateriaRegulacion(listCatalogoMateriaRegulacion);

				fecha = ConvertFechaFiltros.changeFormat(fecha, RegexUtils.REGEX_DDMMYYYY);
				List<Catalogo> listCatalogoMinistrosAcuerdos = consultarMinistrosPorLapso(fecha);
				catalogos.setMinistros(listCatalogoMinistrosAcuerdos);
				
				List<Catalogo> listCatalogoPresidentes = catRepository
						.findByCatalogoOrderByNombreAsc("ministros_presidentes");
				catalogos.setMinistrosPresidentes(listCatalogoPresidentes);

			}
			break;
		case "diversa":
			catalogos = new CatalogoDTO();
			
				List<Catalogo> listCatalogoMateriaRegulacion = catRepository
						.findByCatalogoOrderByNombreAsc("normativa_materiaRegulacion");
				catalogos.setMateriaRegulacion(listCatalogoMateriaRegulacion);

				List<Catalogo> listCatalogoAmbitoIncide = catRepository
						.findByCatalogoOrderByNombreAsc("normativa_ambitoIncide");
				catalogos.setAmbitoIncide(listCatalogoAmbitoIncide);

				List<Catalogo> listCatalogoTipoCircular = catRepository
						.findByCatalogoOrderByNombreAsc("normativa_tipoCircular");
				catalogos.setTipoCircular(listCatalogoTipoCircular);

				List<Catalogo> listCatalogoTipoInstrumento = catRepository
						.findByCatalogoOrderByNombreAsc("normativa_tipoInstrumento");
				catalogos.setTipoInstrumento(listCatalogoTipoInstrumento);
				
				List<Catalogo> listCatalogoPresidentes = catRepository
						.findByCatalogoOrderByNombreAsc("ministros_presidentes");
				catalogos.setMinistrosPresidentes(listCatalogoPresidentes);
			
			break;
		case "seguimiento":
			catalogos = new CatalogoDTO();
			List<Catalogo> listCatalogoTipoAsuntoSeguimiento = catRepository
			.findByCatalogoOrderByNombreAsc("sentencia_tipoAsunto");
			catalogos.setTipoAsunto(listCatalogoTipoAsuntoSeguimiento);
			
			List<Usuario> listCatalogoSecretarioSeguimiento = usuarioRepository.findAll();
			List<Catalogo> listCatalogoSecretariosSeguimiento = new ArrayList<Catalogo>();
			Catalogo catalogo = null;
			List<String> usuariosList = new ArrayList<String>(Arrays.asList("ggarciab", "avazquez", "jegonzaleza", "atoledob", "cmariscal", "fgsandovalf"));
			for(Usuario usuario:listCatalogoSecretarioSeguimiento) {
				if(!usuariosList.contains(usuario.getCorreo())) {
					catalogo = new Catalogo();
					catalogo.setNombre(usuario.getNombre());
					catalogo.setCatalogo("usuarios");
					catalogo.setId(usuario.getId());
					listCatalogoSecretariosSeguimiento.add(catalogo);
					catalogos.setUsuarios(listCatalogoSecretariosSeguimiento);
				}
				
			}
	
			break;
		case "bitacora":
			catalogos = new CatalogoDTO();

			
			List<Usuario> listCatalogoSecretarioBitacora = usuarioRepository.findAll();
			List<Catalogo> listCatalogoSecretariosBitacora = new ArrayList<Catalogo>();
			Catalogo catalogoBit = null;
			List<String> usuariosListBit = new ArrayList<String>(Arrays.asList("ggarciab", "avazquez", "jegonzaleza", "atoledob", "cmariscal", "fgsandovalf"));
			for(Usuario usuario:listCatalogoSecretarioBitacora) {
				if(!usuariosListBit.contains(usuario.getCorreo())) {
					catalogoBit = new Catalogo();
					catalogoBit.setNombre(usuario.getNombre());
					catalogoBit.setCorreo(usuario.getCorreo());
					catalogoBit.setCatalogo("usuarios");
					catalogoBit.setId(usuario.getId());
					listCatalogoSecretariosBitacora.add(catalogoBit);
					catalogos.setUsuarios(listCatalogoSecretariosBitacora);
				}
				
			}
	
			break;
		default:
		}
		return catalogos;
	}
	
	public List<Catalogo> consultarMinistrosPorLapso(String fecha) {
		List<Catalogo> listMinistrosByFecha = new ArrayList<Catalogo>();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		Date date = null;
		try {
			date = formatter.parse(fecha);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Query query = new Query();
		query.addCriteria(Criteria.where("catalogo").is("ministros"));
		query.addCriteria(Criteria.where("primeraSesion").lte(date));
		query.addCriteria(Criteria.where("ultimaSesion").gte(date));
		listMinistrosByFecha = mongoOperations.find(query, Catalogo.class, catalogo);

		return listMinistrosByFecha;

	}

	public Catalogo saveCatalogo(Catalogo catalogo) {
		//Busca que el catalogo no exista
		Query query = new Query();
		query.addCriteria(Criteria.where("catalogo").is(catalogo.getCatalogo()));
		query.addCriteria(Criteria.where("nombre").is(catalogo.getNombre().trim()));

		long count = mongoOperations.count(query, Catalogo.class);
		if (count <= 0) {
			if(catalogo.getCatalogo().equals("ministros")) {
				catalogo.setPrimeraSesion(catalogo.getFechaInicio());
				catalogo.setUltimaSesion(catalogo.getFechaFin());
				}
			catRepository.save(catalogo);
            _bitacoraService.saveOperacion(StatusBitacora._CATALOGO, StatusBitacora._SAVE, catalogo, null);
			return catalogo;
			
		}

		return null;
	}

    public boolean updateCatalogo(String idCatalogo, Catalogo catalogo) {
        Optional<Catalogo> catalogoExist = catRepository.findById(idCatalogo);
        if (catalogoExist.isPresent()) {
            if (catRepository.save(catalogo) != null) {
                _bitacoraService.saveOperacion(StatusBitacora._CATALOGO, StatusBitacora._UPDATE, catalogo, catalogoExist.get());
                return true;
            }
            return false;
        }
        return false;
    }
    
    public List<InformacionDTO> consultarDatosPorColeccionYCampo(String mini,String coleccion, String campo) {
        List<InformacionDTO> Informacions = new ArrayList<InformacionDTO>();
        DistinctIterable<String> ej = mongoTemplate.getCollection(coleccion).distinct(campo,
            Filters.regex(campo, "^" + mini, "i"), String.class);
        MongoCursor<String> cursor = ej.iterator();
        while (cursor.hasNext()) {
          String x = (String) cursor.next();
          InformacionDTO obj = new InformacionDTO();
          obj.setInfo(x);
          Informacions.add(obj);
        }
        return Informacions;
      }

    public boolean deleteCatalogo(String idCatalogo) {
        Optional<Catalogo> catalogoExist = catRepository.findById(idCatalogo);
        if (catalogoExist.isPresent()) {
            _bitacoraService.saveOperacion(StatusBitacora._CATALOGO, StatusBitacora._DELETE, catalogoExist.get(), null);
            catRepository.deleteById(idCatalogo);
            return true;
        }
        return false;
    }

	public Catalogo getCatalogoById(String idCatalogo) {
		 Optional<Catalogo> catalogoExist = catRepository.findById(idCatalogo);
	        if (catalogoExist.isPresent()) {
	            return catalogoExist.get();
	        }
	        return null;
	    }
}
