package mx.gob.scjn.ugacj.sga_captura.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import mx.gob.scjn.ugacj.sga_captura.domain.SentenciaMetadatos;
import org.springframework.stereotype.Repository;

@Repository
public interface SentenciaMetadatosRepository extends MongoRepository<SentenciaMetadatos,String>{


}
