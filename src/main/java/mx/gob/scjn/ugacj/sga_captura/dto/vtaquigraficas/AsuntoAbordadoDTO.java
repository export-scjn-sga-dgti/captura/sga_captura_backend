package mx.gob.scjn.ugacj.sga_captura.dto.vtaquigraficas;

import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import mx.gob.scjn.ugacj.sga_captura.domain.AsuntoAbordado;
import mx.gob.scjn.ugacj.sga_captura.domain.VTaquigraficaAsuntos;
import mx.gob.scjn.ugacj.sga_captura.utils.validaciones.AsuntosAbordados.ValidatorsAsuntosAbordadosConstrains;

public class AsuntoAbordadoDTO {
	
	@NotEmpty(message ="El campo Id de la V.Taquigrafica no puede estar vacío")
	String idVersionTaqui;
	
	@ValidatorsAsuntosAbordadosConstrains
	AsuntoAbordado asunto;
	
	VTaquigraficaAsuntos vt_asuntos;
	
	public String getIdVersionTaqui() {
		return idVersionTaqui;
	}
	public void setIdVersionTaqui(String idVersionTaqui) {
		this.idVersionTaqui = idVersionTaqui;
	}
	public AsuntoAbordado getAsunto() {
		return asunto;
	}
	public void setAsunto(AsuntoAbordado asunto) {
		this.asunto = asunto;
	}
	public VTaquigraficaAsuntos getVt_asuntos() {
		return vt_asuntos;
	}
	public void setVt_asuntos(VTaquigraficaAsuntos vt_asuntos) {
		this.vt_asuntos = vt_asuntos;
	}
}
