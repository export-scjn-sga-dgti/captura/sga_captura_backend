package mx.gob.scjn.ugacj.sga_captura.service.sentencias;

import mx.gob.scjn.ugacj.sga_captura.domain.RubrosTablaVotaciones;
import mx.gob.scjn.ugacj.sga_captura.domain.TablaVotaciones;
import mx.gob.scjn.ugacj.sga_captura.dto.RubrosTablaVotacionesDTO;
import mx.gob.scjn.ugacj.sga_captura.dto.TablaVotacionesRubrosDTO;
import mx.gob.scjn.ugacj.sga_captura.repository.VotacionRepository;
import mx.gob.scjn.ugacj.sga_captura.repository.VotacionesRubrosRepository;
import mx.gob.scjn.ugacj.sga_captura.service.bitacora.BitacoraService;
import mx.gob.scjn.ugacj.sga_captura.utils.StatusBitacora;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class RubrosTablaVotacionesService {

    @Autowired
    private VotacionesRubrosRepository _votacionesRubrosRepository;
    
    @Autowired
    private VotacionRepository _votacionRepository;

    @Autowired
    private TablasVotacionesService _votacionService;
    
    @Autowired
    private SentenciaService sentenciaService;

    @Autowired
    private MongoOperations mongoOperations;
    
    @Autowired
	private BitacoraService _bitacoraService;

    public RubrosTablaVotaciones getVotacionRubroById(String votoId) {
        Optional<RubrosTablaVotaciones> voto = _votacionesRubrosRepository.findById(votoId);
        if (voto.isPresent()) {
            return voto.get();
        }
        return null;
    }

    public String saveRubro(RubrosTablaVotaciones voto) {
        return _votacionesRubrosRepository.save(voto).getId();
    }
   

    public RubrosTablaVotaciones saveRubroVotacion(RubrosTablaVotacionesDTO rubroDTO) {
        String id = _votacionesRubrosRepository.save(rubroDTO.getRubro()).getId();
        rubroDTO.getRubro().setId(id);
        _votacionService.saveRubroByVotacion(rubroDTO);
        sentenciaService.updateTotalVotaciones(rubroDTO.getIdSentencia());
        _bitacoraService.saveOperacion(StatusBitacora._SENTENCIAS_VOTACION_RUBROS, StatusBitacora._SAVE, rubroDTO.getRubro(), null);
        return rubroDTO.getRubro();
    }

    public RubrosTablaVotaciones updateOrdenRubro(RubrosTablaVotaciones rubro) {
    	Optional<RubrosTablaVotaciones> rubroUpdate = _votacionesRubrosRepository.findById(rubro.getId());
    	if(rubroUpdate.isPresent()) {
    		rubroUpdate.get().setRubrosTematicos(rubro.getRubrosTematicos());
        	return _votacionesRubrosRepository.save(rubroUpdate.get());
    	}
    	return null;
    }
    
    public RubrosTablaVotaciones updateVotacionRubro(RubrosTablaVotaciones voto) {
        Optional<RubrosTablaVotaciones> votoRubroUpdate = _votacionesRubrosRepository.findById(voto.getId());
        if (votoRubroUpdate.isPresent()) {
            votoRubroUpdate.map(vot -> {
                vot.setId(voto.getId());
                vot.setRubrosTematicos(voto.getRubrosTematicos());
                vot.setTextoVotacion(voto.getTextoVotacion());
                vot.setTipoVotacion(voto.getTipoVotacion());
                vot.setTipoRubro(voto.getTipoRubro());
                vot.setVotacionEnContra(voto.getVotacionEnContra());
                vot.setVotacionFavor(voto.getVotacionFavor());
                vot.setMinistros(voto.getMinistros());
                vot.setAnalisisProcedencia(voto.getAnalisisProcedencia());
                return votoRubroUpdate;
            });
        }
        _bitacoraService.saveOperacion(StatusBitacora._SENTENCIAS_VOTACION_RUBROS, StatusBitacora._UPDATE, votoRubroUpdate.get(), voto);
        return _votacionesRubrosRepository.save(votoRubroUpdate.get());
    }
    
    public RubrosTablaVotaciones updateVotacionYRubro(RubrosTablaVotacionesDTO rubroTablaVotacionesDTO) {
    	RubrosTablaVotaciones voto = rubroTablaVotacionesDTO.getRubro();
    	
    	Optional<TablaVotaciones> votacion = _votacionRepository.findById(rubroTablaVotacionesDTO.getIdVotacion());
    	if(voto.getSentido() != null) votacion.get().setSentidoResolucion(voto.getSentido());
    	if(voto.getTipoSentencia() != null) votacion.get().setTipoSentencia(voto.getTipoSentencia());
    	_votacionRepository.save(votacion.get());
    	
        Optional<RubrosTablaVotaciones> votoRubroUpdate = _votacionesRubrosRepository.findById(voto.getId());
        Optional<RubrosTablaVotaciones> votoInicial = _votacionesRubrosRepository.findById(voto.getId());
        if (votoRubroUpdate.isPresent()) {
            votoRubroUpdate.map(vot -> {
                vot.setId(voto.getId());
                vot.setRubrosTematicos(voto.getRubrosTematicos());
                vot.setTextoVotacion(voto.getTextoVotacion());
                vot.setTipoVotacion(voto.getTipoVotacion());
                vot.setTipoRubro(voto.getTipoRubro());
                vot.setVotacionEnContra(voto.getVotacionEnContra());
                vot.setVotacionFavor(voto.getVotacionFavor());
                vot.setMinistros(voto.getMinistros());
                vot.setAnalisisProcedencia(voto.getAnalisisProcedencia());
                return votoRubroUpdate;
            });
        }
        _bitacoraService.saveOperacion(StatusBitacora._SENTENCIAS_VOTACION_RUBROS, StatusBitacora._UPDATE, votoRubroUpdate.get(), votoInicial.get());
        return _votacionesRubrosRepository.save(votoRubroUpdate.get());
    }

    public void deleteVotacionRubroVotacion(String idRubro, String idVotacion, String idSentencia) {
        Optional<RubrosTablaVotaciones> rubro = _votacionesRubrosRepository.findById(idRubro);
        if (rubro.isPresent()) {
            _votacionService.deleteRubroVotacion(idVotacion, rubro.get());
            _votacionesRubrosRepository.deleteById(idRubro);
            sentenciaService.updateTotalVotaciones(idSentencia);
            _bitacoraService.saveOperacion(StatusBitacora._SENTENCIAS_VOTACION_RUBROS, StatusBitacora._DELETE, rubro.get(), null);
        }
    }

    public void deleteVotacionRubro(String id) {
        _votacionesRubrosRepository.deleteById(id);
    }

}
