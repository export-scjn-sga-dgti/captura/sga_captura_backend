package mx.gob.scjn.ugacj.sga_captura.domain;

import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;

@Document(collection = "#{@environment.getProperty('indice.voto')}")
//@JsonInclude(JsonInclude.Include.NON_NULL)
public class Voto {

    @Id
    private String id;
    private  String tipoAsunto;
    private  String numExpediente;
    private List<String> tipoVoto;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private String fechaResolucion;
    private List<String> acumulados;
//    @JsonInclude(JsonInclude.Include.NON_NULL)
    private  Boolean isAcumulada;
//    @JsonInclude(JsonInclude.Include.NON_NULL)
    private  Boolean isCA;
    private List<String> ministro;

// NUEVA SECCIÓN DE CAMBIOS PARA LOS CAMPOS DE LA NUEVA VERSIÓN DE VOTOS
    private List<String> causaImprocedencia;
    private List<String> violacionOrganicaAnalizada;
    private List<String> vicioProcesoLegislativo;
    private List<String> violacionInvacionEsferas;
    private List<String> violacionInvacionEsferasFederal;
    private List<String> violacionInvacionEsferasAnalizada;
    private List<String> violacionInvacionEsferasMunicipal;
    private List<String> violacionInvacionPoderesAnalizado;
    private List<String> derechosHumanos;
    private List<String> sentidoResolucion;
	private List<String> tipoEfectos;    
    
//    CAMPOS PREVIAMENTE EXISTENTES
    private  List<String> analisisConstitucionlidad;
    private  Double porcentaje;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTipoAsunto() {
		return tipoAsunto;
	}
	public void setTipoAsunto(String tipoAsunto) {
		this.tipoAsunto = tipoAsunto;
	}
	public String getNumExpediente() {
		return numExpediente;
	}
	public void setNumExpediente(String numExpediente) {
		this.numExpediente = numExpediente;
	}
	public List<String> getTipoVoto() {
		return tipoVoto;
	}
	public void setTipoVoto(List<String> tipoVoto) {
		this.tipoVoto = tipoVoto;
	}
	public String getFechaResolucion() {
		return fechaResolucion;
	}
	public void setFechaResolucion(String fechaResolucion) {
		this.fechaResolucion = fechaResolucion;
	}
	public List<String> getAcumulados() {
		return acumulados;
	}
	public void setAcumulados(List<String> acumulados) {
		this.acumulados = acumulados;
	}
	public Boolean getIsAcumulada() {
		return isAcumulada;
	}
	public void setIsAcumulada(Boolean isAcumulada) {
		this.isAcumulada = isAcumulada;
	}
	public Boolean getIsCA() {
		return isCA;
	}
	public void setIsCA(Boolean isCA) {
		this.isCA = isCA;
	}
	public List<String> getMinistro() {
		return ministro;
	}
	public void setMinistro(List<String> ministro) {
		this.ministro = ministro;
	}
	public List<String> getCausaImprocedencia() {
		return causaImprocedencia;
	}
	public void setCausaImprocedencia(List<String> causaImprocedencia) {
		this.causaImprocedencia = causaImprocedencia;
	}
	public List<String> getViolacionOrganicaAnalizada() {
		return violacionOrganicaAnalizada;
	}
	public void setViolacionOrganicaAnalizada(List<String> violacionOrganicaAnalizada) {
		this.violacionOrganicaAnalizada = violacionOrganicaAnalizada;
	}
	public List<String> getVicioProcesoLegislativo() {
		return vicioProcesoLegislativo;
	}
	public void setVicioProcesoLegislativo(List<String> vicioProcesoLegislativo) {
		this.vicioProcesoLegislativo = vicioProcesoLegislativo;
	}
	public List<String> getViolacionInvacionEsferas() {
		return violacionInvacionEsferas;
	}
	public void setViolacionInvacionEsferas(List<String> violacionInvacionEsferas) {
		this.violacionInvacionEsferas = violacionInvacionEsferas;
	}
	public List<String> getViolacionInvacionEsferasFederal() {
		return violacionInvacionEsferasFederal;
	}
	public void setViolacionInvacionEsferasFederal(List<String> violacionInvacionEsferasFederal) {
		this.violacionInvacionEsferasFederal = violacionInvacionEsferasFederal;
	}
	public List<String> getViolacionInvacionEsferasAnalizada() {
		return violacionInvacionEsferasAnalizada;
	}
	public void setViolacionInvacionEsferasAnalizada(List<String> violacionInvacionEsferasAnalizada) {
		this.violacionInvacionEsferasAnalizada = violacionInvacionEsferasAnalizada;
	}
	public List<String> getViolacionInvacionEsferasMunicipal() {
		return violacionInvacionEsferasMunicipal;
	}
	public void setViolacionInvacionEsferasMunicipal(List<String> violacionInvacionEsferasMunicipal) {
		this.violacionInvacionEsferasMunicipal = violacionInvacionEsferasMunicipal;
	}
	public List<String> getViolacionInvacionPoderesAnalizado() {
		return violacionInvacionPoderesAnalizado;
	}
	public void setViolacionInvacionPoderesAnalizado(List<String> violacionInvacionPoderesAnalizado) {
		this.violacionInvacionPoderesAnalizado = violacionInvacionPoderesAnalizado;
	}
	public List<String> getDerechosHumanos() {
		return derechosHumanos;
	}
	public void setDerechosHumanos(List<String> derechosHumanos) {
		this.derechosHumanos = derechosHumanos;
	}
	public List<String> getSentidoResolucion() {
		return sentidoResolucion;
	}
	public void setSentidoResolucion(List<String> sentidoResolucion) {
		this.sentidoResolucion = sentidoResolucion;
	}
	public List<String> getTipoEfectos() {
		return tipoEfectos;
	}
	public void setTipoEfectos(List<String> tipoEfectos) {
		this.tipoEfectos = tipoEfectos;
	}
	public List<String> getAnalisisConstitucionlidad() {
		return analisisConstitucionlidad;
	}
	public void setAnalisisConstitucionlidad(List<String> analisisConstitucionlidad) {
		this.analisisConstitucionlidad = analisisConstitucionlidad;
	}
	public Double getPorcentaje() {
		return porcentaje;
	}
	public void setPorcentaje(Double porcentaje) {
		this.porcentaje = porcentaje;
	}
    

//  DEPRECATED
//  private  String tipoTema;
//  private  String tipoTemaProcesalSobreElVoto;
//  private  String temaProcesalSobreElVoto;
//  private  String tipoTemaSustantivoSobreElVoto;
//  private  String temaSustantivoSobreElVoto;
  
  
	
    
}
    