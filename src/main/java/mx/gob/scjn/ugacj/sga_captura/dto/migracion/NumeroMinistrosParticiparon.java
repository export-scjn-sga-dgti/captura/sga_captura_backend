package mx.gob.scjn.ugacj.sga_captura.dto.migracion;

public class NumeroMinistrosParticiparon {
    
    String nombre;
    String cantidad = "0";

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCantidad() {
        return this.cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

}
