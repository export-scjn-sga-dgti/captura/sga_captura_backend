package mx.gob.scjn.ugacj.sga_captura.utils;

import java.util.Arrays;

public class FiltrosUtils {
	public static String[] getSubfiltros(String subfiltro) {
		String[] dots = subfiltro.split(":");
		if(dots.length>2) {
			for(int i=0; i<dots.length; i++) {
				String lineDot = dots[i];
				String[] lineCommas = lineDot.split(",");
				if(lineCommas.length>1&i<dots.length-1) {
					String key = lineCommas[lineCommas.length-1];
					String lastValue = lineDot.replace(","+key, "");
					dots[i-1] = dots[i-1]+":"+lastValue;
					dots[i] = key;
				}else if(i==dots.length-1){
					dots[i-1] = dots[i-1]+":"+dots[i];
				}
			}
			return Arrays.copyOfRange(dots, 0, dots.length-1);
		}
		return new String[]{subfiltro};
	}

}
