package mx.gob.scjn.ugacj.sga_captura.domain;

import java.util.ArrayList;
import java.util.List;

public class TipoVicProLegisAnali {
    List<String> tipoVicio;
    String decretoImpugnado;
    String congreso;
    List<String> rubrosTematicos;
    List<String> votacionAFavorVicioPlanteado;
    List<String> votaciontionEnContraVicioPlanteado;
    List<String> todosMinistrosVicioPlanteado;
    String fundamentacion;
    List<String> tipoSentencia;
    boolean votacionMayoriaVicioPlanteado;
    int cantidadAFavorVicioPlanteado;
    String textoVotacionVicioPlanteado;
    List<VotacionTipoSentencia> votacionesTipoSentencia;
    List<VotacionTipoSentencia> votacionRelacionadosExtensionValidez= new ArrayList<VotacionTipoSentencia>();;

    public List<VotacionTipoSentencia> getVotacionRelacionadosExtensionValidez() {
        return this.votacionRelacionadosExtensionValidez;
    }

    public void setVotacionRelacionadosExtensionValidez(
            List<VotacionTipoSentencia> votacionRelacionadosExtensionValidez) {
        this.votacionRelacionadosExtensionValidez = votacionRelacionadosExtensionValidez;
    }


    public List<VotacionTipoSentencia> getVotacionesTipoSentencia() {
        return this.votacionesTipoSentencia;
    }

    public void setVotacionesTipoSentencia(List<VotacionTipoSentencia> votacionesTipoSentencia) {
        this.votacionesTipoSentencia = votacionesTipoSentencia;
    }

    public String getTextoVotacionVicioPlanteado() {
        return this.textoVotacionVicioPlanteado;
    }

    public void setTextoVotacionVicioPlanteado(String textoVotacionVicioPlanteado) {
        this.textoVotacionVicioPlanteado = textoVotacionVicioPlanteado;
    }

    public boolean isVotacionMayoriaVicioPlanteado() {
        return this.votacionMayoriaVicioPlanteado;
    }

    public void setVotacionMayoriaVicioPlanteado(boolean votacionMayoriaVicioPlanetado) {
        this.votacionMayoriaVicioPlanteado = votacionMayoriaVicioPlanetado;
    }

    public int getCantidadAFavorVicioPlanteado() {
        return this.cantidadAFavorVicioPlanteado;
    }

    public void setCantidadAFavorVicioPlanteado(int cantidadAFavorVicioPlanteado) {
        this.cantidadAFavorVicioPlanteado = cantidadAFavorVicioPlanteado;
    }

    public List<String> getVotacionAFavorVicioPlanteado() {
        return this.votacionAFavorVicioPlanteado;
    }

    public void setVotacionAFavorVicioPlanteado(List<String> votacionAFavorVicioPlanteado) {
        this.votacionAFavorVicioPlanteado = votacionAFavorVicioPlanteado;
    }

    public List<String> getVotaciontionEnContraVicioPlanteado() {
        return this.votaciontionEnContraVicioPlanteado;
    }

    public void setVotaciontionEnContraVicioPlanteado(List<String> votaciontionEnContraVicioPlanteado) {
        this.votaciontionEnContraVicioPlanteado = votaciontionEnContraVicioPlanteado;
    }

    public List<String> getTodosMinistrosVicioPlanteado() {
        return this.todosMinistrosVicioPlanteado;
    }

    public void setTodosMinistrosVicioPlanteado(List<String> todosMinistrosVicioPlanteado) {
        this.todosMinistrosVicioPlanteado = todosMinistrosVicioPlanteado;
    }


    public List<String> getTipoVicio() {
        return this.tipoVicio;
    }

    public void setTipoVicio(List<String> tipoVicio) {
        this.tipoVicio = tipoVicio;
    }

    public String getDecretoImpugnado() {
        return this.decretoImpugnado;
    }

    public void setDecretoImpugnado(String decretoImpugnado) {
        this.decretoImpugnado = decretoImpugnado;
    }

    public String getCongreso() {
        return this.congreso;
    }

    public void setCongreso(String congreso) {
        this.congreso = congreso;
    }

    public List<String> getRubrosTematicos() {
        return this.rubrosTematicos;
    }

    public void setRubrosTematicos(List<String> rubrosTematicos) {
        this.rubrosTematicos = rubrosTematicos;
    }

    public String getFundamentacion() {
        return this.fundamentacion;
    }

    public void setFundamentacion(String fundamentacion) {
        this.fundamentacion = fundamentacion;
    }
    public List<String> getTipoSentencia() {
        return this.tipoSentencia;
    }
    public void setTipoSentencia(List<String> tipoSentencia) {
        this.tipoSentencia = tipoSentencia;
    }

}
