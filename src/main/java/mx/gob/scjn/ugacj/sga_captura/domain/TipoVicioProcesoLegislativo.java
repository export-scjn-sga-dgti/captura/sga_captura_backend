package mx.gob.scjn.ugacj.sga_captura.domain;

public class TipoVicioProcesoLegislativo {
    
    String tipoVicio;
    String numeroDecreto;
    String congresoEmitio;

    
    public String getTipoVicio() {
        return this.tipoVicio;
    }

    public void setTipoVicio(String tipoVicio) {
        this.tipoVicio = tipoVicio;
    }

    public String getNumeroDecreto() {
        return this.numeroDecreto;
    }

    public void setNumeroDecreto(String numeroDecreto) {
        this.numeroDecreto = numeroDecreto;
    }

    public String getCongresoEmitio() {
        return this.congresoEmitio;
    }

    public void setCongresoEmitio(String congresoEmitio) {
        this.congresoEmitio = congresoEmitio;
    }


}
