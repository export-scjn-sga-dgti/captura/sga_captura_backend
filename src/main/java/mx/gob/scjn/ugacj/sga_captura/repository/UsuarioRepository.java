package mx.gob.scjn.ugacj.sga_captura.repository;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import mx.gob.scjn.ugacj.sga_captura.domain.Usuario;

@Repository
public interface UsuarioRepository extends MongoRepository<Usuario,String> {
	Optional<Usuario> findByCorreo(String correo);
	Optional<Usuario> findByNombre(String nombre);
	
	@Query(value = "{'nombre': {$regex : ?0, $options: 'i'}}")
	Optional<Usuario> findByNombreRegex(String nombre);
	
}