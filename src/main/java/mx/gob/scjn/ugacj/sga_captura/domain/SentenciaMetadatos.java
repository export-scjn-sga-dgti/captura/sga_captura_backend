package mx.gob.scjn.ugacj.sga_captura.domain;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Document(collection = "#{@environment.getProperty('indice.sentencia.metadatos')}")
@JsonInclude(Include.NON_NULL)
public class SentenciaMetadatos implements Serializable {
	@Id
	String id;
	String idEngrose;
	String asuntoId;
	String archivoURL;
	String idSentencia;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIdEngrose() {
		return idEngrose;
	}
	public void setIdEngrose(String idEngrose) {
		this.idEngrose = idEngrose;
	}
	public String getAsuntoId() {
		return asuntoId;
	}
	public void setAsuntoId(String asuntoId) {
		this.asuntoId = asuntoId;
	}
	public String getArchivoURL() {
		return archivoURL;
	}
	public void setArchivoURL(String archivoURL) {
		this.archivoURL = archivoURL;
	}
	public String getIdSentencia() {
		return idSentencia;
	}
	public void setIdSentencia(String idSentencia) {
		this.idSentencia = idSentencia;
	}
}
