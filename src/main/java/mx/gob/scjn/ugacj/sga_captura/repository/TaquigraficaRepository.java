package mx.gob.scjn.ugacj.sga_captura.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import mx.gob.scjn.ugacj.sga_captura.domain.Taquigraficas;

@Repository
public interface TaquigraficaRepository extends MongoRepository<Taquigraficas,String> {
   List<Taquigraficas> findAll();
    
}

