package mx.gob.scjn.ugacj.sga_captura.utils;

public class RegexUtils {
	public static final String REGEX_YYYYMMDD = "[0-9]{4}-[0-9]{2}-[0-9]{2}";
	public static final String REGEX_DDMMYYYY = "[0-9]{2}-[0-9]{2}-[0-9]{4}";

}
