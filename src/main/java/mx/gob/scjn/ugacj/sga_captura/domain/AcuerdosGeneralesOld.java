package mx.gob.scjn.ugacj.sga_captura.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonFormat;

@Document(collection ="arcgenerales")
public class AcuerdosGeneralesOld implements Serializable {

    @Id
    String id;
	Date fechaAprobacion;
    String ambitoIncident;
    String vigencia;
    Date fechaPublicacionDOF;
    String [] clasificacionMateriaRegulacion;
    String organoEmisor;
    String ministroPresidente;
    String votacion;
    String [] votacionAcuerdoFavor;
    String[] votacionAcuerdoContra;
    String[] ministrosAusentes;
    String [] temasRegulacion;
    String rubro;
    float porcentaje;
    
    public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Date getFechaAprobacion() {
		return fechaAprobacion;
	}
	public void setFechaAprobacion(Date fechaAprobacion) {
		this.fechaAprobacion = fechaAprobacion;
	}
	public String getAmbitoIncident() {
		return ambitoIncident;
	}
	public void setAmbitoIncident(String ambitoIncident) {
		this.ambitoIncident = ambitoIncident;
	}
	public String getVigencia() {
		return vigencia;
	}
	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}
	public Date getFechaPublicacionDOF() {
		return fechaPublicacionDOF;
	}
	public void setFechaPublicacionDOF(Date fechaPublicacionDOF) {
		this.fechaPublicacionDOF = fechaPublicacionDOF;
	}
	public String[] getClasificacionMateriaRegulacion() {
		return clasificacionMateriaRegulacion;
	}
	public void setClasificacionMateriaRegulacion(String[] clasificacionMateriaRegulacion) {
		this.clasificacionMateriaRegulacion = clasificacionMateriaRegulacion;
	}
	public String getOrganoEmisor() {
		return organoEmisor;
	}
	public void setOrganoEmisor(String organoEmisor) {
		this.organoEmisor = organoEmisor;
	}
	public String getMinistroPresidente() {
		return ministroPresidente;
	}
	public void setMinistroPresidente(String ministroPresidente) {
		this.ministroPresidente = ministroPresidente;
	}
	public String getVotacion() {
		return votacion;
	}
	public void setVotacion(String votacion) {
		this.votacion = votacion;
	}
	public String[] getVotacionAcuerdoFavor() {
		return votacionAcuerdoFavor;
	}
	public void setVotacionAcuerdoFavor(String[] votacionAcuerdoFavor) {
		this.votacionAcuerdoFavor = votacionAcuerdoFavor;
	}
	public String[] getVotacionAcuerdoContra() {
		return votacionAcuerdoContra;
	}
	public void setVotacionAcuerdoContra(String[] votacionAcuerdoContra) {
		this.votacionAcuerdoContra = votacionAcuerdoContra;
	}
	public String[] getMinistrosAusentes() {
		return ministrosAusentes;
	}
	public void setMinistrosAusentes(String[] ministrosAusentes) {
		this.ministrosAusentes = ministrosAusentes;
	}
	public String[] getTemasRegulacion() {
		return temasRegulacion;
	}
	public void setTemasRegulacion(String[] temasRegulacion) {
		this.temasRegulacion = temasRegulacion;
	}
	public String getRubro() {
		return rubro;
	}
	public void setRubro(String rubro) {
		this.rubro = rubro;
	}
	public float getPorcentaje() {
		return porcentaje;
	}
	public void setPorcentaje(float porcentaje) {
		this.porcentaje = porcentaje;
	}


    
}
