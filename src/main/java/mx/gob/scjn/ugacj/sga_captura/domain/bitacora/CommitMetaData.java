package mx.gob.scjn.ugacj.sga_captura.domain.bitacora;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class CommitMetaData {
    String author;
    
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    Date commitDate;

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Date getCommitDate() {
        return commitDate;
    }

    public void setCommitDate(Date commitDate) {
        this.commitDate = commitDate;
    }

    @Override
    public String toString() {
        return "CommitMetaData{" +
                "author='" + author + '\'' +
                ", commitDate=" + commitDate +
                '}';
    }
}
