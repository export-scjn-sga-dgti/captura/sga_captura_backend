package mx.gob.scjn.ugacj.sga_captura.domain;

public class MinistrosVotacion {
    String nombre;
    boolean votacion;

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean isVotacion() {
        return this.votacion;
    }

    public void setVotacion(boolean votacion) {
        this.votacion = votacion;
    }


}
