package mx.gob.scjn.ugacj.sga_captura.controller;

import mx.gob.scjn.ugacj.sga_captura.domain.Sentencia;
import mx.gob.scjn.ugacj.sga_captura.domain.Voto;
import mx.gob.scjn.ugacj.sga_captura.dto.TablaVotacionesRubrosDTO;
import mx.gob.scjn.ugacj.sga_captura.service.VotoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;

@RestController
@RequestMapping( value = "/api/sga")
public class VotoController {

    @Autowired
    private VotoService _votoService;

    @RequestMapping(method = RequestMethod.GET,path = "/voto")
    @CrossOrigin()
    public Page<Voto> getVotosAll(@RequestParam(name = "page") Integer page,
                                  @RequestParam(name = "size") Integer size,
                                  @RequestParam(required = false, name = "filtros") String filtros) throws ParseException {
        Pageable paging = PageRequest.of(page-1, size);
        return _votoService.getAllVotosPages(paging, filtros);
    }

    @RequestMapping(method = RequestMethod.GET,path = "/voto/{votoId}", produces = "application/json")
    @CrossOrigin()
    public Voto getVotoById(@PathVariable String votoId){
        return _votoService.getVotoById(votoId);
    }

    @RequestMapping(method = RequestMethod.POST,path = "/voto")
    @CrossOrigin()
    public Voto saveVoto(@RequestBody Voto voto){
        return _votoService.saveVoto(voto);
    }

    @RequestMapping(method = RequestMethod.PUT,path = "/voto")
    @CrossOrigin()
    public Voto updateVoto(@RequestBody Voto voto){
        return _votoService.updateVoto(voto);
    }

    @RequestMapping(method = RequestMethod.DELETE,path = "/voto/{votoId}")
    @CrossOrigin()
    public void deleteVoto(@PathVariable String votoId){
        _votoService.deleteVoto(votoId);
    }
    
    @RequestMapping(method = RequestMethod.GET, path = "/voto/sentencia")
	public Sentencia getSentencia(@RequestParam(required = false, name = "tipoAsunto") String tipoAsunto,
			@RequestParam(required = false, name = "numExpediente") String numeroExpediente)
			throws ParseException {
		return _votoService.validaSentencia(tipoAsunto, numeroExpediente);
	}
    
    @RequestMapping(method = RequestMethod.GET, path = "/voto/votoBySentencia")
	public Voto getVotoBySentencia(@RequestParam(required = false, name = "tipoAsunto") String tipoAsunto,
			@RequestParam(required = false, name = "numExpediente") String numeroExpediente)
			throws ParseException {
		return _votoService.getVotoBySentencia(tipoAsunto, numeroExpediente);
	}
    
    @RequestMapping(method = RequestMethod.GET, path = "/voto/votacionById")
	public TablaVotacionesRubrosDTO getVotacionByID(@RequestParam(required = false, name = "id") String id)
			throws ParseException {
		return _votoService.getVotacionById(id);
	}
}
