package mx.gob.scjn.ugacj.sga_captura.service;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import mx.gob.scjn.ugacj.sga_captura.domain.Sentencia;
import mx.gob.scjn.ugacj.sga_captura.domain.TablaVotaciones;
import mx.gob.scjn.ugacj.sga_captura.domain.Voto;
import mx.gob.scjn.ugacj.sga_captura.dto.TablaVotacionesRubrosDTO;
import mx.gob.scjn.ugacj.sga_captura.repository.SentenciaRepository;
import mx.gob.scjn.ugacj.sga_captura.repository.VotacionRepository;
import mx.gob.scjn.ugacj.sga_captura.repository.VotoRepository;
import mx.gob.scjn.ugacj.sga_captura.service.bitacora.BitacoraService;
import mx.gob.scjn.ugacj.sga_captura.service.porcentaje.PorcentajeService;
import mx.gob.scjn.ugacj.sga_captura.utils.ConvertFechaFiltros;
import mx.gob.scjn.ugacj.sga_captura.utils.FiltrosUtils;
import mx.gob.scjn.ugacj.sga_captura.utils.StatusBitacora;

@Service
public class VotoService {

    @Autowired
    private VotoRepository _votoRepository;

    @Autowired
    private MongoOperations mongoOperations;
    
	@Autowired
	private BitacoraService _bitacoraService;
	
	@Autowired
	private PorcentajeService porcentajeService;
	
	@Autowired
    private SentenciaRepository _sentenciaRepository;
	
	@Autowired
    private VotacionRepository _votacionRepository;

    public Page<Voto> getAllVotosPages(Pageable paging, String filtros) throws ParseException {

        long count = 0;
        Page<Voto> votoPaginable = null;
        List<Voto> voto = null;
        Page<Voto> votosPaginableResponse = null;
        Pageable paging2 = PageRequest.of(paging.getPageNumber() + 1, paging.getPageSize());
        if (filtros != null) {
            Query query = new Query().with(paging);
            Query queryNoPage = new Query();
            //String subs[] = filtros.split(",");
            String subs[] = FiltrosUtils.getSubfiltros(filtros);
            for (String sub : subs) {
                String sTempFiltros[] = sub.split(":");
                String filtro = sTempFiltros[0].trim();
                String filtroValores = sTempFiltros[1].trim();
                if (filtro.trim().equals("fechaResolucion")) {
                    Date fecha = ConvertFechaFiltros.getFormatFecha(filtroValores.trim(), "yyyy-MM-dd");
                    query.addCriteria(Criteria.where(filtro).gte(fecha).lte(fecha));
                    queryNoPage.addCriteria(Criteria.where(filtro).gte(fecha).lte(fecha));
                } else {
                    query.addCriteria(Criteria.where(filtro).is(filtroValores));
                    queryNoPage.addCriteria(Criteria.where(filtro).is(filtroValores));
                }
            }
            List<Voto> listPrecedentes = mongoOperations.find(query, Voto.class);
            count = mongoOperations.count(queryNoPage, Voto.class);

            votoPaginable = new PageImpl<Voto>(listPrecedentes, paging2, count);
            voto = votoPaginable.getContent();
        } else {
            votoPaginable = _votoRepository.findAll(paging);
            voto = votoPaginable.getContent();
            count = votoPaginable.getTotalElements();
            votosPaginableResponse = new PageImpl<Voto>(voto, paging, count);
            return votosPaginableResponse;
        }

        votosPaginableResponse = new PageImpl<Voto>(voto, paging2, count);
        return votosPaginableResponse;
    }

    public Voto getVotoById(String votoId) {
        Optional<Voto> voto = _votoRepository.findById(votoId);
        if (voto.isPresent()) {
            return voto.get();
        }
        return null;
    }

    public Voto saveVoto(Voto voto) {
    	_bitacoraService.saveOperacion(StatusBitacora._VOTOS, StatusBitacora._SAVE, voto, null);
    	porcentajeService.updatePorcentaje(voto);
        return _votoRepository.save(voto);
    }

    public Voto updateVoto(Voto voto) {
        Optional<Voto> votoUpdate = _votoRepository.findById(voto.getId());
        if (votoUpdate.isPresent()) {
        	_bitacoraService.saveOperacion(StatusBitacora._VOTOS, StatusBitacora._UPDATE, voto, votoUpdate.get());
            
        	votoUpdate.map(vot -> {
        		vot.setId(voto.getId());
        		vot.setTipoAsunto(voto.getTipoAsunto());
        		vot.setNumExpediente(voto.getNumExpediente());
        		vot.setTipoVoto(voto.getTipoVoto());
                vot.setFechaResolucion(voto.getFechaResolucion());
                vot.setAcumulados(voto.getAcumulados());
                vot.setIsAcumulada(voto.getIsAcumulada());
                vot.setIsCA(voto.getIsCA());
                vot.setMinistro(voto.getMinistro());
                
                vot.setCausaImprocedencia(voto.getCausaImprocedencia());
                vot.setViolacionOrganicaAnalizada(voto.getViolacionOrganicaAnalizada());
                vot.setVicioProcesoLegislativo(voto.getVicioProcesoLegislativo());
                vot.setViolacionInvacionEsferas(voto.getViolacionInvacionEsferas());
                vot.setViolacionInvacionEsferasFederal(voto.getViolacionInvacionEsferasFederal());
                vot.setViolacionInvacionEsferasAnalizada(voto.getViolacionInvacionEsferasAnalizada());
                vot.setViolacionInvacionEsferasMunicipal(voto.getViolacionInvacionEsferasMunicipal());
                vot.setViolacionInvacionPoderesAnalizado(voto.getViolacionInvacionPoderesAnalizado());
                vot.setDerechosHumanos(voto.getDerechosHumanos());
                vot.setSentidoResolucion(voto.getSentidoResolucion());
                vot.setTipoEfectos(voto.getTipoEfectos());
                
                vot.setAnalisisConstitucionlidad(voto.getAnalisisConstitucionlidad());
                vot.setPorcentaje(voto.getPorcentaje());

                return votoUpdate;
            });
        }
        porcentajeService.updatePorcentaje(votoUpdate.get());
        return _votoRepository.save(votoUpdate.get());
    }

    public void deleteVoto(String id) {
    	Optional<Voto> votoExist = _votoRepository.findById(id);
    	_bitacoraService.saveOperacion(StatusBitacora._VOTOS, StatusBitacora._DELETE, votoExist.get(), null);
        _votoRepository.deleteById(id);
    }
    
    public Sentencia validaSentencia(String tipoAsunto, String numExpediente) {
    	Sentencia sentenciaValida = _sentenciaRepository.findByTipoAsuntoAndNumeroExpediente(tipoAsunto, numExpediente);
    	return sentenciaValida;
    }
    
    public Voto getVotoBySentencia(String tipoAsunto, String numExpediente) {
    	Voto voto = _votoRepository.findByTipoAsuntoAndNumExpediente(tipoAsunto, numExpediente);
    	//Sentencia sentenciaValida = _sentenciaRepository.findByTipoAsuntoAndNumeroExpediente(tipoAsunto, numExpediente);
    	return voto;
    }
    
    public TablaVotacionesRubrosDTO getVotacionById(String votacionId) {
        Optional<TablaVotaciones> votacion = _votacionRepository.findById(votacionId);
        if (votacion.isPresent()) {
            return getVotacionRubro(votacion.get());
        }
        return null;
    }
    
    
    private TablaVotacionesRubrosDTO getVotacionRubro(TablaVotaciones tablaVotaciones) {
        TablaVotacionesRubrosDTO votacionRubro = new TablaVotacionesRubrosDTO();
        votacionRubro.setId(tablaVotaciones.getId());
        votacionRubro.setActosImpugnados(tablaVotaciones.getActosImpugnados());
        votacionRubro.setAprobadasConcideraciones(tablaVotaciones.getAprobadasConcideraciones());
        votacionRubro.setArticulosImpugnados(tablaVotaciones.getArticulosImpugnados());
        votacionRubro.setCongresoEmitioDecreto(tablaVotaciones.getCongresoEmitioDecreto());
        votacionRubro.setDecretoImpugnado(tablaVotaciones.getDecretoImpugnado());
        votacionRubro.setDerechosHumanos(tablaVotaciones.getDerechosHumanos());
        votacionRubro.setMetodologiaAnalisisConstitucionalidad(tablaVotaciones.getMetodologiaAnalisisConstitucionalidad());
        votacionRubro.setSentidoResolucion(tablaVotaciones.getSentidoResolucion());
        votacionRubro.setTextoVotacion(tablaVotaciones.getTextoVotacion());
        votacionRubro.setTipoSentencia(tablaVotaciones.getTipoSentencia());
        votacionRubro.setTipoSentencia(tablaVotaciones.getTipoSentencia());
        votacionRubro.setTipoVicio(tablaVotaciones.getTipoVicio());
        votacionRubro.setTipoViolacionInvacionEsferas(tablaVotaciones.getTipoViolacionInvacionEsferas());
        votacionRubro.setTipoViolacionInvacionPoderes(tablaVotaciones.getTipoViolacionInvacionPoderes());
        votacionRubro.setTipoVotacion(tablaVotaciones.getTipoVotacion());
        votacionRubro.setVotacionEnContra(tablaVotaciones.getVotacionEnContra());
        votacionRubro.setVotacionFavor(tablaVotaciones.getVotacionFavor());
        votacionRubro.setMinistros(tablaVotaciones.getMinistros());
        votacionRubro.setVotacionRubrosTematicos(tablaVotaciones.getVotacionRubrosTematicos());
        votacionRubro.setRubrosTematicosPrincipal(tablaVotaciones.getRubrosTematicosPrincipal());
        votacionRubro.setMateriaAnalizadaPorInvacionEsferas(tablaVotaciones.getMateriaAnalizadaPorInvacionEsferas());
        votacionRubro.setMateriaAnalizadaPorInvacionPoderes(tablaVotaciones.getMateriaAnalizadaPorInvacionPoderes());

        /*if (tablaVotaciones.getRubrosTematicos() != null) {
            votacionRubro.setRubrosTematicos(getRubrosByVotacion(tablaVotaciones.getRubrosTematicos()));
        }

        if (tablaVotaciones.getRubrosEfectoSentencia() != null) {
            votacionRubro.setRubrosEfectoSentencia(getRubrosByVotacion(tablaVotaciones.getRubrosEfectoSentencia()));
        }

        if (tablaVotaciones.getRubrosExtencionInvalidez() != null) {
            votacionRubro.setRubrosExtencionInvalidez(getRubrosByVotacion(tablaVotaciones.getRubrosExtencionInvalidez()));
        }*/
        return votacionRubro;
    }

}
