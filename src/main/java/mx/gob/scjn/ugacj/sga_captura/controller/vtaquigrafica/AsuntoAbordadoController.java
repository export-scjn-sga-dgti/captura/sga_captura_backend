package mx.gob.scjn.ugacj.sga_captura.controller.vtaquigrafica;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mx.gob.scjn.ugacj.sga_captura.dto.statecode.AsuntoResponseDTO;
import mx.gob.scjn.ugacj.sga_captura.dto.vtaquigraficas.AsuntoAbordadoDTO;
import mx.gob.scjn.ugacj.sga_captura.service.vtaquigrafica.AsuntoAbordadoService;

@RestController
@RequestMapping(path = "/api/sga/asuntos_abordados")
public class AsuntoAbordadoController {

	@Autowired
	private AsuntoAbordadoService _asuntoService;
	
	@CrossOrigin()
	@RequestMapping(method = RequestMethod.GET, path = "/{idVt}", produces = "application/json")
	public AsuntoAbordadoDTO getAsuntoAbordado(@PathVariable String idVt,
			@RequestParam(name = "idAsunto", required = true) String idAsunto ) {
		return _asuntoService.getAsuntoAbordado(idVt,idAsunto);
	}
	
	@CrossOrigin()
	@RequestMapping(method = RequestMethod.POST, path = "/")
	public AsuntoResponseDTO saveAsuntoAbordado(@Valid @RequestBody AsuntoAbordadoDTO asunto) {
		System.out.println(asunto.toString());
		return _asuntoService.getAction(asunto, "CREATE");
	}
	
	@CrossOrigin()
	@RequestMapping(method = RequestMethod.PUT, path = "/", produces = "application/json")
	public AsuntoResponseDTO updateAsuntoAbordado(@Valid @RequestBody AsuntoAbordadoDTO asunto) {
		return _asuntoService.getAction(asunto, "UPDATE");
	}
	
	@CrossOrigin()
	@RequestMapping(method = RequestMethod.DELETE, path = "/{idVt}/{id}", produces = "application/json")
	public boolean deleteAsuntoAbordado(@PathVariable String idVt, @PathVariable String id) {
		return _asuntoService.deleteAsuntoAbordado(idVt, id);
	}
	
	@CrossOrigin()
	@RequestMapping(method = RequestMethod.DELETE, path = "/vtaq/{idVt}/asunto/{idAsunto}/tipotema/{tipoTema}/tema/{tema}", produces = "application/json")//procesal,fondo
	public boolean deletetema(@PathVariable String idVt, @PathVariable String idAsunto, @PathVariable String tipoTema, @PathVariable String tema) {
		return _asuntoService.deleteTema(idVt, idAsunto, tipoTema, tema);
	}
	
	@CrossOrigin()
	@RequestMapping(method = RequestMethod.PUT, path = "/vtaq/{idVt}/asunto/{idAsunto}/tipotema/{tipoTema}", produces = "application/json")//procesal,fondo
	public boolean addtema(@PathVariable String idVt, @PathVariable String idAsunto, @PathVariable String tipoTema, @RequestBody List<String> temas) {
		return _asuntoService.addUpdatetema(idVt, idAsunto, tipoTema, temas, "add");
	}
}
