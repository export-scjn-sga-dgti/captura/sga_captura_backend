package mx.gob.scjn.ugacj.sga_captura.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import mx.gob.scjn.ugacj.sga_captura.domain.DiversaNormativa;

public interface DiversaNormativaRepository extends MongoRepository<DiversaNormativa,String> {

}
