package mx.gob.scjn.ugacj.sga_captura.service.vtaquigrafica;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import mx.gob.scjn.ugacj.sga_captura.domain.AsuntoAbordado;
import mx.gob.scjn.ugacj.sga_captura.domain.Sentencia;
import mx.gob.scjn.ugacj.sga_captura.domain.VTaquigraficaAsuntos;
import mx.gob.scjn.ugacj.sga_captura.domain.VersionTaquigrafica;
import mx.gob.scjn.ugacj.sga_captura.dto.PorcentajeDTO;
import mx.gob.scjn.ugacj.sga_captura.dto.statecode.AsuntoResponseDTO;
import mx.gob.scjn.ugacj.sga_captura.dto.statecode.VtaqResponseDTO;
import mx.gob.scjn.ugacj.sga_captura.dto.vtaquigraficas.AsuntoDTO;
import mx.gob.scjn.ugacj.sga_captura.dto.vtaquigraficas.SentenciaIndexObjectDTO;
import mx.gob.scjn.ugacj.sga_captura.dto.vtaquigraficas.TablaVersionesTaquigraficasDTO;
import mx.gob.scjn.ugacj.sga_captura.dto.vtaquigraficas.VersionTaquigraficaDTO;
import mx.gob.scjn.ugacj.sga_captura.repository.AsuntoAbordadoRepository;
import mx.gob.scjn.ugacj.sga_captura.repository.SentenciaRepository;
import mx.gob.scjn.ugacj.sga_captura.repository.VTaquigraficaAsuntosRepository;
import mx.gob.scjn.ugacj.sga_captura.repository.VersionTaquigraficaRepository;
import mx.gob.scjn.ugacj.sga_captura.service.bitacora.BitacoraService;
import mx.gob.scjn.ugacj.sga_captura.utils.CalculoPorcentajeUtils;
import mx.gob.scjn.ugacj.sga_captura.utils.ConvertFechaFiltros;
import mx.gob.scjn.ugacj.sga_captura.utils.FiltrosUtils;
import mx.gob.scjn.ugacj.sga_captura.utils.MethodsUtils;
import mx.gob.scjn.ugacj.sga_captura.utils.RegexUtils;
import mx.gob.scjn.ugacj.sga_captura.utils.StatusBitacora;

@Service
public class VersionTaquigraficaService {

	@Autowired
	private VersionTaquigraficaRepository _vtRepository;

	@Autowired
	private AsuntoAbordadoRepository _asuntoRepository;

	@Autowired
	private VTaquigraficaAsuntosRepository _vtAsuntosRepository;

	@Autowired
	private VTaquigraficaAsuntosRepository vTaquigraficaAsuntosRepository;

	@Autowired
	private MongoOperations mongoOperations;

	@Autowired
	private BitacoraService _bitacoraService;

	@Autowired
	private SentenciaRepository _sentenciaRepository;

	MethodsUtils methods = new MethodsUtils();

	public boolean updatePorcentajeVersionTaquigrafica(VersionTaquigrafica vtaq) {
		List<Double> porcentajes = new ArrayList<Double>();

		List<VTaquigraficaAsuntos> vtaAsunto_TT = vTaquigraficaAsuntosRepository.findByIdVtaquigrafica(vtaq.getId());
		if (vtaAsunto_TT.size() <= 0) {
			porcentajes.add(0d);// porcentaje_asunto
			porcentajes.add(0d);// porcentaje_asuntoVtaq
		}

		for (VTaquigraficaAsuntos tt : vtaAsunto_TT) {
			Optional<AsuntoAbordado> asunto = _asuntoRepository.findById(tt.getIdAsunto());
			if (asunto.isPresent()) {
				PorcentajeDTO porcentajeDTO_asunto = CalculoPorcentajeUtils.mapaPorcetajeAsuntoAbordado();
				double porcentaje_asunto = CalculoPorcentajeUtils.calculoPorcentaje(asunto.get(), porcentajeDTO_asunto);

				PorcentajeDTO porcentajeDTO_asuntoVtaq = CalculoPorcentajeUtils.mapaPorcetajeVTaquigraficaAsuntos();
				double porcentaje_asuntoVtaq = CalculoPorcentajeUtils.calculoPorcentaje(vtaAsunto_TT,
						porcentajeDTO_asuntoVtaq);

				porcentajes.add(porcentaje_asunto);
				porcentajes.add(porcentaje_asuntoVtaq);
			}
		}
		PorcentajeDTO porcentajeDTO_vtaq = CalculoPorcentajeUtils.mapaPorcetajeVersionTaq();
		double porcentaje_vtaq = CalculoPorcentajeUtils.calculoPorcentaje(vtaq, porcentajeDTO_vtaq);

		porcentajes.add(porcentaje_vtaq);
		double sum = porcentajes.stream().mapToDouble(Double::doubleValue).sum();
		double porcentaje = sum / porcentajes.size();
		vtaq.setPorcentaje(CalculoPorcentajeUtils.round(porcentaje, 1));
		System.out.println("porcentaje: " + porcentaje);
		if (_vtRepository.save(vtaq) != null) {
			return true;
		}
		return false;
	}

	public VersionTaquigrafica findByFechaAndDuracion(String fecha, String duracion) {

		Query query = new Query();
		query.addCriteria(Criteria.where("fechaSesion").is(fecha));
		query.addCriteria(Criteria.where("duracionSesion").is(duracion));

		VersionTaquigrafica vt = mongoOperations.findOne(query, VersionTaquigrafica.class);

		return vt;
	}

	public Map<String, Object> filtrosEnTablaTransitiva(String filtros, Pageable paging, long count) {
		Map<String, Object> res = new HashMap<>();
		List<VersionTaquigrafica> vtaquifraficaList = null;
		List<String> filtrosTT = new ArrayList<>(Arrays.asList(""));
		List<String> filtrosT1 = new ArrayList<>(Arrays.asList("fechaSesion"));
		List<String> filtrosT2 = new ArrayList<>(Arrays.asList("asuntoAbordado", "numeroExpediente"));
		String filtrosTTStr = "";
		String filtrosT1Str = "";
		String ffiltrosT2Str = "";

		//String subs[] = filtros.split(",");
		String subs[] = FiltrosUtils.getSubfiltros(filtros);
		for (String sub : subs) {
			String sTempFiltros[] = sub.split(":");
			String filtro = sTempFiltros[0].trim();
			String filtroValores = sTempFiltros[1].trim();
			filtrosTTStr = filtrosTT.contains(filtro) ? filtrosTTStr + sub + "," : filtrosTTStr;
			filtrosT1Str = filtrosT1.contains(filtro) ? filtrosT1Str + sub + "," : filtrosT1Str;
			ffiltrosT2Str = filtrosT2.contains(filtro) ? ffiltrosT2Str + sub + "," : ffiltrosT2Str;
		}
		Query query = new Query().with(paging);
		Query queryNoPage = new Query();

		if (!filtrosT1Str.equals("") && ffiltrosT2Str.equals("")) {
			String sTempFiltros[] = filtrosT1Str.split(":");
			String fecha = ConvertFechaFiltros.changeFormat(sTempFiltros[1].trim(), RegexUtils.REGEX_YYYYMMDD);
			query.addCriteria(Criteria.where(sTempFiltros[0]).gte(fecha).lte(fecha));
			queryNoPage.addCriteria(Criteria.where(sTempFiltros[0]).gte(fecha).lte(fecha));

			// Esta sección quizas en un futuro se pueda usar
			// Query query = new Query(Criteria
			// .where("record.Date_Of_Birth").gte(DateOperators.dateFromString(startDate).withFormat("mm/dd/yyyy"))
			// .andOperator(Criteria.where("record.Date_Of_Birth").lte(DateOperators.dateFromString(endDate).withFormat("mm/dd/yyyy"))
			// )).with(paging);

			vtaquifraficaList = mongoOperations.find(query, VersionTaquigrafica.class);
			count = mongoOperations.count(queryNoPage, VersionTaquigrafica.class);
			Page<VersionTaquigrafica> vtPaginableResult = new PageImpl<VersionTaquigrafica>(vtaquifraficaList, paging,
					count);
			vtaquifraficaList = vtPaginableResult.getContent();

			res.put("vtaquifraficaList", vtaquifraficaList);
			res.put("count", count);
		} else if (!filtrosT1Str.equals("") && !ffiltrosT2Str.equals("")) {
			String sTempFiltros[] = filtrosT1Str.split(":");
			String fecha = ConvertFechaFiltros.changeFormat(sTempFiltros[1].trim(), RegexUtils.REGEX_YYYYMMDD);
			query.addCriteria(Criteria.where(sTempFiltros[0]).gte(fecha).lte(fecha));
			queryNoPage.addCriteria(Criteria.where(sTempFiltros[0]).gte(fecha).lte(fecha));

//				queryNoPage.addCriteria(Criteria.where(sTempFiltros[0]).gte(fecha).lte(fecha));

			vtaquifraficaList = mongoOperations.find(queryNoPage, VersionTaquigrafica.class);
			List<String> vtaqIds = new ArrayList<String>();
			List<String> asuntoIds = new ArrayList<String>();

			for (VersionTaquigrafica vtaq : vtaquifraficaList) {
				vtaqIds.add(vtaq.getId());
			}

			queryNoPage = new Query();
			if(ffiltrosT2Str.endsWith(",")){
				ffiltrosT2Str = ffiltrosT2Str.substring(0,ffiltrosT2Str.length() - 1);
			}
			subs = FiltrosUtils.getSubfiltros(ffiltrosT2Str);
			for (String sub : subs) {
				sTempFiltros = sub.split(":");
				String filtro = sTempFiltros[0].trim();
				String filtroValores = sTempFiltros[1].trim();
				queryNoPage.addCriteria(Criteria.where(filtro).is(filtroValores));
			}
			List<AsuntoAbordado> asuntoAbordadoList = mongoOperations.find(queryNoPage, AsuntoAbordado.class);

			for (AsuntoAbordado asunto : asuntoAbordadoList) {
				asuntoIds.add(asunto.getId());
			}

			List<VTaquigraficaAsuntos> listVtaqAsuntos = _vtAsuntosRepository
					.findAllByIdVtaquigraficaInAndIdAsuntoIn(vtaqIds, asuntoIds);
			vtaqIds = new ArrayList<String>();
			for (VTaquigraficaAsuntos vtaqAsuntos : listVtaqAsuntos) {
				vtaqIds.add(vtaqAsuntos.getIdVtaquigrafica());
			}
			vtaquifraficaList = _vtRepository.findAllByIdIn(vtaqIds, paging);
			count = _vtRepository.findAllByIdInOrderByFechaSesionDesc(vtaqIds).size();
			res.put("vtaquifraficaList", vtaquifraficaList);
			res.put("count", count);

		} else if (filtrosT1Str.equals("") && !ffiltrosT2Str.equals("")) {
			List<String> asuntoIds = new ArrayList<String>();
			List<String> vtaqIds = new ArrayList<String>();
			queryNoPage = new Query();
			//subs = ffiltrosT2Str.split(",");
			if(ffiltrosT2Str.endsWith(",")){
				ffiltrosT2Str = ffiltrosT2Str.substring(0,ffiltrosT2Str.length() - 1);
			}
			subs = FiltrosUtils.getSubfiltros(ffiltrosT2Str);
			for (String sub : subs) {
				String[] sTempFiltros = sub.split(":");
				String filtro = sTempFiltros[0].trim();
				String filtroValores = sTempFiltros[1].trim();
				queryNoPage.addCriteria(Criteria.where(filtro).is(filtroValores));
			}
			List<AsuntoAbordado> asuntoAbordadoList = mongoOperations.find(queryNoPage, AsuntoAbordado.class);

			for (AsuntoAbordado asunto : asuntoAbordadoList) {
				asuntoIds.add(asunto.getId());
			}

			List<VTaquigraficaAsuntos> listVtaqAsuntos = _vtAsuntosRepository.findAllByIdAsuntoIn(asuntoIds);
			for (VTaquigraficaAsuntos vtaqAsuntos : listVtaqAsuntos) {
				vtaqIds.add(vtaqAsuntos.getIdVtaquigrafica());
			}
			vtaquifraficaList = _vtRepository.findAllByIdIn(vtaqIds, paging);
			count = _vtRepository.findAllByIdInOrderByFechaSesionDesc(vtaqIds).size();
			res.put("vtaquifraficaList", vtaquifraficaList);
			res.put("count", count);
		}
		return res;
	}

	public Page<TablaVersionesTaquigraficasDTO> getVersionesPaginadas(Pageable paging, String filtros)
			throws ParseException {

		List<VersionTaquigrafica> vtaquifraficaList = null;
		List<TablaVersionesTaquigraficasDTO> vtTabla = new ArrayList<TablaVersionesTaquigraficasDTO>();
		Page<VersionTaquigrafica> vtPaginableResult = null;
		Page<TablaVersionesTaquigraficasDTO> vtPaginableResponse = null;
		Pageable paging2 = PageRequest.of(paging.getPageNumber(), paging.getPageSize());
		List<AsuntoDTO> asuntosDTOList = null;

		long count = 0;

		if (filtros != null) {
			Map<String, Object> res = filtrosEnTablaTransitiva(filtros, paging, count);
			vtaquifraficaList = (List<VersionTaquigrafica>) res.get("vtaquifraficaList");
			count = (long) res.get("count");
		} else {
			Query query = new Query().with(paging);
			query.with(Sort.by(Sort.Direction.DESC, "fechaSesion"));
			List<VersionTaquigrafica> listAcuerdos = mongoOperations.find(query, VersionTaquigrafica.class);
			vtPaginableResult = new PageImpl<VersionTaquigrafica>(listAcuerdos, paging, count);
			vtaquifraficaList = vtPaginableResult.getContent();
			count = _vtRepository.findAll().size();
		}
		// End
		TablaVersionesTaquigraficasDTO obTabla = null;
		for (int i = 0; i < vtaquifraficaList.size(); i++) {
			obTabla = new TablaVersionesTaquigraficasDTO();
			obTabla.setId(vtaquifraficaList.get(i).getId());
			obTabla.setDuracionSesion(vtaquifraficaList.get(i).getDuracionSesion());
			obTabla.setFechaSesion(ConvertFechaFiltros.changeFormat(vtaquifraficaList.get(i).getFechaSesion(), RegexUtils.REGEX_DDMMYYYY));

			asuntosDTOList = new ArrayList<AsuntoDTO>();
			List<VTaquigraficaAsuntos> vtList = _vtAsuntosRepository.findByIdVtaquigrafica(obTabla.getId());
			// recorre la lista de asuntos de la TT
			for (VTaquigraficaAsuntos vasuntoTT : vtList) {
				AsuntoDTO obAsunto = new AsuntoDTO();
				// busca el asunto para obtener datos
				Optional<AsuntoAbordado> asuntoById = _asuntoRepository.findById(vasuntoTT.getIdAsunto());
				obAsunto.setIdAsunto(asuntoById.get().getId());
				obAsunto.setNumeroExpediente(asuntoById.get().getNumeroExpediente());
				obAsunto.setTipoAsunto(asuntoById.get().getAsuntoAbordado());
				String acum = StringUtils.join(vasuntoTT.getAcumulados(), ",");
				if (acum.length() > 2) {
					obAsunto.setNumeroExpediente(
							obAsunto.getNumeroExpediente() + ", " + StringUtils.join(vasuntoTT.getAcumulados(), ","));
				}

				asuntosDTOList.add(obAsunto);
			}

			obTabla.setAsuntos(asuntosDTOList);
			obTabla.setNumeroAsuntos(asuntosDTOList.size());
			obTabla.setPorcentaje(vtaquifraficaList.get(i).getPorcentaje());
			vtTabla.add(obTabla);

		}
		vtPaginableResponse = new PageImpl<TablaVersionesTaquigraficasDTO>(vtTabla, paging2, count);
		return vtPaginableResponse;
	}

	public VersionTaquigraficaDTO getVersionTaquigrafica(String id) {
		Optional<VersionTaquigrafica> vtResp = _vtRepository.findById(id);
		List<AsuntoDTO> asuntosDTOList = new ArrayList<AsuntoDTO>();
		VersionTaquigraficaDTO version = new VersionTaquigraficaDTO();
		if (vtResp != null) {
			vtResp.get().setFechaSesion(ConvertFechaFiltros.changeFormat(vtResp.get().getFechaSesion(), RegexUtils.REGEX_DDMMYYYY));
			List<VTaquigraficaAsuntos> listTT = _vtAsuntosRepository.findByIdVtaquigrafica(id);
			for (VTaquigraficaAsuntos ojectTT : listTT) {
				AsuntoDTO obAsunto = new AsuntoDTO();
				Optional<AsuntoAbordado> asuntoById = _asuntoRepository.findById(ojectTT.getIdAsunto());
				if (asuntoById.isPresent()) {
					obAsunto.setIdAsunto(asuntoById.get().getId());
					obAsunto.setNumeroExpediente(asuntoById.get().getNumeroExpediente());
					obAsunto.setTipoAsunto(asuntoById.get().getAsuntoAbordado());
					String acum = StringUtils.join(ojectTT.getAcumulados(), ",");
					if (acum.length() > 2) {
						obAsunto.setNumeroExpediente(
								obAsunto.getNumeroExpediente() + ", " + StringUtils.join(ojectTT.getAcumulados(), ","));
					}
					asuntosDTOList.add(obAsunto);
				}
			}
			version.setVtaq(vtResp.get());
			version.setAsuntos(asuntosDTOList);
			return version;
		}
		return null;
	}

	public VtaqResponseDTO saveVersionTaquigrafica(VersionTaquigrafica versionT) {
		versionT.setFechaSesion(ConvertFechaFiltros.changeFormat(versionT.getFechaSesion(), RegexUtils.REGEX_YYYYMMDD));
		VersionTaquigrafica vtResp = findByFechaAndDuracion(versionT.getFechaSesion(), versionT.getDuracionSesion());
		// Si no existe la version taquigrafica, hacemos el procedimiento de guardado
		if (vtResp == null) {

			vtResp = _vtRepository.save(versionT);
			updatePorcentajeVersionTaquigrafica(versionT);

			_bitacoraService.saveOperacion(StatusBitacora._VERSIONES, StatusBitacora._SAVE, vtResp, null);
			VtaqResponseDTO version = new VtaqResponseDTO(HttpStatus.OK.value(), "Version Taquigráfica Creada");
			version.setVersionTaquigrafica(vtResp);
			return version; // se retorna la vt creada con el mensaje y codigo de estado ok
		}
		// si ya existe la vTaq se devuelve el objeto, pero se debe mandar emnsaje que
		// no se pudo crear porque ya existe!
		VtaqResponseDTO version = new VtaqResponseDTO(HttpStatus.BAD_REQUEST.value(),
				"¡No fue posible crear la Versión Taquigráfica por que ya existe!");
		version.setVersionTaquigrafica(vtResp);
		return version;
	}

	// Caso 1: actualizar_ids si existe otra vtaq igual y soy yo => actualizar
	// Caso 2: actualizar_ids no existe otra vtaq igual => actualizar
	// Cas 3: actualizar_ids si existe pero no soy yo => no se puede actualizar
	public VtaqResponseDTO updateVersionTaquigrafica(VersionTaquigrafica versionT) {
		versionT.setFechaSesion(ConvertFechaFiltros.changeFormat(versionT.getFechaSesion(), RegexUtils.REGEX_YYYYMMDD));
		// Verificamos unicidad de los datos
		VersionTaquigrafica vtaq = findByFechaAndDuracion(versionT.getFechaSesion(), versionT.getDuracionSesion());
		boolean ExisteVtaq = vtaq != null ? true : false;

		if (ExisteVtaq && (vtaq.getId().equals(versionT.getId()))) {

			versionT = _vtRepository.save(versionT);
			updatePorcentajeVersionTaquigrafica(versionT);

			_bitacoraService.saveOperacion(StatusBitacora._VERSIONES, StatusBitacora._UPDATE, versionT, vtaq);
			VtaqResponseDTO version = new VtaqResponseDTO(HttpStatus.OK.value(), "Version Taquigráfica actualizada");
			version.setVersionTaquigrafica(versionT);
			return version;
		} else if (!ExisteVtaq) {

			versionT = _vtRepository.save(versionT);
			updatePorcentajeVersionTaquigrafica(versionT);

			_bitacoraService.saveOperacion(StatusBitacora._VERSIONES, StatusBitacora._UPDATE, versionT, vtaq);
			VtaqResponseDTO version = new VtaqResponseDTO(HttpStatus.OK.value(), "Version Taquigráfica actualizada");
			version.setVersionTaquigrafica(versionT);
			return version;
		}
		// no se pudo actualizar
		VtaqResponseDTO version = new VtaqResponseDTO(HttpStatus.BAD_REQUEST.value(),
				"¡No fue posible actualizar la Version Taquigráfica!");
		version.setVersionTaquigrafica(versionT);
		return version;
	}

	public boolean deleteVersionTaquigrafica(String id) {
		// Busca la VT
		Optional<VersionTaquigrafica> vtResp = _vtRepository.findById(id);
		if (vtResp.isPresent()) {
			// Busca todas la TT que contengan esa VT
			List<VTaquigraficaAsuntos> listTT = _vtAsuntosRepository.findByIdVtaquigrafica(id);
			for (VTaquigraficaAsuntos vtAsuntoTT : listTT) {
				// Busca todas las TT que contengan el asunto de la anterior TT
				List<VTaquigraficaAsuntos> listTTAsuntos = _vtAsuntosRepository
						.findByIdAsunto(vtAsuntoTT.getIdAsunto());
				// si mas de una TT tiene ese asunto solo de elimina la asociacion
				// correspondiente a la VT
				if (listTTAsuntos.size() > 1) {
					_vtAsuntosRepository.delete(vtAsuntoTT);

				} else {
					// sino se elimina el asunto ya que solo una VT lo ocupa y se elimina la TT de
					// la VT con el asunto
					Optional<AsuntoAbordado> asunto = _asuntoRepository.findById(listTTAsuntos.get(0).getIdAsunto());
					AsuntoResponseDTO version = new AsuntoResponseDTO(200, "Eliminado");
					version.setIdVersionTaqui(id);
					version.setAsunto(asunto.get());
					version.setVt_asuntos(vtAsuntoTT);
					_bitacoraService.saveOperacion(StatusBitacora._ASUNTOS, StatusBitacora._DELETE, version, null);
					_asuntoRepository.deleteById(listTTAsuntos.get(0).getIdAsunto());
					_vtAsuntosRepository.delete(vtAsuntoTT);

					SentenciaIndexObjectDTO sentencia = new SentenciaIndexObjectDTO();
					sentencia.setTipoAsunto(asunto.get().getAsuntoAbordado());
					sentencia.setNumeroExpediente(asunto.get().getNumeroExpediente());
					deleteSentenciaNotInUse(sentencia);

				}
			}
			// por ultimo se elimina la VT
			_bitacoraService.saveOperacion(StatusBitacora._VERSIONES, StatusBitacora._DELETE, vtResp.get(), null);
			_vtRepository.deleteById(id);
			return true;
		}
		return false;
	}

	public boolean deleteSentenciaNotInUse(SentenciaIndexObjectDTO objetoSentencia) {
		Optional<Sentencia> existSentencia = Optional.ofNullable(_sentenciaRepository
				.findByTipoAsuntoAndNumeroExpedienteAndOrganoRadicacion(objetoSentencia.getTipoAsunto(),
						objetoSentencia.getNumeroExpediente(), objetoSentencia.getOrganoRadicacion()));
		if (existSentencia.isPresent()) {
			if (existSentencia.get().getPorcentajeCaptura() > 10.0) {
				_sentenciaRepository.delete(existSentencia.get());
				return true;
			}
		}
		return false;
	}

}
