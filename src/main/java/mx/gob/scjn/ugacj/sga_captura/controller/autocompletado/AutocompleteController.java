package mx.gob.scjn.ugacj.sga_captura.controller.autocompletado;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mx.gob.scjn.ugacj.sga_captura.service.autocompletado.AutocompleteService;


@RestController
@RequestMapping(path = "/api/sga/autocompletado")
public class AutocompleteController {
	
	@Autowired
	AutocompleteService autocService;

	@CrossOrigin()
	@RequestMapping(method = RequestMethod.GET, path = "/{nombrePantalla}", produces = "application/json")
	public List<String> getOptionsCatalogo(@PathVariable String nombrePantalla, @RequestParam(name = "q") String q,
			@RequestParam(name = "field") String field) {
		
		return autocService.findCoincidence(nombrePantalla, q, field);
	}
}
