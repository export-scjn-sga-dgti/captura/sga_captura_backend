package mx.gob.scjn.ugacj.sga_captura.controller;

import mx.gob.scjn.ugacj.sga_captura.domain.ProcesamientoTexto;
import mx.gob.scjn.ugacj.sga_captura.dto.AnalizaTextoDTO;
import mx.gob.scjn.ugacj.sga_captura.service.ProcesamientoTextoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping( path = "api/sga/procesamiento-texto")
public class ProcesamientoTextoController {

    @Autowired
    private ProcesamientoTextoService _procesamientoTextoService;

    @CrossOrigin()
    @RequestMapping( method = RequestMethod.POST)
    public ProcesamientoTexto getTextoProcesado(@RequestBody AnalizaTextoDTO analisis){
        return _procesamientoTextoService.getTextoAnalizado(analisis);
    }
}
