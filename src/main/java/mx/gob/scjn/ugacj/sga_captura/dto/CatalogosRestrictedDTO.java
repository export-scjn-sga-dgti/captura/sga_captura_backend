package mx.gob.scjn.ugacj.sga_captura.dto;

import java.util.ArrayList;
import java.util.List;

public class CatalogosRestrictedDTO {
	String campo;
	List<String> valor = new ArrayList<String>();
	
	public String getCampo() {
		return campo;
	}
	public void setCampo(String campo) {
		this.campo = campo;
	}
	public List<String> getValor() {
		return valor;
	}
	public void setValor(List<String> valor) {
		this.valor = valor;
	}
	
	
}
