package mx.gob.scjn.ugacj.sga_captura.service.migracion;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import mx.gob.scjn.ugacj.sga_captura.domain.AsuntoAbordado;
import mx.gob.scjn.ugacj.sga_captura.domain.CausaImprocedencia;
import mx.gob.scjn.ugacj.sga_captura.domain.CausasImproSobreseAnali;
import mx.gob.scjn.ugacj.sga_captura.domain.DereHumaViolaAnaliSenten;
import mx.gob.scjn.ugacj.sga_captura.domain.DiversaNormativa;
import mx.gob.scjn.ugacj.sga_captura.domain.DiversaNormativaOld;
import mx.gob.scjn.ugacj.sga_captura.domain.RubrosTablaVotaciones;
import mx.gob.scjn.ugacj.sga_captura.domain.Sentencia;
import mx.gob.scjn.ugacj.sga_captura.domain.SentenciaOld;
import mx.gob.scjn.ugacj.sga_captura.domain.TablaVotaciones;
import mx.gob.scjn.ugacj.sga_captura.domain.Taquigraficas;
import mx.gob.scjn.ugacj.sga_captura.domain.TipoVicInvEsfeAnaSenten;
import mx.gob.scjn.ugacj.sga_captura.domain.TipoVicProLegisAnali;
import mx.gob.scjn.ugacj.sga_captura.domain.TipoVioInvaPodAnaSente;
import mx.gob.scjn.ugacj.sga_captura.domain.VTaquigraficaAsuntos;
import mx.gob.scjn.ugacj.sga_captura.domain.VersionTaquigrafica;
import mx.gob.scjn.ugacj.sga_captura.domain.VotacionTipoSentencia;
import mx.gob.scjn.ugacj.sga_captura.domain.bitacora.JvSnapshots;
import mx.gob.scjn.ugacj.sga_captura.domain.bitacora.JvSnapshotsOld;
import mx.gob.scjn.ugacj.sga_captura.domain.issues.Issues;
import mx.gob.scjn.ugacj.sga_captura.dto.ParticipacionDTO;
import mx.gob.scjn.ugacj.sga_captura.dto.PorcentajeDTO;
import mx.gob.scjn.ugacj.sga_captura.dto.migracion.AsuntosAbordados;
import mx.gob.scjn.ugacj.sga_captura.dto.migracion.NumeroMinistrosParticiparon;
import mx.gob.scjn.ugacj.sga_captura.dto.migracion.Temas;
import mx.gob.scjn.ugacj.sga_captura.dto.statecode.AsuntoResponseDTO;
import mx.gob.scjn.ugacj.sga_captura.dto.statecode.ResponseDTO;
import mx.gob.scjn.ugacj.sga_captura.dto.vtaquigraficas.AsuntoAbordadoDTO;
import mx.gob.scjn.ugacj.sga_captura.repository.AsuntoAbordadoRepository;
import mx.gob.scjn.ugacj.sga_captura.repository.DiversaNormativaOldRepository;
import mx.gob.scjn.ugacj.sga_captura.repository.DiversaNormativaRepository;
import mx.gob.scjn.ugacj.sga_captura.repository.SentenciaOldRepository;
import mx.gob.scjn.ugacj.sga_captura.repository.SentenciaRepository;
import mx.gob.scjn.ugacj.sga_captura.repository.TaquigraficaRepository;
import mx.gob.scjn.ugacj.sga_captura.repository.VTaquigraficaAsuntosRepository;
import mx.gob.scjn.ugacj.sga_captura.repository.VersionTaquigraficaRepository;
import mx.gob.scjn.ugacj.sga_captura.repository.VotacionRepository;
import mx.gob.scjn.ugacj.sga_captura.repository.VotacionesRubrosRepository;
import mx.gob.scjn.ugacj.sga_captura.repository.issues.IssuesRepository;
import mx.gob.scjn.ugacj.sga_captura.service.porcentaje.PorcentajeService;
import mx.gob.scjn.ugacj.sga_captura.service.vtaquigrafica.AsuntoAbordadoService;
import mx.gob.scjn.ugacj.sga_captura.utils.CalculoPorcentajeUtils;
import mx.gob.scjn.ugacj.sga_captura.utils.ConvertFechaFiltros;
import mx.gob.scjn.ugacj.sga_captura.utils.RegexUtils;


@Service
public class MigracionService {
	
	@Autowired
	private TaquigraficaRepository _vtaqOldRep;
	
	@Autowired
	private VersionTaquigraficaRepository vTaqRepository;
	
	@Autowired
	private AsuntoAbordadoService _asuntoService;
	
	@Autowired
	private PorcentajeService porcentajeService;
	
	@Autowired
	private SentenciaOldRepository sentenciaOldRepository;
	
	@Autowired
	private SentenciaRepository sentenciaRepository;

    @Autowired
	private IssuesRepository issuesRepository;
	
	@Autowired
	private DiversaNormativaRepository normativaRepository;
	
	@Autowired
	private DiversaNormativaOldRepository normativaOldRepository;
	

	@Autowired
    private VotacionRepository _votacionRepository;
	
	@Autowired
    private VotacionesRubrosRepository votacionesRubrosRepository;
	
	@Autowired
	private MongoOperations mongoOperations;

	@Autowired
	private VTaquigraficaAsuntosRepository _vtAsuntosRepository;
	
	@Autowired
	private AsuntoAbordadoRepository _asuntoRepository;
	

	public String validateData(AsuntosAbordados asuntoAbordadoOld) {
		if(!asuntoAbordadoOld.getAcumulados()&&asuntoAbordadoOld.getNumExpedienteAcumulados().length>=1) {
			return Arrays.toString(asuntoAbordadoOld.getNumExpedienteAcumulados());
		}
		return null;
	}

	public String validateDataVtaq(Taquigraficas vtaqOld) {
		if(vtaqOld.getFechaSesion()==null) {
			return vtaqOld.getId();
		}
		return null;
	}
	
	public List<String> extractTemas(Temas[] temas){
		List<String> temasList = new ArrayList<String>();
		for(Temas tema: temas) {
			temasList.addAll(Arrays.asList(tema.getTemas()));
		}
		return temasList;
	}
	
	public List<ParticipacionDTO> extractNumeroMinistrosParticiparon(NumeroMinistrosParticiparon[] numeroMinistrosParticiparon) {
		List<ParticipacionDTO> listParticipacion = new ArrayList<ParticipacionDTO>();
		ParticipacionDTO participacion = null;
		for(NumeroMinistrosParticiparon numMinistrosPart:numeroMinistrosParticiparon) {
			participacion = new ParticipacionDTO();
			participacion.setCantidad(Integer.valueOf(numMinistrosPart.getCantidad()));
			participacion.setNombre(numMinistrosPart.getNombre());
			listParticipacion.add(participacion);
		}
		return listParticipacion;
	}
	
	public void findTemasProcesalesRepetidos() {
		List<Sentencia> sentenciasNewList = sentenciaRepository.findAll();
		for(Sentencia sentencia:sentenciasNewList) {
			Query query = new Query();
			Pageable paging = PageRequest.of(0, 1000);
			Map<String, Object> res = filtrosEnTablaTransitiva("asuntoAbordado:"+sentencia.getTipoAsunto()+",numeroExpediente:"+sentencia.getNumeroExpediente()+",", paging, 0);
			List<VersionTaquigrafica> listVatq = (List<VersionTaquigrafica>) res.get("vtaquifraficaList");
			AsuntoAbordado asuntoById = _asuntoService.findByAsuntoAndExpediente(sentencia.getTipoAsunto(), sentencia.getNumeroExpediente());
			
			Gson gson = new Gson();
			
			if(listVatq.size()>1) {
				List<String> comparativeTP = new ArrayList<String>();
				List<String> comparativeTF = new ArrayList<String>();
				
				for(VersionTaquigrafica vtaq:listVatq) {
//					System.out.println(listVatq.size());
					
					Optional<VTaquigraficaAsuntos> tt = _vtAsuntosRepository.findByIdVtaquigraficaAndIdAsunto(vtaq.getId(), asuntoById.getId());
					if(tt.isPresent()) {
						if(tt.get().getTemasProcesales().size()>0) comparativeTP.add(gson.toJson(tt.get().getTemasProcesales()));
						if(tt.get().getTemasFondo().size()>0) comparativeTF.add(gson.toJson(tt.get().getTemasFondo()));
					}
				}
				
				if(comparativeTP.size()==2) {
					if(comparativeTP.get(0).equals(comparativeTP.get(1))) {
						System.out.println("Son idénticos comparative TP: "+sentencia.getTipoAsunto()+" - "+sentencia.getNumeroExpediente() + " " + asuntoById.getId());
						setIssueTemasRepetidos(asuntoById.getId(), "procesales", "asuntoAbordado:"+sentencia.getTipoAsunto()+",numeroExpediente:"+sentencia.getNumeroExpediente());
					}
				}
				
				if(comparativeTF.size()==2) {
					if(comparativeTF.get(0).equals(comparativeTF.get(1))) {
						System.out.println("Son idénticos comparativeTF "+sentencia.getTipoAsunto()+" - "+sentencia.getNumeroExpediente() + " " + asuntoById.getId());
						setIssueTemasRepetidos(asuntoById.getId(), "de fondo", "asuntoAbordado:"+sentencia.getTipoAsunto()+",numeroExpediente:"+sentencia.getNumeroExpediente());
					}
				}
				
				if(comparativeTP.size()==3) {
					if(comparativeTP.get(0).equals(comparativeTP.get(1))||comparativeTP.get(0).equals(comparativeTP.get(2))
							||comparativeTP.get(1).equals(comparativeTP.get(2))) {
						System.out.println("Son idénticos comparative TP "+sentencia.getTipoAsunto()+" - "+sentencia.getNumeroExpediente() + " " + asuntoById.getId());
						setIssueTemasRepetidos(asuntoById.getId(), "procesales", "asuntoAbordado:"+sentencia.getTipoAsunto()+",numeroExpediente:"+sentencia.getNumeroExpediente());
					}
				}
				
				if(comparativeTF.size()==3) {
					if(comparativeTF.get(0).equals(comparativeTF.get(1))||comparativeTF.get(0).equals(comparativeTF.get(2))
							||comparativeTF.get(1).equals(comparativeTF.get(2))) {
						System.out.println("Son idénticos comparative TF "+sentencia.getTipoAsunto()+" - "+sentencia.getNumeroExpediente() + " " + asuntoById.getId());
						setIssueTemasRepetidos(asuntoById.getId(), "de fondo", "asuntoAbordado:"+sentencia.getTipoAsunto()+",numeroExpediente:"+sentencia.getNumeroExpediente());
					}
				}
				
				if(comparativeTP.size()==4) {
					if(comparativeTP.get(0).equals(comparativeTP.get(1))||comparativeTP.get(0).equals(comparativeTP.get(2))
							||comparativeTP.get(0).equals(comparativeTP.get(3))||	
							comparativeTP.get(1).equals(comparativeTP.get(2))||comparativeTP.get(1).equals(comparativeTP.get(3)) ||
							comparativeTP.get(2).equals(comparativeTP.get(3))
							
							) {
						System.out.println("Son idénticos comparative TP "+sentencia.getTipoAsunto()+" - "+sentencia.getNumeroExpediente() + " " + asuntoById.getId());
						setIssueTemasRepetidos(asuntoById.getId(), "procesales", "asuntoAbordado:"+sentencia.getTipoAsunto()+",numeroExpediente:"+sentencia.getNumeroExpediente());
					}
				}
				
				if(comparativeTF.size()==4) {
					if(comparativeTF.get(0).equals(comparativeTF.get(1))||comparativeTF.get(0).equals(comparativeTF.get(2))
							||comparativeTF.get(0).equals(comparativeTF.get(3))||	
							comparativeTF.get(1).equals(comparativeTF.get(2))||comparativeTF.get(1).equals(comparativeTF.get(3)) ||
							comparativeTF.get(2).equals(comparativeTF.get(3))
							
							) {
						System.out.println("Son idénticos comparative TP "+sentencia.getTipoAsunto()+" - "+sentencia.getNumeroExpediente() + " " + asuntoById.getId());
						setIssueTemasRepetidos(asuntoById.getId(), "procesales", "asuntoAbordado:"+sentencia.getTipoAsunto()+",numeroExpediente:"+sentencia.getNumeroExpediente());
					}
				}
				
			}
			
		}
		System.out.println("Ce fini");
		
	}
	
	public void setIssueTemasRepetidos(String id, String tipoTema, String detail){
		Issues issue = new Issues();
		issue.setResumen("No deben de tener temas "+tipoTema+" idénticos los asunto que son vistos en diferentes sesiones "+detail);
		issue.setSeccion("VTaq");
		issue.setType("ERROR_CAPTURA");
		
		Map<String, String> mapaIssue = new HashMap<>();
		mapaIssue.put("asuntoId", id);
		
		issue.setDetail(mapaIssue);
		issuesRepository.save(issue);
	}
	
	public Map<String, Object> filtrosEnTablaTransitiva(String filtros, Pageable paging, long count) {
		Map<String, Object> res = new HashMap<>();
		List<VersionTaquigrafica> vtaquifraficaList = null;
		List<String> filtrosTT = new ArrayList<>(Arrays.asList(""));
		List<String> filtrosT1 = new ArrayList<>(Arrays.asList("fechaSesion"));
		List<String> filtrosT2 = new ArrayList<>(Arrays.asList("asuntoAbordado", "numeroExpediente"));
		String filtrosTTStr = "";
		String filtrosT1Str = "";
		String ffiltrosT2Str = "";

		String subs[] = filtros.split(",");
		for (String sub : subs) {
			String sTempFiltros[] = sub.split(":");
			String filtro = sTempFiltros[0].trim();
			filtrosTTStr = filtrosTT.contains(filtro) ? filtrosTTStr + sub + "," : filtrosTTStr;
			filtrosT1Str = filtrosT1.contains(filtro) ? filtrosT1Str + sub + "," : filtrosT1Str;
			ffiltrosT2Str = filtrosT2.contains(filtro) ? ffiltrosT2Str + sub + "," : ffiltrosT2Str;
		}
		Query query = new Query().with(paging);
		Query queryNoPage = new Query();

		if (!filtrosT1Str.equals("") && ffiltrosT2Str.equals("")) {
			String sTempFiltros[] = filtrosT1Str.split(":");
			
			String fecha = ConvertFechaFiltros.changeFormat(sTempFiltros[1].trim(), RegexUtils.REGEX_YYYYMMDD);
			query.addCriteria(Criteria.where(sTempFiltros[0]).gte(fecha).lte(fecha));
			queryNoPage.addCriteria(Criteria.where(sTempFiltros[0]).gte(fecha).lte(fecha));

			// Esta sección quizas en un futuro se pueda usar
			// Query query = new Query(Criteria
			// .where("record.Date_Of_Birth").gte(DateOperators.dateFromString(startDate).withFormat("mm/dd/yyyy"))
			// .andOperator(Criteria.where("record.Date_Of_Birth").lte(DateOperators.dateFromString(endDate).withFormat("mm/dd/yyyy"))
			// )).with(paging);

			vtaquifraficaList = mongoOperations.find(query, VersionTaquigrafica.class);
			count = mongoOperations.count(queryNoPage, VersionTaquigrafica.class);
			Page<VersionTaquigrafica> vtPaginableResult = new PageImpl<VersionTaquigrafica>(vtaquifraficaList, paging,
					count);
			vtaquifraficaList = vtPaginableResult.getContent();

			res.put("vtaquifraficaList", vtaquifraficaList);
			res.put("count", count);
		} else if (!filtrosT1Str.equals("") && !ffiltrosT2Str.equals("")) {
			String sTempFiltros[] = filtrosT1Str.split(":");
			String fecha = ConvertFechaFiltros.changeFormat(sTempFiltros[1].trim(),RegexUtils.REGEX_YYYYMMDD);
			query.addCriteria(Criteria.where(sTempFiltros[0]).gte(fecha).lte(fecha));
			queryNoPage.addCriteria(Criteria.where(sTempFiltros[0]).gte(fecha).lte(fecha));

//				queryNoPage.addCriteria(Criteria.where(sTempFiltros[0]).gte(fecha).lte(fecha));

			vtaquifraficaList = mongoOperations.find(queryNoPage, VersionTaquigrafica.class);
			List<String> vtaqIds = new ArrayList<String>();
			List<String> asuntoIds = new ArrayList<String>();

			for (VersionTaquigrafica vtaq : vtaquifraficaList) {
				vtaqIds.add(vtaq.getId());
			}

			queryNoPage = new Query();
			subs = ffiltrosT2Str.split(",");
			for (String sub : subs) {
				sTempFiltros = sub.split(":");
				String filtro = sTempFiltros[0].trim();
				String filtroValores = sTempFiltros[1].trim();
				queryNoPage.addCriteria(Criteria.where(filtro).is(filtroValores));
			}
			List<AsuntoAbordado> asuntoAbordadoList = mongoOperations.find(queryNoPage, AsuntoAbordado.class);

			for (AsuntoAbordado asunto : asuntoAbordadoList) {
				asuntoIds.add(asunto.getId());
			}

			List<VTaquigraficaAsuntos> listVtaqAsuntos = _vtAsuntosRepository
					.findAllByIdVtaquigraficaInAndIdAsuntoIn(vtaqIds, asuntoIds);
			vtaqIds = new ArrayList<String>();
			for (VTaquigraficaAsuntos vtaqAsuntos : listVtaqAsuntos) {
				vtaqIds.add(vtaqAsuntos.getIdVtaquigrafica());
			}
			vtaquifraficaList = vTaqRepository.findAllByIdIn(vtaqIds, paging);
			count = vTaqRepository.findAllByIdInOrderByFechaSesionDesc(vtaqIds).size();
			res.put("vtaquifraficaList", vtaquifraficaList);
			res.put("count", count);

		} else if (filtrosT1Str.equals("") && !ffiltrosT2Str.equals("")) {
			List<String> asuntoIds = new ArrayList<String>();
			List<String> vtaqIds = new ArrayList<String>();
			queryNoPage = new Query();
			subs = ffiltrosT2Str.split(",");
			for (String sub : subs) {
				String[] sTempFiltros = sub.split(":");
				String filtro = sTempFiltros[0].trim();
				String filtroValores = sTempFiltros[1].trim();
				queryNoPage.addCriteria(Criteria.where(filtro).is(filtroValores));
			}
			List<AsuntoAbordado> asuntoAbordadoList = mongoOperations.find(queryNoPage, AsuntoAbordado.class);

			for (AsuntoAbordado asunto : asuntoAbordadoList) {
				asuntoIds.add(asunto.getId());
			}

			List<VTaquigraficaAsuntos> listVtaqAsuntos = _vtAsuntosRepository.findAllByIdAsuntoIn(asuntoIds);
			for (VTaquigraficaAsuntos vtaqAsuntos : listVtaqAsuntos) {
				vtaqIds.add(vtaqAsuntos.getIdVtaquigrafica());
			}
			vtaquifraficaList = vTaqRepository.findAllByIdIn(vtaqIds, paging);
			count = vTaqRepository.findAllByIdInOrderByFechaSesionDesc(vtaqIds).size();
			res.put("vtaquifraficaList", vtaquifraficaList);
			res.put("count", count);
		}
		return res;
	}
	
	public String getDateAsunto(String numExpediente, String asuntoAbordado) {
		Query query = new Query();
		if(numExpediente.equals("CONTRADICCIÓN DE CRITERIOS (ANTES CONTRADICCIÓN DE TESIS)")) {
			numExpediente = "CONTRADICCIÓN DE TESIS";
		}
		query.addCriteria(Criteria.where("state.asuntos.numExpediente").is(numExpediente));
		query.addCriteria(Criteria.where("state.asuntos.asuntoAbordado").is(asuntoAbordado));
		query.addCriteria(Criteria.where("type").is("Update"));
		query.addCriteria(Criteria.where("seccion").is("Versiones Taquigráficas"));
		List<JvSnapshotsOld> bitacoraList = mongoOperations.find(query, JvSnapshotsOld.class);
		if(bitacoraList.size()>=1) {
			for(Object snapshot: bitacoraList) {
				String jsonInString = new Gson().toJson(snapshot);
				JSONObject mJSONObject = null;
				try {
					mJSONObject = new JSONObject(jsonInString);
					String date = (String) mJSONObject.getJSONObject("commitMetaData").get("commitDate");
					String autor = (String) mJSONObject.getJSONObject("commitMetaData").get("author");
					if(!autor.equals("ggarciab")) {
						String[] parts = date.split(",");
						date = parts[1];
						parts = date.trim().split(" ");
						date = parts[0];
						return date;
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return "";
	}
	
	

	public void populateDatabaseTaq() throws ParseException {
		
//		List<String> erroresFechaSesion = new ArrayList<String>();
		List<String> erroresIncoincidenciaExpAcum = new ArrayList<String>();
//		List<String> erroresSinDuracion = new ArrayList<String>();
		
		List<Taquigraficas> versionesTaquigraficasOld = _vtaqOldRep.findAll();
		VersionTaquigrafica vtaqNew = null;
		
		System.out.println("Total Vtaq: "+versionesTaquigraficasOld.size());
		
		List<Double> porcentajes = null;
		int totalAsuntosConTemas=0;
		for(Taquigraficas vtaqOld: versionesTaquigraficasOld) {
			if(validateDataVtaq(vtaqOld)!=null) {
//				erroresFechaSesion.add(validateDataVtaq(vtaqOld));
				
				Issues issue = new Issues();
				issue.setResumen("No se capturó la Fecha de sesión");
				issue.setSeccion("VTaq");
				issue.setType("ERROR_CAPTURA");
				
				Map<String, String> mapaIssue = new HashMap<>();
				mapaIssue.put("idvtaqOld", vtaqOld.getId());
				
				issue.setDetail(mapaIssue);
				issuesRepository.save(issue);
				
			}else {
				if(vtaqOld.getDuracionSesion().equals("")) {
					Issues issue = new Issues();
					issue.setResumen("No se capturó la Duración de sesión");
					issue.setSeccion("VTaq");
					issue.setType("ERROR_CAPTURA");
					
					Map<String, String> mapaIssue = new HashMap<>();
					mapaIssue.put("idvtaqOld", vtaqOld.getId());
					
					issue.setDetail(mapaIssue);
					issuesRepository.save(issue);
					
//					erroresSinDuracion.add("id: "+ vtaqOld.getId()+" fecha:"+ConvertFechaFiltros.convertEngFechaTextToDateStringv2(vtaqOld.getFechaSesion()));
				}
//				if(vtaqOld.getId().equals("60edf5450e012e7401f216d3")) {
//					System.out.println("Here");
//				}
//				if(ConvertFechaFiltros.convertEngFechaTextToDateString(vtaqOld.getFechaSesion()).equals("03/05/2022")) {
//					System.out.println("here");
//				}
					
				vtaqNew = new VersionTaquigrafica();
				vtaqNew.setFechaSesion(ConvertFechaFiltros.convertEngFechaTextToDateString(vtaqOld.getFechaSesion()));
				vtaqNew.setDuracionSesion(vtaqOld.getDuracionSesion().equals("")?"00:00":vtaqOld.getDuracionSesion());
				vtaqNew.setNumeroMinistrasMinistrosParticiparonEnSesion(vtaqOld.getNumeroMinistrosParticiparon());
				vtaqNew.setNombreMinistrasMinistrosPresentesEnSesion(Arrays.asList(vtaqOld.getMinistrosPrecentesEnSesion()));
				vtaqNew.setNombreMinistrasMinistrosAusentesEnSesion(Arrays.asList(vtaqOld.getMinitrosAusentesEnSesion()));
				vtaqNew.setHasPresidenteDecano(vtaqOld.getPresidenteDecano().equals("")?false:true);
				vtaqNew.setNombrePresidenteDecano(vtaqOld.getPresidenteDecano().equals("")?"":vtaqOld.getPresidenteDecano());
				
				vtaqNew.setPorcentaje(0d);

				vTaqRepository.save(vtaqNew);
				porcentajes = new ArrayList<Double>();
				
				System.out.println("******************Se crea la Vtaq Id: "+vtaqOld.getId()+"  fecha: "+vtaqNew.getFechaSesion()+"*****************");
//				
				AsuntoAbordado asuntoAbordadoNew = null; 
				VTaquigraficaAsuntos vtaAsunto_TT = null;
				AsuntoAbordadoDTO asuntoToSave = null;
				
				for(AsuntosAbordados asuntoAbordadoOld: vtaqOld.getAsuntos()) {
					
					if(validateData(asuntoAbordadoOld)!=null) {
						erroresIncoincidenciaExpAcum.add(validateData(asuntoAbordadoOld));
					}
					
					asuntoAbordadoNew = new AsuntoAbordado();
					vtaAsunto_TT = new VTaquigraficaAsuntos();
					asuntoToSave = new AsuntoAbordadoDTO();
					
					//Se asignan datos del asunto abordado
					asuntoAbordadoNew.setAsuntoAbordado(asuntoAbordadoOld.getAsuntoAbordado());
					asuntoAbordadoNew.setNumeroExpediente(asuntoAbordadoOld.getNumExpediente());
					asuntoAbordadoNew.setAcumulada(asuntoAbordadoOld.getAcumulados());
//					asuntoAbordadoNew.setAcumulados(Arrays.asList(asuntoAbordadoOld.getNumExpedienteAcumulados()));//Se acordó que no se pone aquí.
					asuntoAbordadoNew.setCa(asuntoAbordadoOld.getCa());
					//
					//Se asignan datos a la versión taquigráfica
					vtaAsunto_TT.setPonente(asuntoAbordadoOld.getPonente());
					vtaAsunto_TT.setAcumulados(Arrays.asList(asuntoAbordadoOld.getNumExpedienteAcumulados()));
					
					if(asuntoAbordadoOld.getTemasProcesalesAbordados().length>0||asuntoAbordadoOld.getTemasFondoAbordados().length>0) {
						totalAsuntosConTemas++;
					}
					
					List<String> newTemasP = new ArrayList<String>();
					List<String> temasP = extractTemas(asuntoAbordadoOld.getTemasProcesalesAbordados());
					for(String tema:temasP) {
						if(!newTemasP.contains(tema.trim())) newTemasP.add(tema.trim());
					}
					Collections.sort(newTemasP);
					
					List<String> newTemasF = new ArrayList<String>();
					List<String> temasF = extractTemas(asuntoAbordadoOld.getTemasFondoAbordados());
					for(String tema:temasF) {
						if(!newTemasF.contains(tema.trim())) newTemasF.add(tema.trim());
					}
					Collections.sort(newTemasF);
					
					vtaAsunto_TT.setTemasProcesales(newTemasP);
					vtaAsunto_TT.setTemasFondo(newTemasF);
					
					vtaAsunto_TT.setNumeroMinistrosParticiparonEnAsunto(extractNumeroMinistrosParticiparon(asuntoAbordadoOld.getNumeroMinistrosParticiparon()));
					//
					
					asuntoToSave.setIdVersionTaqui(vtaqNew.getId());
					asuntoToSave.setAsunto(asuntoAbordadoNew);
					asuntoToSave.setVt_asuntos(vtaAsunto_TT);
					
					//*******Se calcula y actualiza el porcentaje de llenado para cada asunto**********
					if(asuntoAbordadoNew.isAcumulada()) {
						asuntoAbordadoNew.setAcumulados(Arrays.asList(asuntoAbordadoOld.getNumExpedienteAcumulados()));
					}

					PorcentajeDTO porcentajeDTO_asunto =  CalculoPorcentajeUtils.mapaPorcetajeAsuntoAbordado();
					double porcentaje_asunto = CalculoPorcentajeUtils.calculoPorcentaje(asuntoAbordadoNew, porcentajeDTO_asunto);

					PorcentajeDTO porcentajeDTO_asuntoVtaq =  CalculoPorcentajeUtils.mapaPorcetajeVTaquigraficaAsuntos();
					double porcentaje_asuntoVtaq = CalculoPorcentajeUtils.calculoPorcentaje(vtaAsunto_TT, porcentajeDTO_asuntoVtaq);
					
					porcentajes.add(porcentaje_asunto);
					porcentajes.add(porcentaje_asuntoVtaq);
					//*******END***********************

					//Aquí se manda a guardar el asunto abordado
					AsuntoResponseDTO response = _asuntoService.getAction(asuntoToSave, "CREATE");
					if(response.getStatusCode()==HttpStatus.BAD_REQUEST.value()) {
						System.out.println(response.getMessage()+" getIdVersionTaqui: "+asuntoToSave.getIdVersionTaqui()+"  getAsuntoAbordado: "+asuntoToSave.getAsunto().getAsuntoAbordado()+" "+asuntoToSave.getAsunto().getNumeroExpediente());
					}else {
						System.out.println(response.getMessage());
					}

					System.out.println("Se crea el asunto abordado: "+asuntoAbordadoNew.getAsuntoAbordado()+" "+asuntoAbordadoNew.getNumeroExpediente());
					
				}
				PorcentajeDTO porcentajeDTO_vtaq =  CalculoPorcentajeUtils.mapaPorcetajeVersionTaq();
				double porcentaje_vtaq = CalculoPorcentajeUtils.calculoPorcentaje(vtaqNew, porcentajeDTO_vtaq);
				
				porcentajes.add(porcentaje_vtaq);
				double sum = porcentajes.stream().mapToDouble(Double::doubleValue).sum();
				double porcentaje = sum/porcentajes.size();
				vtaqNew.setPorcentaje(CalculoPorcentajeUtils.round(porcentaje, 1));
				System.out.println("porcentaje: "+porcentaje);
				vTaqRepository.save(vtaqNew);
			}
		}
		System.out.println("Total de asuntos con temas: "+totalAsuntosConTemas);
//		System.out.println("*********Sin fecha******");
//		for(String idError: erroresFechaSesion) {
//			System.out.println(idError);
//		}
		System.out.println("*********Inconicdencia NumExp Acum******");
		for(String incoincidencia: erroresIncoincidenciaExpAcum) {
			System.out.println(incoincidencia);
		}
//		System.out.println("*********Sin duración******");
//		for(String sinDuracion: erroresSinDuracion) {
//			System.out.println(sinDuracion);
//		}
		
		findTemasProcesalesRepetidos();
		System.out.println("Ce fini");
		
		
	}
	
	public List<String> replaceInList(List<String> list) {
		List<String> listnew = new ArrayList<String>();
		for(String item: list) {
			item = item.replaceAll("[\\r\\n]+", " ");
			item = item.replaceAll("\\r|\\n", " ");
			item = item.replaceAll("\n", "").replaceAll("\r", " ");
			listnew.add(item);
		}
		return listnew;
	}
	
	
	public List<String> fillVotacionTipoViolacionInvasionPoderesAnalizadoenSentencia(Sentencia sentencia, SentenciaOld sentenciaOld, List<Double> porcentajes) {
		List<String> VotacionTipoVicioProcesoLegislativoAnalizadoenSe_ID = new ArrayList<String>();
		TablaVotaciones tablaVotacion = null;

		for(TipoVioInvaPodAnaSente tipoVicInvEsfeAnaSenten : sentenciaOld.getTipoVioInvaPodAnaSente()) {
			tablaVotacion = new TablaVotaciones();
			tablaVotacion.setTipoViolacionInvacionPoderes(tipoVicInvEsfeAnaSenten.getTipoViolacion());
			tablaVotacion.setActosImpugnados(tipoVicInvEsfeAnaSenten.getActosImpugnados());
			tablaVotacion.setRubrosTematicosPrincipal(tipoVicInvEsfeAnaSenten.getRubrosTematicos());//
			
			tablaVotacion.setTextoVotacion(tipoVicInvEsfeAnaSenten.getTextoVotacionVicioPlanteado());
			tablaVotacion.setVotacionFavor(tipoVicInvEsfeAnaSenten.getVotacionAFavorVicioPlanteado());
			tablaVotacion.setVotacionEnContra(tipoVicInvEsfeAnaSenten.getVotaciontionEnContraVicioPlanteado());
			tablaVotacion.setMinistros(tipoVicInvEsfeAnaSenten.getTodosMinistrosVicioPlanteado());
			tablaVotacion.setAprobadasConcideraciones(tipoVicInvEsfeAnaSenten.isVotacionMayoriaVicioPlanteado());
			
			tablaVotacion.setSentidoResolucion(tipoVicInvEsfeAnaSenten.getFundamentacion()!=null?tipoVicInvEsfeAnaSenten.getFundamentacion().toUpperCase():null);
			tablaVotacion.setMetodologiaAnalisisConstitucionalidad(tipoVicInvEsfeAnaSenten.getMetodologia());
			List<String> tipoSentencia = new ArrayList<String>();
			for(String val:tipoVicInvEsfeAnaSenten.getTipoSentencia()) tipoSentencia.add(val.trim().toUpperCase());
			tablaVotacion.setTipoSentencia(tipoSentencia);
			
			tablaVotacion.setNumeroVotos(tipoVicInvEsfeAnaSenten.getCantidadAFavorVicioPlanteado());//REVISAR
			
			tablaVotacion.setTipoVotacion("votacionTipoViolacionInvacionPoderesAnalizadoenSentencia");

			_votacionRepository.save(tablaVotacion);
			VotacionTipoVicioProcesoLegislativoAnalizadoenSe_ID.add(tablaVotacion.getId());
			//*********Rubros  temáticos relacionados con los efectos de la sentencia: (dereHumaViolaAnaliSenten/votacionesTipoSentencia), cuando selecciona Sentido de la resolución: Fundada***********
			List<String> listRubrosTID = new ArrayList<String>();
			RubrosTablaVotaciones rubrosT = null;

			for(VotacionTipoSentencia votacionTipoSentencia : tipoVicInvEsfeAnaSenten.getVotacionesTipoSentencia()) {
				rubrosT = new RubrosTablaVotaciones();
				
				rubrosT.setTextoVotacion(votacionTipoSentencia.getTextoVotacionTipoSentencia());
				rubrosT.setMinistros(votacionTipoSentencia.getTodosMinistrosTipoSentencia());
				rubrosT.setVotacionEnContra(votacionTipoSentencia.getVotaciontionEnContraTipoSentencia());
				rubrosT.setVotacionFavor(votacionTipoSentencia.getVotacionAFavorTipoSentencia());
				rubrosT.setRubrosTematicos(votacionTipoSentencia.getRubroTemTipSentencia());
				rubrosT.setTipoRubro("rubrosEfectoSentencia");
				votacionesRubrosRepository.save(rubrosT);
				listRubrosTID.add(rubrosT.getId());
			}
			
			List<String> rubros = tablaVotacion.getRubrosEfectoSentencia()!=null?tablaVotacion.getRubrosEfectoSentencia(): new ArrayList<String>();
			rubros.addAll(listRubrosTID);
			tablaVotacion.setRubrosEfectoSentencia(rubros);
			//*************************************Rubros temáticos relacionados con la extensión de invalidez: (dereHumaViolaAnaliSenten/votacionRelacionadosExtensionValidez)******************
			listRubrosTID = new ArrayList<String>();
			rubrosT = null;

			for(VotacionTipoSentencia votacoinRelExtV : tipoVicInvEsfeAnaSenten.getVotacionRelacionadosExtensionValidez()) {
				rubrosT = new RubrosTablaVotaciones();
				
				rubrosT.setTextoVotacion(votacoinRelExtV.getTextoVotacionTipoSentencia());
				rubrosT.setMinistros(votacoinRelExtV.getTodosMinistrosTipoSentencia());
				rubrosT.setVotacionEnContra(votacoinRelExtV.getVotaciontionEnContraTipoSentencia());
				rubrosT.setVotacionFavor(votacoinRelExtV.getVotacionAFavorTipoSentencia());
				rubrosT.setRubrosTematicos(votacoinRelExtV.getRubroTemTipSentencia());
				rubrosT.setTipoRubro("rubrosExtencionInvalidez");
				votacionesRubrosRepository.save(rubrosT);
				listRubrosTID.add(rubrosT.getId());
			}
			rubros = new ArrayList<String>();
			rubros = tablaVotacion.getRubrosExtencionInvalidez()!=null?tablaVotacion.getRubrosExtencionInvalidez(): new ArrayList<String>();
			rubros.addAll(listRubrosTID);
			tablaVotacion.setRubrosExtencionInvalidez(rubros);
			//*******************************************************
			_votacionRepository.save(tablaVotacion);
			//Pidieron que no se contemplara el cálculo del porcentaje de las tablas de votación para que no se vea tan in completo
//			double porcentaje_TablaVotacion = porcentajeService.calculoPorcentaje(tablaVotacion, "votacionDerechosHumanosCuyaViolacionAnalizaSentencia");//Dejo este mismo tipo de validación ya que son los mismos datos que en derechos humanos
//			porcentajes.add(porcentaje_TablaVotacion);
		}
//		votacionTipoViolacionInvacionPoderesAnalizadoenSentencia
		List<String> causas = sentencia.getVotacionTipoViolacionInvacionPoderesAnalizadoenSentencia()!=null?sentencia.getVotacionTipoViolacionInvacionPoderesAnalizadoenSentencia(): new ArrayList<String>();
		causas.addAll(VotacionTipoVicioProcesoLegislativoAnalizadoenSe_ID);
		return causas;
	}
	
	public List<String> fillVotacionTipoViolacionInvasionEsferasAnalizadoenSentencia(Sentencia sentencia, SentenciaOld sentenciaOld, List<Double> porcentajes) {
		List<String> VotacionTipoVicioProcesoLegislativoAnalizadoenSe_ID = new ArrayList<String>();
		TablaVotaciones tablaVotacion = null;

		for(TipoVicInvEsfeAnaSenten tipoVicInvEsfeAnaSenten : sentenciaOld.getTipoVicInvEsfeAnaSenten()) {
			tablaVotacion = new TablaVotaciones();
			tablaVotacion.setTipoViolacionInvacionEsferas(tipoVicInvEsfeAnaSenten.getTipoViolacion());
			tablaVotacion.setActosImpugnados(tipoVicInvEsfeAnaSenten.getActosImpugnados());
			tablaVotacion.setRubrosTematicosPrincipal(tipoVicInvEsfeAnaSenten.getRubrosTematicos());//
			
			tablaVotacion.setTextoVotacion(tipoVicInvEsfeAnaSenten.getTextoVotacionVicioPlanteado());
			tablaVotacion.setVotacionFavor(tipoVicInvEsfeAnaSenten.getVotacionAFavorVicioPlanteado());
			tablaVotacion.setVotacionEnContra(tipoVicInvEsfeAnaSenten.getVotaciontionEnContraVicioPlanteado());
			tablaVotacion.setMinistros(tipoVicInvEsfeAnaSenten.getTodosMinistrosVicioPlanteado());
			tablaVotacion.setAprobadasConcideraciones(tipoVicInvEsfeAnaSenten.isVotacionMayoriaVicioPlanteado());
			
			tablaVotacion.setSentidoResolucion(tipoVicInvEsfeAnaSenten.getFundamentacion()!=null?tipoVicInvEsfeAnaSenten.getFundamentacion().toUpperCase():null);
			tablaVotacion.setMetodologiaAnalisisConstitucionalidad(tipoVicInvEsfeAnaSenten.getMetodologia().toUpperCase());
			List<String> tipoSentencia = new ArrayList<String>();
			for(String val:tipoVicInvEsfeAnaSenten.getTipoSentencia()) tipoSentencia.add(val.trim().toUpperCase());
			tablaVotacion.setTipoSentencia(tipoSentencia);
			
			tablaVotacion.setNumeroVotos(tipoVicInvEsfeAnaSenten.getCantidadAFavorVicioPlanteado());//REVISAR
			
			tablaVotacion.setTipoVotacion("votacionTipoViolacionEsferasAnalizadoenSentencia");

			_votacionRepository.save(tablaVotacion);
			VotacionTipoVicioProcesoLegislativoAnalizadoenSe_ID.add(tablaVotacion.getId());
			//*********Rubros  temáticos relacionados con los efectos de la sentencia: (dereHumaViolaAnaliSenten/votacionesTipoSentencia), cuando selecciona Sentido de la resolución: Fundada***********
			List<String> listRubrosTID = new ArrayList<String>();
			RubrosTablaVotaciones rubrosT = null;

			for(VotacionTipoSentencia votacionTipoSentencia : tipoVicInvEsfeAnaSenten.getVotacionesTipoSentencia()) {
				rubrosT = new RubrosTablaVotaciones();
				
				rubrosT.setTextoVotacion(votacionTipoSentencia.getTextoVotacionTipoSentencia());
				rubrosT.setMinistros(votacionTipoSentencia.getTodosMinistrosTipoSentencia());
				rubrosT.setVotacionEnContra(votacionTipoSentencia.getVotaciontionEnContraTipoSentencia());
				rubrosT.setVotacionFavor(votacionTipoSentencia.getVotacionAFavorTipoSentencia());
				rubrosT.setRubrosTematicos(votacionTipoSentencia.getRubroTemTipSentencia());
				rubrosT.setTipoRubro("rubrosEfectoSentencia");
				votacionesRubrosRepository.save(rubrosT);
				listRubrosTID.add(rubrosT.getId());
			}
			
			List<String> rubros = tablaVotacion.getRubrosEfectoSentencia()!=null?tablaVotacion.getRubrosEfectoSentencia(): new ArrayList<String>();
			rubros.addAll(listRubrosTID);
			tablaVotacion.setRubrosEfectoSentencia(rubros);
			//*************************************Rubros temáticos relacionados con la extensión de invalidez: (dereHumaViolaAnaliSenten/votacionRelacionadosExtensionValidez)******************
			listRubrosTID = new ArrayList<String>();
			rubrosT = null;

			for(VotacionTipoSentencia votacoinRelExtV : tipoVicInvEsfeAnaSenten.getVotacionRelacionadosExtensionValidez()) {
				rubrosT = new RubrosTablaVotaciones();
				
				rubrosT.setTextoVotacion(votacoinRelExtV.getTextoVotacionTipoSentencia());
				rubrosT.setMinistros(votacoinRelExtV.getTodosMinistrosTipoSentencia());
				rubrosT.setVotacionEnContra(votacoinRelExtV.getVotaciontionEnContraTipoSentencia());
				rubrosT.setVotacionFavor(votacoinRelExtV.getVotacionAFavorTipoSentencia());
				rubrosT.setRubrosTematicos(votacoinRelExtV.getRubroTemTipSentencia());
				rubrosT.setTipoRubro("rubrosExtencionInvalidez");
				votacionesRubrosRepository.save(rubrosT);
				listRubrosTID.add(rubrosT.getId());
			}
			rubros = new ArrayList<String>();
			rubros = tablaVotacion.getRubrosExtencionInvalidez()!=null?tablaVotacion.getRubrosExtencionInvalidez(): new ArrayList<String>();
			rubros.addAll(listRubrosTID);
			tablaVotacion.setRubrosExtencionInvalidez(rubros);
			//*******************************************************
			_votacionRepository.save(tablaVotacion);
			//Pidieron que no se contemplara el cálculo del porcentaje de las tablas de votación para que no se vea tan in completo
//			double porcentaje_TablaVotacion = porcentajeService.calculoPorcentaje(tablaVotacion, "votacionDerechosHumanosCuyaViolacionAnalizaSentencia");//Dejo este mismo tipo de validación ya que son los mismos datos que en derechos humanos
//			porcentajes.add(porcentaje_TablaVotacion);
		}
//		votacionTipoViolacionEsferasAnalizadoenSentencia
		List<String> causas = sentencia.getVotacionTipoViolacionInvacionEsferasAnalizadoenSentencia()!=null?sentencia.getVotacionTipoViolacionInvacionEsferasAnalizadoenSentencia(): new ArrayList<String>();
		causas.addAll(VotacionTipoVicioProcesoLegislativoAnalizadoenSe_ID);
		return causas;
	}
	
	public List<String> fillVotacionTipoVicioProcesoLegislativoAnalizadoenSentencia(Sentencia sentencia, SentenciaOld sentenciaOld, List<Double> porcentajes) {
		List<String> VotacionTipoVicioProcesoLegislativoAnalizadoenSe_ID = new ArrayList<String>();
		TablaVotaciones tablaVotacion = null;

		for(TipoVicProLegisAnali tipVicioprLegAnS : sentenciaOld.getTipoVicProLegisAnali()) {
			tablaVotacion = new TablaVotaciones();
			tablaVotacion.setTipoVicio(tipVicioprLegAnS.getTipoVicio());
			tablaVotacion.setCongresoEmitioDecreto(tipVicioprLegAnS.getCongreso());
			tablaVotacion.setDecretoImpugnado(tipVicioprLegAnS.getDecretoImpugnado());
			
			tablaVotacion.setRubrosTematicosPrincipal(tipVicioprLegAnS.getRubrosTematicos());//
			
			tablaVotacion.setTextoVotacion(tipVicioprLegAnS.getTextoVotacionVicioPlanteado());
			tablaVotacion.setVotacionFavor(tipVicioprLegAnS.getVotacionAFavorVicioPlanteado());
			tablaVotacion.setVotacionEnContra(tipVicioprLegAnS.getVotaciontionEnContraVicioPlanteado());
			tablaVotacion.setMinistros(tipVicioprLegAnS.getTodosMinistrosVicioPlanteado());
			tablaVotacion.setAprobadasConcideraciones(tipVicioprLegAnS.isVotacionMayoriaVicioPlanteado());
			
			tablaVotacion.setSentidoResolucion(tipVicioprLegAnS.getFundamentacion()!=null?tipVicioprLegAnS.getFundamentacion().toUpperCase():null);

			
			List<String> tipoSentencia = new ArrayList<String>();
			for(String val:tipVicioprLegAnS.getTipoSentencia()) tipoSentencia.add(val.trim().toUpperCase());
			tablaVotacion.setTipoSentencia(tipoSentencia);
			
			tablaVotacion.setNumeroVotos(tipVicioprLegAnS.getCantidadAFavorVicioPlanteado());//REVISAR
			
//			derHumViolAnS.getVotacionesTipoSentencia() //REVISAR NO SE SI SE PONE
			tablaVotacion.setTipoVotacion("votacionTipoVicioProcesoLegislativoAnalizadoenSentencia");

			_votacionRepository.save(tablaVotacion);
			VotacionTipoVicioProcesoLegislativoAnalizadoenSe_ID.add(tablaVotacion.getId());
			//*********Rubros  temáticos relacionados con los efectos de la sentencia: (dereHumaViolaAnaliSenten/votacionesTipoSentencia), cuando selecciona Sentido de la resolución: Fundada***********
			List<String> listRubrosTID = new ArrayList<String>();
			RubrosTablaVotaciones rubrosT = null;

			for(VotacionTipoSentencia votacionTipoSentencia : tipVicioprLegAnS.getVotacionesTipoSentencia()) {
				rubrosT = new RubrosTablaVotaciones();
				
				rubrosT.setTextoVotacion(votacionTipoSentencia.getTextoVotacionTipoSentencia());
				rubrosT.setMinistros(votacionTipoSentencia.getTodosMinistrosTipoSentencia());
				rubrosT.setVotacionEnContra(votacionTipoSentencia.getVotaciontionEnContraTipoSentencia());
				rubrosT.setVotacionFavor(votacionTipoSentencia.getVotacionAFavorTipoSentencia());
				rubrosT.setRubrosTematicos(votacionTipoSentencia.getRubroTemTipSentencia());
				rubrosT.setTipoRubro("rubrosEfectoSentencia");
				votacionesRubrosRepository.save(rubrosT);
				listRubrosTID.add(rubrosT.getId());
			}
			
			List<String> rubros = tablaVotacion.getRubrosEfectoSentencia()!=null?tablaVotacion.getRubrosEfectoSentencia(): new ArrayList<String>();
			rubros.addAll(listRubrosTID);
			tablaVotacion.setRubrosEfectoSentencia(rubros);
			//*************************************Rubros temáticos relacionados con la extensión de invalidez: (dereHumaViolaAnaliSenten/votacionRelacionadosExtensionValidez)******************
			listRubrosTID = new ArrayList<String>();
			rubrosT = null;

			for(VotacionTipoSentencia votacoinRelExtV : tipVicioprLegAnS.getVotacionRelacionadosExtensionValidez()) {
				rubrosT = new RubrosTablaVotaciones();
				
				rubrosT.setTextoVotacion(votacoinRelExtV.getTextoVotacionTipoSentencia());
				rubrosT.setMinistros(votacoinRelExtV.getTodosMinistrosTipoSentencia());
				rubrosT.setVotacionEnContra(votacoinRelExtV.getVotaciontionEnContraTipoSentencia());
				rubrosT.setVotacionFavor(votacoinRelExtV.getVotacionAFavorTipoSentencia());
				rubrosT.setRubrosTematicos(votacoinRelExtV.getRubroTemTipSentencia());
				rubrosT.setTipoRubro("rubrosExtencionInvalidez");
				votacionesRubrosRepository.save(rubrosT);
				listRubrosTID.add(rubrosT.getId());
			}
			rubros = new ArrayList<String>();
			rubros = tablaVotacion.getRubrosExtencionInvalidez()!=null?tablaVotacion.getRubrosExtencionInvalidez(): new ArrayList<String>();
			rubros.addAll(listRubrosTID);
			tablaVotacion.setRubrosExtencionInvalidez(rubros);
			//*******************************************************
			_votacionRepository.save(tablaVotacion);
			//Pidieron que no se contemplara el cálculo del porcentaje de las tablas de votación para que no se vea tan in completo
//			double porcentaje_TablaVotacion = porcentajeService.calculoPorcentaje(tablaVotacion, "votacionDerechosHumanosCuyaViolacionAnalizaSentencia");//Dejo este mismo tipo de validación ya que son los mismos datos que en derechos humanos
//			porcentajes.add(porcentaje_TablaVotacion);
		}
//		votacionTipoVicioProcesoLegislativoAnalizadoenSentencia
		List<String> causas = sentencia.getVotacionTipoVicioProcesoLegislativoAnalizadoenSentencia()!=null?sentencia.getVotacionTipoVicioProcesoLegislativoAnalizadoenSentencia(): new ArrayList<String>();
		causas.addAll(VotacionTipoVicioProcesoLegislativoAnalizadoenSe_ID);
		return causas;
	}
	
	
	public List<String> fillDerHumCuyaViolAnalizaSentencia(Sentencia sentencia, SentenciaOld sentenciaOld, List<Double> porcentajes) {
		List<String> DerHumCuyaViolAnalizaSentencia_ID = new ArrayList<String>();
		TablaVotaciones tablaVotacion = null;

		for(DereHumaViolaAnaliSenten derHumViolAnS : sentenciaOld.getDereHumaViolaAnaliSenten()) {
			tablaVotacion = new TablaVotaciones();
			tablaVotacion.setTextoVotacion(derHumViolAnS.getTextoVotacionVicioPlanteado());//DONE
			tablaVotacion.setVotacionFavor(derHumViolAnS.getVotacionAFavorVicioPlanteado());//DONE
			tablaVotacion.setVotacionEnContra(derHumViolAnS.getVotaciontionEnContraVicioPlanteado());//DONE
			tablaVotacion.setSentidoResolucion(derHumViolAnS.getFundamentacion()!=null?derHumViolAnS.getFundamentacion().toUpperCase():null);//DONE
			tablaVotacion.setArticulosImpugnados(derHumViolAnS.getArticulos()!=null?derHumViolAnS.getArticulos():new ArrayList<String>() );//DONE
			tablaVotacion.setActosImpugnados(new ArrayList<String>());//DONE
			tablaVotacion.setAprobadasConcideraciones(derHumViolAnS.isVotacionMayoriaVicioPlanetado());//DONE
			
			List<String> derechos = new ArrayList<String>();
			for(String val:derHumViolAnS.getDerechos()) derechos.add(val.trim().toUpperCase());
			tablaVotacion.setDerechosHumanos(derechos);//DONE
			
			tablaVotacion.setRubrosTematicosPrincipal(derHumViolAnS.getRubros());//DONE
			
			tablaVotacion.setMetodologiaAnalisisConstitucionalidad(derHumViolAnS.getMetodologia()!=null?derHumViolAnS.getMetodologia().toUpperCase():null);//DONE
			
			List<String> tipoSentencia = new ArrayList<String>();
			for(String val:derHumViolAnS.getTipoSentencia()) tipoSentencia.add(val.trim().toUpperCase());
			tablaVotacion.setTipoSentencia(tipoSentencia);//DONE
			
			tablaVotacion.setMinistros(derHumViolAnS.getTodosMinistrosVicioPlanteado());//DONE
			tablaVotacion.setNumeroVotos(derHumViolAnS.getCantidadAFavorVicioPlanteado());//esta tengo duda si es correcto colocarlo aquí?????????????????? (REVISAR EN DATOS SI PARA ESTE CASO HAY ALGUNO)
			tablaVotacion.setTipoVotacion("votacionDerechosHumanosCuyaViolacionAnalizaSentencia");//DONE

			_votacionRepository.save(tablaVotacion);
			DerHumCuyaViolAnalizaSentencia_ID.add(tablaVotacion.getId());
			//*********Rubros  temáticos relacionados con los efectos de la sentencia: (dereHumaViolaAnaliSenten/votacionesTipoSentencia), cuando selecciona Sentido de la resolución: Fundada***********
			List<String> listRubrosTID = new ArrayList<String>();
			RubrosTablaVotaciones rubrosT = null;

			for(VotacionTipoSentencia votacionTipoSentencia : derHumViolAnS.getVotacionesTipoSentencia()) {
				rubrosT = new RubrosTablaVotaciones();
				
				rubrosT.setTextoVotacion(votacionTipoSentencia.getTextoVotacionTipoSentencia());
				rubrosT.setMinistros(votacionTipoSentencia.getTodosMinistrosTipoSentencia());
				rubrosT.setVotacionEnContra(votacionTipoSentencia.getVotaciontionEnContraTipoSentencia());
				rubrosT.setVotacionFavor(votacionTipoSentencia.getVotacionAFavorTipoSentencia());
				rubrosT.setRubrosTematicos(votacionTipoSentencia.getRubroTemTipSentencia());
				rubrosT.setTipoRubro("rubrosEfectoSentencia");
				votacionesRubrosRepository.save(rubrosT);
				listRubrosTID.add(rubrosT.getId());
			}
			
			List<String> rubros = tablaVotacion.getRubrosEfectoSentencia()!=null?tablaVotacion.getRubrosEfectoSentencia(): new ArrayList<String>();
			rubros.addAll(listRubrosTID);
			tablaVotacion.setRubrosEfectoSentencia(rubros);
			//*************************************Rubros temáticos relacionados con la extensión de invalidez: (dereHumaViolaAnaliSenten/votacionRelacionadosExtensionValidez)******************
			listRubrosTID = new ArrayList<String>();
			rubrosT = null;

			for(VotacionTipoSentencia votacoinRelExtV : derHumViolAnS.getVotacionRelacionadosExtensionValidez()) {
				rubrosT = new RubrosTablaVotaciones();
				
				rubrosT.setTextoVotacion(votacoinRelExtV.getTextoVotacionTipoSentencia());
				rubrosT.setMinistros(votacoinRelExtV.getTodosMinistrosTipoSentencia());
				rubrosT.setVotacionEnContra(votacoinRelExtV.getVotaciontionEnContraTipoSentencia());
				rubrosT.setVotacionFavor(votacoinRelExtV.getVotacionAFavorTipoSentencia());
				rubrosT.setRubrosTematicos(votacoinRelExtV.getRubroTemTipSentencia());
				rubrosT.setTipoRubro("rubrosExtencionInvalidez");
				votacionesRubrosRepository.save(rubrosT);
				listRubrosTID.add(rubrosT.getId());
			}
			rubros = new ArrayList<String>();
			rubros = tablaVotacion.getRubrosExtencionInvalidez()!=null?tablaVotacion.getRubrosExtencionInvalidez(): new ArrayList<String>();
			rubros.addAll(listRubrosTID);
			tablaVotacion.setRubrosExtencionInvalidez(rubros);
			//*******************************************************
			_votacionRepository.save(tablaVotacion);
			//Pidieron que no se contemplara el cálculo del porcentaje de las tablas de votación para que no se vea tan in completo
//			double porcentaje_TablaVotacion = porcentajeService.calculoPorcentaje(tablaVotacion, "votacionDerechosHumanosCuyaViolacionAnalizaSentencia");
//			porcentajes.add(porcentaje_TablaVotacion);
		}

		List<String> causas = sentencia.getVotacionDerechosHumanosCuyaViolacionAnalizaSentencia()!=null?sentencia.getVotacionDerechosHumanosCuyaViolacionAnalizaSentencia(): new ArrayList<String>();
		causas.addAll(DerHumCuyaViolAnalizaSentencia_ID);
		return causas;
	}
	
	public List<String> fillTablaCausasImprocSobreAnaliz(Sentencia sentencia, SentenciaOld sentenciaOld, List<Double> porcentajes) {
		List<String> causasImproSobreseAnali_ID = new ArrayList<String>();
		TablaVotaciones tablaVotacion = null;
		for(CausasImproSobreseAnali causaImpSobrA : sentenciaOld.getCausasImproSobreseAnali()) {
			tablaVotacion = new TablaVotaciones();
			tablaVotacion.setActosImpugnados(causaImpSobrA.getActosImpugnados());
			tablaVotacion.setArticulosImpugnados(causaImpSobrA.getActosImpugnados());
			tablaVotacion.setSentidoResolucion(causaImpSobrA.getFundamentacion()!=null?causaImpSobrA.getFundamentacion().toUpperCase():null);
			tablaVotacion.setVotacionFavor(causaImpSobrA.getVotacionAFavorVicioPlanteado());
			tablaVotacion.setVotacionEnContra(causaImpSobrA.getVotaciontionEnContraVicioPlanteado());
			tablaVotacion.setMinistros(causaImpSobrA.getTodosMinistrosVicioPlanteado());
			tablaVotacion.setTextoVotacion(causaImpSobrA.getTextoVotacionVicioPlanteado());
			tablaVotacion.setNumeroVotos(0);
			tablaVotacion.setTipoVotacion("votacionCausasImprocedenciaySobreseimientoAnalizadas");
			
			double cantidadFilled = porcentajeService.calculoPorcentaje(tablaVotacion, "votacionCausasImprocedenciaySobreseimientoAnalizadas");
			if(cantidadFilled==0d) {
				System.out.println("este dato viene vacío en "+sentencia.getTipoAsunto()+" "+sentencia.getNumeroExpediente());
			}else {
				//Aquí revisar si ya no se están ocupando:
				//votacionMayoriaVicioPlanetado
				//cantidadAFavorVicioPlanteado
				_votacionRepository.save(tablaVotacion);
				causasImproSobreseAnali_ID.add(tablaVotacion.getId());

				List<String> listRubrosTID = new ArrayList<String>();
				RubrosTablaVotaciones rubrosT = null;

				for(CausaImprocedencia causasImp : causaImpSobrA.getCausasDeImprocedencias()) {
					rubrosT = new RubrosTablaVotaciones();
					rubrosT.setAnalisisProcedencia(causasImp.getCausaImprocedencia().stream().map(String::toUpperCase) .collect(Collectors.toList()));
					rubrosT.setRubrosTematicos(causasImp.getRubrosTematicos());
					rubrosT.setTipoRubro("rubrosTematicos");
					votacionesRubrosRepository.save(rubrosT);
					listRubrosTID.add(rubrosT.getId());
				}
				List<String> rubros = tablaVotacion.getRubrosTematicos()!=null?tablaVotacion.getRubrosTematicos(): new ArrayList<String>();
				rubros.addAll(listRubrosTID);
				tablaVotacion.setRubrosTematicos(rubros);
				_votacionRepository.save(tablaVotacion);
				//Pidieron que no se contemplara el cálculo del porcentaje de las tablas de votación para que no se vea tan in completo
//				double porcentaje_TablaVotacion = porcentajeService.calculoPorcentaje(tablaVotacion, "votacionCausasImprocedenciaySobreseimientoAnalizadas");
//				porcentajes.add(porcentaje_TablaVotacion);
			}
		}
		
		List<String> causas = sentencia.getVotacionCausasImprocedenciaySobreseimientoAnalizadas()!=null?sentencia.getVotacionCausasImprocedenciaySobreseimientoAnalizadas(): new ArrayList<String>();
		causas.addAll(causasImproSobreseAnali_ID);
		return causas;
	}
	
	public List<String> setListDerechosHumanosCuyaViolacionPlantea(List<String> listaToAdd, List<String> derechosHumanosCuyaViolacionPlantea){
		for(String val : listaToAdd) {
			if(!derechosHumanosCuyaViolacionPlantea.contains(val.toUpperCase().trim())) {
				derechosHumanosCuyaViolacionPlantea.add(val.toUpperCase().trim());
			}
		}
		return derechosHumanosCuyaViolacionPlantea;
	}
	
	public void issuesDuplicados(Sentencia sentencia, SentenciaOld sentenciaOld, int size) {
		Issues issue = new Issues();
		issue.setResumen("Se duplicó la sentencia");
		issue.setSeccion("Sentencias");
		issue.setType("ERROR_CAPTURA");
		
		Map<String, String> mapaIssue = new HashMap<>();
		mapaIssue.put("idvtaqOld", sentenciaOld.getId());
		mapaIssue.put("NumRepeticiones: ", size+"");
		mapaIssue.put("tipoAsunto", sentencia.getTipoAsunto());
		mapaIssue.put("numExpediente", sentencia.getNumeroExpediente());
		
		issue.setDetail(mapaIssue);
		issuesRepository.save(issue);
	}
	
	public void issuesInconsistenciaFechaResolucion(Sentencia sentencia, SentenciaOld sentenciaOld) {
		Issues issue = new Issues();
		issue.setResumen("No coinciden las FechaResolucion de la sentencia");
		issue.setSeccion("Sentencias");
		issue.setType("ERROR_SISTEMA");
		
		Map<String, String> mapaIssue = new HashMap<>();
		mapaIssue.put("idSentenciaOld", sentenciaOld.getId());
		mapaIssue.put("fechaResolucionOld", ConvertFechaFiltros.convertEngFechaTextToDateString(sentenciaOld.getFechaResolucion())+"");
		mapaIssue.put("sentenciaOld", sentenciaOld.getTipoAsunto()+" "+sentenciaOld.getNumExpedientes());
		
		mapaIssue.put("idSentenciaNew", sentencia.getId()+"");
		mapaIssue.put("fechaResolucionNew", sentencia.getFechaResolucion());
		mapaIssue.put("sentenciaNew", sentencia.getTipoAsunto()+" "+sentencia.getNumeroExpediente());
		
		issue.setDetail(mapaIssue);
		issuesRepository.save(issue);
	}
	
	public void issuesSentenciaInexistente(Sentencia sentencia) {
		Issues issue = new Issues();
		issue.setResumen("No se registró el asunto en Sentencias");
		issue.setSeccion("Sentencias");
		issue.setType("ERROR_SISTEMA");
		
		Map<String, String> mapaIssue = new HashMap<>();
		mapaIssue.put("idSentencia", sentencia.getId());
		mapaIssue.put("tipoAsunto", sentencia.getTipoAsunto());
		mapaIssue.put("numExpediente", sentencia.getNumeroExpediente());
		
		issue.setDetail(mapaIssue);
		issuesRepository.save(issue);
	}
	
	public ResponseDTO populateSentencias() {
		int count =0;
		List<Sentencia> sentenciasNewList = sentenciaRepository.findAll();
		for(Sentencia sentencia:sentenciasNewList) {
			System.out.println("**********************************");
			List<SentenciaOld> sentenciaOld = sentenciaOldRepository.findByTipoAsuntoAndNumExpedientes(sentencia.getTipoAsunto(), sentencia.getNumeroExpediente());
			if(!sentenciaOld.isEmpty()) {
				if(sentenciaOld.size()>1) {
//					System.out.println("Se duplicó la sentencia: "+sentencia.getTipoAsunto()+" "+sentencia.getNumeroExpediente());
					issuesDuplicados(sentencia, sentenciaOld.get(count), sentenciaOld.size());	
				}else{
					
					if(!sentencia.getFechaResolucion().equals(
						ConvertFechaFiltros.convertEngFechaTextToDateString(sentenciaOld.get(0).getFechaResolucion()))) {
						//Inconsistencia en fecha de resolución
						issuesInconsistenciaFechaResolucion(sentencia, sentenciaOld.get(0));
					}
					count++;
					
					if(sentenciaOld.get(0).getNumExpedientes().equals("176/2021")) {
						System.out.println("u");
					}
					
					//LOS SIGUIENTES DATOS YA NO SE MIGRAN PORQUE YA SE TIENEN ALMACENADOS CUANDO SE CREARON LAS SENTENCIAS EN AUTOMÁTICO:
					//Tipo de asunto, Número de expediente, Órgano de radicación, fechaResolucion y Ponente
					
					//*****************PRIMERA SECCIÓN ANTES DE LAS TABLAS TRANSITIVAS******************************************
					sentencia.setTipoPersonaPromovente(sentenciaOld.get(0).getTipoPersonaPromovente().toUpperCase());
					sentencia.setTipoPersonaJuridicoColectiva(sentenciaOld.get(0).getTipoPersonaJuridicoDemandada().toUpperCase());//Se activa al seleccionar un elemento del campo anterior
					sentencia.setTipoViolacionPlanteadaenDemanda(sentenciaOld.get(0).getTipoViolacionesPlanteadasDemanda().toUpperCase());		
					//derechosHumanosViolacionAnaliza y derechosHumanosViolacion parecen tener la misma información, revisar (Yo lo fusioné por el  momento)
					List<String> derechosHumanosCuyaViolacionPlantea = new ArrayList<String>();
					setListDerechosHumanosCuyaViolacionPlantea(Arrays.asList(sentenciaOld.get(0).getDerechosHumanosViolacion()), derechosHumanosCuyaViolacionPlantea);
//					setListDerechosHumanosCuyaViolacionPlantea(sentenciaOld.get(0).getDerechosHumanosViolacionAnaliza(), derechosHumanosCuyaViolacionPlantea);
					sentencia.setDerechosHumanosCuyaViolacionPlantea(derechosHumanosCuyaViolacionPlantea);
					//
					List<String> violOrgPlanteada = new ArrayList<String>();
					for(String val:sentenciaOld.get(0).getViolacionOrganicaPlanteada()) violOrgPlanteada.add(val.trim().toUpperCase());
					sentencia.setViolacionOrganicaPlanteada(violOrgPlanteada);
					
					List<String> tipViciosprocLeg = new ArrayList<String>();
					for(String val:sentenciaOld.get(0).getTipoVicioProcesoLegislativo()) tipViciosprocLeg.add(val.trim().toUpperCase());
					sentencia.setTipoVicioProcesoLegislativo(tipViciosprocLeg);
					
					sentencia.setTipoViolacionInvacionEsferas(sentenciaOld.get(0).getTipoViolacionInvasionEsferas().toUpperCase());
					sentencia.setTipoViolacionInvacionPoderes(sentenciaOld.get(0).getTipoViolacionInvasionPoderes().toUpperCase());
					//*****************END PRIMERA SECCIÓN ANTES DE LAS TABLAS TRANSITIVAS****************************************
					
					List<Double> porcentajes = new ArrayList<Double>();
					
//					**TABLA DE VOTACIÓN -> Causas de improcedencia y de sobreseimiento analizada		=> CORRECTAMENTE IMPLEMENTADA
					sentencia.setVotacionCausasImprocedenciaySobreseimientoAnalizadas(
							fillTablaCausasImprocSobreAnaliz(sentencia, sentenciaOld.get(0), porcentajes));
					
					//PROPONGO RETIRAR ESTE CAMPO YA QUE LO FUSIONÉ EN LA PRIMERA SECCIÓN
					sentencia.setDerechosHumanosCuyaViolacionPlanteaByDerechosHumanos(sentenciaOld.get(0).getDerechosHumanosViolacionAnaliza());
					
//					**TABLA DE VOTACIÓN -> Derechos humanos cuya violación se analiza en la sentencia   => CORRECTAMENTE IMPLEMENTADA
					sentencia.setVotacionDerechosHumanosCuyaViolacionAnalizaSentencia(
							fillDerHumCuyaViolAnalizaSentencia(sentencia, sentenciaOld.get(0), porcentajes));
					
					//Aquí también hay duda si esta es la correcta, ya que hay varias similares
//					sentenciaOld.get(0).getViolacionOrganica() Y sentenciaOld.get(0).getViolacionesAnalizadasSentencia() //PREGUNTAR A LALO SI ESTE LO OCUPAN // PERO YA REVISÉ NO LO OCUPAN, DE IGUAL FORMA VER SI HAY CÓDIGO OBSOLETO
					List<String> violOrgAnSentencia = new ArrayList<String>();
					for(String val:sentenciaOld.get(0).getViolacionOrganica()) violOrgAnSentencia.add(val.trim().toUpperCase());
					sentencia.setViolacionOrganicaAnalizadaenSentencia(violOrgAnSentencia);
					
//					**TABLA DE VOTACIÓN -> Tipo de vicio en el proceso legislativo anaizado en la sentencia  => CORRECTAMENTE IMPLEMENTADA (APARENTEMENTE PORQUE NO HAY DATOS PARA VALIDAR)
					sentencia.setVotacionTipoVicioProcesoLegislativoAnalizadoenSentencia(
							fillVotacionTipoVicioProcesoLegislativoAnalizadoenSentencia(sentencia, sentenciaOld.get(0), porcentajes));
					
					sentencia.setTipoViolacionInvacionEsferasMateriaAnalizadaenSentencia(sentenciaOld.get(0).getTipoViolacionInvasionEsferasAnalizandoSentencia().toUpperCase());
					//Tipo de violación por invasión de esferas. => tipoViolacionInvasionEsferas
//					y Materia analizada por invasión de esferas.  => tipoViolacionInvasionEsferasInfundado
//					Comparten el mismo valor: AI 102/2017, 112/2017, 114/2017, CC 29/2018, AI 158/2017, 45/2019, 31/2019
//					NO comparten el mismo valor: AI 32/2019, AI 40/2018,  
//					CC 81/2017, además este tiene un valor que no corresponde al catálogo, quizas se esté mezclando con el campo tipoViolacion dentro de tipoVicInvEsfeAnaSenten
					//Preguntar si no se está sobre escribiendo algún campo.
					//Además están usando el mismo catálogo, solo que tienen diferentes nombres, pero son igualitos
					//Revisar por qué csentencia_materia y csentencia_tipoviolacioninvasionesferas son iguales
					
					sentencia.setMateriaAnalizadaInvasionEsferas(sentenciaOld.get(0).getTipoViolacionInvasionEsferasInfundado().trim().toUpperCase());//(Materia analizada por invasión de esferas) ESTA NO ESTÁ APARECIENDO EN EL FORMULARIO DEL FRONT - REVISAR
					
//					**TABLA DE VOTACIÓN -> Tipo de violación por invasión de esferas analizado en la sentencia:
					sentencia.setVotacionTipoViolacionInvacionEsferasAnalizadoenSentencia(
							fillVotacionTipoViolacionInvasionEsferasAnalizadoenSentencia(sentencia, sentenciaOld.get(0), porcentajes));
					
					
					sentencia.setTipoViolacionInvacionPoderesAnalizadoenSentencia(sentenciaOld.get(0).getTipoViolacionInvasionPoderesAnalizandoSentencia().toUpperCase());
					 sentencia.setMateriaAnalizadaInvasionPoderes(sentenciaOld.get(0).getTipoViolacionInvasionEsferasfundado().trim().toUpperCase()); //(Materia analizada por invasión de poderes:)  ESTA NO ESTÁ APARECIENDO EN EL FORMULARIO DEL FRONT - REVISAR
//					
					
					
//					**TABLA DE VOTACIÓN -> Tipo de violación por invasión de poderes analizado en la sentencia:
					sentencia.setVotacionTipoViolacionInvacionPoderesAnalizadoenSentencia(
							fillVotacionTipoViolacionInvasionPoderesAnalizadoenSentencia(sentencia, sentenciaOld.get(0), porcentajes));

					//*****************SEGUNDA SECCIÓN DESPUÉS DE LAS TABLAS TRANSITIVAS******************************************
					sentencia.setMomentoSurteEfectoSentencia(sentenciaOld.get(0).getMomentoSurteEfectosDeclaracionInvalidez().toUpperCase());
					sentencia.setNumeroVotacionesRealizadas(sentenciaOld.get(0).getNumeroVotacionesSentenciaPorHoja());
					sentencia.setNumeroVotacionesRegistradas(sentenciaOld.get(0).getNumeroVotacionesSentenciaAcumuladas());
					sentencia.setTramiteEngrose(sentenciaOld.get(0).getTramiteEngrose().toUpperCase());
					sentencia.setDiasTranscurridosEntreDictadoSentenciayFirmaEngrose(!sentenciaOld.get(0).getPlazoEntreDictadoYFechaFirmaEngrose().equals("")?Integer.valueOf(sentenciaOld.get(0).getPlazoEntreDictadoYFechaFirmaEngrose()):0);
					sentencia.setFechaSentencia(sentenciaOld.get(0).getFechaSentencia());
					sentencia.setFechaFirmaEngrose(sentenciaOld.get(0).getFechaFirmaEngrose());
					//*****************END SEGUNDA SECCIÓN DESPUÉS DE LAS TABLAS TRANSITIVAS******************************************
					
//					getTotalVotacion(sentencia.getVotacionCausasImprocedenciaySobreseimientoAnalizadas());
//					getTotalVotacion(sentencia.getVotacionDerechosHumanosCuyaViolacionAnalizaSentencia());
//					getTotalVotacion(sentencia.getVotacionTipoVicioProcesoLegislativoAnalizadoenSentencia());
//					getTotalVotacion(sentencia.getVotacionTipoViolacionInvacionEsferasAnalizadoenSentencia());
//					getTotalVotacion(sentencia.getVotacionTipoViolacionInvacionPoderesAnalizadoenSentencia());

					
					//SECCIÓN DE CAMPOS POR REVISAR QUÉ MÉTODO LOS LLENA
					sentencia.setTotalVotacionCausasImprocedenciaySobreseimientoAnalizadas(getTotalVotacion(sentencia.getVotacionCausasImprocedenciaySobreseimientoAnalizadas()));
					sentencia.setTotalVotacionDerechosHumanosCuyaViolacionAnalizaSentencia(getTotalVotacion(sentencia.getVotacionDerechosHumanosCuyaViolacionAnalizaSentencia()));
					sentencia.setTotalVotacionTipoVicioProcesoLegislativoAnalizadoenSentencia(getTotalVotacion(sentencia.getVotacionTipoVicioProcesoLegislativoAnalizadoenSentencia()));
					sentencia.setTotalVotacionTipoViolacionInvacionEsferasAnalizadoenSentencia(getTotalVotacion(sentencia.getVotacionTipoViolacionInvacionEsferasAnalizadoenSentencia()));
					sentencia.setTotalVotacionTipoViolacionInvacionPoderesAnalizadoenSentencia(getTotalVotacion(sentencia.getVotacionTipoViolacionInvacionPoderesAnalizadoenSentencia()));
//					

					
					double porcentaje_sentencia = porcentajeService.calculoPorcentaje(sentencia, "sentencia");
					porcentajes.add(porcentaje_sentencia);
					double sum = porcentajes.stream().mapToDouble(Double::doubleValue).sum();
					double porcentaje = sum/porcentajes.size();
					sentencia.setPorcentajeCaptura(CalculoPorcentajeUtils.round(porcentaje, 1));
					System.out.println("porcentaje: "+porcentaje);
					
					sentenciaRepository.save(sentencia);
				}
			}else {
//				System.out.println("No se encontró la sentencia by Null: "+sentencia.getTipoAsunto()+" "+sentencia.getNumeroExpediente());
				issuesSentenciaInexistente(sentencia);
			}
		}
		System.out.println("Total sentencias ingestadas: "+count);
		return null;
	}
	
	public int getTotalVotacion(List<String> idsTablasVotacion) {
		if(idsTablasVotacion.size()>0) {
			System.out.println("U here");
		}
		int totalRubros = 0;
		List<TablaVotaciones> tablasVotacionList = (List<TablaVotaciones>) _votacionRepository.findAllById(idsTablasVotacion);
		for(TablaVotaciones tablaVotacion: tablasVotacionList) {
			totalRubros+=1;//Esto por defecto una tabla de votación global corresponde a un voto
			//Esto por cada votación adicional se suma el voto
			totalRubros+=(tablaVotacion.getRubrosEfectoSentencia().size()+tablaVotacion.getRubrosExtencionInvalidez().size());
		}
		return totalRubros;
	}
	


	public ResponseDTO populateNormativas() {
		
		DiversaNormativa newNormativa;
		List<DiversaNormativaOld> normativasOld = normativaOldRepository.findAll();
		
		for(DiversaNormativaOld oldNormativa : normativasOld) {
			
			newNormativa = new DiversaNormativa();

			newNormativa.setAmbitoIncide(oldNormativa.getAmbitoIncide());
			newNormativa.setDocumentos(oldNormativa.getDocumentos());
			newNormativa.setFechaAprobacion(oldNormativa.getFechaAprobacion());
			newNormativa.setId(oldNormativa.getId());
			newNormativa.setMinistroPresidente(oldNormativa.getMinistroPresidente());
			newNormativa.setNumero(oldNormativa.getNumero());
			newNormativa.setPorcentaje(oldNormativa.getPorcentaje());
			newNormativa.setTipoCircular(oldNormativa.getTipoCircular());
			newNormativa.setTipoInstrumento(oldNormativa.getTipoInstrumento());
			newNormativa.setUrlIntranet(oldNormativa.getUrlIntranet());

			//normativaRepository.save(newNormativa);
		}

		return null;
	}
	
	public void deteccionIssuesVtaq() {
		List<Taquigraficas> versionesTaquigraficasOld = _vtaqOldRep.findAll();
		List<VersionTaquigrafica> versionesTaquigraficasNew = vTaqRepository.findAll();
		VersionTaquigrafica vtaqNew = null;
		
		System.out.println("Total VtaqOld: "+versionesTaquigraficasOld.size()+" - Total VtaqNew: "+versionesTaquigraficasNew.size());
		
		System.out.println("Asuntos cuya vtaq no cuenta con fecha de sesión:");
		List<Double> porcentajes = null;
		int totalAsuntosConTemas=0;
		for(Taquigraficas vtaqOld: versionesTaquigraficasOld) {
			if(validateDataVtaq(vtaqOld)!=null) {
				System.out.println("IdVtaq: "+vtaqOld.getId());
				for(AsuntosAbordados asunto:vtaqOld.getAsuntos()) {
					System.out.println(" "+asunto.getAsuntoAbordado()+" - "+asunto.getNumExpediente());
				}
			}
		}
	}
	
	public void deteccionIssuesSentencias() {
		List<SentenciaOld> sentenciasOldList = sentenciaOldRepository.findAll();
		List<String> sentenciasListNoDuplicate = new ArrayList<String>();
		System.out.println("Duplicadas:");
		for(SentenciaOld sentencia:sentenciasOldList) {
			if(!sentenciasListNoDuplicate.contains(sentencia.getTipoAsunto()+" "+sentencia.getNumExpedientes())) {
				sentenciasListNoDuplicate.add(sentencia.getTipoAsunto()+" "+sentencia.getNumExpedientes());
			}else {
				System.out.println("Duplicadas: "+sentencia.getTipoAsunto()+" "+sentencia.getNumExpedientes());
			}
			if(sentencia.getTipoAsunto().trim().isEmpty()||sentencia.getNumExpedientes().trim().isEmpty()) {
				System.out.println("Datos vacios: "+sentencia.getTipoAsunto()+" "+sentencia.getNumExpedientes());
			}
		}
		
	}
	
	
	public ResponseDTO contadoresSentencias() {
		long totalSentenciasOld = sentenciaOldRepository.count();
		long totalSentenciasNew = sentenciaRepository.count();
		System.out.println("totalSentenciasOld: "+totalSentenciasOld+" totalSentenciasNew"+totalSentenciasNew);
		
		List<SentenciaOld> sentenciasOldList = sentenciaOldRepository.findAll();
		List<Sentencia> sentenciasNewList = sentenciaRepository.findAll();
		
		System.out.println("*******************Sentencias viejas***************************");
		for(SentenciaOld sentencia:sentenciasOldList) {
			System.out.println(sentencia.getTipoAsunto()+" "+sentencia.getNumExpedientes());
		}
		
		System.out.println("*******************Sentencias nuevas***************************");
		for(Sentencia sentencia:sentenciasNewList) {
			System.out.println(sentencia.getTipoAsunto()+" "+sentencia.getNumeroExpediente());
		}
		
		
//		_vtaqOldRep.findAll();
//		vTaqRepository
		return null;
	}

	public ResponseDTO contadoresVtaq() {
		List<VersionTaquigrafica> treintas = vTaqRepository.findByPorcentaje(30d);
		List<VersionTaquigrafica> veintes = vTaqRepository.findByPorcentaje(20d);
		List<VersionTaquigrafica> dieces = vTaqRepository.findByPorcentaje(10d);
		List<VersionTaquigrafica> ceros = vTaqRepository.findByPorcentaje(0d);
		System.out.println("treintas: "+treintas.size());
		System.out.println("veintes: "+veintes.size());
		System.out.println("dieces: "+dieces.size());
		System.out.println("ceros: "+ceros.size());
		return null;
	}
	
	public void updateTipoAsuntoCC(String tipoAsuntoOld, String tipoAsuntoNew) {
		List<AsuntoAbordado> asuntosList = _asuntoRepository.findAll();
		for(AsuntoAbordado asunto:asuntosList) {
			if(asunto.getAsuntoAbordado().trim().equals(tipoAsuntoOld)) {
				asunto.setAsuntoAbordado(tipoAsuntoNew.trim());
				_asuntoRepository.save(asunto);
			}
		}
		
		List<Sentencia> sentenciasNewList = sentenciaRepository.findAll();
		for(Sentencia sentencia:sentenciasNewList) {
			if(sentencia.getTipoAsunto().trim().equals(tipoAsuntoOld)) {
				sentencia.setTipoAsunto(tipoAsuntoNew.trim());
				sentenciaRepository.save(sentencia);
			}
		}
	}
	
	
}