package mx.gob.scjn.ugacj.sga_captura.controller.sentencias;

import java.text.ParseException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mx.gob.scjn.ugacj.sga_captura.domain.Sentencia;
import mx.gob.scjn.ugacj.sga_captura.dto.SentenciaDTO;
import mx.gob.scjn.ugacj.sga_captura.service.sentencias.SentenciaService;
import mx.gob.scjn.ugacj.sga_captura.service.usuarios.UsuariosService;

@RestController
@RequestMapping(value = "/api/sga")
public class SentenciasController {

	@Autowired
	private SentenciaService _sentenciaService;


	@RequestMapping(method = RequestMethod.GET, path = "/sentencia")
	public Page<Sentencia> getSentenciaAll(@RequestParam(name = "page") Integer page,
			@RequestParam(name = "size") Integer size, @RequestParam(required = false, name = "filtros") String filtros)
			throws ParseException {
		Pageable paging = PageRequest.of(page - 1, size);
		return _sentenciaService.getAllSentenciasPages(paging, filtros);
	}

	@RequestMapping(method = RequestMethod.GET, path = "/sentencia/{sentenciaId}", produces = "application/json")
	public SentenciaDTO getSentenciaById(@PathVariable String sentenciaId) {
		return _sentenciaService.getSentenciaById(sentenciaId);
	}

	@RequestMapping(method = RequestMethod.PUT, path = "/sentencia")
	public Sentencia updateSentencia(@Valid @RequestBody Sentencia sentencia) {
		return _sentenciaService.updateSentencia(sentencia);
	}


	@RequestMapping(method = RequestMethod.GET, path = "/sentencia/numExpediente/{expediente}/tipoAsunto/{asunto}")
	public Sentencia getExistSentencia(@PathVariable String expediente, @PathVariable String asunto) {
		return _sentenciaService.ExistSentenciaByVersionesTaq(expediente, asunto);
	}

}
