package mx.gob.scjn.ugacj.sga_captura.service.autocompletado;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import mx.gob.scjn.ugacj.sga_captura.domain.AcuerdosGenerales;
import mx.gob.scjn.ugacj.sga_captura.domain.Voto;

@Service
public class AutocompleteService {

	@Autowired
	private MongoOperations mongoOperations;

	public List<String> findCoincidenceInVotos(String q, String field) {
		List<Voto> listVotos = null;
		Criteria regex = Criteria.where(field).regex(".*" + q + ".*", "i");
		listVotos = mongoOperations.find(new Query().addCriteria(regex), Voto.class);
		List<String> text = new ArrayList<String>();

//		for (Voto voto : listVotos) {
//			if (field.equals("temaProcesalSobreElVoto") && !text.contains(voto.getTemaProcesalSobreElVoto())) {
//				text.add(voto.getTemaProcesalSobreElVoto());
//			} else if (field.equals("temaSustantivoSobreElVoto")
//					&& !text.contains(voto.getTemaSustantivoSobreElVoto())) {
//				text.add(voto.getTemaSustantivoSobreElVoto());
//			}
//		}
		Collections.sort(text); // ordena la lista de forma ASC
		return text;
	}

	private List<String> findCoincidenceInAcuerdos(String q, String field) {
		List<AcuerdosGenerales> listAcuerdos = null;
		Criteria regex = Criteria.where(field).regex(".*" + q + ".*", "i");
		listAcuerdos = mongoOperations.find(new Query().addCriteria(regex), AcuerdosGenerales.class);
		List<String> text = new ArrayList<String>();

		for (AcuerdosGenerales acuerdo : listAcuerdos) {
			for (int i = 0; i < acuerdo.getTemasEspecificosRegulacion().size(); i++) {
				if(acuerdo.getTemasEspecificosRegulacion().get(i).contains(q)) {
					String value = acuerdo.getTemasEspecificosRegulacion().get(i);
					if (field.equals("temasEspecificosRegulacion") && !text.contains(value)) {
						text.add(value);
					}
				}
				
			}
		}
		Collections.sort(text); // ordena la lista de forma ASC
		return text;
	}

	public List<String> findCoincidence(String nombrePantalla, String q, String field) {
		List<String> coincidences = null;
		if (nombrePantalla.equals("votos")) {
			coincidences = findCoincidenceInVotos(q, field);
		} else if (nombrePantalla.equals("acuerdos")) {
			coincidences = findCoincidenceInAcuerdos(q, field);
		}
		Collections.sort(coincidences); // ordena la lista de forma ASC
		return coincidences;
	}

}
