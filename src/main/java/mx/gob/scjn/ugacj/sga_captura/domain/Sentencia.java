package mx.gob.scjn.ugacj.sga_captura.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;

@Document(collection = "#{@environment.getProperty('indice.sentencia')}")
//@JsonInclude(Include.NON_NULL)
public class Sentencia implements Serializable {

	@Id
	String id;

	@NotBlank
	String tipoAsunto;

	@NotBlank
	@Pattern(message="Formato no valido" , regexp="[0-9]{1,6}/[0-9]{1,4}")
	String numeroExpediente;

	@NotBlank
	String organoRadicacion;

	//@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
	String fechaResolucion;
	@NotBlank
	String ponente;
	String tipoPersonaPromovente;
	String tipoPersonaJuridicoColectiva;
	String tipoViolacionPlanteadaenDemanda;
	List<String> derechosHumanosCuyaViolacionPlantea = new ArrayList<>();
	List<String> derechosHumanosCuyaViolacionPlanteaByDerechosHumanos = new ArrayList<>();
	List<String> violacionOrganicaPlanteada = new ArrayList<>();
	List<String> tipoVicioProcesoLegislativo = new ArrayList<>();
	String tipoViolacionInvacionEsferas;
	String tipoViolacionInvacionPoderes;
	//tablas de votación>
	List<String> votacionCausasImprocedenciaySobreseimientoAnalizadas = new ArrayList<>();
	List<String> votacionDerechosHumanosCuyaViolacionAnalizaSentencia = new ArrayList<>();
	List<String> votacionTipoVicioProcesoLegislativoAnalizadoenSentencia = new ArrayList<>();
	List<String> votacionTipoViolacionInvacionEsferasAnalizadoenSentencia = new ArrayList<>();
	List<String> votacionTipoViolacionInvacionPoderesAnalizadoenSentencia = new ArrayList<>();
	//end tablas de votación>
	List<String> violacionOrganicaAnalizadaenSentencia = new ArrayList<>();
	String tipoViolacionInvacionEsferasMateriaAnalizadaenSentencia;
	
	String materiaAnalizadaInvasionEsferas;
	String materiaAnalizadaInvasionPoderes;
	
	String tipoViolacionInvacionPoderesAnalizadoenSentencia;
	String momentoSurteEfectoSentencia;
	int numeroVotacionesRealizadas = 0;
	int numeroVotacionesRegistradas = 0;
	String tramiteEngrose;
	int diasTranscurridosEntreDictadoSentenciayFirmaEngrose = 0;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
	Date fechaSentencia;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
	Date fechaFirmaEngrose;
	private List<String> acumulados;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private  Boolean isAcumulada;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private  Boolean isCA;
	Integer totalVotacionCausasImprocedenciaySobreseimientoAnalizadas = 0;
	Integer totalVotacionDerechosHumanosCuyaViolacionAnalizaSentencia = 0;
	Integer totalVotacionTipoVicioProcesoLegislativoAnalizadoenSentencia = 0;
	Integer totalVotacionTipoViolacionInvacionEsferasAnalizadoenSentencia = 0;
	Integer totalVotacionTipoViolacionInvacionPoderesAnalizadoenSentencia = 0;
	String origen = "";
	Double porcentajeCaptura = Double.valueOf(0);

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTipoAsunto() {
		return tipoAsunto;
	}

	public void setTipoAsunto(String tipoAsunto) {
		this.tipoAsunto = tipoAsunto;
	}

	public String getNumeroExpediente() {
		return numeroExpediente;
	}

	public void setNumeroExpediente(String numeroExpediente) {
		this.numeroExpediente = numeroExpediente;
	}

	public String getOrganoRadicacion() {
		return organoRadicacion;
	}

	public void setOrganoRadicacion(String organoRadicacion) {
		this.organoRadicacion = organoRadicacion;
	}

	public String getFechaResolucion() {
		return fechaResolucion;
	}

	public void setFechaResolucion(String fechaResolucion) {
		this.fechaResolucion = fechaResolucion;
	}

	public String getPonente() {
		return ponente;
	}

	public void setPonente(String ponente) {
		this.ponente = ponente;
	}

	public String getTipoPersonaPromovente() {
		return tipoPersonaPromovente;
	}

	public void setTipoPersonaPromovente(String tipoPersonaPromovente) {
		this.tipoPersonaPromovente = tipoPersonaPromovente;
	}

	public String getTipoPersonaJuridicoColectiva() {
		return tipoPersonaJuridicoColectiva;
	}

	public void setTipoPersonaJuridicoColectiva(String tipoPersonaJuridicoColectiva) {
		this.tipoPersonaJuridicoColectiva = tipoPersonaJuridicoColectiva;
	}

	public String getTipoViolacionPlanteadaenDemanda() {
		return tipoViolacionPlanteadaenDemanda;
	}

	public void setTipoViolacionPlanteadaenDemanda(String tipoViolacionPlanteadaenDemanda) {
		this.tipoViolacionPlanteadaenDemanda = tipoViolacionPlanteadaenDemanda;
	}

	public List<String> getDerechosHumanosCuyaViolacionPlantea() {
		return derechosHumanosCuyaViolacionPlantea;
	}

	public void setDerechosHumanosCuyaViolacionPlantea(List<String> derechosHumanosCuyaViolacionPlantea) {
		this.derechosHumanosCuyaViolacionPlantea = derechosHumanosCuyaViolacionPlantea;
	}

	public List<String> getViolacionOrganicaPlanteada() {
		return violacionOrganicaPlanteada;
	}

	public void setViolacionOrganicaPlanteada(List<String> violacionOrganicaPlanteada) {
		this.violacionOrganicaPlanteada = violacionOrganicaPlanteada;
	}

	public List<String> getTipoVicioProcesoLegislativo() {
		return tipoVicioProcesoLegislativo;
	}

	public void setTipoVicioProcesoLegislativo(List<String> tipoVicioProcesoLegislativo) {
		this.tipoVicioProcesoLegislativo = tipoVicioProcesoLegislativo;
	}

	public String getTipoViolacionInvacionEsferas() {
		return tipoViolacionInvacionEsferas;
	}

	public void setTipoViolacionInvacionEsferas(String tipoViolacionInvacionEsferas) {
		this.tipoViolacionInvacionEsferas = tipoViolacionInvacionEsferas;
	}

	public String getTipoViolacionInvacionPoderes() {
		return tipoViolacionInvacionPoderes;
	}

	public void setTipoViolacionInvacionPoderes(String tipoViolacionInvacionPoderes) {
		this.tipoViolacionInvacionPoderes = tipoViolacionInvacionPoderes;
	}

	public List<String> getVotacionCausasImprocedenciaySobreseimientoAnalizadas() {
		return votacionCausasImprocedenciaySobreseimientoAnalizadas;
	}

	public void setVotacionCausasImprocedenciaySobreseimientoAnalizadas(List<String> votacionCausasImprocedenciaySobreseimientoAnalizadas) {
		this.votacionCausasImprocedenciaySobreseimientoAnalizadas = votacionCausasImprocedenciaySobreseimientoAnalizadas;
	}

	public List<String> getVotacionDerechosHumanosCuyaViolacionAnalizaSentencia() {
		return votacionDerechosHumanosCuyaViolacionAnalizaSentencia;
	}

	public void setVotacionDerechosHumanosCuyaViolacionAnalizaSentencia(List<String> votacionDerechosHumanosCuyaViolacionAnalizaSentencia) {
		this.votacionDerechosHumanosCuyaViolacionAnalizaSentencia = votacionDerechosHumanosCuyaViolacionAnalizaSentencia;
	}

	public List<String> getVotacionTipoVicioProcesoLegislativoAnalizadoenSentencia() {
		return votacionTipoVicioProcesoLegislativoAnalizadoenSentencia;
	}

	public void setVotacionTipoVicioProcesoLegislativoAnalizadoenSentencia(List<String> votacionTipoVicioProcesoLegislativoAnalizadoenSentencia) {
		this.votacionTipoVicioProcesoLegislativoAnalizadoenSentencia = votacionTipoVicioProcesoLegislativoAnalizadoenSentencia;
	}

	public List<String> getVotacionTipoViolacionInvacionEsferasAnalizadoenSentencia() {
		return votacionTipoViolacionInvacionEsferasAnalizadoenSentencia;
	}

	public void setVotacionTipoViolacionInvacionEsferasAnalizadoenSentencia(List<String> votacionTipoViolacionInvacionEsferasAnalizadoenSentencia) {
		this.votacionTipoViolacionInvacionEsferasAnalizadoenSentencia = votacionTipoViolacionInvacionEsferasAnalizadoenSentencia;
	}

	public List<String> getVotacionTipoViolacionInvacionPoderesAnalizadoenSentencia() {
		return votacionTipoViolacionInvacionPoderesAnalizadoenSentencia;
	}

	public void setVotacionTipoViolacionInvacionPoderesAnalizadoenSentencia(List<String> votacionTipoViolacionInvacionPoderesAnalizadoenSentencia) {
		this.votacionTipoViolacionInvacionPoderesAnalizadoenSentencia = votacionTipoViolacionInvacionPoderesAnalizadoenSentencia;
	}

	public List<String> getViolacionOrganicaAnalizadaenSentencia() {
		return violacionOrganicaAnalizadaenSentencia;
	}

	public void setViolacionOrganicaAnalizadaenSentencia(List<String> violacionOrganicaAnalizadaenSentencia) {
		this.violacionOrganicaAnalizadaenSentencia = violacionOrganicaAnalizadaenSentencia;
	}

	public String getTipoViolacionInvacionEsferasMateriaAnalizadaenSentencia() {
		return tipoViolacionInvacionEsferasMateriaAnalizadaenSentencia;
	}

	public void setTipoViolacionInvacionEsferasMateriaAnalizadaenSentencia(String tipoViolacionInvacionEsferasMateriaAnalizadaenSentencia) {
		this.tipoViolacionInvacionEsferasMateriaAnalizadaenSentencia = tipoViolacionInvacionEsferasMateriaAnalizadaenSentencia;
	}

	public String getTipoViolacionInvacionPoderesAnalizadoenSentencia() {
		return tipoViolacionInvacionPoderesAnalizadoenSentencia;
	}

	public void setTipoViolacionInvacionPoderesAnalizadoenSentencia(String tipoViolacionInvacionPoderesAnalizadoenSentencia) {
		this.tipoViolacionInvacionPoderesAnalizadoenSentencia = tipoViolacionInvacionPoderesAnalizadoenSentencia;
	}

	public String getMomentoSurteEfectoSentencia() {
		return momentoSurteEfectoSentencia;
	}

	public void setMomentoSurteEfectoSentencia(String momentoSurteEfectoSentencia) {
		this.momentoSurteEfectoSentencia = momentoSurteEfectoSentencia;
	}

	public int getNumeroVotacionesRealizadas() {
		return numeroVotacionesRealizadas;
	}

	public void setNumeroVotacionesRealizadas(int numeroVotacionesRealizadas) {
		this.numeroVotacionesRealizadas = numeroVotacionesRealizadas;
	}

	public int getNumeroVotacionesRegistradas() {
		return numeroVotacionesRegistradas;
	}

	public void setNumeroVotacionesRegistradas(int numeroVotacionesRegistradas) {
		this.numeroVotacionesRegistradas = numeroVotacionesRegistradas;
	}

	public String getTramiteEngrose() {
		return tramiteEngrose;
	}

	public void setTramiteEngrose(String tramiteEngrose) {
		this.tramiteEngrose = tramiteEngrose;
	}

	
	public int getDiasTranscurridosEntreDictadoSentenciayFirmaEngrose() {
		return diasTranscurridosEntreDictadoSentenciayFirmaEngrose;
	}

	public void setDiasTranscurridosEntreDictadoSentenciayFirmaEngrose(
			int diasTranscurridosEntreDictadoSentenciayFirmaEngrose) {
		this.diasTranscurridosEntreDictadoSentenciayFirmaEngrose = diasTranscurridosEntreDictadoSentenciayFirmaEngrose;
	}

	public Boolean getIsAcumulada() {
		return isAcumulada;
	}

	public void setIsAcumulada(Boolean isAcumulada) {
		this.isAcumulada = isAcumulada;
	}

	public Boolean getIsCA() {
		return isCA;
	}

	public void setIsCA(Boolean isCA) {
		this.isCA = isCA;
	}

	public Date getFechaSentencia() {
		return fechaSentencia;
	}

	public void setFechaSentencia(Date fechaSentencia) {
		this.fechaSentencia = fechaSentencia;
	}

	public Date getFechaFirmaEngrose() {
		return fechaFirmaEngrose;
	}

	public void setFechaFirmaEngrose(Date fechaFirmaEngrose) {
		this.fechaFirmaEngrose = fechaFirmaEngrose;
	}

	public List<String> getAcumulados() {
		return acumulados;
	}

	public void setAcumulados(List<String> acumulados) {
		this.acumulados = acumulados;
	}

	public Boolean getAcumulada() {
		return isAcumulada;
	}

	public void setAcumulada(Boolean acumulada) {
		isAcumulada = acumulada;
	}

	public Boolean getCA() {
		return isCA;
	}

	public void setCA(Boolean CA) {
		isCA = CA;
	}

	public Integer getTotalVotacionCausasImprocedenciaySobreseimientoAnalizadas() {
		return totalVotacionCausasImprocedenciaySobreseimientoAnalizadas;
	}

	public void setTotalVotacionCausasImprocedenciaySobreseimientoAnalizadas(Integer totalVotacionCausasImprocedenciaySobreseimientoAnalizadas) {
		this.totalVotacionCausasImprocedenciaySobreseimientoAnalizadas = totalVotacionCausasImprocedenciaySobreseimientoAnalizadas;
	}

	public Integer getTotalVotacionDerechosHumanosCuyaViolacionAnalizaSentencia() {
		return totalVotacionDerechosHumanosCuyaViolacionAnalizaSentencia;
	}

	public void setTotalVotacionDerechosHumanosCuyaViolacionAnalizaSentencia(Integer totalVotacionDerechosHumanosCuyaViolacionAnalizaSentencia) {
		this.totalVotacionDerechosHumanosCuyaViolacionAnalizaSentencia = totalVotacionDerechosHumanosCuyaViolacionAnalizaSentencia;
	}

	public Integer getTotalVotacionTipoVicioProcesoLegislativoAnalizadoenSentencia() {
		return totalVotacionTipoVicioProcesoLegislativoAnalizadoenSentencia;
	}

	public void setTotalVotacionTipoVicioProcesoLegislativoAnalizadoenSentencia(Integer totalVotacionTipoVicioProcesoLegislativoAnalizadoenSentencia) {
		this.totalVotacionTipoVicioProcesoLegislativoAnalizadoenSentencia = totalVotacionTipoVicioProcesoLegislativoAnalizadoenSentencia;
	}

	public Integer getTotalVotacionTipoViolacionInvacionEsferasAnalizadoenSentencia() {
		return totalVotacionTipoViolacionInvacionEsferasAnalizadoenSentencia;
	}

	public void setTotalVotacionTipoViolacionInvacionEsferasAnalizadoenSentencia(Integer totalVotacionTipoViolacionInvacionEsferasAnalizadoenSentencia) {
		this.totalVotacionTipoViolacionInvacionEsferasAnalizadoenSentencia = totalVotacionTipoViolacionInvacionEsferasAnalizadoenSentencia;
	}

	public Integer getTotalVotacionTipoViolacionInvacionPoderesAnalizadoenSentencia() {
		return totalVotacionTipoViolacionInvacionPoderesAnalizadoenSentencia;
	}

	public void setTotalVotacionTipoViolacionInvacionPoderesAnalizadoenSentencia(Integer totalVotacionTipoViolacionInvacionPoderesAnalizadoenSentencia) {
		this.totalVotacionTipoViolacionInvacionPoderesAnalizadoenSentencia = totalVotacionTipoViolacionInvacionPoderesAnalizadoenSentencia;
	}

	public List<String> getDerechosHumanosCuyaViolacionPlanteaByDerechosHumanos() {
		return derechosHumanosCuyaViolacionPlanteaByDerechosHumanos;
	}

	public void setDerechosHumanosCuyaViolacionPlanteaByDerechosHumanos(List<String> derechosHumanosCuyaViolacionPlanteaByDerechosHumanos) {
		this.derechosHumanosCuyaViolacionPlanteaByDerechosHumanos = derechosHumanosCuyaViolacionPlanteaByDerechosHumanos;
	}

	public String getOrigen() {
		return origen;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	public Double getPorcentajeCaptura() {
		return porcentajeCaptura;
	}

	public void setPorcentajeCaptura(Double porcentajeCaptura) {
		this.porcentajeCaptura = porcentajeCaptura;
	}
	
	

	public String getMateriaAnalizadaInvasionEsferas() {
		return materiaAnalizadaInvasionEsferas;
	}

	public void setMateriaAnalizadaInvasionEsferas(String materiaAnalizadaInvasionEsferas) {
		this.materiaAnalizadaInvasionEsferas = materiaAnalizadaInvasionEsferas;
	}
	
	

	public String getMateriaAnalizadaInvasionPoderes() {
		return materiaAnalizadaInvasionPoderes;
	}

	public void setMateriaAnalizadaInvasionPoderes(String materiaAnalizadaInvasionPoderes) {
		this.materiaAnalizadaInvasionPoderes = materiaAnalizadaInvasionPoderes;
	}

	@Override
	public String toString() {
		return "Sentencia{" +
				"id='" + id + '\'' +
				", tipoAsunto='" + tipoAsunto + '\'' +
				", numeroExpediente='" + numeroExpediente + '\'' +
				", organoRadicacion='" + organoRadicacion + '\'' +
				", fechaResolucion='" + fechaResolucion + '\'' +
				", ponente='" + ponente + '\'' +
				", tipoPersonaPromovente='" + tipoPersonaPromovente + '\'' +
				", tipoPersonaJuridicoColectiva='" + tipoPersonaJuridicoColectiva + '\'' +
				", tipoViolacionPlanteadaenDemanda='" + tipoViolacionPlanteadaenDemanda + '\'' +
				", derechosHumanosCuyaViolacionPlantea=" + derechosHumanosCuyaViolacionPlantea +
				", derechosHumanosCuyaViolacionPlanteaByDerechosHumanos=" + derechosHumanosCuyaViolacionPlanteaByDerechosHumanos +
				", violacionOrganicaPlanteada=" + violacionOrganicaPlanteada +
				", tipoVicioProcesoLegislativo=" + tipoVicioProcesoLegislativo +
				", tipoViolacionInvacionEsferas='" + tipoViolacionInvacionEsferas + '\'' +
				", tipoViolacionInvacionPoderes='" + tipoViolacionInvacionPoderes + '\'' +
				", votacionCausasImprocedenciaySobreseimientoAnalizadas=" + votacionCausasImprocedenciaySobreseimientoAnalizadas +
				", votacionDerechosHumanosCuyaViolacionAnalizaSentencia=" + votacionDerechosHumanosCuyaViolacionAnalizaSentencia +
				", votacionTipoVicioProcesoLegislativoAnalizadoenSentencia=" + votacionTipoVicioProcesoLegislativoAnalizadoenSentencia +
				", votacionTipoViolacionInvacionEsferasAnalizadoenSentencia=" + votacionTipoViolacionInvacionEsferasAnalizadoenSentencia +
				", votacionTipoViolacionInvacionPoderesAnalizadoenSentencia=" + votacionTipoViolacionInvacionPoderesAnalizadoenSentencia +
				", violacionOrganicaAnalizadaenSentencia=" + violacionOrganicaAnalizadaenSentencia +
				", tipoViolacionInvacionEsferasMateriaAnalizadaenSentencia='" + tipoViolacionInvacionEsferasMateriaAnalizadaenSentencia + '\'' +
				", materiaAnalizadaInvasionEsferas='" + materiaAnalizadaInvasionEsferas + '\'' +
				", materiaAnalizadaInvasionPoderes='" + materiaAnalizadaInvasionPoderes + '\'' +
				", tipoViolacionInvacionPoderesAnalizadoenSentencia='" + tipoViolacionInvacionPoderesAnalizadoenSentencia + '\'' +
				", momentoSurteEfectoSentencia='" + momentoSurteEfectoSentencia + '\'' +
				", numeroVotacionesRealizadas=" + numeroVotacionesRealizadas +
				", numeroVotacionesRegistradas=" + numeroVotacionesRegistradas +
				", tramiteEngrose='" + tramiteEngrose + '\'' +
				", diasTranscurridosEntreDictadoSentenciayFirmaEngrose=" + diasTranscurridosEntreDictadoSentenciayFirmaEngrose +
				", fechaSentencia=" + fechaSentencia +
				", fechaFirmaEngrose=" + fechaFirmaEngrose +
				", acumulados=" + acumulados +
				", isAcumulada=" + isAcumulada +
				", isCA=" + isCA +
				", totalVotacionCausasImprocedenciaySobreseimientoAnalizadas=" + totalVotacionCausasImprocedenciaySobreseimientoAnalizadas +
				", totalVotacionDerechosHumanosCuyaViolacionAnalizaSentencia=" + totalVotacionDerechosHumanosCuyaViolacionAnalizaSentencia +
				", totalVotacionTipoVicioProcesoLegislativoAnalizadoenSentencia=" + totalVotacionTipoVicioProcesoLegislativoAnalizadoenSentencia +
				", totalVotacionTipoViolacionInvacionEsferasAnalizadoenSentencia=" + totalVotacionTipoViolacionInvacionEsferasAnalizadoenSentencia +
				", totalVotacionTipoViolacionInvacionPoderesAnalizadoenSentencia=" + totalVotacionTipoViolacionInvacionPoderesAnalizadoenSentencia +
				", origen='" + origen + '\'' +
				", porcentajeCaptura=" + porcentajeCaptura +
				'}';
	}

	/**
     * @return Boolean return the isAcumulada
     */
    public Boolean isIsAcumulada() {
        return isAcumulada;
    }

    /**
     * @return Boolean return the isCA
     */
    public Boolean isIsCA() {
        return isCA;
    }

}
