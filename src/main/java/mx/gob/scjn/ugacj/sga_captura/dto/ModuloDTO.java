package mx.gob.scjn.ugacj.sga_captura.dto;

import java.util.ArrayList;
import java.util.List;

public class ModuloDTO {
	String modulo;
	PermisosDTO permisos;
	List<String> camposRestricted = new ArrayList<String>();
	List<CatalogosRestrictedDTO> catalogosRestricted = new ArrayList<CatalogosRestrictedDTO>();
	List<AsuntoDTO> asuntos = new ArrayList<AsuntoDTO>();
	
	public String getModulo() {
		return modulo;
	}
	public void setModulo(String modulo) {
		this.modulo = modulo;
	}
	public PermisosDTO getPermisos() {
		return permisos;
	}
	public void setPermisos(PermisosDTO permisos) {
		this.permisos = permisos;
	}
	public List<String> getCamposRestricted() {
		return camposRestricted;
	}
	public void setCamposRestricted(List<String> camposRestricted) {
		this.camposRestricted = camposRestricted;
	}
	public List<CatalogosRestrictedDTO> getCatalogosRestricted() {
		return catalogosRestricted;
	}
	public void setCatalogosRestricted(List<CatalogosRestrictedDTO> catalogosRestricted) {
		this.catalogosRestricted = catalogosRestricted;
	}
	public List<AsuntoDTO> getAsuntos() {
		return asuntos;
	}
	public void setAsuntos(List<AsuntoDTO> asuntos) {
		this.asuntos = asuntos;
	}
	
	
}
