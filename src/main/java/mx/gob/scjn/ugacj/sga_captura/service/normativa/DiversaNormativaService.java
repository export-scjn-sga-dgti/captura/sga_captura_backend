package mx.gob.scjn.ugacj.sga_captura.service.normativa;

import java.text.ParseException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import mx.gob.scjn.ugacj.sga_captura.domain.DiversaNormativa;
import mx.gob.scjn.ugacj.sga_captura.domain.MateriaRegulacion;
import mx.gob.scjn.ugacj.sga_captura.dto.normativa.DiversaNormativaDTO;
import mx.gob.scjn.ugacj.sga_captura.dto.statecode.DivNormativaResponseDTO;
import mx.gob.scjn.ugacj.sga_captura.repository.DiversaNormativaRepository;
import mx.gob.scjn.ugacj.sga_captura.repository.MateriaRegulacionRepository;
import mx.gob.scjn.ugacj.sga_captura.service.bitacora.BitacoraService;
import mx.gob.scjn.ugacj.sga_captura.service.porcentaje.PorcentajeService;
import mx.gob.scjn.ugacj.sga_captura.utils.StatusBitacora;

@Service
public class DiversaNormativaService {

	@Autowired
	private DiversaNormativaRepository _dnRepository;

	@Autowired
	private MateriaRegulacionRepository _mrRepository;

	@Autowired
	private MongoOperations mongoOperations;
	
	@Autowired
	private BitacoraService _bitacoraService;
	
	@Autowired
	private PorcentajeService porcentajeService;

	public Page<DiversaNormativa> getNormativaAllPages(Pageable paging, String filtros) throws ParseException {
		long count = 0;
		Page<DiversaNormativa> normativaPaginable = null;
		List<DiversaNormativa> normativa = null;
		Page<DiversaNormativa> normativaPaginableResponse = null;
		Pageable paging2 = PageRequest.of(paging.getPageNumber(), paging.getPageSize());
		if (filtros != null) {
			Query query = new Query().with(paging);
			Query queryNoPage = new Query();
			String subs[] = filtros.split(",");
			for (String sub : subs) {
				String sTempFiltros[] = sub.split(":");
				String filtro = sTempFiltros[0].trim();
				String filtroValores = sTempFiltros[1].trim();

				query.addCriteria(Criteria.where(filtro).is(filtroValores));
				queryNoPage.addCriteria(Criteria.where(filtro).is(filtroValores));

			}
			List<DiversaNormativa> listNormativa = mongoOperations.find(query, DiversaNormativa.class);
			count = mongoOperations.count(queryNoPage, DiversaNormativa.class);
			normativaPaginable = new PageImpl<DiversaNormativa>(listNormativa, paging2, count);
			normativa = normativaPaginable.getContent();
		} else {
			normativaPaginable = _dnRepository.findAll(paging);
			normativa = normativaPaginable.getContent();
			count = normativaPaginable.getTotalElements();
		}
		normativaPaginableResponse = new PageImpl<DiversaNormativa>(normativa, paging2, count);
		return normativaPaginableResponse;
	}
	

	public DivNormativaResponseDTO saveDiversaNormativa(@Valid DiversaNormativa diversaN) {
		// TODO Auto-generated method stub
		DivNormativaResponseDTO respuesta = null;
		if (diversaN.getFechaAprobacion() != null || diversaN.getFechaAprobacion().equals("")) {
			porcentajeService.updatePorcentaje(diversaN);
			DiversaNormativa diversaNnew = _dnRepository.save(diversaN);
			_bitacoraService.saveOperacion(StatusBitacora._DIVERSA, StatusBitacora._SAVE, diversaNnew, null);
			respuesta = new DivNormativaResponseDTO(HttpStatus.OK.value(), "Diversa Normativa creada");
			respuesta.setDiversaNormativa(diversaNnew);
			return respuesta;
		}
		respuesta = new DivNormativaResponseDTO(HttpStatus.BAD_REQUEST.value(),
				"No fue posible crear la Diversa Normativa");
		respuesta.setDiversaNormativa(diversaN);
		return respuesta;
	}

	public DivNormativaResponseDTO updateDiversaNormativa(@Valid DiversaNormativa diversaN) {
		DivNormativaResponseDTO respuesta = null;
		Optional<DiversaNormativa> diversaExist = _dnRepository.findById(diversaN.getId());
		if (diversaExist.isPresent()) {
			porcentajeService.updatePorcentaje(diversaN);
			_dnRepository.save(diversaN);
			_bitacoraService.saveOperacion(StatusBitacora._DIVERSA, StatusBitacora._UPDATE, diversaN, diversaExist.get());
			respuesta = new DivNormativaResponseDTO(HttpStatus.OK.value(), "Diversa Normativa actualizada");
			respuesta.setDiversaNormativa(diversaN);
			return respuesta;
		}
		respuesta = new DivNormativaResponseDTO(HttpStatus.BAD_REQUEST.value(),
				"No fue posible actualizar la Diversa Normativa");
		respuesta.setDiversaNormativa(diversaN);
		return respuesta;
	}

	public boolean deleteDiversaNormativa(String id) {
		DiversaNormativaDTO diversaDTO = new DiversaNormativaDTO();
		Optional<DiversaNormativa> diversaExist = _dnRepository.findById(id);
		if (diversaExist.isPresent()) {
			List<MateriaRegulacion> materiaLista = _mrRepository.findByIdDiversaNormativa(id);
			diversaDTO.setDiversaNormativa(diversaExist.get());
			diversaDTO.setMaterias(materiaLista);
			_bitacoraService.saveOperacion(StatusBitacora._DIVERSA, StatusBitacora._DELETE, diversaDTO,  null);
			_mrRepository.deleteAll(materiaLista);
			_dnRepository.deleteById(id);
			return true;
		}
		// TODO Auto-generated method stub
		return false;
	}

	public DiversaNormativaDTO getDiversaNormativa(String id) {
		DiversaNormativaDTO diversaNormativa = new DiversaNormativaDTO();
		Optional<DiversaNormativa> dNormativa = _dnRepository.findById(id);
		if (dNormativa.isPresent()) {
			List<MateriaRegulacion> materiaLista = _mrRepository.findByIdDiversaNormativa(id);
			diversaNormativa.setDiversaNormativa(dNormativa.get());
			diversaNormativa.setMaterias(materiaLista);
			return diversaNormativa;
		}
		// TODO Auto-generated method stub
		return null;
	}

}
