package mx.gob.scjn.ugacj.sga_captura.domain;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import mx.gob.scjn.ugacj.sga_captura.dto.ParticipacionDTO;

@Document(collection = "#{@environment.getProperty('indice.vtaquigrafica.vtaqasunto')}")
@JsonInclude(Include.NON_NULL)
public class VTaquigraficaAsuntos {
	@Id
	String id;
	String idVtaquigrafica;
	String idAsunto;
	String ponente;
	List<String> temasProcesales;
	List<String> temasFondo;
//	List<String> nombresMinistrosParticiparonEnAsunto;
	List<ParticipacionDTO> numeroMinistrosParticiparonEnAsunto;
	List<String> acumulados;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIdVtaquigrafica() {
		return idVtaquigrafica;
	}
	public void setIdVtaquigrafica(String idVtaquigrafica) {
		this.idVtaquigrafica = idVtaquigrafica;
	}
	public String getIdAsunto() {
		return idAsunto;
	}
	public void setIdAsunto(String idAsunto) {
		this.idAsunto = idAsunto;
	}
	public String getPonente() {
		return ponente.toUpperCase();
	}
	public void setPonente(String ponente) {
		this.ponente = ponente;
	}
	public List<String> getTemasProcesales() {
		return temasProcesales;
	}
	public void setTemasProcesales(List<String> temasProcesales) {
		this.temasProcesales = temasProcesales;
	}
	public List<String> getTemasFondo() {
		return temasFondo;
	}
	public void setTemasFondo(List<String> temasFondo) {
		this.temasFondo = temasFondo;
	}
//	public List<String> getNombresMinistrosParticiparonEnAsunto() {
//		nombresMinistrosParticiparonEnAsunto.replaceAll(String::toUpperCase);
//		return nombresMinistrosParticiparonEnAsunto;
//	}
//	public void setNombresMinistrosParticiparonEnAsunto(List<String> nombresMinistrosParticiparonEnAsunto) {
//		this.nombresMinistrosParticiparonEnAsunto = nombresMinistrosParticiparonEnAsunto;
//	}
	public List<ParticipacionDTO> getNumeroMinistrosParticiparonEnAsunto() {
		return numeroMinistrosParticiparonEnAsunto;
	}
	public void setNumeroMinistrosParticiparonEnAsunto(List<ParticipacionDTO> numeroMinistrosParticiparonEnAsunto) {
		this.numeroMinistrosParticiparonEnAsunto = numeroMinistrosParticiparonEnAsunto;
	}
	public List<String> getAcumulados() {
		return acumulados;
	}
	public void setAcumulados(List<String> acumulados) {
		this.acumulados = acumulados;
	}
}
