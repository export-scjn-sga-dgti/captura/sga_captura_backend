package mx.gob.scjn.ugacj.sga_captura.domain;

import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;


@Document(collection = "diversaNormativa")

@JsonInclude(Include.NON_NULL)
public class DiversaNormativaOld {
	@Id
	String id;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
	Date fechaAprobacion;
	
	String ambitoIncide;
	String tipoInstrumento;
	String tipoCircular;
	String ministroPresidente;
	String urlIntranet;
	String numero;
	List<String> documentos;
	float porcentaje;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Date getFechaAprobacion() {
		return fechaAprobacion;
	}
	public void setFechaAprobacion(Date fechaAprobacion) {
		this.fechaAprobacion = fechaAprobacion;
	}
	public String getAmbitoIncide() {
		return ambitoIncide;
	}
	public void setAmbitoIncide(String ambitoIncide) {
		this.ambitoIncide = ambitoIncide;
	}
	public String getTipoInstrumento() {
		return tipoInstrumento;
	}
	public void setTipoInstrumento(String tipoInstrumento) {
		this.tipoInstrumento = tipoInstrumento;
	}
	public String getTipoCircular() {
		return tipoCircular;
	}
	public void setTipoCircular(String tipoCircular) {
		this.tipoCircular = tipoCircular;
	}
	public String getMinistroPresidente() {
		return ministroPresidente;
	}
	public void setMinistroPresidente(String ministroPresidente) {
		this.ministroPresidente = ministroPresidente;
	}
	public String getUrlIntranet() {
		return urlIntranet;
	}
	public void setUrlIntranet(String urlIntranet) {
		this.urlIntranet = urlIntranet;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public List<String> getDocumentos() {
		return documentos;
	}
	public void setDocumentos(List<String> documentos) {
		this.documentos = documentos;
	}
	public float getPorcentaje() {
		return porcentaje;
	}
	public void setPorcentaje(float porcentaje) {
		this.porcentaje = porcentaje;
	}
	
}
