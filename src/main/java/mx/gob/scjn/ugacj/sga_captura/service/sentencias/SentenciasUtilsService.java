package mx.gob.scjn.ugacj.sga_captura.service.sentencias;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import mx.gob.scjn.ugacj.sga_captura.dto.sentencias.utils.ActoReclamadoDTO;
import mx.gob.scjn.ugacj.sga_captura.dto.sentencias.utils.LoginActoDTO;
import mx.gob.scjn.ugacj.sga_captura.dto.sentencias.utils.OrigenDocumentoDTO;
import mx.gob.scjn.ugacj.sga_captura.restclient.RestClients;

@Service
public class SentenciasUtilsService {

	String urlHojaVotacion = "http://bovsij01.scjn.pjf.gob.mx/SvcActoReclamado/Api/listadoHojaVotacion";
	String urlActo = "http://scsij04.scjn.pjf.gob.mx/SvcActoReclamado/Api/ActoReclamado";
	String urlToken = "http://scsij04.scjn.pjf.gob.mx/SvcActoReclamado/Api/login/Authenticate";
	String descargaDocumento="http://bovsij01.scjn.pjf.gob.mx/SvcActoReclamado/Api/GetDocumento";

	public String obtenerToken() {
		RestTemplate plantilla = new RestTemplate();
		LoginActoDTO login = new LoginActoDTO();
		login.setUserName("usrActoReclamado");
		login.setPassword("4ctoReclam4do");
		String accessToken = plantilla.postForObject(urlToken, login, String.class);

		return accessToken;
	}

	public String getActoReclamado(ActoReclamadoDTO act) {

		RestTemplate plantilla = new RestTemplate();
		String token = obtenerToken();

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		headers.set("Authorization", "Bearer " + token.replace("\"", ""));

		HttpEntity<ActoReclamadoDTO> entity = new HttpEntity<>(act, headers);

		ResponseEntity<String> datos = plantilla.exchange(urlActo, HttpMethod.POST, entity, String.class);

		String response = String.valueOf(datos.getBody());
		response = response.replaceAll("^\"|\"$", "");
		return datos.getBody();
//		return ""+response+"";
	}
	
	public StringBuffer replaceAll(String templateText, Pattern pattern,
            Function<Matcher, String> replacer) {
Matcher matcher = pattern.matcher(templateText);
StringBuffer result = new StringBuffer();
while (matcher.find()) {
matcher.appendReplacement(result, replacer.apply(matcher));
}
matcher.appendTail(result);
return result;
}

	public OrigenDocumentoDTO[] getHojasVotacion(ActoReclamadoDTO act) {

		RestTemplate plantilla = new RestTemplate();
		String token = obtenerToken();

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		headers.set("Authorization", "Bearer " + token.replace("\"", ""));

		HttpEntity<ActoReclamadoDTO> entity = new HttpEntity<>(act, headers);

		ResponseEntity<String> datos = plantilla.exchange(urlHojaVotacion, HttpMethod.POST, entity, String.class);
		Gson json = new Gson();
		String docs = datos.getBody();

		OrigenDocumentoDTO[] o = json.fromJson(docs, OrigenDocumentoDTO[].class);
		return o;

	}

	public ResponseEntity<?> getDocumento(OrigenDocumentoDTO origenD, String name) {
		String token = obtenerToken();
		Gson gson = new Gson();
		RestClients mRC = new RestClients();
		InputStream inputStream = mRC.callToServicePost2(descargaDocumento, gson.toJson(origenD),
				token.replace("\"", ""));
		InputStreamResource resource = new InputStreamResource(inputStream);
		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-type", "application/pdf");
		headers.set("Content-Disposition", "attachment; filename=\"" + name + ".pdf\""); // to view in browser change
																							// attachment to inline
		int size;
		try {
			size = inputStream.available();
			return ResponseEntity.ok().headers(headers).contentLength(size)
					.contentType(MediaType.parseMediaType("application/octet-stream")).body(resource);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
