package mx.gob.scjn.ugacj.sga_captura.dto.migracion;

import java.util.List;

public class UsuarioCatalogoOldDTO {

	String nombre;
    List<String> valor;
    
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public List<String> getValor() {
		return valor;
	}
	public void setValor(List<String> valor) {
		this.valor = valor;
	}
    

}
