package mx.gob.scjn.ugacj.sga_captura.utils.validaciones.RubrosVotaciones;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = { RubrosVotacionesValidators.class})
@Target( {ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidatorsRubrosConstrains {

    String message() default "Se debe tener al menos 1 voto ya sea en el campo votosAFavor o votosEnContra.";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

}
