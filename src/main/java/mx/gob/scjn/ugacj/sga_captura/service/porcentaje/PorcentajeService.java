package mx.gob.scjn.ugacj.sga_captura.service.porcentaje;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import mx.gob.scjn.ugacj.sga_captura.domain.AcuerdosGenerales;
import mx.gob.scjn.ugacj.sga_captura.domain.DiversaNormativa;
import mx.gob.scjn.ugacj.sga_captura.domain.Sentencia;
import mx.gob.scjn.ugacj.sga_captura.domain.TablaVotaciones;
import mx.gob.scjn.ugacj.sga_captura.domain.Voto;
import mx.gob.scjn.ugacj.sga_captura.domain.porcentaje.Porcentaje;
import mx.gob.scjn.ugacj.sga_captura.dto.porcentaje.FieldValidationDTO;
import mx.gob.scjn.ugacj.sga_captura.repository.VotacionRepository;
import mx.gob.scjn.ugacj.sga_captura.repository.porcentaje.PorcentajeRepository;
import mx.gob.scjn.ugacj.sga_captura.utils.CalculoPorcentajeUtils;


@Service
public class PorcentajeService {
	
	@Autowired
	private PorcentajeRepository porcentajeRepository;
	
	 @Autowired
	 private VotacionRepository _votacionRepository;
	
	public double getPorcentaje(Field field, Object objClass, FieldValidationDTO fieldSntcData) {
		double porcentageTotal = 0d;
		String value = getFieldValue(field, objClass);
		if (fieldSntcData.getCriterio().toUpperCase().equals("NotEmpty".toUpperCase()) && value != null) {
			return fieldSntcData.getValue();
		} else if (fieldSntcData.getCriterio().toUpperCase().equals("optionEmbebed-container".toUpperCase()) && value != null) {
			double valc = 0;
			for (FieldValidationDTO fieldVEmbbd : fieldSntcData.getFieldsValidation()) {
				valc = getPorcentaje(field, objClass, fieldVEmbbd);
				if(valc!=0) {
					porcentageTotal += valc;
					break;
				}
			}
			if(valc==0&&value!=null&&!value.equals("")) {
				porcentageTotal += fieldSntcData.getValue();
			}
		} else if (fieldSntcData.getCriterio().toUpperCase().equals("OptionEmbebed-DefaultNotEmpty".toUpperCase()) && value != null) {
			if (fieldSntcData.getOptionValue().contains(value.toUpperCase())) {// Si seleccionaron la opción para
																				// validar un subcampo anidado
				for (FieldValidationDTO fieldVEmbbd : fieldSntcData.getFieldsValidation()) {
					Class<?> clazz = objClass.getClass();
					try {
						Field fieldEmb = clazz.getDeclaredField(fieldVEmbbd.getFieldName());
						porcentageTotal += getPorcentaje(fieldEmb, objClass, fieldVEmbbd);
					} catch (NoSuchFieldException e) {
						e.printStackTrace();
						return 0d;
					} catch (SecurityException e) {
						e.printStackTrace();
						return 0d;
					}
				}
			} else {// Si seleccionaron una opción para validar que solo tenga valor
				return 0d;
			}
		} else {
			return 0d;
		}
		return porcentageTotal;
	}

	public double calculoPorcentaje(Object objClass, String form) {
		double porcentageTotal = 0d;
		List<Porcentaje> fieldsSentencias = porcentajeRepository.findByForm(form);
		if (objClass != null) {
			Class<?> clazz = objClass.getClass();
			int count=0; 
			for (Field field : clazz.getDeclaredFields()) {// Recorre todos los campos de la clase
//				System.out.println("count: "+(count++)+" "+field.getName());
				Porcentaje fieldSntcDataP = fieldsSentencias.stream()
						.filter(fieldSntc -> field.getName().equals(fieldSntc.getFieldName())).findAny().orElse(null);
				if (fieldSntcDataP != null) {
					FieldValidationDTO fieldSntcData = new FieldValidationDTO();
					fieldSntcData.setCriterio(fieldSntcDataP.getCriterio());
					fieldSntcData.setFieldName(fieldSntcDataP.getFieldName());
					fieldSntcData.setFieldsValidation(fieldSntcDataP.getFieldsValidation());
					fieldSntcData.setOptionValue(fieldSntcDataP.getOptionValue());
					fieldSntcData.setValue(fieldSntcDataP.getValue());
					
					porcentageTotal += getPorcentaje(field, objClass, fieldSntcData);
				}
			}
		}
		return porcentageTotal;
	}
	
	private String getFieldValue(Field field, Object objClass) {
		try {
			field.setAccessible(true);
			Class clazz = null;
			try {
				clazz = field.get(objClass).getClass();
			} catch (Exception e) {
				return null;
			}

			Gson g = new Gson();
			String value = field.get(objClass).toString();
			if (clazz.isInstance(new Date()) | clazz.isInstance(new String()) | clazz.isInstance(new Integer(0))
					| clazz.isInstance(new Boolean(false))) {
				if (value != null) {
					return value.equals("") ? null : value;
				}
			} else if (clazz.isInstance(new ArrayList()) || clazz.getSimpleName().equals("ArrayList")) {
				Object fieldValue = field.get(objClass);
				value = "";
				if (fieldValue instanceof List) { // raw type
					for (Object element : ((List) fieldValue)) {
						value += (element + ",");
					}
				}
				return value.equals("") ? null : value;
			} else {
				System.out.println("Tipo faltante:" + clazz.getSimpleName());
				return null;
			}
		} catch (IllegalArgumentException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("fuera foco");
		return null;
	}
	
	public Sentencia updatePorcentaje(Sentencia sentencia) {
    	List<Double> porcentajes = new ArrayList<Double>();
    	
    	List<TablaVotaciones> listVotacionesCausas = (List<TablaVotaciones>) _votacionRepository.findAllById(sentencia.getVotacionCausasImprocedenciaySobreseimientoAnalizadas());
    	for(TablaVotaciones votacion : listVotacionesCausas) {
    		porcentajes.add(calculoPorcentaje(votacion, "votacionCausasImprocedenciaySobreseimientoAnalizadas"));
    	}
    	List<TablaVotaciones> listVotacionesDH = (List<TablaVotaciones>) _votacionRepository.findAllById(sentencia.getVotacionDerechosHumanosCuyaViolacionAnalizaSentencia());
    	for(TablaVotaciones votacion : listVotacionesDH) {
    		porcentajes.add(calculoPorcentaje(votacion, "votacionDerechosHumanosCuyaViolacionAnalizaSentencia"));
    	}
    	List<TablaVotaciones> listVotacionesTipVicPrLegAnzSntc = (List<TablaVotaciones>) _votacionRepository.findAllById(sentencia.getVotacionTipoVicioProcesoLegislativoAnalizadoenSentencia());
    	for(TablaVotaciones votacion : listVotacionesTipVicPrLegAnzSntc) {
    		porcentajes.add(calculoPorcentaje(votacion, "votacionDerechosHumanosCuyaViolacionAnalizaSentencia"));
    	}
    	List<TablaVotaciones> listVotacionesViolacionEsferas = (List<TablaVotaciones>) _votacionRepository.findAllById(sentencia.getVotacionTipoViolacionInvacionEsferasAnalizadoenSentencia());
    	for(TablaVotaciones votacion : listVotacionesViolacionEsferas) {
    		porcentajes.add(calculoPorcentaje(votacion, "votacionCausasImprocedenciaySobreseimientoAnalizadas"));
    	}
    	List<TablaVotaciones> listVotacionesViolacionPoderes = (List<TablaVotaciones>) _votacionRepository.findAllById(sentencia.getVotacionTipoViolacionInvacionPoderesAnalizadoenSentencia());
    	for(TablaVotaciones votacion : listVotacionesViolacionPoderes) {
    		porcentajes.add(calculoPorcentaje(votacion, "votacionCausasImprocedenciaySobreseimientoAnalizadas"));
    	}
        double porcentaje_TablaVotacion = calculoPorcentaje(sentencia, "sentencia");
		porcentajes.add(porcentaje_TablaVotacion);
		
		double sum = porcentajes.stream().mapToDouble(Double::doubleValue).sum();
		double porcentaje = sum/porcentajes.size();
		sentencia.setPorcentajeCaptura(CalculoPorcentajeUtils.round(porcentaje, 1));
		return sentencia;
    }
	
	public Voto updatePorcentaje(Voto voto) {
		List<Double> porcentajes = new ArrayList<Double>();
		double porcentaje_voto = calculoPorcentaje(voto, "voto");
		porcentajes.add(porcentaje_voto);
		double sum = porcentajes.stream().mapToDouble(Double::doubleValue).sum();
		double porcentaje = sum/porcentajes.size();
		voto.setPorcentaje(CalculoPorcentajeUtils.round(porcentaje, 1));
		return voto;
	}
	
	public AcuerdosGenerales updatePorcentaje(AcuerdosGenerales acuerdo) {
		List<Double> porcentajes = new ArrayList<Double>();
		double porcentaje_acuerdo = calculoPorcentaje(acuerdo, "acuerdo");
		porcentajes.add(porcentaje_acuerdo);
		double sum = porcentajes.stream().mapToDouble(Double::doubleValue).sum();
		double porcentaje = sum/porcentajes.size();
		acuerdo.setPorcentaje(CalculoPorcentajeUtils.round(porcentaje, 1));
		return acuerdo;
	}
	
	public DiversaNormativa updatePorcentaje(DiversaNormativa diversaNormativa) {
		List<Double> porcentajes = new ArrayList<Double>();
		double porcentaje_acuerdo = calculoPorcentaje(diversaNormativa, "diversaNormativa");
		porcentajes.add(porcentaje_acuerdo);
		double sum = porcentajes.stream().mapToDouble(Double::doubleValue).sum();
		double porcentaje = sum/porcentajes.size();
		diversaNormativa.setPorcentaje(CalculoPorcentajeUtils.round(porcentaje, 1));
		return diversaNormativa;
	}

	
}
