package mx.gob.scjn.ugacj.sga_captura.service.bitacora;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import mx.gob.scjn.ugacj.sga_captura.domain.bitacora.CommitMetaData;
import mx.gob.scjn.ugacj.sga_captura.domain.bitacora.JvSnapshots;
import mx.gob.scjn.ugacj.sga_captura.repository.JvSnapshotsRepository;
import mx.gob.scjn.ugacj.sga_captura.utils.ConvertFechaFiltros;
import mx.gob.scjn.ugacj.sga_captura.utils.RegexUtils;
import mx.gob.scjn.ugacj.sga_captura.utils.StatusBitacora;

@Service
public class BitacoraService {

	@Autowired
	private MongoOperations mongoOperations;

	@Autowired
	private JvSnapshotsRepository _jvsRepository;

	@Autowired
	private HttpServletRequest request;

	public JvSnapshots getBitacoraById(String id) {
		Optional<JvSnapshots> bitacoraExist = _jvsRepository.findById(id);
		if (bitacoraExist.isPresent()) {
			return bitacoraExist.get();
		}
		// TODO Auto-generated method stub
		return null;
	}

	public Page<JvSnapshots> getAllJvSnapshotsByPagin(Pageable paging, String filtros)throws ParseException {
		List<JvSnapshots> bitacoraList = null;
		Page<JvSnapshots> bitacoraPaginada = null;
		Page<JvSnapshots> bitacoraPaginableResponse = null;
		long count = 0;

		if (filtros != null) {
			Query query = new Query().with(paging);
			Query queryNoPage = new Query();
			String subs[] = filtros.split(",");
			for (String sub : subs) {
				String sTempFiltros[] = sub.split(":");
				String filtro = sTempFiltros[0].trim();
				String filtroValores = sTempFiltros[1].trim();
				if (filtro.trim().equals("commitMetaData.commitDate")) {
					Date fecha = ConvertFechaFiltros.getFormatFecha(filtroValores.trim(), "yyyy-MM-dd");
					LocalDateTime fecha1 = fecha.toInstant().atZone(TimeZone.getTimeZone("America/Toronto").toZoneId()).toLocalDateTime();
					LocalDateTime fecha2 = fecha.toInstant().atZone(TimeZone.getTimeZone("America/Toronto").toZoneId())
						      .toLocalDateTime().plusDays(1);
					System.out.println("fecha1 :"+fecha1+"\nfecha2: "+fecha2);
					
					query.addCriteria(Criteria.where(filtro).gte(fecha1).lt(fecha2));
					queryNoPage.addCriteria(Criteria.where(filtro).gte(fecha1).lte(fecha2));
					
					query.with(Sort.by(Sort.Direction.DESC, "commitMetaData.commitDate"));
					queryNoPage.with(Sort.by(Sort.Direction.DESC, "commitMetaData.commitDate"));

				} else {
					query.addCriteria(Criteria.where(filtro).is(filtroValores));
					query.with(Sort.by(Sort.Direction.DESC, "commitMetaData.commitDate"));

					queryNoPage.addCriteria(Criteria.where(filtro).is(filtroValores));
					queryNoPage.with(Sort.by(Sort.Direction.DESC, "commitMetaData.commitDate"));
				}
			}
			bitacoraList = mongoOperations.find(query, JvSnapshots.class);
			count = mongoOperations.count(queryNoPage, JvSnapshots.class);

			String resumen = "";
			String jsonInString = null;
			JSONObject mJSONObject = null;

			for (JvSnapshots bitL : bitacoraList) {
				jsonInString = new Gson().toJson(bitL.getState());
				try {
					mJSONObject = new JSONObject(jsonInString);

					resumen = "";

					if (bitL.getSeccion().equals(StatusBitacora._SENTENCIAS)) {
						resumen += (mJSONObject.has("tipoAsunto") ? mJSONObject.get("tipoAsunto") : " ");
						resumen += " "
								+ (mJSONObject.has("numeroExpediente") ? mJSONObject.get("numeroExpediente") : "");
						resumen += "; Órgano: "
								+ (mJSONObject.has("organoRadicacion") ? mJSONObject.get("organoRadicacion") : "");
						resumen += "; Fecha: "
								+ (mJSONObject.has("fechaResolucion") ?  ConvertFechaFiltros.changeFormat(mJSONObject.get("fechaResolucion").toString(), RegexUtils.REGEX_YYYYMMDD) : "");
						resumen += "; Ponente: " + (mJSONObject.has("ponente") ? mJSONObject.get("ponente") : "");

					} else if (bitL.getSeccion().equals(StatusBitacora._SENTENCIAS_VOTACION)) {
						resumen += (mJSONObject.has("tipoVotacion") ? mJSONObject.get("tipoVotacion") : " ");

					} else if (bitL.getSeccion().equals(StatusBitacora._SENTENCIAS_VOTACION_RUBROS)) {
						resumen += (mJSONObject.has("tipoRubro") ? "Rubro de " + mJSONObject.get("tipoRubro") : " ");

					} else if (bitL.getSeccion().equals(StatusBitacora._CATALOGO)) {
						resumen += "Catálogo: " + (mJSONObject.has("catalogo") ? mJSONObject.get("catalogo") : "");
						resumen += "; Nombre: " + (mJSONObject.has("nombre") ? mJSONObject.get("nombre") : "");

					} else if (bitL.getSeccion().equals(StatusBitacora._VERSIONES)) {
						resumen += "Fecha de Sesión: "
								+ (mJSONObject.has("fechaSesion") ?  ConvertFechaFiltros.changeFormat(mJSONObject.get("fechaSesion").toString(), RegexUtils.REGEX_YYYYMMDD) : "");
						resumen += "; Duración: "
								+ (mJSONObject.has("duracionSesion") ? mJSONObject.get("duracionSesion") : "");
						resumen += "; Presidente Decano: "
								+ (mJSONObject.has("nombrePresidenteDecano") ? mJSONObject.get("nombrePresidenteDecano")
										: "");

					} else if (bitL.getSeccion().equals(StatusBitacora._ASUNTOS)) {
						JSONObject vt_asuntosObj = mJSONObject.has("vt_asuntos")
								? (JSONObject) mJSONObject.get("vt_asuntos")
								: null;
						JSONObject asuntoObj = mJSONObject.has("asunto") ? (JSONObject) mJSONObject.get("asunto")
								: null;
						resumen += (asuntoObj != null
								? asuntoObj.has("asuntoAbordado") ? asuntoObj.get("asuntoAbordado") : ""
								: "");
						resumen += " " + (asuntoObj != null
								? asuntoObj.has("numeroExpediente") ? asuntoObj.get("numeroExpediente") : ""
								: "");
						resumen += vt_asuntosObj != null
								? vt_asuntosObj.has("ponente") ? "; Ponente: " + vt_asuntosObj.get("ponente") : ""
								: "";

					} else if (bitL.getSeccion().equals(StatusBitacora._DIVERSA)) {
						JSONObject diversaObj = mJSONObject.has("diversaNormativa")
								? (JSONObject) mJSONObject.get("diversaNormativa")
								: null;
						resumen += "Fecha de Aprobación: " + (diversaObj != null
								? diversaObj.has("fechaAprobacion") ? ConvertFechaFiltros.changeFormat(diversaObj.get("fechaAprobacion").toString(), RegexUtils.REGEX_YYYYMMDD) : ""
								: "");
						resumen += "; Tipo de Instrumento: " + (diversaObj != null
								? diversaObj.has("tipoInstrumento") ? diversaObj.get("tipoInstrumento") : ""
								: "");
						resumen += "; Número: "
								+ (diversaObj != null ? diversaObj.has("numero") ? diversaObj.get("numero") : "" : "");

					} else if (bitL.getSeccion().equals(StatusBitacora._VOTOS)) {
						resumen += (mJSONObject.has("tipoAsunto") ? mJSONObject.get("tipoAsunto") : "");
						resumen += " " + (mJSONObject.has("numExpediente") ? mJSONObject.get("numExpediente") : "");
						resumen += "; Fecha de Resolución: "
								+ (mJSONObject.has("fechaResolucion") ? ConvertFechaFiltros.changeFormat(mJSONObject.get("fechaResolucion").toString(), RegexUtils.REGEX_YYYYMMDD) : "");
						resumen += "; Tipo Voto: " + (mJSONObject.has("tipoVoto") ? mJSONObject.get("tipoVoto") : "");

					} else if (bitL.getSeccion().equals(StatusBitacora._ACUERDOS)) {
						resumen += "Fecha de Aprobación: "
								+ (mJSONObject.has("fechaAprobacion") ? ConvertFechaFiltros.changeFormat(mJSONObject.get("fechaAprobacion").toString(), RegexUtils.REGEX_YYYYMMDD) : "");
						resumen += "; Ámbito: "
								+ (mJSONObject.has("ambitoIncide") ? mJSONObject.get("ambitoIncide") : "");
						resumen += "; Vigencia: " + (mJSONObject.has("vigencia") ? mJSONObject.get("vigencia") : "");
						resumen += "; Órgano: "
								+ (mJSONObject.has("organoEmisor") ? mJSONObject.get("organoEmisor") : "");

					} else if (bitL.getSeccion().equals(StatusBitacora._MATERIA)) {
						resumen += "Materia: "
								+ (mJSONObject.has("materiaRegulacion") ? mJSONObject.get("materiaRegulacion") : "");

					}

					bitL.setResumen(resumen);

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

			bitacoraPaginableResponse = new PageImpl<JvSnapshots>(bitacoraList, paging, count);

		} else

		{
			bitacoraPaginada = _jvsRepository.findAll(paging);
			bitacoraList = bitacoraPaginada.getContent();
			count = bitacoraPaginada.getTotalElements();

			String resumen = "";
			String jsonInString = null;
			JSONObject mJSONObject = null;

			for (JvSnapshots bitL : bitacoraList) {
				jsonInString = new Gson().toJson(bitL.getState());
				try {
					mJSONObject = new JSONObject(jsonInString);

					resumen = "";

					if (bitL.getSeccion().equals(StatusBitacora._SENTENCIAS)) {
						resumen += (mJSONObject.has("tipoAsunto") ? mJSONObject.get("tipoAsunto") : " ");
						resumen += " "
								+ (mJSONObject.has("numeroExpediente") ? mJSONObject.get("numeroExpediente") : "");
						resumen += "; Órgano: "
								+ (mJSONObject.has("organoRadicacion") ? mJSONObject.get("organoRadicacion") : "");
						resumen += "; Fecha: "
								+ (mJSONObject.has("fechaResolucion") ? ConvertFechaFiltros.changeFormat(mJSONObject.get("fechaResolucion").toString(), RegexUtils.REGEX_YYYYMMDD) : "");
						resumen += "; Ponente: " + (mJSONObject.has("ponente") ? mJSONObject.get("ponente") : "");

					} else if (bitL.getSeccion().equals(StatusBitacora._SENTENCIAS_VOTACION)) {
						resumen += (mJSONObject.has("tipoVotacion") ? mJSONObject.get("tipoVotacion") : " ");

					} else if (bitL.getSeccion().equals(StatusBitacora._SENTENCIAS_VOTACION_RUBROS)) {
						resumen += (mJSONObject.has("tipoRubro") ? "Rubro de " + mJSONObject.get("tipoRubro") : " ");

					}else if (bitL.getSeccion().equals(StatusBitacora._CATALOGO)) {
						resumen += "Catálogo: " + (mJSONObject.has("catalogo") ? mJSONObject.get("catalogo") : "");
						resumen += "; Nombre: " + (mJSONObject.has("nombre") ? mJSONObject.get("nombre") : "");

					} else if (bitL.getSeccion().equals(StatusBitacora._VERSIONES)) {
						resumen += "Fecha de Sesión: "
								+ (mJSONObject.has("fechaSesion") ? ConvertFechaFiltros.changeFormat(mJSONObject.get("fechaSesion").toString(), RegexUtils.REGEX_YYYYMMDD) : "");
						resumen += "; Duración: "
								+ (mJSONObject.has("duracionSesion") ? mJSONObject.get("duracionSesion") : "");
						resumen += "; Presidente Decano: "
								+ (mJSONObject.has("nombrePresidenteDecano") ? mJSONObject.get("nombrePresidenteDecano")
										: "");

					} else if (bitL.getSeccion().equals(StatusBitacora._ASUNTOS)) {
						JSONObject vt_asuntosObj = mJSONObject.has("vt_asuntos")
								? (JSONObject) mJSONObject.get("vt_asuntos")
								: null;
						JSONObject asuntoObj = mJSONObject.has("asunto") ? (JSONObject) mJSONObject.get("asunto")
								: null;
						resumen += (asuntoObj != null
								? asuntoObj.has("asuntoAbordado") ? asuntoObj.get("asuntoAbordado") : ""
								: "");
						resumen += " " + (asuntoObj != null
								? asuntoObj.has("numeroExpediente") ? asuntoObj.get("numeroExpediente") : ""
								: "");
						resumen += vt_asuntosObj != null
								? vt_asuntosObj.has("ponente") ? "; Ponente: " + vt_asuntosObj.get("ponente") : ""
								: "";

					} else if (bitL.getSeccion().equals(StatusBitacora._DIVERSA)) {
						JSONObject diversaObj = mJSONObject.has("diversaNormativa")
								? (JSONObject) mJSONObject.get("diversaNormativa")
								: null;
						resumen += "Fecha de Aprobación: " + (diversaObj != null
								? diversaObj.has("fechaAprobacion") ? ConvertFechaFiltros.changeFormat(diversaObj.get("fechaAprobacion").toString(), RegexUtils.REGEX_YYYYMMDD)  : ""
								: "");
						resumen += "; Tipo de Instrumento: " + (diversaObj != null
								? diversaObj.has("tipoInstrumento") ? diversaObj.get("tipoInstrumento") : ""
								: "");
						resumen += "; Número: "
								+ (diversaObj != null ? diversaObj.has("numero") ? diversaObj.get("numero") : "" : "");

					} else if (bitL.getSeccion().equals(StatusBitacora._VOTOS)) {
						resumen += (mJSONObject.has("tipoAsunto") ? mJSONObject.get("tipoAsunto") : "");
						resumen += " " + (mJSONObject.has("numExpediente") ? mJSONObject.get("numExpediente") : "");
						resumen += "; Fecha de Resolución: "
								+ (mJSONObject.has("fechaResolucion") ? ConvertFechaFiltros.changeFormat(mJSONObject.get("fechaResolucion").toString(), RegexUtils.REGEX_YYYYMMDD) : "");
						resumen += "; Tipo Voto: " + (mJSONObject.has("tipoVoto") ? mJSONObject.get("tipoVoto") : "");

					} else if (bitL.getSeccion().equals(StatusBitacora._ACUERDOS)) {
						resumen += "Fecha de Aprobación: "
								+ (mJSONObject.has("fechaAprobacion") ? ConvertFechaFiltros.changeFormat(mJSONObject.get("fechaAprobacion").toString(), RegexUtils.REGEX_YYYYMMDD) : "");
						resumen += "; Ámbito: "
								+ (mJSONObject.has("ambitoIncide") ? mJSONObject.get("ambitoIncide") : "");
						resumen += "; Vigencia: " + (mJSONObject.has("vigencia") ? mJSONObject.get("vigencia") : "");
						resumen += "; Órgano: "
								+ (mJSONObject.has("organoEmisor") ? mJSONObject.get("organoEmisor") : "");

					} else if (bitL.getSeccion().equals(StatusBitacora._MATERIA)) {
						resumen += "Materia: "
								+ (mJSONObject.has("materiaRegulacion") ? mJSONObject.get("materiaRegulacion") : "");

					}
					bitL.setResumen(resumen);

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			bitacoraPaginableResponse = new PageImpl<JvSnapshots>(bitacoraList, paging, count);

		}
		return bitacoraPaginableResponse;
	}

	public void saveOperacion(String seccion, String type, Object state, Object beginState) {

		Optional<JvSnapshots> jv = Optional.ofNullable(dataJvSnapshots(seccion, type, state, beginState));
		if (jv.isPresent()) {
			_jvsRepository.save(jv.get());
		}
	}

	public JvSnapshots dataJvSnapshots(String seccion, String type, Object state, Object beginState) {

		if (request.getAttribute("username") != null && !request.getAttribute("username").toString().isEmpty()) {
			JvSnapshots jv = new JvSnapshots();
			CommitMetaData commitMetaData = new CommitMetaData();
			jv.setSeccion(seccion);
			jv.setType(type);
			jv.setBeginState(beginState);
			jv.setState(state);
			commitMetaData.setAuthor(request.getAttribute("username").toString());
			commitMetaData.setCommitDate(new Date());
			jv.setCommitMetaData(commitMetaData);
			return jv;
		}
		return null;
	}

}
