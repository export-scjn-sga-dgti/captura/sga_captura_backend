package mx.gob.scjn.ugacj.sga_captura.service.vtaquigrafica;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import mx.gob.scjn.ugacj.sga_captura.domain.AsuntoAbordado;
import mx.gob.scjn.ugacj.sga_captura.domain.Sentencia;
import mx.gob.scjn.ugacj.sga_captura.domain.VTaquigraficaAsuntos;
import mx.gob.scjn.ugacj.sga_captura.domain.VersionTaquigrafica;
import mx.gob.scjn.ugacj.sga_captura.dto.PorcentajeDTO;
import mx.gob.scjn.ugacj.sga_captura.dto.statecode.AsuntoResponseDTO;
import mx.gob.scjn.ugacj.sga_captura.dto.vtaquigraficas.AsuntoAbordadoDTO;
import mx.gob.scjn.ugacj.sga_captura.dto.vtaquigraficas.SentenciaIndexObjectDTO;
import mx.gob.scjn.ugacj.sga_captura.mildleware.SentenciasMilddleware;
import mx.gob.scjn.ugacj.sga_captura.repository.AsuntoAbordadoRepository;
import mx.gob.scjn.ugacj.sga_captura.repository.SentenciaRepository;
import mx.gob.scjn.ugacj.sga_captura.repository.VTaquigraficaAsuntosRepository;
import mx.gob.scjn.ugacj.sga_captura.repository.VersionTaquigraficaRepository;
import mx.gob.scjn.ugacj.sga_captura.restclient.RestClients;
import mx.gob.scjn.ugacj.sga_captura.service.bitacora.BitacoraService;
import mx.gob.scjn.ugacj.sga_captura.service.porcentaje.PorcentajeService;
import mx.gob.scjn.ugacj.sga_captura.utils.CalculoPorcentajeUtils;
import mx.gob.scjn.ugacj.sga_captura.utils.ConvertFechaFiltros;
import mx.gob.scjn.ugacj.sga_captura.utils.MethodsUtils;
import mx.gob.scjn.ugacj.sga_captura.utils.RegexUtils;
import mx.gob.scjn.ugacj.sga_captura.utils.StatusBitacora;

@Service
public class AsuntoAbordadoService {
	@Autowired
	private AsuntoAbordadoRepository _asuntoRepository;

	@Autowired
	private VersionTaquigraficaRepository _vtRepository;

	@Autowired
	private VTaquigraficaAsuntosRepository _vtAsuntosRepository;

	@Autowired
	private SentenciaRepository _sentenciaRepository;
	
	@Autowired
	private VTaquigraficaAsuntosRepository vTaquigraficaAsuntosRepository;

	@Autowired
	private MongoOperations mongoOperations;
	
	@Autowired
	private PorcentajeService porcentajeService;

	@Value("${path.api.create.sentencia}")
	private String pathCreate;

	@Value("${path.api.search.sentencia}")
	private String pathSearch;

	MethodsUtils methods = new MethodsUtils();

	RestClients restClient = new RestClients();

	@Autowired
	private BitacoraService _bitacoraService;

	public AsuntoAbordadoDTO getAsuntoAbordado(String idVt, String idAsunto) {
		AsuntoAbordadoDTO asunto = new AsuntoAbordadoDTO();
		Optional<AsuntoAbordado> asuntoExist = _asuntoRepository.findById(idAsunto);
		if (asuntoExist != null) {
			VTaquigraficaAsuntos vtaqAsunto = findByIdVersionAndIdAsunto(idVt, idAsunto);
			if (vtaqAsunto != null) {
				asunto.setIdVersionTaqui(idVt);
				asunto.setAsunto(asuntoExist.get());
				asunto.setVt_asuntos(vtaqAsunto);
				return asunto;
			}
		}
		return null;
	}

	public boolean asuntoDuplicate(String idVtaq, AsuntoAbordado asunto) {
		VTaquigraficaAsuntos asociacionExist = findByIdVersionAndIdAsunto(idVtaq, asunto.getId());
		if (asociacionExist != null) {
			return true;
		}
		return false;
	}

	public String getTipoUpdate(AsuntoAbordado asuntoToUpdate, AsuntoAbordado asuntoVivo) {
		List<String> asuntosAcumOrigen = asuntoVivo.getAcumulados();
		Gson gson = new Gson();
		asuntoToUpdate.setAcumulados(null);
		asuntoToUpdate.setAcumulada(false);
		
		asuntoVivo.setAcumulados(null);
		asuntoVivo.setAcumulada(false);
		if (gson.toJson(asuntoToUpdate).equals(gson.toJson(asuntoVivo))) {
			asuntoVivo.setAcumulados(asuntosAcumOrigen);
			return "UPDATE_TT";
		}
		asuntoVivo.setAcumulados(asuntosAcumOrigen);
		return "UPDATE_IDS_TT";
	}

	public String asuntoExist(AsuntoAbordado asunto, String idVtaq) {
		// Existe un asunto con el mismo tipoAsunto y numExp?
		AsuntoAbordado asuntoExist = findByAsuntoAndExpediente(asunto.getAsuntoAbordado(),
				asunto.getNumeroExpediente());
		// Si existe el asunto y tiene Id
		if (asuntoExist != null) {
			if (asuntoDuplicate(idVtaq, asuntoExist)) {
				return "MSJ_ERROR_DUPLICATE";
			}
			return "SI_EXISTE";
		}
		return "NO_EXISTE";
	}

	public boolean asuntoEstaEnUso(AsuntoAbordado asunto) {
		List<VTaquigraficaAsuntos> listVatAsuntosTT = _vtAsuntosRepository.findByIdAsunto(asunto.getId());
		if (listVatAsuntosTT.size() > 1) {
			return true;
		}
		return false;
	}

	public AsuntoResponseDTO setRespuesta(int status, String msj, AsuntoAbordado asunto,
			VTaquigraficaAsuntos vtaqAsuto_TT) {
		AsuntoResponseDTO respuesta = new AsuntoResponseDTO(status, msj);
		respuesta.setIdVersionTaqui(vtaqAsuto_TT != null ? vtaqAsuto_TT.getIdVtaquigrafica() : null);
		respuesta.setAsunto(asunto);
		respuesta.setVt_asuntos(vtaqAsuto_TT);
		return respuesta;
	}

	public AsuntoResponseDTO completeAction(String action, AsuntoAbordado asunto, VTaquigraficaAsuntos vtaqAsuto_TT) {
		AsuntoResponseDTO respuesta = null;
		AsuntoResponseDTO respuestaBegin = null;

		if (action.equals("CREATE")) {
			//Colocar el año de la fecha de la versión taquigráfica (sesión)
			_asuntoRepository.save(asunto);
			vtaqAsuto_TT.setIdAsunto(asunto.getId());
			_vtAsuntosRepository.save(vtaqAsuto_TT);
			
			Optional<VersionTaquigrafica> vtaq = _vtRepository.findById(vtaqAsuto_TT.getIdVtaquigrafica());
			if(vtaq.isPresent()) {
				updatePorcentajeVersionTaquigrafica(vtaq.get());
			}

			respuesta = setRespuesta(HttpStatus.OK.value(), "Asunto Abordado Creado", asunto, vtaqAsuto_TT);
			_bitacoraService.saveOperacion(StatusBitacora._ASUNTOS, StatusBitacora._SAVE, respuesta, null);
			return respuesta;

		} else if (action.equals("CREATE_TT")) {
			//Colocar el año de la última versión taquigráfica donde se registró
			AsuntoAbordado asuntoExist = findByAsuntoAndExpediente(asunto.getAsuntoAbordado(),
					asunto.getNumeroExpediente());
			vtaqAsuto_TT.setIdAsunto(asuntoExist.getId());
			_vtAsuntosRepository.save(vtaqAsuto_TT);
			
			Optional<VersionTaquigrafica> vtaq = _vtRepository.findById(vtaqAsuto_TT.getIdVtaquigrafica());
			if(vtaq.isPresent()) {
				updatePorcentajeVersionTaquigrafica(vtaq.get());
			}
			
			respuesta = setRespuesta(HttpStatus.OK.value(), "Se ha creado el Asunto Abordado", asuntoExist,
					vtaqAsuto_TT);
			_bitacoraService.saveOperacion(StatusBitacora._ASUNTOS, StatusBitacora._SAVE, respuesta, null);
			return respuesta;

		} else if (action.equals("MSJ_ERROR_DUPLICATE")) {
			return setRespuesta(HttpStatus.BAD_REQUEST.value(),
					"¡No fue posible crear el Asunto Abordado porque ya existe", null, null);

		} else if (action.equals("UPDATE_TT")) {
			Optional<VTaquigraficaAsuntos> asuntoBegin = _vtAsuntosRepository.findById(vtaqAsuto_TT.getId());
			respuestaBegin = setRespuesta(HttpStatus.OK.value(), "Asunto Anterior", asunto, asuntoBegin.get());
			_vtAsuntosRepository.save(vtaqAsuto_TT);
			
			Optional<VersionTaquigrafica> vtaq = _vtRepository.findById(vtaqAsuto_TT.getIdVtaquigrafica());
			if(vtaq.isPresent()) {
				updatePorcentajeVersionTaquigrafica(vtaq.get());
			}
			
			respuesta = setRespuesta(HttpStatus.OK.value(), "Se ha actualizado el Asunto Abordado", asunto,
					vtaqAsuto_TT);
			_bitacoraService.saveOperacion(StatusBitacora._ASUNTOS, StatusBitacora._UPDATE, respuesta, respuestaBegin);
			return respuesta;

		} else if (action.equals("UPDATE_IDS_UPDATE_TT")) {
			Optional<VTaquigraficaAsuntos> TtExist = _vtAsuntosRepository.findById(vtaqAsuto_TT.getId());
			Optional<AsuntoAbordado> asuntoExist = _asuntoRepository.findById(asunto.getId());
			respuestaBegin = setRespuesta(HttpStatus.OK.value(), "Asunto Anterior", asuntoExist.get(), TtExist.get());

			_asuntoRepository.save(asunto);
			_vtAsuntosRepository.save(vtaqAsuto_TT);
			
			Optional<VersionTaquigrafica> vtaq = _vtRepository.findById(vtaqAsuto_TT.getIdVtaquigrafica());
			if(vtaq.isPresent()) {
				updatePorcentajeVersionTaquigrafica(vtaq.get());
			}

			System.out.println("UPDATE_IDS_UPDATE_TT");
			respuesta = setRespuesta(HttpStatus.OK.value(), "Se ha actualizado el Asunto Abordado", asunto,
					vtaqAsuto_TT);
			_bitacoraService.saveOperacion(StatusBitacora._ASUNTOS, StatusBitacora._UPDATE, respuesta, respuestaBegin);
			return respuesta;

		} else if (action.equals("UPDATE_TT_DELETE_IDS")) {

			AsuntoAbordado asuntoExist = findByAsuntoAndExpediente(asunto.getAsuntoAbordado(),
					asunto.getNumeroExpediente());
			Optional<VTaquigraficaAsuntos> TtExist = _vtAsuntosRepository.findById(vtaqAsuto_TT.getId());
			vtaqAsuto_TT.setIdAsunto(asuntoExist.getId());
			
			respuestaBegin = setRespuesta(HttpStatus.OK.value(), "Asunto Anterior", asuntoExist, TtExist.get());
			
			_vtAsuntosRepository.save(vtaqAsuto_TT);
			_asuntoRepository.deleteById(asunto.getId());
			asunto.setId(asuntoExist.getId());
			
			Optional<VersionTaquigrafica> vtaq = _vtRepository.findById(vtaqAsuto_TT.getIdVtaquigrafica());
			if(vtaq.isPresent()) {
				updatePorcentajeVersionTaquigrafica(vtaq.get());
			}

			respuesta = setRespuesta(HttpStatus.OK.value(), "Se ha actualizado el Asunto Abordado", asunto, vtaqAsuto_TT);
			_bitacoraService.saveOperacion(StatusBitacora._ASUNTOS, StatusBitacora._UPDATE, respuesta, respuestaBegin);
			return respuesta;
			
		} else if (action.equals("UPDATE_TT_SET_IDS")) {

			AsuntoAbordado asuntoExist = findByAsuntoAndExpediente(asunto.getAsuntoAbordado(),
					asunto.getNumeroExpediente());
			respuestaBegin = setRespuesta(HttpStatus.OK.value(), "Asunto Anterior", asuntoExist, vtaqAsuto_TT);
			vtaqAsuto_TT.setIdAsunto(asuntoExist.getId());
			_vtAsuntosRepository.save(vtaqAsuto_TT);
			asunto.setId(asuntoExist.getId());
			
			Optional<VersionTaquigrafica> vtaq = _vtRepository.findById(vtaqAsuto_TT.getIdVtaquigrafica());
			if(vtaq.isPresent()) {
				updatePorcentajeVersionTaquigrafica(vtaq.get());
			}

			respuesta = setRespuesta(HttpStatus.OK.value(), "Se ha actualizado el Asunto Abordado", asunto, vtaqAsuto_TT);
			_bitacoraService.saveOperacion(StatusBitacora._ASUNTOS, StatusBitacora._UPDATE, respuesta, respuestaBegin);
			return respuesta;
			
		} else if (action.equals("CREATE_IDS_UPDATE_TT")) {
			respuestaBegin = setRespuesta(HttpStatus.OK.value(), "Asunto Anterior", asunto, vtaqAsuto_TT);
			asunto.setId(null);
			_asuntoRepository.save(asunto);
			vtaqAsuto_TT.setIdAsunto(asunto.getId());
			_vtAsuntosRepository.save(vtaqAsuto_TT);
			
			Optional<VersionTaquigrafica> vtaq = _vtRepository.findById(vtaqAsuto_TT.getIdVtaquigrafica());
			if(vtaq.isPresent()) {
				updatePorcentajeVersionTaquigrafica(vtaq.get());
			}

			System.out.println("CREATE_IDS_UPDATE_TT");
			respuesta = setRespuesta(HttpStatus.OK.value(), "Se ha actualizado el Asunto Abordado", asunto, vtaqAsuto_TT);
			_bitacoraService.saveOperacion(StatusBitacora._ASUNTOS, StatusBitacora._UPDATE, respuesta, respuestaBegin);
			return respuesta;
			
		}
		return null;
	}

	public boolean sentencia2UpdateExiste(SentenciaIndexObjectDTO objetoSentencia) {
		Optional<Sentencia> existSentencia = Optional.ofNullable(_sentenciaRepository
				.findByTipoAsuntoAndNumeroExpedienteAndOrganoRadicacion(objetoSentencia.getTipoAsunto(),
						objetoSentencia.getNumeroExpediente(), objetoSentencia.getOrganoRadicacion()));
		if (existSentencia.isPresent()) {
			return true;
		}
		return false;
	}
	
	public boolean deleteSentenciaNotInUse(SentenciaIndexObjectDTO objetoSentencia) {
		Optional<Sentencia> existSentencia = Optional.ofNullable(_sentenciaRepository
				.findByTipoAsuntoAndNumeroExpedienteAndOrganoRadicacion(objetoSentencia.getTipoAsunto(),
						objetoSentencia.getNumeroExpediente(), objetoSentencia.getOrganoRadicacion()));
		if (existSentencia.isPresent()) {
			if (existSentencia.get().getPorcentajeCaptura() > 10.0) {
				_sentenciaRepository.delete(existSentencia.get());
				return true;
			}
		}
		return false;
	}

	public boolean sentenciaOrigenEstaEnUso(SentenciaIndexObjectDTO objetoSentencia) {
		Optional<Sentencia> sentencia = _sentenciaRepository.findById(objetoSentencia.getId());
		if (sentencia.isPresent()) {
			if (sentencia.get().getPorcentajeCaptura() > 10.0) {
				return true;
			}
		}
		return false;
	}
	
	
	private Sentencia converteSentencia(SentenciaIndexObjectDTO dtoSentencia) {
		Sentencia sentencia = new Sentencia();
		sentencia.setId(dtoSentencia.getId());
		sentencia.setTipoAsunto(dtoSentencia.getTipoAsunto());
		sentencia.setNumeroExpediente(dtoSentencia.getNumeroExpediente());
		sentencia.setPonente(dtoSentencia.getPonente());
		sentencia.setFechaResolucion(ConvertFechaFiltros.changeFormat(dtoSentencia.getFechaResolucion(), RegexUtils.REGEX_YYYYMMDD));
		sentencia.setOrganoRadicacion(dtoSentencia.getOrganoRadicacion());
		sentencia.setCA(dtoSentencia.isCa());
		sentencia.setAcumulada(dtoSentencia.isAcumulada());
		sentencia.setAcumulados(dtoSentencia.getAcumulados());
		double porcentaje_sentencia = porcentajeService.calculoPorcentaje(sentencia, "sentencia");
		sentencia.setPorcentajeCaptura(CalculoPorcentajeUtils.round(porcentaje_sentencia, 1));
		
		return sentencia;
	}

	public boolean completeAction(String action, SentenciaIndexObjectDTO objetoSentencia) {
		objetoSentencia.setFechaResolucion(ConvertFechaFiltros.changeFormat(objetoSentencia.getFechaResolucion(), RegexUtils.REGEX_YYYYMMDD));
		if (action.equals("CREATE_SENTENCIA2UPDATE")) {
			objetoSentencia.setId(null);
			Sentencia sentencia = _sentenciaRepository.save(converteSentencia(objetoSentencia));
			_bitacoraService.saveOperacion(StatusBitacora._SENTENCIAS, StatusBitacora._SAVE, sentencia , null);

		} else if (action.equals("UPDATE_SENTENCIAORIGEN")) {
			Optional<Sentencia> sentenciaBegin = _sentenciaRepository.findById(objetoSentencia.getId());
			Sentencia sentencia = _sentenciaRepository.save(converteSentencia(objetoSentencia));
			_bitacoraService.saveOperacion(StatusBitacora._SENTENCIAS, StatusBitacora._UPDATE, sentencia , sentenciaBegin);

		} else if (action.equals("UPDATE_SENTENCIA2UPDATE_DELETE_SENTENCIAORIGEN")) {
			Sentencia sentencia2Update = _sentenciaRepository.findByTipoAsuntoAndNumeroExpediente(
					objetoSentencia.getTipoAsunto(), objetoSentencia.getNumeroExpediente());
			_bitacoraService.saveOperacion(StatusBitacora._SENTENCIAS, StatusBitacora._DELETE, sentencia2Update, null);
			_sentenciaRepository.deleteById(objetoSentencia.getId());
			objetoSentencia.setId(sentencia2Update.getId());
			Sentencia sentencia = _sentenciaRepository.save(converteSentencia(objetoSentencia));
			_bitacoraService.saveOperacion(StatusBitacora._SENTENCIAS, StatusBitacora._UPDATE, sentencia , sentencia2Update);

		} else if (action.equals("UPDATE_SENTENCIA2UPDATE")) {
			Sentencia sentencia2Update = _sentenciaRepository.findByTipoAsuntoAndNumeroExpediente(
					objetoSentencia.getTipoAsunto(), objetoSentencia.getNumeroExpediente());
			
			Sentencia sentencia2UpdateOld = _sentenciaRepository.findByTipoAsuntoAndNumeroExpediente(
					objetoSentencia.getTipoAsunto(), objetoSentencia.getNumeroExpediente());
			
			sentencia2Update.setNumeroExpediente(objetoSentencia.getNumeroExpediente());
			sentencia2Update.setTipoAsunto(objetoSentencia.getTipoAsunto());
			sentencia2Update.setOrganoRadicacion(objetoSentencia.getOrganoRadicacion());
			sentencia2Update.setFechaResolucion(objetoSentencia.getFechaResolucion());
			sentencia2Update.setPonente(objetoSentencia.getPonente());
			
			
			objetoSentencia.setId(sentencia2Update.getId());
			Sentencia sentencia = _sentenciaRepository.save(sentencia2Update);
			_bitacoraService.saveOperacion(StatusBitacora._SENTENCIAS, StatusBitacora._UPDATE, sentencia , sentencia2UpdateOld);

		} else {
			return false;
		}
		return true;
	}

	public boolean getAction(SentenciaIndexObjectDTO objetoSentencia, String action) {
		boolean sentencia2UpdateExiste = sentencia2UpdateExiste(objetoSentencia);
		boolean sentenciaOrigenEnUso = false;
		boolean asuntoOrigenEnUso = false;

		if (action.equals("CREATE")) {
			if (!sentencia2UpdateExiste) {
				completeAction("CREATE_SENTENCIA2UPDATE", objetoSentencia);
			} else {
				completeAction("UPDATE_SENTENCIA2UPDATE", objetoSentencia);
			}

		} else if (action.equals("UPDATE_IDS_TT")) {
			sentenciaOrigenEnUso = sentenciaOrigenEstaEnUso(objetoSentencia);
			asuntoOrigenEnUso = objetoSentencia.isInUse();

			if (!sentencia2UpdateExiste && !sentenciaOrigenEnUso && asuntoOrigenEnUso) {
				completeAction("CREATE_SENTENCIA2UPDATE", objetoSentencia);

			} else if (!sentencia2UpdateExiste && !sentenciaOrigenEnUso && !asuntoOrigenEnUso) {
				completeAction("UPDATE_SENTENCIAORIGEN", objetoSentencia);

			} else if (!sentencia2UpdateExiste && sentenciaOrigenEnUso && asuntoOrigenEnUso) {
				completeAction("CREATE_SENTENCIA2UPDATE", objetoSentencia);

			}
			if (!sentencia2UpdateExiste && sentenciaOrigenEnUso && !asuntoOrigenEnUso) {
				completeAction("CREATE_SENTENCIA2UPDATE", objetoSentencia);

			}

			else if (sentencia2UpdateExiste && sentenciaOrigenEnUso && !asuntoOrigenEnUso) {
				completeAction("UPDATE_SENTENCIA2UPDATE", objetoSentencia);

			} else if (sentencia2UpdateExiste && !sentenciaOrigenEnUso && asuntoOrigenEnUso) {
				completeAction("UPDATE_SENTENCIA2UPDATE", objetoSentencia);

			} else if (sentencia2UpdateExiste && !sentenciaOrigenEnUso && !asuntoOrigenEnUso) {
				completeAction("UPDATE_SENTENCIA2UPDATE_DELETE_SENTENCIAORIGEN", objetoSentencia);

			}
		}
		return false;
	}
	
	public boolean updatePorcentajeVersionTaquigrafica(VersionTaquigrafica vtaq) {
		List<Double> porcentajes = new ArrayList<Double>();
		
		List<VTaquigraficaAsuntos> vtaAsunto_TT = vTaquigraficaAsuntosRepository.findByIdVtaquigrafica(vtaq.getId());
		if(vtaAsunto_TT.size()<=0) {
			porcentajes.add(0d);//porcentaje_asunto
			porcentajes.add(0d);//porcentaje_asuntoVtaq
		}
		
		for(VTaquigraficaAsuntos tt : vtaAsunto_TT) {
			Optional<AsuntoAbordado> asunto = _asuntoRepository.findById(tt.getIdAsunto());
			if(asunto.isPresent()) {
				PorcentajeDTO porcentajeDTO_asunto =  CalculoPorcentajeUtils.mapaPorcetajeAsuntoAbordado();
				double porcentaje_asunto = CalculoPorcentajeUtils.calculoPorcentaje(asunto.get(), porcentajeDTO_asunto);

				PorcentajeDTO porcentajeDTO_asuntoVtaq =  CalculoPorcentajeUtils.mapaPorcetajeVTaquigraficaAsuntos();
				double porcentaje_asuntoVtaq = CalculoPorcentajeUtils.calculoPorcentaje(tt, porcentajeDTO_asuntoVtaq);
				
				porcentajes.add(porcentaje_asunto);
				porcentajes.add(porcentaje_asuntoVtaq);
			}
		}
		PorcentajeDTO porcentajeDTO_vtaq =  CalculoPorcentajeUtils.mapaPorcetajeVersionTaq();
		double porcentaje_vtaq = CalculoPorcentajeUtils.calculoPorcentaje(vtaq, porcentajeDTO_vtaq);
		
		porcentajes.add(porcentaje_vtaq);
		double sum = porcentajes.stream().mapToDouble(Double::doubleValue).sum();
		double porcentaje = sum/porcentajes.size();
		vtaq.setPorcentaje(CalculoPorcentajeUtils.round(porcentaje, 1));
		System.out.println("porcentaje: "+porcentaje);
		if(_vtRepository.save(vtaq) != null) {
			return true;
		}
		return false;
	}

	public AsuntoResponseDTO getAction(AsuntoAbordadoDTO asunto, String action) {
		// Se inicializan las variables
		AsuntoAbordado asuntoToUpdate = asunto.getAsunto();
		AsuntoAbordado asuntoOrigen = null;
		String idVtaq = asunto.getIdVersionTaqui();
		VTaquigraficaAsuntos vtaqAsuto_TT = asunto.getVt_asuntos();
		vtaqAsuto_TT.setIdVtaquigrafica(idVtaq);
		asuntoToUpdate.setAcumulados(vtaqAsuto_TT.getAcumulados());
		AsuntoResponseDTO respuesta = null;
		boolean isInUse = false;
		if (asuntoToUpdate.getId() != null) {
			asuntoOrigen = _asuntoRepository.findById(asuntoToUpdate.getId()).get();
		}
		//

		if (action.equals("CREATE")) {

			action = asuntoExist(asuntoToUpdate, asunto.getIdVersionTaqui());
			if (action.equals("NO_EXISTE")) {
				respuesta = completeAction("CREATE", asuntoToUpdate, vtaqAsuto_TT);
				action = "CREATE";
			} else if (action.equals("SI_EXISTE")) {
				respuesta = completeAction("CREATE_TT", asuntoToUpdate, vtaqAsuto_TT);
				asuntoOrigen = respuesta.getAsunto();
				action = "UPDATE_IDS_TT";
			} else if (action.equals("MSJ_ERROR_DUPLICATE")) {
				return completeAction(action, null, null);

			}

		} else if (action.equals("UPDATE")) {
			action = getTipoUpdate(asuntoToUpdate, asuntoOrigen);
			isInUse = asuntoEstaEnUso(asuntoOrigen);

			if (action.equals("UPDATE_TT")) {
				respuesta = completeAction(action, asuntoToUpdate, vtaqAsuto_TT);

			} else if (action.equals("UPDATE_IDS_TT")) {
				action = asuntoExist(asuntoToUpdate, asunto.getIdVersionTaqui());
				if (action.equals("MSJ_ERROR_DUPLICATE")) {
					return completeAction(action, null, null);

				}
				boolean asuntoExist = action.equals("SI_EXISTE") ? true : false;

				if (!asuntoExist && !isInUse) {
					respuesta = completeAction("UPDATE_IDS_UPDATE_TT", asuntoToUpdate, vtaqAsuto_TT);
				} else if (asuntoExist && !isInUse) {
					respuesta = completeAction("UPDATE_TT_DELETE_IDS", asuntoToUpdate, vtaqAsuto_TT);
				} else if (asuntoExist && isInUse) {
					respuesta = completeAction("UPDATE_TT_SET_IDS", asuntoToUpdate, vtaqAsuto_TT);
				} else if (!asuntoExist && isInUse) {
					respuesta = completeAction("CREATE_IDS_UPDATE_TT", asuntoToUpdate, vtaqAsuto_TT);
				}
			}
			action = "UPDATE_IDS_TT";
		}
		/// Consultar acumulados y datos de ponente y fechasesion
		String idSentencia = asuntoOrigen != null
				? SentenciasMilddleware.getExistSentencia(asuntoOrigen.getNumeroExpediente(),
						asuntoOrigen.getAsuntoAbordado())
				: null;
		SentenciaIndexObjectDTO objetoSentencia = getDetallesAsunto(respuesta.getAsunto());
		objetoSentencia.setId(idSentencia);
		objetoSentencia.setInUse(isInUse);
		getAction(objetoSentencia, action);
		return respuesta;
	}
	
	public boolean deleteTema(String idVt, String idAsunto, String tipotema,String tema) {
		VTaquigraficaAsuntos vtasuntoTT = findByIdVersionAndIdAsunto(idVt, idAsunto);
		if(vtasuntoTT!=null) {
			if(tipotema.equals("fondo")) {
				if(vtasuntoTT.getTemasFondo().contains(tema)) {
					vtasuntoTT.getTemasFondo().remove(tema);
				}	
			}else if(tipotema.equals("procesal")) {
				if(vtasuntoTT.getTemasProcesales().contains(tema)) {
					vtasuntoTT.getTemasProcesales().remove(tema);
				}
			}
			_vtAsuntosRepository.save(vtasuntoTT);
			return true;
		}
		return false;
	}
	public boolean addUpdatetema(String idVt, String idAsunto, String tipotema, List<String> temas, String action) {
		VTaquigraficaAsuntos vtasuntoTT = findByIdVersionAndIdAsunto(idVt, idAsunto);
		if(vtasuntoTT!=null) {
			if(tipotema.equals("fondo")) {
				vtasuntoTT.setTemasFondo(temas.stream().map(String::trim).collect(Collectors.toList()));
			}else if(tipotema.equals("procesal")) {
				vtasuntoTT.setTemasProcesales(temas.stream().map(String::trim).collect(Collectors.toList()));
			}
			_vtAsuntosRepository.save(vtasuntoTT);
			return true;
		}
		return false;
	}

	public boolean deleteAsuntoAbordado(String idVt, String id) {
		AsuntoResponseDTO respuesta = null;
		
		// Obtiene una lista de todas las tt que contienen el asunto
		List<VTaquigraficaAsuntos> listTT = _vtAsuntosRepository.findByIdAsunto(id);

		// Busco que el asunto exista
		Optional<AsuntoAbordado> asuntoExist = _asuntoRepository.findById(id);

		// Busca en la tabla transitiva el asunto que contiene la relacion
		VTaquigraficaAsuntos vtasuntoTT = findByIdVersionAndIdAsunto(idVt, id);
		
		respuesta = setRespuesta(HttpStatus.OK.value(), "Asunto Anterior", asuntoExist.get(), vtasuntoTT);

		if (listTT.size() > 1) {
			// elimina la asociación
			_vtAsuntosRepository.delete(vtasuntoTT);
			
			Optional<VersionTaquigrafica> vtaq = _vtRepository.findById(vtasuntoTT.getIdVtaquigrafica());
			if(vtaq.isPresent()) {
				updatePorcentajeVersionTaquigrafica(vtaq.get());
			}
			
			return true;
		} else {
			Gson gson = new Gson();
			// verifica unicidad de la relacion TT
			if (gson.toJson(listTT.get(0)).equals(gson.toJson(vtasuntoTT))) {
				_vtAsuntosRepository.delete(vtasuntoTT);
				_asuntoRepository.delete(asuntoExist.get());
				
				Optional<VersionTaquigrafica> vtaq = _vtRepository.findById(vtasuntoTT.getIdVtaquigrafica());
				if(vtaq.isPresent()) {
					updatePorcentajeVersionTaquigrafica(vtaq.get());
				}
				
				SentenciaIndexObjectDTO sentencia = new SentenciaIndexObjectDTO();
				sentencia.setTipoAsunto(asuntoExist.get().getAsuntoAbordado());
				sentencia.setNumeroExpediente(asuntoExist.get().getNumeroExpediente());
				return deleteSentenciaNotInUse(sentencia);
			}
		}
		_bitacoraService.saveOperacion(StatusBitacora._ASUNTOS, StatusBitacora._DELETE, respuesta, null );
		return false;

	}

	// Consultas mediante Criteria
	public AsuntoAbordado findByAsuntoAndExpediente(String asunto, String expediente) {

		Query query = new Query();
		query.addCriteria(Criteria.where("asuntoAbordado").is(asunto));
		query.addCriteria(Criteria.where("numeroExpediente").is(expediente));

		AsuntoAbordado asuntoA = mongoOperations.findOne(query, AsuntoAbordado.class);

		return asuntoA;
	}

	public VTaquigraficaAsuntos findByIdVersionAndIdAsunto(String idVtaq, String idAsunto) {

		Query query = new Query();
		query.addCriteria(Criteria.where("idVtaquigrafica").is(idVtaq));
		query.addCriteria(Criteria.where("idAsunto").is(idAsunto));

		VTaquigraficaAsuntos vtAsuntoA = mongoOperations.findOne(query, VTaquigraficaAsuntos.class);

		return vtAsuntoA;
	}

	public SentenciaIndexObjectDTO getDetallesAsunto(AsuntoAbordado asunto) {
		List<String> vtaqIds = new ArrayList<String>();
		List<VTaquigraficaAsuntos> ListasuntosTT = _vtAsuntosRepository.findByIdAsunto(asunto.getId());
		for (VTaquigraficaAsuntos asuntoTT : ListasuntosTT) {
			vtaqIds.add(asuntoTT.getIdVtaquigrafica());
			// creamos un array de acumulados unicos
			if(asunto.getAcumulados()==null) {
				asunto.setAcumulados(new ArrayList<String>());
			}
			if (asuntoTT.getAcumulados() != null) {
				for (int i = 0; i < asuntoTT.getAcumulados().size(); i++) {
					if (!asunto.getAcumulados().contains(asuntoTT.getAcumulados().get(i))) {
						asunto.getAcumulados().add(asuntoTT.getAcumulados().get(i));
					}
				}
			}
		}
		List<VersionTaquigrafica> listVtaq = _vtRepository.findAllByIdInOrderByFechaSesionDesc(vtaqIds);
		VTaquigraficaAsuntos vtaqAsuntoTT = findByIdVersionAndIdAsunto(listVtaq.get(0).getId(), asunto.getId());

		// se crea el objeto de sentencia
		SentenciaIndexObjectDTO sentenciaObject = new SentenciaIndexObjectDTO();
		sentenciaObject.setFechaResolucion(ConvertFechaFiltros.changeFormat(listVtaq.get(0).getFechaSesion(), RegexUtils.REGEX_YYYYMMDD));
		sentenciaObject.setNumeroExpediente(asunto.getNumeroExpediente());
		sentenciaObject.setTipoAsunto(asunto.getAsuntoAbordado());
		sentenciaObject.setCa(asunto.isCa());
		sentenciaObject.setAcumulada(asunto.isAcumulada());
		sentenciaObject.setAcumulados(asunto.getAcumulados());
		sentenciaObject.setPonente(vtaqAsuntoTT.getPonente());

		return sentenciaObject;
	}
}
