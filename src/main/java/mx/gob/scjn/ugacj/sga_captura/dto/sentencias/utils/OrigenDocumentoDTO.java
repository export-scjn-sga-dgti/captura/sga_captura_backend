package mx.gob.scjn.ugacj.sga_captura.dto.sentencias.utils;

public class OrigenDocumentoDTO {
    int Origen;
    Long AsuntoID;
    int Consecutivo;
    int Anio;
    String TipoAsunto;
    String DocumentoID;
    
	public int getOrigen() {
		return Origen;
	}
	public void setOrigen(int origen) {
		Origen = origen;
	}
	public Long getAsuntoID() {
		return AsuntoID;
	}
	public void setAsuntoID(Long asuntoID) {
		AsuntoID = asuntoID;
	}
	public int getConsecutivo() {
		return Consecutivo;
	}
	public void setConsecutivo(int consecutivo) {
		Consecutivo = consecutivo;
	}
	public int getAnio() {
		return Anio;
	}
	public void setAnio(int anio) {
		Anio = anio;
	}
	public String getTipoAsunto() {
		return TipoAsunto;
	}
	public void setTipoAsunto(String tipoAsunto) {
		TipoAsunto = tipoAsunto;
	}
	public String getDocumentoID() {
		return DocumentoID;
	}
	public void setDocumentoID(String documentoID) {
		DocumentoID = documentoID;
	}
}
