package mx.gob.scjn.ugacj.sga_captura.dto.seguimiento;

import java.util.ArrayList;
import java.util.List;

public class AsuntoSeguimientoDTO {

	String tipoAsunto;
	String numExpediente;
	List<String> secretarios = new ArrayList<String>();;
	List<String> editores = new ArrayList<String>();;
	List<String> sesiones = new ArrayList<String>();;
	boolean isAcumulada = false;
	boolean isCa = false;
	List<String> acumulados = new ArrayList<String>();
	List<String> temasFondo = new ArrayList<String>();
	List<String> temasProcesales = new ArrayList<String>();
	String anioResolucion;
	
	public String getTipoAsunto() {
		return tipoAsunto;
	}
	public void setTipoAsunto(String tipoAsunto) {
		this.tipoAsunto = tipoAsunto;
	}
	public String getNumExpediente() {
		return numExpediente;
	}
	public void setNumExpediente(String numExpediente) {
		this.numExpediente = numExpediente;
	}
	public List<String> getSecretarios() {
		return secretarios;
	}
	public void setSecretarios(List<String> secretarios) {
		this.secretarios = secretarios;
	}
	public List<String> getEditores() {
		return editores;
	}
	public void setEditores(List<String> editores) {
		this.editores = editores;
	}
	public List<String> getSesiones() {
		return sesiones;
	}
	public void setSesiones(List<String> sesiones) {
		this.sesiones = sesiones;
	}
	public boolean isAcumulada() {
		return isAcumulada;
	}
	public void setAcumulada(boolean isAcumulada) {
		this.isAcumulada = isAcumulada;
	}
	public boolean isCa() {
		return isCa;
	}
	public void setCa(boolean isCa) {
		this.isCa = isCa;
	}
	public List<String> getAcumulados() {
		return acumulados;
	}
	public void setAcumulados(List<String> acumulados) {
		this.acumulados = acumulados;
	}
	public List<String> getTemasFondo() {
		return temasFondo;
	}
	public void setTemasFondo(List<String> temasFondo) {
		this.temasFondo = temasFondo;
	}
	public List<String> getTemasProcesales() {
		return temasProcesales;
	}
	public void setTemasProcesales(List<String> temasProcesales) {
		this.temasProcesales = temasProcesales;
	}
	public String getAnioResolucion() {
		return anioResolucion;
	}
	public void setAnioResolucion(String anioResolucion) {
		this.anioResolucion = anioResolucion;
	}
	
	
	
	
}
