package mx.gob.scjn.ugacj.sga_captura.domain;

import java.util.ArrayList;
import java.util.List;

public class TipoVioInvaPodAnaSente {
    String tipoViolacion;
    List<String> actosImpugnados;
    List<String> rubrosTematicos;
    String metodologia;
    String fundamentacion;
    List<String> tipoSentencia;
    List<String> votacionAFavorVicioPlanteado;
    List<String> votaciontionEnContraVicioPlanteado;
    List<String> todosMinistrosVicioPlanteado;
    boolean votacionMayoriaVicioPlanteado;
    int cantidadAFavorVicioPlanteado;
    String textoVotacionVicioPlanteado;
    List<VotacionTipoSentencia> votacionesTipoSentencia;
    List<VotacionTipoSentencia> votacionRelacionadosExtensionValidez= new ArrayList<VotacionTipoSentencia>();;

    public List<VotacionTipoSentencia> getVotacionRelacionadosExtensionValidez() {
        return this.votacionRelacionadosExtensionValidez;
    }

    public void setVotacionRelacionadosExtensionValidez(
            List<VotacionTipoSentencia> votacionRelacionadosExtensionValidez) {
        this.votacionRelacionadosExtensionValidez = votacionRelacionadosExtensionValidez;
    }

    
    public List<VotacionTipoSentencia> getVotacionesTipoSentencia() {
        return this.votacionesTipoSentencia;
    }

    public void setVotacionesTipoSentencia(List<VotacionTipoSentencia> votacionesTipoSentencia) {
        this.votacionesTipoSentencia = votacionesTipoSentencia;
    }

    public String getTextoVotacionVicioPlanteado() {
        return this.textoVotacionVicioPlanteado;
    }

    public void setTextoVotacionVicioPlanteado(String textoVotacionVicioPlanteado) {
        this.textoVotacionVicioPlanteado = textoVotacionVicioPlanteado;
    }

   
    public boolean isVotacionMayoriaVicioPlanteado() {
        return this.votacionMayoriaVicioPlanteado;
    }

    public void setVotacionMayoriaVicioPlanteado(boolean votacionMayoriaVicioPlanteado) {
        this.votacionMayoriaVicioPlanteado = votacionMayoriaVicioPlanteado;
    }

    public int getCantidadAFavorVicioPlanteado() {
        return this.cantidadAFavorVicioPlanteado;
    }

    public void setCantidadAFavorVicioPlanteado(int cantidadAFavorVicioPlanteado) {
        this.cantidadAFavorVicioPlanteado = cantidadAFavorVicioPlanteado;
    }

    public List<String> getVotacionAFavorVicioPlanteado() {
        return this.votacionAFavorVicioPlanteado;
    }

    public void setVotacionAFavorVicioPlanteado(List<String> votacionAFavorVicioPlanteado) {
        this.votacionAFavorVicioPlanteado = votacionAFavorVicioPlanteado;
    }

    public List<String> getVotaciontionEnContraVicioPlanteado() {
        return this.votaciontionEnContraVicioPlanteado;
    }

    public void setVotaciontionEnContraVicioPlanteado(List<String> votaciontionEnContraVicioPlanteado) {
        this.votaciontionEnContraVicioPlanteado = votaciontionEnContraVicioPlanteado;
    }

    public List<String> getTodosMinistrosVicioPlanteado() {
        return this.todosMinistrosVicioPlanteado;
    }

    public void setTodosMinistrosVicioPlanteado(List<String> todosMinistrosVicioPlanteado) {
        this.todosMinistrosVicioPlanteado = todosMinistrosVicioPlanteado;
    }

    

    public String getMetodologia() {
        return this.metodologia;
    }

    public void setMetodologia(String metodologia) {
        this.metodologia = metodologia;
    }
    

    public List<String> getTipoSentencia() {
        return this.tipoSentencia;
    }

    public void setTipoSentencia(List<String> tipoSentencia) {
        this.tipoSentencia = tipoSentencia;
    }

    public String getTipoViolacion() {
        return this.tipoViolacion;
    }

    public void setTipoViolacion(String tipoViolacion) {
        this.tipoViolacion = tipoViolacion;
    }

    public List<String> getActosImpugnados() {
        return this.actosImpugnados;
    }

    public void setActosImpugnados(List<String> actosImpugnados) {
        this.actosImpugnados = actosImpugnados;
    }

    public List<String> getRubrosTematicos() {
        return this.rubrosTematicos;
    }

    public void setRubrosTematicos(List<String> rubrosTematicos) {
        this.rubrosTematicos = rubrosTematicos;
    }

    public String getFundamentacion() {
        return this.fundamentacion;
    }

    public void setFundamentacion(String fundamentacion) {
        this.fundamentacion = fundamentacion;
    }

}
