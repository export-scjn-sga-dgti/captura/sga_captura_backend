package mx.gob.scjn.ugacj.sga_captura.dto.vtaquigraficas;

public class AsuntoDTO {
	String idAsunto;
	String tipoAsunto;
	String numeroExpediente;
	
	public String getTipoAsunto() {
		return tipoAsunto;
	}
	public void setTipoAsunto(String tipoAsunto) {
		this.tipoAsunto = tipoAsunto;
	}
	public String getNumeroExpediente() {
		return numeroExpediente;
	}
	public void setNumeroExpediente(String numeroExpediente) {
		this.numeroExpediente = numeroExpediente;
	}
	public String getIdAsunto() {
		return idAsunto;
	}
	public void setIdAsunto(String idAsunto) {
		this.idAsunto = idAsunto;
	}
}
