package mx.gob.scjn.ugacj.sga_captura.dto.vtaquigraficas;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

public class TablaVersionesTaquigraficasDTO {
	String id;
	//@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
	String fechaSesion;
	String duracionSesion;
	int numeroAsuntos;
	List<AsuntoDTO> asuntos;
	double porcentaje;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getFechaSesion() {
		return fechaSesion;
	}
	public void setFechaSesion(String fechaSesion) {
		this.fechaSesion = fechaSesion;
	}
	public String getDuracionSesion() {
		return duracionSesion;
	}
	public void setDuracionSesion(String duracionSesion) {
		this.duracionSesion = duracionSesion;
	}
	public int getNumeroAsuntos() {
		return numeroAsuntos;
	}
	public void setNumeroAsuntos(int numeroAsuntos) {
		this.numeroAsuntos = numeroAsuntos;
	}
	public List<AsuntoDTO> getAsuntos() {
		return asuntos;
	}
	public void setAsuntos(List<AsuntoDTO> asuntos) {
		this.asuntos = asuntos;
	}
	public double getPorcentaje() {
		return porcentaje;
	}
	public void setPorcentaje(double porcentaje) {
		this.porcentaje = porcentaje;
	}
	
}
