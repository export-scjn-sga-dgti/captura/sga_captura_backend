package mx.gob.scjn.ugacj.sga_captura.domain;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import mx.gob.scjn.ugacj.sga_captura.dto.migracion.AsuntosOldDTO;
import mx.gob.scjn.ugacj.sga_captura.dto.migracion.VistasOldDTO;

@Document(collection = "usuario")
public class UsuarioOld implements Serializable{

	@Id
	String id;
	String nombre;
    String correo;
    List<AsuntosOldDTO> asuntos;
    List<AsuntosOldDTO> asuntosSentencias;
    VistasOldDTO [] vistas;
    boolean validador;
    boolean modificadorVotos = false;
    boolean asignadorAsuntos = false;
    
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	
	public List<AsuntosOldDTO> getAsuntos() {
		return asuntos;
	}
	public void setAsuntos(List<AsuntosOldDTO> asuntos) {
		this.asuntos = asuntos;
	}
	public List<AsuntosOldDTO> getAsuntosSentencias() {
		return asuntosSentencias;
	}
	public void setAsuntosSentencias(List<AsuntosOldDTO> asuntosSentencias) {
		this.asuntosSentencias = asuntosSentencias;
	}
	public VistasOldDTO[] getVistas() {
		return vistas;
	}
	public void setVistas(VistasOldDTO[] vistas) {
		this.vistas = vistas;
	}
	public boolean isValidador() {
		return validador;
	}
	public void setValidador(boolean validador) {
		this.validador = validador;
	}
	public boolean isModificadorVotos() {
		return modificadorVotos;
	}
	public void setModificadorVotos(boolean modificadorVotos) {
		this.modificadorVotos = modificadorVotos;
	}
	public boolean isAsignadorAsuntos() {
		return asignadorAsuntos;
	}
	public void setAsignadorAsuntos(boolean asignadorAsuntos) {
		this.asignadorAsuntos = asignadorAsuntos;
	}
    
    
}
