package mx.gob.scjn.ugacj.sga_captura.service.migracion;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.gob.scjn.ugacj.sga_captura.domain.Catalogo;
import mx.gob.scjn.ugacj.sga_captura.domain.Usuario;
import mx.gob.scjn.ugacj.sga_captura.domain.UsuarioOld;
import mx.gob.scjn.ugacj.sga_captura.dto.AsuntoDTO;
import mx.gob.scjn.ugacj.sga_captura.dto.CatalogosRestrictedDTO;
import mx.gob.scjn.ugacj.sga_captura.dto.ModuloDTO;
import mx.gob.scjn.ugacj.sga_captura.dto.PermisosDTO;
import mx.gob.scjn.ugacj.sga_captura.dto.migracion.AsuntosOldDTO;
import mx.gob.scjn.ugacj.sga_captura.dto.migracion.UsuarioCatalogoOldDTO;
import mx.gob.scjn.ugacj.sga_captura.dto.migracion.VistasOldDTO;
import mx.gob.scjn.ugacj.sga_captura.dto.statecode.ResponseDTO;
import mx.gob.scjn.ugacj.sga_captura.repository.CatalogoRepository;
import mx.gob.scjn.ugacj.sga_captura.repository.UsuarioOldRepository;
import mx.gob.scjn.ugacj.sga_captura.repository.UsuarioRepository;
import mx.gob.scjn.ugacj.sga_captura.utils.ConvertFechaFiltros;

@Service
public class UsuariosMigracionService {

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	private UsuarioOldRepository usuarioOldRepository;
	
	@Autowired
	CatalogoRepository catalogoRepository;

	public List<String> setCamposrestrictedVtaq(List<Boolean> vistas, List<String> camposRestricted) {
		if (vistas.contains(false)) {
			for (int i = 0; i < vistas.size(); i++) {
				if (!vistas.get(i) && i == 0)
					camposRestricted.add("fechaSesion");
				if (!vistas.get(i) && i == 1)
					camposRestricted.add("duracionSesion");
				if (!vistas.get(i) && i == 2)
					camposRestricted.add("numeroMinistrasMinistrosParticiparonEnSesion");
				if (!vistas.get(i) && i == 3)
					camposRestricted.add("nombreMinistrasMinistrosPresentesEnSesion");
				if (!vistas.get(i) && i == 4)
					camposRestricted.add("nombreMinistrasMinistrosAusentesEnSesion");
				if (!vistas.get(i) && i == 5)
					camposRestricted.add("asuntoAbordado");
				if (!vistas.get(i) && i == 6)
					camposRestricted.add("numeroExpediente");
				if (!vistas.get(i) && i == 7)
					camposRestricted.add("temasProcesales");
				if (!vistas.get(i) && i == 8)
					camposRestricted.add("temasFondo");
				if (!vistas.get(i) && i == 9)
					camposRestricted.add("numeroMinistrosParticiparonEnAsunto");
				if (!vistas.get(i) && i == 10)
					camposRestricted.add("hasPresidenteDecano");
				if (!vistas.get(i) && i == 11)
					camposRestricted.add("ponente");
			}
		}
		return camposRestricted;
	}

	public PermisosDTO setBasicPermisos(Boolean visor, PermisosDTO permisos) {
		if (visor) {
			permisos.setCrear(false);
			permisos.setEditar(false);
			permisos.setEliminar(false);
			permisos.setVer(true);
		}
		return permisos;
	}

	public ResponseDTO populateUsuarios() {

		List<UsuarioOld> usuarioOldList = usuarioOldRepository.findAll();
		Usuario usuarioNew = null;
		for (UsuarioOld usuarioOld : usuarioOldList) {
			usuarioNew = new Usuario();
			usuarioNew.setNombre(usuarioOld.getNombre());
			usuarioNew.setCorreo(usuarioOld.getCorreo());
			
			if(usuarioOld.getCorreo().equals("fguerra")) {
				System.out.println("U");
			}

			List<ModuloDTO> modulos = new ArrayList<ModuloDTO>();
			ModuloDTO modulo = null;
			PermisosDTO permisos = null;
			for (VistasOldDTO vista : usuarioOld.getVistas()) {
				modulo = new ModuloDTO();
				permisos = new PermisosDTO();

				modulo.setModulo(vista.getNombre());

				// permisos y camposRestricted
				List<String> camposRestricted = new ArrayList<String>();
				if (vista.getNombre().equals("taquigrafica")) {
					camposRestricted = setCamposrestrictedVtaq(vista.getCampos(), camposRestricted);
					permisos.setAsignacionAsuntos(usuarioOld.isAsignadorAsuntos());
					if(vista.getVisor()!=null) {
						setBasicPermisos(vista.getVisor(), permisos);
					}
					

					List<AsuntoDTO> asuntosList = new ArrayList<AsuntoDTO>();
					AsuntoDTO asunto = null;
					for (AsuntosOldDTO asuntoOld : usuarioOld.getAsuntos()) {
						asunto = new AsuntoDTO();
						asunto.setNumExpediente(asuntoOld.getNumExpediente());
						asunto.setTipoAsunto(asuntoOld.getTipoAsunto());
						asuntosList.add(asunto);
					}
					modulo.setAsuntos(asuntosList);

				} else if (vista.getNombre().equals("sentencia")) {
					if (!usuarioOld.isModificadorVotos())
						camposRestricted.add("numeroVotacionesRealizadas");

					permisos.setSuperadmin(usuarioOld.isValidador());
					setBasicPermisos(vista.getVisor(), permisos);

					List<AsuntoDTO> asuntosList = new ArrayList<AsuntoDTO>();
					AsuntoDTO asunto = null;
					for (AsuntosOldDTO asuntoOld : usuarioOld.getAsuntosSentencias()) {
						asunto = new AsuntoDTO();
						asunto.setNumExpediente(asuntoOld.getNumExpediente());
						asunto.setTipoAsunto(asuntoOld.getTipoAsunto());
						asuntosList.add(asunto);
					}
					modulo.setAsuntos(asuntosList);
				}
				modulo.setPermisos(permisos);
				modulo.setCamposRestricted(camposRestricted);

				// catalogosRestricted
				List<CatalogosRestrictedDTO> catalogosRestrictedList = new ArrayList<CatalogosRestrictedDTO>();
				CatalogosRestrictedDTO catalogoRestricted = null;
				if(vista.getCatalogo()!=null) {
					for (UsuarioCatalogoOldDTO catalogo : vista.getCatalogo()) {
						if(!catalogo.getNombre().equals("")) {
							catalogoRestricted = new CatalogosRestrictedDTO();
							catalogoRestricted.setCampo(catalogo.getNombre());
							catalogoRestricted.setValor(catalogo.getValor());
							catalogosRestrictedList.add(catalogoRestricted);
						}
					}
				}
				modulo.setCatalogosRestricted(catalogosRestrictedList);
				
				
				modulos.add(modulo);
			}
			
			usuarioNew.setModulos(modulos);
			usuarioRepository.save(usuarioNew);

		}

		return null;
	}
	
	public void printCatalogo(String nombre) {
		System.out.println(nombre);
		List<Catalogo> catalogoList = catalogoRepository.findByCatalogoOrderByNombreAsc(nombre);
		for(Catalogo cat:catalogoList) {
			System.out.println(cat.getNombre());
		}
		System.out.println("Ce fini");
	}
	
	public void printCatalogoMinistros(String nombre) {
		System.out.println(nombre);
		List<Catalogo> catalogoList = catalogoRepository.findByCatalogoOrderByNombreAsc(nombre);
		for(Catalogo cat:catalogoList) {
			String fechaIni = ConvertFechaFiltros.convertEngFechaTextToDateString(cat.getFechaInicio());
			String fechaFin = ConvertFechaFiltros.convertEngFechaTextToDateString(cat.getFechaFin());
			String fechaPrimSesion = ConvertFechaFiltros.convertEngFechaTextToDateString(cat.getPrimeraSesion());
			String fechaUltiSesion = ConvertFechaFiltros.convertEngFechaTextToDateString(cat.getUltimaSesion());
			System.out.println(cat.getNombre());
		}
		System.out.println("Ce fini");
	}
	
	public void printCatalogos() {

//		printCatalogo("ministros_presidentes");
		printCatalogo("normativa_tipoInstrumento");
		printCatalogoMinistros("ministros");
		
	}

}
