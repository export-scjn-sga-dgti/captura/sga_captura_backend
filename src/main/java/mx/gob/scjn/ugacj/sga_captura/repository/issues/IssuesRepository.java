package mx.gob.scjn.ugacj.sga_captura.repository.issues;

import org.springframework.data.mongodb.repository.MongoRepository;

import mx.gob.scjn.ugacj.sga_captura.domain.issues.Issues;

public interface IssuesRepository extends MongoRepository<Issues,String> {

}
