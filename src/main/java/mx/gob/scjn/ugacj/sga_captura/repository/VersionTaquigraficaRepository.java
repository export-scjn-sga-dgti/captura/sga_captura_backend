package mx.gob.scjn.ugacj.sga_captura.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import mx.gob.scjn.ugacj.sga_captura.domain.VersionTaquigrafica;

public interface VersionTaquigraficaRepository extends MongoRepository<VersionTaquigrafica,String>{

	Page<VersionTaquigrafica> findAllByOrderByFechaSesionDesc(Pageable paging);

	List<VersionTaquigrafica> findAllByIdInOrderByFechaSesionDesc(List<String> vtaqIds);
	List<VersionTaquigrafica> findAllByIdIn(List<String> vtaqIds, Pageable paging);
	List<VersionTaquigrafica> findByPorcentaje(double porcentaje);
	
}
