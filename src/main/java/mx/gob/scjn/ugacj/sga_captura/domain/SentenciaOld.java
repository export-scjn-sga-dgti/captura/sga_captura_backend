package mx.gob.scjn.ugacj.sga_captura.domain;

import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "sentencia")
public class SentenciaOld {
	
private static final long serialVersionUID = 1L;
    
    @Id
    private String id;
    String idElastic;
    int idEngroso;
    int asuntoID;
    String numExpedientes;
    String tipoAsunto;  
    String organoRadicacion;
    Date fechaResolucion; 
    String ponente;
  //*****************PRIMERA SECCIÓN ANTES DE LAS TABLAS TRANSITIVAS*******
    String tipoPersonaPromovente;
    String tipoPersonaJuridicoDemandada;
    String tipoViolacionesPlanteadasDemanda;
    String [] derechosHumanosViolacion;
    List<String> violacionOrganicaPlanteada;
    String tipoVicioProcesoLegislativo [];
    String tipoViolacionInvasionEsferas;
    String tipoViolacionInvasionPoderes;
  //*****************END PRIMERA SECCIÓN ANTES DE LAS TABLAS TRANSITIVAS***
    
  //**TABLAS DE VOTACIÓN****************
    CausasImproSobreseAnali[] causasImproSobreseAnali;
    
    List<String> derechosHumanosViolacionAnaliza;//SE PROPONE QUITAR
    DereHumaViolaAnaliSenten[] dereHumaViolaAnaliSenten;//**TABLA DE VOTACIÓN -> Derechos humanos cuya violación se analiza en la sentencia
    
    List<String> violacionOrganica;
    String violacionesAnalizadasSentencia;//revisar si se ocupa o no???
    TipoVicProLegisAnali[] tipoVicProLegisAnali;//**TABLA DE VOTACIÓN -> Tipo de vicio en el proceso legislativo anaizado en la sentencia
    
    String tipoViolacionInvasionEsferasAnalizandoSentencia;
    String tipoViolacionInvasionEsferasInfundado;
    TipoVicInvEsfeAnaSenten[] tipoVicInvEsfeAnaSenten;//**TABLA DE VOTACIÓN -> Tipo de violación por invasión de esferas analizado en la sentencia
    
    String tipoViolacionInvasionPoderesAnalizandoSentencia;
    String tipoViolacionInvasionEsferasfundado;
    TipoVioInvaPodAnaSente[] tipoVioInvaPodAnaSente;//**TABLA DE VOTACIÓN -> Tipo de violación por invasión de poderes analizado en la senten
  //**END TABLAS DE VOTACIÓN****************
    
  //*****************SEGUNDA SECCIÓN DESPUÉS DE LAS TABLAS TRANSITIVAS***
    String momentoSurteEfectosDeclaracionInvalidez;
    int numeroVotacionesSentenciaPorHoja;
    int numeroVotacionesSentenciaAcumuladas;
    String tramiteEngrose;
    String plazoEntreDictadoYFechaFirmaEngrose;
    Date fechaSentencia;
    Date fechaFirmaEngrose;
  //*****************END SEGUNDA SECCIÓN DESPUÉS DE LAS TABLAS TRANSITIVAS******
    
    //NO SE OCUPAN
    String causasImprocedenciaAnalizadas;
    DerechosHumanos derechosHumanosViolacionDeclaraInfundada[];
    DerechosHumanos derechosHumanosViolacionDeclarafundada[];
    TipoVicioProcesoLegislativo tipoVicioProcesoLegislativoInfundado;
    TipoVicioProcesoLegislativo tipoVicioProcesoLegislativoFundado; 
    String tipoViolacionInvasionPoderesFundado;
    String tipoViolacionInvasionPoderesInfundado;
    String metodologiaAnalisisConstitucionalidad;
    String sentenciaDesestimacionFaltaDeVotacionCalificada;
    String sentenciaInterpretacionConforme;
    String tipoSentenciaAmbitoPersonalValidez;
    String tipoSentenciaCuantoEfectosPretensiones;
    String tipoSentenciaEfectosOrdenJuridico;
    String sentenciaCondenadaALegislar;
    String sentenciaCondenadaANoLegislar;
    String [] tipoVicioProcesoLegislativoAnalizadoSentencia;
    String accionesYcontroversias;
    String tipoAmparo;
    String tipoSentenciaEfectosPasado;
    //END NO SE OCUPAN
  
    boolean fechaValidada = false;
    String origen;
    float porcentaje;
    
    
    
   
    public boolean isFechaValidada() {
		return fechaValidada;
	}

	public void setFechaValidada(boolean fechaValidada) {
		this.fechaValidada = fechaValidada;
	}

	public void setTipoVicioProcesoLegislativoFundado(TipoVicioProcesoLegislativo tipoVicioProcesoLegislativoFundado) {
		this.tipoVicioProcesoLegislativoFundado = tipoVicioProcesoLegislativoFundado;
	}

	public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getOrigen(){
        return this.origen;
    }
    
    public int getNumeroVotacionesSentenciaPorHoja() {
        return this.numeroVotacionesSentenciaPorHoja;
    }

    public void setNumeroVotacionesSentenciaPorHoja(int numeroVotacionesSentenciaPorHoja) {
        this.numeroVotacionesSentenciaPorHoja = numeroVotacionesSentenciaPorHoja;
    }

    public int getNumeroVotacionesSentenciaAcumuladas() {
        return this.numeroVotacionesSentenciaAcumuladas;
    }

    public void setNumeroVotacionesSentenciaAcumuladas(int numeroVotacionesSentenciaAcumuladas) {
        this.numeroVotacionesSentenciaAcumuladas = numeroVotacionesSentenciaAcumuladas;
    }


    public  DereHumaViolaAnaliSenten[] getDereHumaViolaAnaliSenten() {
		return this.dereHumaViolaAnaliSenten;
	}

    public void setDereHumaViolaAnaliSenten( DereHumaViolaAnaliSenten [] dereHumaViolaAnaliSenten) {
		this.dereHumaViolaAnaliSenten = dereHumaViolaAnaliSenten;
	}

	public TipoVicProLegisAnali[] getTipoVicProLegisAnali() {
		return this.tipoVicProLegisAnali;
	}

    public void setTipoVicProLegisAnali(TipoVicProLegisAnali[] tipoVicProLegisAnali) {
		this.tipoVicProLegisAnali = tipoVicProLegisAnali;
	}

	public TipoVicInvEsfeAnaSenten[]  getTipoVicInvEsfeAnaSenten() {
		return this.tipoVicInvEsfeAnaSenten;
	}

    public void setTipoVicInvEsfeAnaSenten(TipoVicInvEsfeAnaSenten[] tipoVicInvEsfeAnaSenten) {
		this.tipoVicInvEsfeAnaSenten = tipoVicInvEsfeAnaSenten;
	}

	public TipoVioInvaPodAnaSente[] getTipoVioInvaPodAnaSente() {
		return this.tipoVioInvaPodAnaSente;
	}

    public void setTipoVioInvaPodAnaSente(TipoVioInvaPodAnaSente[] tipoVioInvaPodAnaSente) {
		this.tipoVioInvaPodAnaSente = tipoVioInvaPodAnaSente;
	}

	public CausasImproSobreseAnali [] getCausasImproSobreseAnali() {
		return this.causasImproSobreseAnali;
	}

    public void setCausasImproSobreseAnali(CausasImproSobreseAnali[] causasImproSobreseAnali) {
		this.causasImproSobreseAnali = causasImproSobreseAnali;
	}

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdElastic() {
        return this.idElastic;
    }

    public void setIdElastic(String idElastic) {
        this.idElastic = idElastic;
    }

    public int getIdEngroso() {
        return this.idEngroso;
    }

    public void setIdEngroso(int idEngroso) {
        this.idEngroso = idEngroso;
    }

    public int getAsuntoID() {
        return this.asuntoID;
    }

    public void setAsuntoID(int asuntoID) {
        this.asuntoID = asuntoID;
    }

    public String getNumExpedientes() {
        return this.numExpedientes;
    }

    public void setNumExpedientes(String numExpedientes) {
        this.numExpedientes = numExpedientes;
    }

    public String getTipoAsunto() {
        return this.tipoAsunto;
    }

    public void setTipoAsunto(String tipoAsunto) {
        this.tipoAsunto = tipoAsunto;
    }

    public String getOrganoRadicacion() {
        return this.organoRadicacion;
    }

    public void setOrganoRadicacion(String organoRadicacion) {
        this.organoRadicacion = organoRadicacion;
    }

    public Date getFechaResolucion() {
        return this.fechaResolucion;
    }

    public void setFechaResolucion(Date fechaResolucion) {
        this.fechaResolucion = fechaResolucion;
    }

    public String getPonente() {
        return this.ponente;
    }

    public void setPonente(String ponente) {
        this.ponente = ponente;
    }

    public String getTipoPersonaPromovente() {
        return this.tipoPersonaPromovente;
    }

    public void setTipoPersonaPromovente(String tipoPersonaPromovente) {
        this.tipoPersonaPromovente = tipoPersonaPromovente;
    }

    public String getTipoPersonaJuridicoDemandada() {
        return this.tipoPersonaJuridicoDemandada;
    }

    public void setTipoPersonaJuridicoDemandada(String tipoPersonaJuridicoDemandada) {
        this.tipoPersonaJuridicoDemandada = tipoPersonaJuridicoDemandada;
    }

    public String getTipoViolacionesPlanteadasDemanda() {
        return this.tipoViolacionesPlanteadasDemanda;
    }

    public void setTipoViolacionesPlanteadasDemanda(String tipoViolacionesPlanteadasDemanda) {
        this.tipoViolacionesPlanteadasDemanda = tipoViolacionesPlanteadasDemanda;
    }

    public String [] getDerechosHumanosViolacion() {
        return this.derechosHumanosViolacion;
    }

    public void setDerechosHumanosViolacion(String derechosHumanosViolacion []) {
        this.derechosHumanosViolacion = derechosHumanosViolacion;
    }

    public List<String> getViolacionOrganicaPlanteada() {
        return this.violacionOrganicaPlanteada;
    }

    public void setViolacionOrganicaPlanteada(List <String> violacionOrganicaPlanteada) {
        this.violacionOrganicaPlanteada = violacionOrganicaPlanteada;
    }

    public String getTipoViolacionInvasionEsferas() {
        return this.tipoViolacionInvasionEsferas;
    }

    public void setTipoViolacionInvasionEsferas(String tipoViolacionInvasionEsferas) {
        this.tipoViolacionInvasionEsferas = tipoViolacionInvasionEsferas;
    }

    public String getTipoViolacionInvasionPoderes() {
        return this.tipoViolacionInvasionPoderes;
    }

    public void setTipoViolacionInvasionPoderes(String tipoViolacionInvasionPoderes) {
        this.tipoViolacionInvasionPoderes = tipoViolacionInvasionPoderes;
    }

    public String getCausasImprocedenciaAnalizadas() {
        return this.causasImprocedenciaAnalizadas;
    }

    public void setCausasImprocedenciaAnalizadas(String causasImprocedenciaAnalizadas) {
        this.causasImprocedenciaAnalizadas = causasImprocedenciaAnalizadas;
    }

    public String getViolacionesAnalizadasSentencia() {
        return this.violacionesAnalizadasSentencia;
    }

    public void setViolacionesAnalizadasSentencia(String violacionesAnalizadasSentencia) {
        this.violacionesAnalizadasSentencia = violacionesAnalizadasSentencia;
    }

    public List<String> getDerechosHumanosViolacionAnaliza() {
        return this.derechosHumanosViolacionAnaliza;
    }

    public void setDerechosHumanosViolacionAnaliza(List<String> derechosHumanosViolacionAnaliza) {
        this.derechosHumanosViolacionAnaliza = derechosHumanosViolacionAnaliza;
    }

    public DerechosHumanos[] getDerechosHumanosViolacionDeclaraInfundada() {
        return this.derechosHumanosViolacionDeclaraInfundada;
    }

    public void setDerechosHumanosViolacionDeclaraInfundada(DerechosHumanos[] derechosHumanosViolacionDeclaraInfundada) {
        this.derechosHumanosViolacionDeclaraInfundada = derechosHumanosViolacionDeclaraInfundada;
    }

    public DerechosHumanos[] getDerechosHumanosViolacionDeclarafundada() {
        return this.derechosHumanosViolacionDeclarafundada;
    }

    public void setDerechosHumanosViolacionDeclarafundada(DerechosHumanos[] derechosHumanosViolacionDeclarafundada) {
        this.derechosHumanosViolacionDeclarafundada = derechosHumanosViolacionDeclarafundada;
    }

    public List<String> getViolacionOrganica() {
        return this.violacionOrganica;
    }

    public void setViolacionOrganica(List<String> violacionOrganica) {
        this.violacionOrganica = violacionOrganica;
    }

   
    public TipoVicioProcesoLegislativo getTipoVicioProcesoLegislativoFundado() {
        return this.tipoVicioProcesoLegislativoFundado;
    }

    public void setTipoVicioProcesoegisLativoFundado(TipoVicioProcesoLegislativo tipoVicioProcesoLegislativoFundado) {
        this.tipoVicioProcesoLegislativoFundado = tipoVicioProcesoLegislativoFundado;
    }

    public String getTipoViolacionInvasionEsferasAnalizandoSentencia() {
        return this.tipoViolacionInvasionEsferasAnalizandoSentencia;
    }

    public void setTipoViolacionInvasionEsferasAnalizandoSentencia(
            String tipoViolacionInvasionEsferasAnalizandoSentencia) {
        this.tipoViolacionInvasionEsferasAnalizandoSentencia = tipoViolacionInvasionEsferasAnalizandoSentencia;
    }

    public String getTipoViolacionInvasionEsferasInfundado() {
        return this.tipoViolacionInvasionEsferasInfundado;
    }

    public void setTipoViolacionInvasionEsferasInfundado(String tipoViolacionInvasionEsferasInfundado) {
        this.tipoViolacionInvasionEsferasInfundado = tipoViolacionInvasionEsferasInfundado;
    }

    public String getTipoViolacionInvasionEsferasfundado() {
        return this.tipoViolacionInvasionEsferasfundado;
    }

    public void setTipoViolacionInvasionEsferasfundado(String tipoViolacionInvasionEsferasfundado) {
        this.tipoViolacionInvasionEsferasfundado = tipoViolacionInvasionEsferasfundado;
    }

    public String getTipoViolacionInvasionPoderesAnalizandoSentencia() {
        return this.tipoViolacionInvasionPoderesAnalizandoSentencia;
    }

    public void setTipoViolacionInvasionPoderesAnalizandoSentencia(
            String tipoViolacionInvasionPoderesAnalizandoSentencia) {
        this.tipoViolacionInvasionPoderesAnalizandoSentencia = tipoViolacionInvasionPoderesAnalizandoSentencia;
    }

    public String getTipoViolacionInvasionPoderesInfundado() {
        return this.tipoViolacionInvasionPoderesInfundado;
    }

    public void setTipoViolacionInvasionPoderesInfundado(String tipoViolacionInvasionPoderesInfundado) {
        this.tipoViolacionInvasionPoderesInfundado = tipoViolacionInvasionPoderesInfundado;
    }

    public String getTipoViolacionInvasionPoderesFundado() {
        return this.tipoViolacionInvasionPoderesFundado;
    }

    public void setTipoViolacionInvasionPoderesFundado(String tipoViolacionInvasionPoderesFundado) {
        this.tipoViolacionInvasionPoderesFundado = tipoViolacionInvasionPoderesFundado;
    }

    public String getMetodologiaAnalisisConstitucionalidad() {
        return this.metodologiaAnalisisConstitucionalidad;
    }

    public void setMetodologiaAnalisisConstitucionalidad(String metodologiaAnalisisConstitucionalidad) {
        this.metodologiaAnalisisConstitucionalidad = metodologiaAnalisisConstitucionalidad;
    }

    public String getSentenciaDesestimacionFaltaDeVotacionCalificada() {
        return this.sentenciaDesestimacionFaltaDeVotacionCalificada;
    }

    public void setSentenciaDesestimacionFaltaDeVotacionCalificada(
            String sentenciaDesestimacionFaltaDeVotacionCalificada) {
        this.sentenciaDesestimacionFaltaDeVotacionCalificada = sentenciaDesestimacionFaltaDeVotacionCalificada;
    }

    public String getSentenciaInterpretacionConforme() {
        return this.sentenciaInterpretacionConforme;
    }

    public void setSentenciaInterpretacionConforme(String sentenciaInterpretacionConforme) {
        this.sentenciaInterpretacionConforme = sentenciaInterpretacionConforme;
    }

    public String getTipoSentenciaAmbitoPersonalValidez() {
        return this.tipoSentenciaAmbitoPersonalValidez;
    }

    public void setTipoSentenciaAmbitoPersonalValidez(String tipoSentenciaAmbitoPersonalValidez) {
        this.tipoSentenciaAmbitoPersonalValidez = tipoSentenciaAmbitoPersonalValidez;
    }

    public String getTipoSentenciaCuantoEfectosPretensiones() {
        return this.tipoSentenciaCuantoEfectosPretensiones;
    }

    public void setTipoSentenciaCuantoEfectosPretensiones(String tipoSentenciaCuantoEfectosPretensiones) {
        this.tipoSentenciaCuantoEfectosPretensiones = tipoSentenciaCuantoEfectosPretensiones;
    }

    public String getTipoSentenciaEfectosOrdenJuridico() {
        return this.tipoSentenciaEfectosOrdenJuridico;
    }

    public void setTipoSentenciaEfectosOrdenJuridico(String tipoSentenciaEfectosOrdenJuridico) {
        this.tipoSentenciaEfectosOrdenJuridico = tipoSentenciaEfectosOrdenJuridico;
    }

    public String getSentenciaCondenadaALegislar() {
        return this.sentenciaCondenadaALegislar;
    }

    public void setSentenciaCondenadaALegislar(String sentenciaCondenadaALegislar) {
        this.sentenciaCondenadaALegislar = sentenciaCondenadaALegislar;
    }

    public String getSentenciaCondenadaANoLegislar() {
        return this.sentenciaCondenadaANoLegislar;
    }

    public void setSentenciaCondenadaANoLegislar(String sentenciaCondenadaANoLegislar) {
        this.sentenciaCondenadaANoLegislar = sentenciaCondenadaANoLegislar;
    }

    public String getMomentoSurteEfectosDeclaracionInvalidez() {
        return this.momentoSurteEfectosDeclaracionInvalidez;
    }

    public void setMomentoSurteEfectosDeclaracionInvalidez(String momentoSurteEfectosDeclaracionInvalidez) {
        this.momentoSurteEfectosDeclaracionInvalidez = momentoSurteEfectosDeclaracionInvalidez;
    }
    
    public String getTramiteEngrose() {
        return this.tramiteEngrose;
    }

    public void setTramiteEngrose(String tramiteEngrose) {
        this.tramiteEngrose = tramiteEngrose;
    }

    public String getPlazoEntreDictadoYFechaFirmaEngrose() {
        return this.plazoEntreDictadoYFechaFirmaEngrose;
    }

    public void setPlazoEntreDictadoYFechaFirmaEngrose(String plazoEntreDictadoYFechaFirmaEngrose) {
        this.plazoEntreDictadoYFechaFirmaEngrose = plazoEntreDictadoYFechaFirmaEngrose;
    }

    public String [] getTipoVicioProcesoLegislativo() {
        return this.tipoVicioProcesoLegislativo;
    }

    public void setTipoVicioProcesoLegislativo(String tipoVicioProcesoLegislativo []) {
        this.tipoVicioProcesoLegislativo = tipoVicioProcesoLegislativo;
    }

    public TipoVicioProcesoLegislativo getTipoVicioProcesoLegislativoInfundado() {
        return this.tipoVicioProcesoLegislativoInfundado;
    }

    public void setTipoVicioProcesoLegislativoInfundado(TipoVicioProcesoLegislativo tipoVicioProcesoLegislativoInfundado) {
        this.tipoVicioProcesoLegislativoInfundado = tipoVicioProcesoLegislativoInfundado;
    }
    
    public String [] getTipoVicioProcesoLegislativoAnalizadoSentencia() {
		return this.tipoVicioProcesoLegislativoAnalizadoSentencia;
	}

    public void setTipoVicioProcesoLegislativoAnalizadoSentencia( String tipoVicioProcesoLegislativoAnalizadoSentencia []) {
		this.tipoVicioProcesoLegislativoAnalizadoSentencia = tipoVicioProcesoLegislativoAnalizadoSentencia;
	}


    public String getAccionesYcontroversias() {
        return this.accionesYcontroversias;
    }

    public void setAccionesYcontroversias(String accionesYcontroversias) {
        this.accionesYcontroversias = accionesYcontroversias;
    }

    public String getTipoAmparo() {
        return this.tipoAmparo;
    }

    public void setTipoAmparo(String tipoAmparo) {
        this.tipoAmparo = tipoAmparo;
    }

    public String getTipoSentenciaEfectosPasado() {
        return this.tipoSentenciaEfectosPasado;
    }

    public void setTipoSentenciaEfectosPasado(String tipoSentenciaEfectosPasado) {
      this.tipoSentenciaEfectosPasado = tipoSentenciaEfectosPasado;
    }


    public float getPorcentaje() {
        return this.porcentaje;
    }

    public void setPorcentaje(float porcentaje) {
        this.porcentaje = porcentaje;
    }

	public Date getFechaSentencia() {
		return fechaSentencia;
	}

	public void setFechaSentencia(Date fechaSentencia) {
		this.fechaSentencia = fechaSentencia;
	}

	public Date getFechaFirmaEngrose() {
		return fechaFirmaEngrose;
	}

	public void setFechaFirmaEngrose(Date fechaFirmaEngrose) {
		this.fechaFirmaEngrose = fechaFirmaEngrose;
	}

}
