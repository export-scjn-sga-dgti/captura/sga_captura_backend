package mx.gob.scjn.ugacj.sga_captura.controller.sentencias;

import mx.gob.scjn.ugacj.sga_captura.domain.TablaVotaciones;
import mx.gob.scjn.ugacj.sga_captura.dto.TablaVotacionSentenciaDTO;
import mx.gob.scjn.ugacj.sga_captura.dto.TablaVotacionesRubrosDTO;
import mx.gob.scjn.ugacj.sga_captura.service.sentencias.TablasVotacionesService;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

@RestController
@RequestMapping( value = "/api/sga")
public class TablasVotacionesController {

    @Autowired
    private TablasVotacionesService _votacionService;

    @RequestMapping(method = RequestMethod.GET,path = "/tablaVotaciones")
    @CrossOrigin()
    public Page<TablaVotaciones> getVotacionesAll(@RequestParam(name = "page") Integer page,
                                                  @RequestParam(name = "size") Integer size,
                                                  @RequestParam(required = false, name = "filtros") String filtros){
        Pageable paging = PageRequest.of(page-1, size);
        return _votacionService.getAllVotacionesPages(paging, filtros);
    }

    @RequestMapping(method = RequestMethod.GET,path = "/tablaVotaciones/{votacionId}", produces = "application/json")
    @CrossOrigin()
    public TablaVotacionesRubrosDTO getVotacionById(@PathVariable String votacionId){
        System.out.println(votacionId);
        return _votacionService.getVotacionById(votacionId);
    }

    @RequestMapping(method = RequestMethod.POST,path = "/tablaVotaciones")
    @CrossOrigin()
    public TablaVotacionesRubrosDTO saveVotacion(@Valid @RequestBody TablaVotacionSentenciaDTO votacion){
        return _votacionService.saveVotacion(votacion);
    }

    @RequestMapping(method = RequestMethod.PUT,path = "/tablaVotaciones")
    @CrossOrigin()
    public TablaVotacionesRubrosDTO updateVotacion(@Valid @RequestBody TablaVotacionesRubrosDTO votacion){
        return _votacionService.updateVotacion(votacion);
    }
    
    @RequestMapping(method = RequestMethod.PUT,path = "/tablaVotaciones/orden-rubros")
    @CrossOrigin()
    public TablaVotaciones updateOrdenVotacion(@RequestBody TablaVotacionesRubrosDTO votacion){
        return _votacionService.updateOrdenVotacion(votacion);
    }

    @RequestMapping(method = RequestMethod.DELETE,path = "/tablaVotaciones/votacion/{idVotacion}/sentencia/{idSentencia}")
    @CrossOrigin()
    public Map<String, Integer> deleteVotacion(@PathVariable String idVotacion, @PathVariable String idSentencia) throws JSONException {
        return  _votacionService.deleteVotacionWithZiseVotos(idVotacion, idSentencia);
    }

}
